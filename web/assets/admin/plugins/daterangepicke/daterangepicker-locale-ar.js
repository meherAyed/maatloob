let dateRangeOptions = {
    locale : {
        "format": "DD-MM-YYYY",
        "separator": " - ",
        "applyLabel": "تطبيق",
        "cancelLabel": "إلغاء",
        "fromLabel": "من",
        "toLabel": "إلى",
        "customRangeLabel": "غير ذلك",
        'direction' : 'rtl'
    },
    ranges : {
        'اليوم': [moment(), moment()],
        'أمس': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'اخر 7 ايام': [moment().subtract(6, 'days'), moment()],
        'آخر 30 يوم': [moment().subtract(29, 'days'), moment()],
        'هذا الشهر': [moment().startOf('month'), moment().endOf('month')],
        'الشهر الماضي': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'هذه السنة': [moment().startOf('year'),moment()]
    }
};