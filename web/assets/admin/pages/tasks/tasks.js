$(document).ready(function () {
  let deleteTaskModal = $('#delete-task-modal');
  $('.btn-delete-task').click(function () {
    $('.btn-delete-task.processing').removeClass('processing');
    $(this).addClass('processing');
    deleteTaskModal.modal("show");
  });

  $('#btn-delete-task').on('click', function () {
    let task = $('.btn-delete-task.processing');
    $.ajax({
      url: task.data('url'),
      method: 'DELETE',
      beforeSend: function () {
        deleteTaskModal.addClass("loading-modal");
      },
      success: function (data) {
        task.closest('tr').remove();
        deleteTaskModal.modal("hide");
        deleteTaskModal.removeClass("loading-modal");
        miniToaster.fire({
          type: 'success',
          title: data.message
        });
      },
      error: function (error) {
        $('#delete-task-modal').modal("hide");
        deleteTaskModal.removeClass("loading-modal");
        miniToaster.fire({
          type: 'error',
          title: error.responseJSON.message
        });
      }
    })
  });
});
