$(document).ready(function () {
  $('.task-files').lightGallery({
    share: false,
    autoplayControls: false,
    autoplay: false
  });
  let deleteTaskModal = $('#delete-task-modal');
  $('#btn-delete-task').on('click', function () {
    let elem = $(this);
    let targetUrl = elem.data('target-url');
    $.ajax({
      url: elem.data('url'),
      method: 'DELETE',
      beforeSend: function () {
        deleteTaskModal.loading();
      },
      success: function (data) {
        miniToaster.fire({
          type:'success',
          title: data.message
        });
        window.location = targetUrl;
      },
      error: function (error) {
        $('#delete-task-modal').modal("hide");
        deleteTaskModal.loading("stop");
        miniToaster.fire({
          type: 'error',
          title: 'Error task was not removed,repeat again'
        });
      }
    })
  });
})