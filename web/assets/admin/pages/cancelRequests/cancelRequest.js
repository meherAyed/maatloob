$(document).ready(function () {
  $('#iframe-preview-page').load(function () {
    $(this).contents().find("body").addClass("report-inspecting");
  });
  let showShowDiscussionModal = $('#discussion-modal');
  $('.show-discussion').click(function () {
    showShowDiscussionModal.find('iframe').attr('src', $(this).data('url'));
    showShowDiscussionModal.modal("show");
  });
  let readReasonModal = $('#cancel-reason-modal');
  $('.read-reason').click(function () {
    readReasonModal.find('.modal-body p').text($(this).data('reason'));
    readReasonModal.modal("show");
  });
  let taskActionModal = $('#task-action-modal');
  let guiltyUserSelector = $('#guilty-user');
  $('.take-action-btn').click(function () {
    let id = $(this).data('id');
    $('#form-cancel-action input#cancel-request-token').val(id);
    let users = $(this).data('users');
    $('#cancel-type').val(null).trigger('change');
    guiltyUserSelector.find('option:not(:disabled):not(.none)').remove();
    guiltyUserSelector.append(
      '<option value="' + users.fromUser.id + '">' + users.fromUser.name + '</option>' +
      '<option value="' + users.forUser.id + '">' + users.forUser.name + '</option>'
    ).selectpicker('refresh');
    $('#damage-amount').attr('max', $(this).data('task-price'));
    taskActionModal.find('.has-error').removeClass('has-error').find('error-msg').remove();
    taskActionModal.modal('show');
  });

  /** Treat action */
  $(document).on('change', '#cancel-type', function () {
    if ($(this).val() && $(this).val() !== 'request') {
      $('#guilty-user').closest('.field-group').removeClass('hidden');
    } else {
      $('#guilty-user').val(null).trigger('change').closest('.field-group').addClass('hidden');
    }
  });
  $(document).on('change', '#guilty-user', function () {
    $(this).closest('.field-group').find('.text-info').addClass('hidden');
    if ($(this).val() && $(this).val() !== 'none') {
      $('#caused-damage').closest('.field-group').removeClass('hidden')
    } else {
      if ($(this).val() === 'none') {
        $(this).closest('.field-group').find('.text-info').removeClass('hidden');
      }
      $('#caused-damage').attr('checked', false).trigger('change').closest('.field-group').addClass('hidden')
    }
  });
  $(document).on('change', '#caused-damage', function () {
    if ($(this).is(':checked')) {
      $('#damage-amount').closest('.field-group').removeClass('hidden')
    } else {
      $('#damage-amount').val(null).closest('.field-group').addClass('hidden')
    }
  });
  $(document).on('keyup change', '#damage-amount', function (e) {
    if ($(this).val() > $(this).data('max')) {
      $(this).next().addClass('text-danger');
    }
  });
  $(document).on('click', '#btn-submit-action:not(.loading)', function () {
    let form = $('#form-cancel-action');
    let isValid = true;
    $.each(form.find('.field-group:not(.hidden) input.required,.field-group:not(.hidden) select.required'), function (i, elem) {
      if (!$(elem).val()) {
        if (!$(elem).closest('.field-group').hasClass('has-error'))
          $(elem).closest('.field-group').addClass('has-error').append('<span class="error-msg">' + $(elem).data('error-msg') + '</span>');
        isValid = false;
      } else {
        $(elem).closest('.field-group').removeClass('has-error').find('.error-msg').remove()
      }
    });
    if (isValid) {
      let data = new FormData(form[0]);
      taskActionModal.addClass('loading-modal');
      axios.post($(this).data('url'), data).then(function (response) {
        $('.take-action-btn[data-id="'+data.get('id')+'"]').closest('tr').remove();
        miniToaster.fire({
          type: 'success',
          title: response.data.message
        });
        taskActionModal.removeClass('loading-modal');
        taskActionModal.modal('hide');
      }).catch(function (error) {
        let errorMsg = error.response.data.message;
        miniToaster.fire({
          type: 'error',
          title: errorMsg
        });
        taskActionModal.removeClass('loading-modal');
      })
    }
  });
});