$(document).ready(function () {
  $('#language-selector').on('change', function () {
    if ($(this).val()) {
      $('#messages-container').removeClass('hidden');
      $('.new-text-header i').attr('class', $(this).find('option:selected').data('icon'));
      $('#language_identifier').val($(this).val());
      $('#language_name').val($(this).find('option:selected').text());
      $('#language_icon').val($(this).find('option:selected').data('icon-path'));
    } else {
      $('#messages-container').addClass('hidden');
      $('#language_identifier').val("");
      $('#language_name').val("");
      $('#language_icon').val("");
    }
  });
  $('.new-translation').on('change', function (e) {
    if ($(this).val()) {
      $(this).removeClass('empty').addClass('valid')
    } else {
      $(this).addClass('empty').removeClass('valid')
    }
    let percent =parseFloat((($('.new-translation:not(.empty)').length/$('.new-translation').length)*100).toFixed(2));
    $('#progression-percent').text(percent + '%');
    $('.translation-progress').css('width', percent + '%');
    let btnSaveLanguage = $('#btn-save-language');
    if(percent<100){
      btnSaveLanguage.text(btnSaveLanguage.data('save'))
    }else{
      btnSaveLanguage.text(btnSaveLanguage.data('publish'))
    }
  });
  $('#btn-import-translation').on('click',function () {
    $('#import-file').click()
  });
  $('#import-file').on('change',function () {
    $(this).closest('form').submit();
  })
  let isScrolling = null;
  $(window).on('scroll', function () {
    if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight - 150) {
      clearTimeout(isScrolling);
      $('#submit-block').removeClass('floating').css('bottom', 0);
    } else {
      clearTimeout(isScrolling);
      isScrolling = setTimeout(function () {
        $('#submit-block').addClass('floating').css('bottom', document.body.scrollHeight - (window.innerHeight + window.scrollY) - 125);
      }, 10);
    }
  });
  $(window).trigger('scroll');
})