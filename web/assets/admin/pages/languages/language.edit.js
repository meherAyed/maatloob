$(document).ready(function () {
  $('.new-translation').on('change', function (e) {
    if ($(this).val()) {
      $(this).removeClass('empty').addClass('valid')
    } else {
      $(this).addClass('empty').removeClass('valid')
    }
    let percent = parseFloat((($('.new-translation:not(.empty)').length / $('.new-translation').length) * 100).toFixed(2));
    $('#progression-percent').text(percent + '%');
    $('.translation-progress').css('width', percent + '%');
  });
  $('#search-msg').on('keyup', function () {
    let searchFor = $(this).val();
    $.each($('.message-block'), function (i, elem) {
      let inputs = $(elem).find('textarea');
      if ( $(inputs[0]).val().toLowerCase().includes(searchFor.toLowerCase()) || $(inputs[1]).val().toLowerCase().includes(searchFor.toLowerCase())) {
        $(elem).removeClass('hidden');
      } else {
        $(elem).addClass('hidden');
      }
    });
    $(window).trigger('scroll');
  });
  $('#btn-import-translation').on('click', function () {
    $('#import-file').click()
  });
  $('#import-file').on('change', function () {
    $(this).closest('form').submit();
  });
  let isScrolling = null;
  $(window).on('scroll', function () {
    if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight - 150) {
      clearTimeout(isScrolling);
      $('#submit-block').removeClass('floating').css('bottom', 0);
    } else {
      clearTimeout(isScrolling);
      isScrolling = setTimeout(function () {
        $('#submit-block').addClass('floating').css('bottom', document.body.scrollHeight - (window.innerHeight + window.scrollY) - 125);
      }, 10);
    }
  });
  $(window).trigger('scroll');
});