$(document).ready(function () {
    let isRtl = $('html').attr('dir') === 'rtl';

    $('.owl-carousel').owlCarousel({
        loop: true,
        rtl: isRtl,
        margin: 10,
        items: 1
    });
    $(document).on('click', '.btn-slide', function () {
        if ($(this).hasClass('btn-slide-prev')) {
            $(this).closest('.owl-carousel').find('.owl-prev').click()
        } else {
            $(this).closest('.owl-carousel').find('.owl-next').click()
        }
    });

    $(document).on('click','.btn-send-reminder-email:not(.disabled)',function () {
        let btn = $(this);
        btn.closest('tr').loading();
        fetch(btn.data('url'),{method:'POST'})
          .then(e => e.json())
          .then(data => {
            miniToaster.fire({
                type: 'success',
                title: data.message
            });
            btn.addClass('disabled');
            btn.closest('tr').loading('stop');
        })
    });


    $('#delay-tasks-table').DataTable({
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "bAutoWidth": false,
        "pageLength": 8,
        "columnDefs": [{
            "targets": [6], // column or columns numbers
            "orderable": false,  // set orderable for selected columns

        }],
        "fnDrawCallback": function (oSettings) {
            if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
            }
        }
    });
    let monthData = Array.from({length: moment().month() + 1}, (x, i) => moment.monthsShort()[i]);
    /** User widget */
    (function () {
        new Chartist.Line('#stats-users-widget .ct-chart', {
            labels: monthData,
            series: [
                monthData.map((e, index) => {
                    e = 0;
                    $('#stats-users-widget').data('evolution').forEach(elem => {
                        if (index + 1 === parseInt(elem.month))
                            e = elem.totalUsers;
                    });
                    return e;
                })
            ]
        }, {
            low: 0,
            showArea: true,
            showPoint: false,
            showLine: true,
            fullWidth: true,
            chartPadding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            },
            axisX: {
                showLabel: false,
                showGrid: false,
                offset: 0
            },
            axisY: {
                showLabel: false,
                showGrid: false,
                offset: 0
            }
        });
    })();

    /** Tasks widget */
    (function () {
        new Chartist.Line('#stats-task-widget .ct-chart', {
            labels: monthData,
            series: [
                monthData.map((e, index) => {
                    e = 0;
                    $('#stats-task-widget').data('evolution').forEach(elem => {
                        if (index + 1 === parseInt(elem.month))
                            e = elem.totalTasks;
                    });
                    return e;
                })
            ]
        }, {
            low: 0,
            showArea: true,
            showPoint: false,
            fullWidth: true,
            chartPadding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            },
            axisX: {
                showLabel: false,
                showGrid: false,
                offset: 0
            },
            axisY: {
                showLabel: false,
                showGrid: false,
                offset: 0
            }
        });
    })();

    /** Transactions widget */
    (function () {
        new Chartist.Line('#stats-transaction-widget .ct-chart', {
            labels: monthData,
            series: [
                monthData.map((e, index) => {
                    e = 0;
                    $('#stats-transaction-widget').data('evolution').forEach(elem => {
                        if (index + 1 === parseInt(elem.month))
                            e = elem.totalTransactions;
                    });
                    return e;
                })
            ]
        }, {
            low: 0,
            showArea: true,
            showPoint: false,
            fullWidth: true,
            chartPadding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            },
            axisX: {
                showLabel: false,
                showGrid: false,
                offset: 0
            },
            axisY: {
                showLabel: false,
                showGrid: false,
                offset: 0
            }
        });
    })();

    /** Revenue widget */
    if($('#stats-revenue-widget').length){
        (function () {
            new Chartist.Line('#stats-revenue-widget .ct-chart', {
                labels: monthData,
                series: [
                    monthData.map((e, index) => {
                        e = 0;
                        $('#stats-revenue-widget').data('evolution').forEach(elem => {
                            if (index + 1 === parseInt(elem.month))
                                e = elem.revenue;
                        });
                        return e;
                    })
                ]
            }, {
                low: 0,
                showArea: true,
                showPoint: false,
                fullWidth: true,
                chartPadding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
                axisX: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                },
                axisY: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                }
            });
        })();
    }
    let serverStorage = $("#server-storage");
    Highcharts.chart('server-storage', {
        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: ` <i class="fa fa-database float-left mr-5"></i> ${serverStorage.data("title")}`,
            useHTML: true
        },
        pane: {
            startAngle: -120,
            endAngle: 120,
            background: [{
                backgroundColor: {
                    linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },
        yAxis: {
            min: 0,
            max: serverStorage.data('total'),
            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',
            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 2,
                rotation: 'auto'
            },
            title: {
                text: serverStorage.data("metric")
            },
            plotBands: [{
                from: 0,
                to: serverStorage.data('total') / 2,
                color: '#55BF3B' // green
            }, {
                from: serverStorage.data('total') / 2,
                to: serverStorage.data('total') / 1.3,
                color: '#DDDF0D' // yellow
            }, {
                from: serverStorage.data('total') / 1.3,
                to: serverStorage.data('total'),
                color: '#DF5353' // red
            }]
        },
        tooltip: {
            useHTML: true,
            style: {
                fontSize: '16px',
                direction: isRtl ? 'rtl' : 'ltr',
            },
            valueSuffix: ' ' + serverStorage.data("metric"),
        },
        series: [{
            name: serverStorage.data("info"),
            data: [serverStorage.data('used')],
        }]
    });
    const modifiedTaskData = [];
    const modifiedOfferData = [];
    offerData.forEach((elem,index)=>{
        let x = moment(elem['x']);
        let prevDate = moment(elem['x']).add(-1,'day').format('YYYY-MM-DD');
        let nextDate = moment(elem['x']).add(1,'day').format('YYYY-MM-DD');
        if(index > 0){
            if(moment(modifiedOfferData[modifiedOfferData.length-1][0]).format('YYYY-MM-DD') !== prevDate)
                modifiedOfferData.push([moment(prevDate).toDate().getTime(),0]);
        }
        modifiedOfferData.push([x.toDate().getTime(),parseInt(elem['y'])]);
        if(index < (offerData.length -1 ) && offerData[index+1]['x'] !== nextDate ){
            modifiedOfferData.push([moment(nextDate).toDate().getTime(),0])
        }
    });
    taskData.forEach((elem,index)=>{
        let x = moment(elem['x']);
        let prevDate = moment(elem['x']).add(-1,'day').format('YYYY-MM-DD');
        let nextDate = moment(elem['x']).add(1,'day').format('YYYY-MM-DD');
        if(index > 0){
            if(moment(modifiedTaskData[modifiedTaskData.length-1][0]).format('YYYY-MM-DD') !== prevDate)
                modifiedTaskData.push([moment(prevDate).toDate().getTime(),0]);
        }
        modifiedTaskData.push([x.toDate().getTime(),parseInt(elem['y'])]);
        if(index < (taskData.length -1 ) && taskData[index+1]['x'] !== nextDate ){
            modifiedTaskData.push([moment(nextDate).toDate().getTime(),0])
        }
    });
    let taskOfferChart = $('#task-offers');
    Highcharts.chart('task-offers', {
        chart: {
            direction: isRtl,
            type: 'spline',
        },
        legend: {
            rtl: true
        },
        title: {
            text: taskOfferChart.data('title'),
        },
        xAxis: {
            type: 'datetime',
            title: {
                text: taskOfferChart.data('xaxis')
            },
            labels: {
                formatter: function () {
                    return moment(this.value).format("DD.MMMM");
                }
            },
            reversed: isRtl
        },
        tooltip: {
            useHTML: true,
            style: {
                fontSize: '16px',
                direction: isRtl ? 'rtl' : 'ltr',
            },
            formatter: function () {
                return moment(this.point.x).format("DD,MMM") + '<br/><span style="color:' + this.series.color + '">' + this.series.name + '</span> : ' + Highcharts.numberFormat(this.point.y, 0);
            }
        },
        yAxis: {
            title: {
                text: taskOfferChart.data('yaxis')
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }],
            min: 0
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: true
                }
            }
        },
        series: [{
            name: taskOfferChart.data('tasks'),
            data: modifiedTaskData

        }, {
            name: taskOfferChart.data('offers'),
            data: modifiedOfferData
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    plotOptions: {
                        series: {
                            marker: {
                                radius: 2.5
                            }
                        }
                    }
                }
            }]
        }
    });
    let topCategoryChart = $('#top-categories');
    new Highcharts.Chart({
        colors: ["#f2a654", "#83B483"],
        chart: {
            zoomType: 'xy',
            renderTo: 'top-categories',
            type: 'column',
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: topCategory.map(elem => elem['name']),
            title: {
                text: topCategoryChart.data('axis')
            },
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: topCategoryChart.data('yaxis')
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:15px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true,
            borderRadius: 10,
            borderWidth: 2
        },
        plotOptions: {
            series: {
                groupPadding: 0.2
            },
            column: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: [
            {
                name: topCategoryChart.data('serie-task'),
                data: topCategory.map(elem => parseInt(elem['totalTasks']))
            },
            {
                name: topCategoryChart.data('serie-offer'),
                data: topCategory.map(elem => parseInt(elem['totalOffers']))

            }

        ]
    });

    /** Widget top poster & tasker */
    am4core.ready(function () {
        am4core.useTheme(am4themes_animated);
        function chartTopPoster() {
            /**
             * Chart design taken from Samsung health app
             */
            let chart = am4core.create("top-poster", am4charts.XYChart);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

            chart.paddingBottom = 30;
            chart.paddingLeft = 30;
            chart.rtl = isRtl;

            chart.data = topFivePoster;

            let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "name";
            categoryAxis.renderer.grid.template.strokeOpacity = 0;
            categoryAxis.renderer.minGridDistance = 10;
            categoryAxis.renderer.labels.template.dy = 35;
            categoryAxis.renderer.tooltip.dy = 35;

            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.inside = true;
            valueAxis.renderer.labels.template.fillOpacity = 0.3;
            valueAxis.renderer.grid.template.strokeOpacity = 0;
            valueAxis.min = 0;
            valueAxis.cursorTooltipEnabled = false;
            valueAxis.renderer.baseGrid.strokeOpacity = 0;
            valueAxis.renderer.labels.template.dx = -30;
            valueAxis.renderer.labels.template.dy = 20;

            let series = chart.series.push(new am4charts.ColumnSeries);
            series.dataFields.valueY = "money";
            series.dataFields.categoryX = "name";
            series.tooltipText = "{valueY.value} " + currencySymbol;
            series.tooltip.pointerOrientation = "vertical";
            series.tooltip.dy = -6;
            series.columnsContainer.zIndex = 100;

            let columnTemplate = series.columns.template;
            columnTemplate.width = am4core.percent(30);
            columnTemplate.maxWidth = 50;
            columnTemplate.column.cornerRadius(60, 60, 10, 10);
            columnTemplate.strokeOpacity = 0;

            series.heatRules.push({
                target: columnTemplate,
                property: "fill",
                dataField: "valueY",
                min: am4core.color("#e5dc36"),
                max: am4core.color("#5faa46")
            });
            series.mainContainer.mask = undefined;

            let cursor = new am4charts.XYCursor();
            chart.cursor = cursor;
            cursor.lineX.disabled = true;
            cursor.lineY.disabled = true;
            cursor.behavior = "none";

            let bullet = columnTemplate.createChild(am4charts.CircleBullet);
            bullet.circle.radius = 20;
            bullet.valign = "bottom";
            bullet.align = "center";
            bullet.isMeasured = true;
            bullet.mouseEnabled = false;
            bullet.verticalCenter = "bottom";
            bullet.interactionsEnabled = false;

            let outlineCircle = bullet.createChild(am4core.Circle);
            outlineCircle.adapter.add("radius", function (radius, target) {
                let circleBullet = target.parent;
                return circleBullet.circle.pixelRadius + 7;
            });

            let image = bullet.createChild(am4core.Image);
            image.width = 60;
            image.height = 60;
            image.horizontalCenter = "middle";
            image.verticalCenter = "middle";
            image.propertyFields.href = "picture";
            bullet.states.create("hover");
            image.adapter.add("mask", function (mask, target) {
                let circleBullet = target.parent;
                return circleBullet.circle;
            });

            let previousBullet;
            chart.cursor.events.on("cursorpositionchanged", function (event) {
                let dataItem = series.tooltipDataItem;
                if (dataItem.column) {
                    let bullet = dataItem.column.children.getIndex(1);
                    if (previousBullet && previousBullet != bullet) {
                        previousBullet.isHover = false;
                    }
                    if (previousBullet != bullet) {
                        let hs = bullet.states.getKey("hover");
                        hs.properties.dy = -bullet.parent.pixelHeight + (bullet.parent.pixelHeight > 20 ? 20 : 0);
                        bullet.isHover = true;
                        previousBullet = bullet;
                    }
                }
            });
        }
        function chartTopTasker() {
            /**
             * Chart design taken from Samsung health app
             */
            let chart = am4core.create("top-tasker", am4charts.XYChart);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

            chart.paddingBottom = 30;
            chart.paddingLeft = 30;
            chart.rtl = isRtl;
            chart.data = topFiveTasker;

            let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "name";
            categoryAxis.renderer.grid.template.strokeOpacity = 0;
            categoryAxis.renderer.minGridDistance = 10;
            categoryAxis.renderer.labels.template.dy = 35;
            categoryAxis.renderer.tooltip.dy = 35;

            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.inside = true;
            valueAxis.renderer.labels.template.fillOpacity = 0.3;
            valueAxis.renderer.grid.template.strokeOpacity = 0;
            valueAxis.min = 0;
            valueAxis.cursorTooltipEnabled = false;
            valueAxis.renderer.baseGrid.strokeOpacity = 0;
            valueAxis.renderer.labels.template.dx = -30;
            valueAxis.renderer.labels.template.dy = 20;

            let series = chart.series.push(new am4charts.ColumnSeries);
            series.dataFields.valueY = "money";
            series.dataFields.categoryX = "name";
            series.tooltipText = "{valueY.value} " + currencySymbol;
            series.tooltip.pointerOrientation = "vertical";
            series.tooltip.dy = -6;
            series.columnsContainer.zIndex = 100;

            let columnTemplate = series.columns.template;
            columnTemplate.width = am4core.percent(30);
            columnTemplate.maxWidth = 50;
            columnTemplate.column.cornerRadius(60, 60, 10, 10);
            columnTemplate.strokeOpacity = 0;

            series.heatRules.push({
                target: columnTemplate,
                property: "fill",
                dataField: "valueY",
                min: am4core.color("#e5dc36"),
                max: am4core.color("#5faa46")
            });
            series.mainContainer.mask = undefined;

            let cursor = new am4charts.XYCursor();
            chart.cursor = cursor;
            cursor.lineX.disabled = true;
            cursor.lineY.disabled = true;
            cursor.behavior = "none";

            let bullet = columnTemplate.createChild(am4charts.CircleBullet);
            bullet.circle.radius = 20;
            bullet.valign = "bottom";
            bullet.align = "center";
            bullet.isMeasured = true;
            bullet.mouseEnabled = false;
            bullet.verticalCenter = "bottom";
            bullet.interactionsEnabled = false;

            let outlineCircle = bullet.createChild(am4core.Circle);
            outlineCircle.adapter.add("radius", function (radius, target) {
                let circleBullet = target.parent;
                return circleBullet.circle.pixelRadius + 7;
            });

            let image = bullet.createChild(am4core.Image);
            image.width = 60;
            image.height = 60;
            image.horizontalCenter = "middle";
            image.verticalCenter = "middle";
            image.propertyFields.href = "picture";
            bullet.states.create("hover");
            image.adapter.add("mask", function (mask, target) {
                let circleBullet = target.parent;
                return circleBullet.circle;
            });

            let previousBullet;
            chart.cursor.events.on("cursorpositionchanged", function (event) {
                let dataItem = series.tooltipDataItem;
                if (dataItem.column) {
                    let bullet = dataItem.column.children.getIndex(1);
                    if (previousBullet && previousBullet != bullet) {
                        previousBullet.isHover = false;
                    }
                    if (previousBullet != bullet) {
                        let hs = bullet.states.getKey("hover");
                        hs.properties.dy = -bullet.parent.pixelHeight + (bullet.parent.pixelHeight > 20 ? 20 : 0);
                        bullet.isHover = true;
                        previousBullet = bullet;
                    }
                }
            });
        }
        chartTopPoster();
        chartTopTasker();
    });
    /** Create profits chart */
    if($('#profits-chart').length){
        (function createProfitChart() {
            let totalProfits = $('#total-profits');
            let netProfits = $('#net-profits');
            let todayProfits = Array.from({length: moment().hour() + 1}, (x, i) => [(i < 10 ? '0' + i : i) + ":00", 0, 0]);
            let weekProfits = Array.from({length: moment().day() }, (x, i) => [moment.weekdays()[i+1>6?0:i+1], 0, 0]);
            let monthProfits = Array.from({length: moment().format('DD')}, (x, i) => [moment().set({date: i + 1}).format('DD-MMM'), 0, 0]);
            let yearProfits = Array.from({length: moment().month() + 1}, (x, i) => [moment.monthsShort()[i], 0, 0]);
            let yearTotalProfits = 0;
            let yearNetProfits = 0;
            profitsStats.year.forEach(yearData => {
                yearTotalProfits += parseFloat(yearData.totalPrice);
                yearNetProfits += parseFloat(yearData.totalPrice) - parseFloat(yearData.tax);
                if(yearProfits[parseInt(yearData.month) - 1]){
                    yearProfits[parseInt(yearData.month) - 1][1] = parseFloat(yearData.totalPrice);
                    yearProfits[parseInt(yearData.month) - 1][2] = parseFloat(yearData.tax);
                }
            });
            let monthTotalProfits = 0;
            let monthNetProfits = 0;
            profitsStats.month.forEach(monthData => {
                monthTotalProfits += parseFloat(monthData.totalPrice);
                monthNetProfits += parseFloat(monthData.totalPrice) - parseFloat(monthData.tax);
                if(monthProfits[parseInt(monthData.day) - 1]){
                    monthProfits[parseInt(monthData.day) - 1][1] = parseFloat(monthData.totalPrice);
                    monthProfits[parseInt(monthData.day) - 1][2] = parseFloat(monthData.tax);
                }
            });
            let weekTotalProfits = 0;
            let weekNetProfits = 0;
            profitsStats.week.forEach(weekData => {
                weekTotalProfits += parseFloat(weekData.totalPrice);
                weekNetProfits += parseFloat(weekData.totalPrice) - parseFloat(weekData.tax);
                if(weekProfits[parseInt(weekData.day)]){
                    weekProfits[parseInt(weekData.day)][1] = parseFloat(weekData.totalPrice);
                    weekProfits[parseInt(weekData.day)][2] = parseFloat(weekData.tax);
                }
            });
            let dayTotalProfits = 0;
            let dayNetProfits = 0;
            profitsStats.day.forEach(todayData => {
                dayTotalProfits += parseFloat(todayData.totalPrice);
                dayNetProfits += parseFloat(todayData.totalPrice) - parseFloat(todayData.tax);
                if (todayProfits[parseInt(todayData.hour)]) {
                    todayProfits[parseInt(todayData.hour)][1] = parseFloat(todayData.totalPrice);
                    todayProfits[parseInt(todayData.hour)][2] = parseFloat(todayData.tax);
                }
            });
            totalProfits.text(monthTotalProfits.toFixed(2));
            netProfits.text(monthNetProfits.toFixed(2));
            let currentData = monthProfits;
            let chartOptions = {
                responsive: true,
                legendCallback: function (chart) {
                    let legendHtml = [];
                    for (let i = 0; i < chart.data.datasets.length; i++) {
                        legendHtml.push('<button type="button" class="btn btn-theme-primary chart-legend" style="background-color:' + chart.data.datasets[i].backgroundColor + '" ' +
                          ' onclick="updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ')">' + chart.data.datasets[i].label + '</button >');
                    }
                    return legendHtml.join("");
                },
                legend: {
                    display: false
                },
                tooltips: {
                    xPadding: 20,
                    yPadding: 10,
                    displayColors: !1,
                    callbacks: {
                        label: function (x) {
                            return x["xLabel"] + " : " + x["yLabel"] + currencySymbol
                        }, title: function () {
                            return null
                        }
                    }
                },
                hover: {mode: "label"},
                scales: {
                    xAxes: [{
                        scaleLabel: {show: !0, labelString: "Month"},
                        ticks: {
                            fontColor: "rgba(255, 255, 255, 0.7)",
                            fontStyle: 600,
                            reverse: true
                        },
                        gridLines: {color: "rgba(255, 255, 255, 0.1)", lineWidth: 1}
                    }],
                    yAxes: [{
                        display: !1,
                    }]
                },
                plugins: {
                    datalabels: {
                        labels: {
                            title: {
                                font: {
                                    weight: 'bold'
                                }
                            }
                        },
                        formatter: function (value, context) {
                            return value + ' ' + currencySymbol;
                        }
                    }
                }
            };
            let profitsChartElem = document.getElementById('chart-line');

            updateDataset = function (e, datasetIndex) {
                var index = datasetIndex;
                var ci = e.view.weightChart;
                var meta = ci.getDatasetMeta(index);
                $(e.target).toggleClass('inactive');
                // See controller.isDatasetVisible comment
                meta.hidden = meta.hidden === null ? !ci.data.datasets[index].hidden : null;
                // We hid a dataset ... rerender the chart
                ci.update();
            };
            let generateProfitChartDataset = data => {
                let labels = data.map(function (x) {
                    return x[0];
                });
                let seriesOne = data.map(function (x) {
                    return x[1].toFixed(2)
                });
                let seriesTwo = data.map(function (x) {
                    return x[2].toFixed(2)
                });
                if (isRtl) {
                    labels = labels.reverse();
                    seriesOne = seriesOne.reverse();
                    seriesTwo = seriesTwo.reverse();
                }
                return {
                    labels: labels,
                    datasets: [
                        {
                            label: $(profitsChartElem).data('total'),
                            borderWidth: 2,
                            data: seriesOne,
                            borderColor: "rgba(255, 255, 255, 0.8)",
                            backgroundColor: "rgba(255, 255, 255, 0.15)",
                            pointHoverBorderWidth: 2,
                            pointRadius: 4,
                            pointHitRadius: 10,
                            datalabels: {
                                color: '#FFFFFF'
                            }
                        }, {
                            label: $(profitsChartElem).data('country-tax'),
                            borderWidth: 2,
                            data: seriesTwo,
                            borderColor: "rgba(253,0,0, 0.8)",
                            backgroundColor: "rgb(253,0,30,0.2)",
                            datalabels: {
                                color: 'rgb(253,0,0)'
                            }
                        }
                    ]
                }
            };
            let createProfitChart = (options, data) => {
                return new Chart(profitsChartElem.getContext('2d'), {
                    responsive: true,
                    type: "line",
                    data: generateProfitChartDataset(data),
                    options: options
                });
            };
            window.weightChart = createProfitChart(chartOptions, currentData);
            document.getElementById("profits-series-filter").innerHTML = weightChart.generateLegend();
            $(document).on('click', '#profits-time button:not(.active)', function () {
                $(this).parent().find('button.active').removeClass('active');
                $(this).addClass('active');
                switch ($(this).data('type')) {
                    case 'today' : {
                        totalProfits.text(dayTotalProfits);
                        netProfits.text(dayNetProfits);
                        currentData = todayProfits;
                    }
                        break;
                    case 'week' : {
                        totalProfits.text(weekTotalProfits);
                        netProfits.text(weekNetProfits);
                        currentData = weekProfits;
                    }
                        break;
                    case 'month' : {
                        totalProfits.text(monthTotalProfits);
                        netProfits.text(monthNetProfits);
                        currentData = monthProfits;
                    }
                        break;
                    case 'year' : {
                        totalProfits.text(yearTotalProfits);
                        netProfits.text(yearNetProfits);
                        currentData = yearProfits;
                    }
                        break;
                }
                window.weightChart.destroy();
                window.weightChart = createProfitChart(chartOptions, currentData);
                $('#profits-series-filter button.inactive').removeClass('inactive');

            })
        })()
    }
});
