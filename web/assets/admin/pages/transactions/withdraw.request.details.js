$(document).ready(function () {
  $('.fileinput input').on('change', e => {
    previewFile(e.currentTarget, $(e.currentTarget).closest('.fileinput').find('.img-preview'))
  });
  $(document).on('click', '.fileinput-delete', function (e) {
    e.preventDefault();
    let imgPreview = $(this).closest('.fileinput').find('.img-preview');
    $(this).closest('.fileinput').find('input').val("").trigger('change');
    let originalImg = imgPreview.data('background');
    imgPreview.css('background-image', `url(${originalImg})`);
    $(this).closest('.fileinput').removeClass('has-img');
  });
  $(document).on('click','.btn-request-action',function () {
    $(this).closest('form').append('<input type="hidden" name="action" value="'+$(this).data('action')+'">').submit()
  })
});