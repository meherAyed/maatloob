$.nicescroll.options.autohidemode="leave";
function formIsValid(form){
    let valid = true;
    $(form).find('.has-error').removeClass('has-error')
    let inputToValidate = $(form).find('.validate');
    $.each(inputToValidate,function (i,elem) {
        if(_.isEmpty($(elem).val())){
            $(elem).closest('.form-group').addClass('has-error');
            valid = false;
        }
    });
    return valid;
}

$(document).ready(function () {

    /** timer */
    let clockTime = $('#clock .time');
    let clockDate = $('#clock .date');
    let timerID = setInterval(updateTime, 1000);
    updateTime();
    function updateTime() {
        clockTime.text(moment().format('hh:mm:ss'));
        clockDate.text(moment().format('YYYY-MM-DD ddd'));
    };

    /** Filter selector */
    $(document).on('change', '.filter-selector', function () {
        let url = $(this).val();
        if(url){
            window.location = url;
        }
    });
    /** Search filter */
    $(document).on('keypress', '#search', function (e) {
        if (e.which === 13) {
            let search = $(this).val();
            if (search.length > 0)
                window.location = $('#search').data('url').replace('%23search%23', search);
            else{
                let url = $('#search').data('url').replace('search=%23search%23', '');
                url = url.replace('&&','&');
                if(url[url.length-1] === '&' || url[url.length-1] === '?')
                    url = url.slice(0, -1);
                window.location = url;
            }
        }
    });

    /** Change region selector */
    $(document).on('change','#region-selector',function () {
        $('html').addClass('no-scroll').loading();
       fetch($(this).find('option:selected').data('url'),{method:'POST'})
           .then(resp => window.location.reload())
    });
    let cb =function (start, end) {
        $('#data-date-filter span').html(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
    };

    let dateRangeFilter = $('#data-date-filter');
    let start = moment().startOf('year');
    let end = moment();
    if(dateRangeFilter.length>0){
        if(dateRangeFilter.data('start').length>0){
            let parsedStartDate = moment(dateRangeFilter.data('start'),'DD-MM-YYYY');
            start = parsedStartDate.isValid()?parsedStartDate:start;
        }
        if(dateRangeFilter.data('end').length>0){
            let parsedEndDate = moment(dateRangeFilter.data('end'),'DD-MM-YYYY');
            end = parsedEndDate.isValid()?parsedEndDate:end;
        }
        dateRangeOptions = {
            ...{
                startDate: start,
                endDate: end,
            }
            ,...dateRangeOptions
        };
        dateRangeFilter.daterangepicker(dateRangeOptions, cb)
            .on('apply.daterangepicker',function (e,data) {
                let url = $(this).data('url').replace('_start_',data.startDate.format('DD-MM-YYYY'))
                    .replace('_end_',data.endDate.format('DD-MM-YYYY'))
                window.location.href = url;
        });
        cb(start, end);
    }
    $('#show-notification').on('click',function () {
        if($('#new-notification').length>0){
            let url = $(this).data('url');
            $(this).parent().find('.badge-danger').remove();
            $.ajax({
                url: url,
                method: 'PATCH'
            })
        }
    });
    $(document).on('change', 'select.table-pagination-length', function () {
        let pageLength = $(this).val();
        let url=$(this).data('url');
        let reload=$(this).data('reload');
        $.ajax({
            url: url,
            method: 'post',
            data: {tablePageLength: pageLength},
            success: function () {
                window.location = reload;
            }
        })
    });

    $.each($('[data-plugin="rating"]'),(i,elem) => {
        $(elem).addClass('rating');
        let option = {};
        if($(elem).data('score')){
            option.score = $(elem).data('score');
            option.targetType = null;
        }
        if($(elem).data('stars'))
            option.number= $(elem).data('stars');
        if($(elem).data('readonly'))
            option.readOnly = $(elem).data('readonly');

        if($('html').attr('dir')==='rtl')
            option.starHalf = '/assets/img/star-half-rtl.png';

        $(elem).raty(option)
    });
    $('input,textarea,select').on('keyup change',function (e) {
        if($(this).closest('.form-group').hasClass('has-error') && $(this).val()){
            $(this).closest('.form-group').removeClass('has-error').find('.error-msg').remove();
        }
        if($(this).hasClass('is-invalid') && $(this).val()){
            $(this).removeClass('is-invalid').closest('.form-input-theme').find('.invalid-feedback').remove();
        }
    });
    $(document).on('keyup', '.number-validate', function (e) {
        let num = parseFloat(this.value),
            min = $(this).data('min')??$(this).attr('min'),
            max = $(this).data('max')??$(this).attr('max');

        if (isNaN(num)) { this.value = ""; return;  }
        if(max && num > max) $(this).val(max).trigger('change');
        if(min && min>num ) $(this).val(min).trigger('change');
    });
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    $(document).on('click','.btn-flag-item',function () {
        let btn = $(this);
        let item = btn.data('item');
        let list = btn.data('list');
        if(btn.find('svg').attr('data-prefix')==='fad'){
            btn.find('svg').attr('data-prefix','fal').closest('tr').removeClass('flagged');
        }else{
            btn.find('svg').attr('data-prefix','fad').closest('tr').addClass('flagged');
        }
        $.ajax({
            url: '/administration/items-indicator',
            method:'POST',
            data: {item:item,list:list}
        })
    });

    $(document).on('keyup','.number-percentage-validate',function (e) {
        let val = ""+$(this).val();
        if(val[val.length-1] === '%') val = val.slice(0, -1);
        if(isNaN(val)){
            $(this).val($(this).data('prev')??0)
        }else{
            $(this).data('prev',$(this).val())
        }
    });
});

function preventDefault(event) {
    event.preventDefault();
}

function previewFile(imgFile, previewContainer) {
    let file = imgFile.files[0];
    let reader = new FileReader();
    reader.onloadend = function () {
        previewContainer.css('background-image', `url(${reader.result})`);
        previewContainer.closest('.fileinput').addClass('has-img');
    };
    if (file) {
        reader.readAsDataURL(file);
    } else {
        previewContainer.css('background-image', `url(${previewContainer.data('background')})`);
        previewContainer.closest('.fileinput').addClass('has-img');
    }
}
