$(document).ready(function () {
  let deleteReportModal = $('#delete-report-modal');
  $('.btn-delete-report').click(function () {
    $('.btn-delete-report.processing').removeClass('processing');
    $(this).addClass('processing');
    deleteReportModal.modal("show");
  });

  $('#btn-delete-report').on('click',function () {
    let report = $('.btn-delete-report.processing');
    $.ajax({
      url: report.data('url'),
      method: 'DELETE',
      beforeSend:function(){
        deleteReportModal.addClass('loading-modal');
      },
      success:function (data) {
        report.closest('tr').remove();
        deleteReportModal.removeClass("loading-modal");
        deleteReportModal.modal("hide");
        miniToaster.fire({
          type:'success',
          title: data.message
        });            },
      error:function (error) {
        deleteReportModal.modal("hide");
        miniToaster.fire({
          type:'error',
          title: error.responseJSON.message
        });
      }
    })
  });
});