$(document).ready(function () {
  let banDuration = $('#ban-duration');
  let banType = $('#ban-type');
  let unbanWithPenalty = $('#unban-with-penalty');
  let unbanPenalty = $('#unban-penalty');
  let banReason = $('#ban-reason');


  banDuration.datetimepicker({
    fontAwesome: true,
    language: moment.locale(),
    defaultDate: moment(),
    startDate: moment().add('1','hours').format('YYYY-MM-DD HH:mm'),
    locale: moment.locale(),
    autoclose: true,
    icons: {
      leftArrow: "fas fa-chevron-left",
      rightArrow: "fas fa-chevron-right"
    }
  });

  banType.on('change', function (e) {
    if ($(this).val() === 'permanent') {
      banDuration.closest('.form-group').addClass('hidden')
    } else {
      banDuration.closest('.form-group').removeClass('hidden')
    }
  });

  unbanWithPenalty.on('change', function (e) {
    if ($(this).is(':checked')) {
      unbanPenalty.closest('.form-group').removeClass('hidden')
    } else {
      unbanPenalty.closest('.form-group').addClass('hidden')
    }
  });

  $(document).on('click','.unlock-stars',function () {
    let elem = $(this);
    let url = elem.data('url');
    elem.closest('tr').loading();
    $.ajax({
      method: 'PATCH',
      url: url,
      success:function (data) {
        miniToaster.fire({
          type: 'success',
          title: data.message
        });
        elem.closest('tr').loading('stop');
        elem.remove();
      },
      error: function (data) {
        let resp = data.responseJSON;
        miniToaster.fire({
          type: 'error',
          title: resp.message
        });
        elem.closest('tr').loading('stop');
      }
    })
  });
  let banUserModal = $('#block-user-modal');
  let updateBalanceModal = $('#update-balance-modal');
  $('.btn-ban-user').click(function () {
    $('.btn-ban-user.processing').removeClass('processing');
    banReason.val("");
    unbanPenalty.val("");
    banDuration.val("");
    banUserModal.find('.has-error').removeClass('has-error');
    $(this).addClass('processing');
    banUserModal.modal('show');
    banType.val('permanent').trigger('change');
    unbanWithPenalty.attr('checked',false).trigger('change');
  });
  $('#btn-ban-user').on('click',function (e) {
    let valid = true;
    let data = {};
    if(_.isEmpty(banReason.val())){
      banReason.closest('.form-group').addClass('has-error');
      valid = false;
    }else{
      data.reason = banReason.val();
    }
    data.type = banType.val();
    if(banType.val()==="temporary" && _.isEmpty(banDuration.val())){
      banDuration.closest('.form-group').addClass('has-error');
      valid = false;
    } else if(banType.val()==="temporary") {
      data.duration = banDuration.val()
    }
    if(unbanWithPenalty.is(':checked') && _.isEmpty(unbanPenalty.val())){
      unbanPenalty.closest('.form-group').addClass('has-error');
      valid = false;
    }else if (!_.isEmpty(unbanPenalty.val())){
      data.unbanPenalty = unbanPenalty.val()
    }
    let url = $('.btn-ban-user.processing').data('url');
    if(url && valid){
      $.ajax({
        url: url,
        method: 'POST',
        data: data,
        beforeSend:function(){
          banUserModal.addClass('loading-modal');
        },
        success: function (data) {
          $('.btn-ban-user.processing').closest('tr').remove();
          miniToaster.fire({
            type: 'success',
            title: data.message
          });
          banUserModal.removeClass('loading-modal');
          banUserModal.modal('hide');
        },
        error: function (data) {
          let resp = data.responseJSON;
          miniToaster.fire({
            type: 'error',
            title: resp.message
          });
          banUserModal.modal('hide');
          banUserModal.removeClass('loading-modal');
        }
      })
    }
  });
  $('.btn-update-balance').on('click', function () {
    $('.btn-update-balance.processing').removeClass('processing');
    $('#new-balance').val(0);
    updateBalanceModal.modal('show');
    $(this).addClass('processing');
  });
  $('#btn-update-balance').on('click',function (e) {
    let balanceInput = $('#new-balance');
    let operation = $('#balance-update-operation');
    if(!balanceInput.val()){
      balanceInput.parent().addClass('has-error');
    }
    let url = $('.btn-update-balance.processing').data('url');
    if(url && balanceInput.val()){
      $.ajax({
        url: url,
        method: 'POST',
        data: {balance: balanceInput.val(),operation:operation.val()},
        beforeSend: function () {
          updateBalanceModal.addClass('loading-modal');
        },
        success: function (data) {
          $('.btn-update-balance.processing').closest('tr').find('.user-balance').text(data.balance);
          miniToaster.fire({
            type: 'success',
            title: data.message
          });
          updateBalanceModal.removeClass('loading-modal');
          updateBalanceModal.modal('hide');
        },
        error: function (error) {
          Swal.showValidationMessage(
            `Request failed: ${error}`
          );
          updateBalanceModal.addClass('loading-modal');
          updateBalanceModal.modal('hide');
        }
      });
    }
  });
});