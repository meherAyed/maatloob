$(document).ready(function () {
  let banReasonModal = $('#ban-reason-modal');
  let restoreUserModal = $('#restore-user-modal');
  $('.read-reason').click(function () {
    banReasonModal.find('.modal-body p').text($(this).find('.reason-text').text());
    banReasonModal.modal("show");
  });
  $('.restore-user').click(function () {
    $('.operating-user').removeClass('operating-user');
    $(this).closest('tr').addClass('operating-user');
    $('#penalty').val($(this).data('penalty'));
    restoreUserModal.modal("show");
  });
  $('#btn-restore-account').on('click', function (e) {
    let elem = $('.operating-user');
    $.ajax({
      url: elem.find('.restore-user').data('url'),
      method: 'POST',
      data: {penalty: $('#penalty').val()},
      beforeSend: function () {
        restoreUserModal.addClass('loading-modal')
      },
      success: function (data) {
        miniToaster.fire({
          type: 'success',
          title: data.message
        });
        restoreUserModal.removeClass('loading-modal');
        $('#restore-user-modal').modal("hide");
        elem.remove();
      },
      error: function (error) {
        restoreUserModal.removeClass('loading-modal');
        miniToaster.fire({
          type: 'error',
          title: error.responseJSON.message
        });
      }
    })
  });
});
