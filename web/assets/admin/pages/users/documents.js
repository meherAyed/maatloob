$(document).ready(function () {
  let verificationModal = $('#refuseVerificationModal');
  $('.refuse-doc-verification').on('click', function () {
    verificationModal.modal();
    $('#refuseReason').val("");
    $('#refuseVerificationBtn').data('doc-id', $(this).closest('.user-doc').data('id'))
  });
  $('#refuseVerificationBtn').on('click', function (e) {
    e.preventDefault();
    let elem = $(this);
    let documentId = elem.data('doc-id');
    $.ajax({
      url: $(this).data('url'),
      method: 'post',
      data: {verified: 0, id: documentId,refuseReason: $('#refuseReason').val()},
      beforeSend: function () {
        verificationModal.loading();
      },
      success: function () {
        $('.user-doc[data-id="' + documentId + '"]').remove();
        $('.tooltip').remove();
        verificationModal.loading("stop");
      },
      error:function () {
        verificationModal.loading("stop");
      }
    })
  });
  $('.verify-doc').on('click', function (e) {
    e.preventDefault();
    let elem = $(this);
    let documentId = elem.closest('.user-doc').data('id');
    $.ajax({
      url: elem.data('url'),
      method: 'post',
      data: {verified: 1, id: documentId},
      beforeSend: function () {
        elem.closest('tr').loading();
      },
      success: function (data) {
        elem.closest('tr').loading('stop');
        elem.closest('.user-doc').find('.badge-danger').remove();
        elem.closest('.user-doc').find('.badge-success').removeClass('hidden');
        elem.closest('.user-doc').find('.refuse-doc-verification').remove();
        elem.remove();
        miniToaster.fire({
          type: 'success',
          title: data.message
        });
        $('.tooltip').remove();
      }
    })
  });
  $('.doc-files').lightGallery({
    share: false,
    autoplayControls: false,
    autoplay: false
  });
  $('.show-doc-files').on('click', function () {
    $(this).parent().find('.doc-files img').click()
  });
  $('.description-cell div').readmore({collapsedHeight: 50});
});