$(document).ready(function () {
  function toggleIcon(e) {
    let elem = $(e.target)
      .prev('.panel-heading')
      .find('.more-less');
    elem.attr('data-icon', elem.data('icon') === 'plus' ? 'minus' : 'plus')
  }
  $('.panel-group').on('hidden.bs.collapse shown.bs.collapse', toggleIcon);
  $(document).on('click', '.send-notification-btn', function (e) {
    e.preventDefault();
    let btn = $(this);
    btn.closest('.panel').loading();
    $.ajax({
      url: btn.attr('href'),
      method: 'POST',
      success: function (data) {
        btn.closest('.panel').loading('stop');
        btn.closest('.panel').find('.last-send-date').text(moment().format('DD-MM-YYYY HH:mm:ss'));
        miniToaster.fire({
          type: 'success',
          title: data.message
        });
      },
      error: function (data) {
        let resp = data.responseJSON;
        miniToaster.fire({
          type: 'error',
          title: resp.message
        });
        btn.closest('.panel').loading('stop');
      }
    })
  });
  let deleteNotificationModal = $('#delete-notification-modal');
  $('.btn-delete-notification').click(function (e) {
    e.preventDefault();
    $('.btn-delete-notification.active').removeClass('active');
    $(this).addClass('active');
    deleteNotificationModal.modal('show');
  });
  $('#btn-delete-notification').click(function () {
    let elem = $('.btn-delete-notification.active');
    let url = elem.attr('href');
    $.ajax({
      url: url,
      method: 'DELETE',
      beforeSend: function () {
        deleteNotificationModal.addClass('loading-modal');
      },
      success: function (data) {
        miniToaster.fire({
          type: 'success',
          title: data.message
        });
        elem.closest('.panel').remove();
        deleteNotificationModal.removeClass('loading').modal('hide');
      },
      error: function (error) {
        Swal.showValidationMessage(
          `Request failed: ${error}`
        );
        deleteNotificationModal.removeClass('loading-modal').modal('hide');
      }
    });
  });
})