$(document).ready(function () {
    var regmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var regpass = /^(?=.*[-!$%^&*@çàéè#()_+|~=`{}\[\]:";'<>?,.\/])(?=.*[A-Z])([a-zA-Z0-9 -!$%^&*@çàéè#()_+|~=`{}\[\]:";'<>?,.\/]{8,})$/;
    $("#login-page form input").blur(function () {
        if (!$(this).val()) {
            $(this).parent().addClass('has-error');
            $(this).parent().children('.error-msg').children('span').hide();
            $(this).parent().children('.error-msg').children('.required-msg').show();
        }
    });

    /*** test on blur event of the password input ***/
    $("#login-page form input.password").keyup(function () {
        if (!$(this).val()) {
            /**** show the error required message and hide the other messages ***/
            $(this).parent().addClass('has-error');
            $(this).parent().children('.error-msg').children('span').hide();
            $(this).parent().children('.error-msg').children('.required-msg').show();
        }
        else if(!regpass.test($(this).val()) && $('#login-page form input.password-confirm').length > 0) {
            $(this).parent().addClass('has-error');
            $(this).parent().children('.error-msg').children('span').hide();
            $(this).parent().children('.error-msg').children('.format-msg').show();
        }
        else {
            $(this).parent().removeClass('has-error');
        }
    });

    /*** test on blur event of the password confirm input ***/
    if($('#login-page form input.password-confirm').length > 0) {
        $("#login-page form input.password-confirm").keyup(function () {
            if (!$(this).val()) {
                /**** show the error required message and hide the other messages ***/
                $(this).parent().addClass('has-error');
                $(this).parent().children('.error-msg').children('span').hide();
                $(this).parent().children('.error-msg').children('.required-msg').show();
            }
            else if($(this).val() != $("#login-page form input.password").val()) {
                $(this).parent().addClass('has-error');
                $(this).parent().children('.error-msg').children('span').hide();
                $(this).parent().children('.error-msg').children('.conform-msg').show();
            }
            else {
                $(this).parent().removeClass('has-error');
            }
        });
    }

    /*** test on blur event of the email input ***/
    $("#login-page form input.email").keyup(function () {
        if (!$(this).val()) {
            /**** show the error required message and hide the other messages ***/
            $(this).parent().addClass('has-error');
            $(this).parent().children('.error-msg').children('span').hide();
            $(this).parent().children('.error-msg').children('.required-msg').show();
        }
        else if(!regmail.test($(this).val())) {
            $(this).parent().addClass('has-error');
            $(this).parent().children('.error-msg').children('span').hide();
            $(this).parent().children('.error-msg').children('.conform-msg').show();
        }
        else {
            $(this).parent().removeClass('has-error');
        }
    });

    /*** test on checked checkbox ***/
    $('#login-page form input.inputCheckbox').change(function() {
        if(!this.checked) {
            $('#login-page form input.inputCheckbox').parent().addClass('error');
        }
    });

    /*** prvent copy paste ***/
    $('input[type="password"]').on('cut copy paste',function(e) {
        e.preventDefault();
    });

    $("#login-page form .btn").click(function (e) {

        var isValid = true;

        $("#login-page form input.form-control").blur();

        /*** test if there an enmpty fields ***/
        $("input.form-control").each(function () {
            if (!$(this).val()) {
                isValid = false;
            }
        });

        if($('#login-page form input.password-confirm').length > 0) {
            if( !$("#login-page form input.password").val() == '' && !regpass.test($("#login-page form input.password").val())) {
                isValid = false;
                $("#login-page form input.password").parent().addClass('has-error');
                $("#login-page form input.password + .error-msg span").hide();
                $("#login-page form input.password + .error-msg .format-msg").show();
            }
        }

        /*** test password conform ***/
        if($('#login-page form input.password-confirm').length > 0) {
            if ($("#login-page form input.password-confirm").val() != '' && $("#login-page form input.password").val() != $("#login-page form input.password-confirm").val()) {
                isValid = false;
                /*** hsow the the appropriate error message ***/
                $("#login-page form input.password-confirm").parent().addClass('has-error');
                $("#login-page form input.password-confirm + .error-msg span").hide();
                $("#login-page form input.password-confirm + .error-msg .conform-msg").show();
            }
            else if ($("#login-page form input.password-confirm").val() == '') {
                /*** hsow the the appropriate error message ***/
                $("#login-page form input.password-confirm").parent().addClass('has-error');
                $("#login-page form input.password-confirm + .error-msg span").hide();
                $("#login-page form input.password-confirm + .error-msg .required-msg").show();
            }
        }

        /*** test on checkbox ***/
        if($('#login-page form input.inputCheckbox').length > 0) {
            if(!$('#login-page form input.inputCheckbox').prop('checked')) {
                isValid = false;
                $('#login-page form input.inputCheckbox').parent().addClass('error');
            }
            else {
                $('#login-page form input.inputCheckbox').parent().removeClass('error');
            }
        }

        /*** test on mail format ***/
        if( !$("#login-page form input.email").val() == '' && !regmail.test($("#login-page form input.email").val())) {
            isValid = false;
            $("#login-page form input.email").parent().addClass('has-error');
            $("#login-page form input.email + .error-msg .required-msg").hide();
            $("#login-page form input.email + .error-msg .conform-msg").show();
        }

        else if ($("#login-page form input.email").val() == '') {
            $("#login-page form input.email").parent().addClass('has-error');
            $("#login-page form input.email + .error-msg .conform-msg").hide();
            $("#login-page form input.email + .error-msg .required-msg").show();
        }

        /*** submit if all conditions are applied ***/
        if (isValid) {
            $("#login-page form").submit();
        }else{
            e.preventDefault();
        }
    })
});