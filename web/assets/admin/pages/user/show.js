$(document).ready(function () {
  $('.datepicker').datepicker();
  $('#user_coverPicture,#user_picture').on('change', e => {
    previewFile(e.currentTarget, $(e.currentTarget).parent())
  });
  $(document).on('click', 'label.reset-file', function (e) {
    e.preventDefault();
    $(this).parent().find('input[type=file]').val("").trigger('change');
  });
  $('.portfolio-images').lightGallery({
    share: false,
    autoplayControls: false,
    autoplay: false
  });
  $(document).on('click', '#show-poster-reviews-top:not(.active)', function (e) {
    if (e.originalEvent)
      $('#show-poster-reviews').click();
  });
  $(document).on('click', '#show-poster-reviews:not(.active)', function (e) {
    if (e.originalEvent)
      $('#show-poster-reviews-top').click();
  });

  $(document).on('click', '#show-tasker-reviews-top:not(.active)', function (e) {
    if (e.originalEvent)
      $('#show-tasker-reviews').click();
  });
  $(document).on('click', '#show-tasker-reviews:not(.active)', function (e) {
    if (e.originalEvent)
      $('#show-tasker-reviews-top').click();
  })
});