$(document).ready(function () {
  $('.datepicker').datepicker({
    language:$('html').attr('lang'),
  });
  $('#user_coverPicture_file,#user_picture_file').on('change', e => {
    previewFile(e.currentTarget,$(e.currentTarget).parent())
  });
  $(document).on('click','label.reset-file',function (e) {
    e.preventDefault();
    $(this).parent().find('input[type=file]').val("").trigger('change');
  });
  $('.bootstrap-tagsinput input').keydown(function( event ) {
    if ( event.which == 13 ) {
      $(this).blur();
      $(this).focus();
      return false;
    }
  })
});