$(document).ready(function () {
  $(document).on('keyup change','input.config-input', function () {
    if ($(this).hasClass('required')) {
      if (!$(this).val())
        $(this).closest('.form-group').addClass('has-error');
      else
        $(this).closest('.form-group').removeClass('has-error');
    }
  });
  $('.btn-add-profits').on('click', function (e) {
    e.preventDefault();
    let index = $('.profit-config-block').length;
    let inputName = $(this).data('name');
    let block = `
                   <div class="form-line d-flex profit-config-block mb-10">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">${$(this).data('min')}</span>
                                </div>
                                <input class="form-control number-validate config-input required" type="number" name="${inputName}[${index}][min]"
                                       value="0" data-min="0">
                            </div>
                            <span class="error-msg">${$(this).data('required-msg')}</span>
                        </div>
                        <div class="form-group ml-10">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">${$(this).data('max')}</span>
                                </div>
                                <input class="form-control number-validate config-input required" type="number" name="${inputName}[${index}][max]"
                                       value="0" data-min="0">
                            </div>
                            <span class="error-msg">${$(this).data('required-msg')}</span>
                        </div>
                        <div class="form-group ml-10">
                            <div class="input-group">
                                <input class="form-control number-validate config-input required" type="number" name="${inputName}[${index}][percent]"
                                       value="0" data-min="0" data-max="100">
                                 <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                 </div>
                            </div>
                            <span class="error-msg">${$(this).data('required-msg')}</span>
                        </div>
                        <div class="ml-10">
                            <button class="btn  btn-sm btn-icon btn-round waves-effect btn-flat btn-clean waves-classic
                                             waves-effect waves-classic btn-remove-profit" data-toggle="tooltip">
                                <i class="fa fa-trash-alt font-size-16"></i>
                            </button>
                        </div>
                    </div>
                `;
    $('#profits-container').append($(block));
  });
  $(document).on('click','.btn-remove-profit',function (e) {
    e.preventDefault();
    $(this).closest('.profit-config-block').remove();
  });
  tinymce.init({
    selector: '.invoice-template',
    plugins: 'print preview searchreplace autolink directionality  visualblocks visualchars fullscreen image link  table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools  contextmenu colorpicker textpattern help',
    toolbar1: 'formatselect | bold italic underline strikethrough forecolor backcolor fontsizeselect fontselect | link | alignleft aligncenter alignright alignjustify  | table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | numlist bullist outdent indent  | removeformat',
    height: 500,
    menubar: "file | edit | view| table | format | insert",
    toolbar: "print",
    relative_urls : false,
    remove_script_host : false,
    convert_urls : true,
    image_list: imageList,
    directionality : $('html').attr('dir'),
    language: $('html').attr('lang'),
  });
  $(document).on('keyup', '.tva-input', function (e) {
    if ($(this).val() > 100 || $(this).val() < 0)
      e.preventDefault()
  });

  $(document).on('focusin', function (e) {
    if ($(e.target).closest(".tox-tinymce-aux, .moxman-window, .tam-assetmanager-root").length) {
      e.stopImmediatePropagation();
    }
  });

  $('form#system_config').on('submit',function (e) {
    if(!generalSettingValid())
      e.preventDefault();
  });
  function generalSettingValid() {
    let valid = true;
    let requiredInput = $('input.config-input.required');
    requiredInput.closest('.from-group').removeClass('has-error');
    $.each(requiredInput, function (i, input) {
      if (!input.value) {
        $(input).closest('.form-group').addClass('has-error');
        valid = false;
      }
    });
    return valid;
  }
});
