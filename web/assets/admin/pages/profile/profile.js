$(document).ready(function () {
  $('#profile_coverPicture_file,#profile_picture_file').on('change', e => {
    previewFile(e.currentTarget, $(e.currentTarget).parent())
  });
  $(document).on('click', 'label.reset-file', function (e) {
    e.preventDefault();
    $(this).parent().find('input[type=file]').val("").trigger('change');
  })
});
$(document).ready(function () {

  $('#change-password').on('click',function (e) {
    e.preventDefault();
    $('#password-block').toggleClass('active');
  });


  $('.pwd-block input[type="password"]').keyup(function () {
    if($(this).val().length>0){
      $(this).parent().removeClass('has-error').find('.error-msg span').hide();
    }
    if ($(this).hasClass('password') && $(this).val().length > 0 && $(this).val().length<8) {
      $(this).parent().addClass('has-error');
      $(this).parent().children('.error-msg').children('span').hide();
      $(this).parent().children('.error-msg').children('.format-msg').show();
    }
  });

  $("#reset-password-form").on('submit',function (e) {
    let form = $(this);
    e.preventDefault();
    let isValid = true;
    $('.pwd-block input').parent().removeClass('has-error').find('.error-msg span').hide();
    $.each($('.pwd-block input[type="password"]'),(index,input) => {
      if(!input.value){
        isValid = false;
        $(input).parent().addClass('has-error').find('.required-msg').show();
      }
    });
    if ($(".pwd-block input.password").val() && $(".pwd-block input.password").val().length<8) {
      isValid = false;
      $(".pwd-block input.password").parent().addClass('has-error');
      $(".pwd-block input.password + .error-msg span").hide();
      $(".pwd-block input.password + .error-msg .format-msg").show();
    }
    if ($(".pwd-block input.password-confirm").val() != '' && $(".pwd-block input.password").val() != $(".pwd-block input.password-confirm").val()) {
      isValid = false;
      /*** show the the appropriate error message ***/
      $(".pwd-block input.password-confirm").parent().addClass('has-error');
      $(".pwd-block input.password-confirm + .error-msg span").hide();
      $(".pwd-block input.password-confirm + .error-msg .conform-msg").show();
    }


    if (isValid) {
      let data = $(this).serializeArray();
      let resetBtn = $('#reset-pwd-btn');
      $.ajax({
        method:'POST',
        data: data,
        beforeSend:function () {
          resetBtn.addClass('loading')
        },
        success:function (data) {
          miniToaster.fire({
            type:'success',
            title: data.message
          });
          resetBtn.removeClass('loading');
          form.find('input[type="password"]').val("");
        },
        error:function (error) {
          let response = error.responseJSON;
          Object.keys(response.errors).forEach(key => {
            response.errors[key].forEach(message => {
              $(`#${key}`).parent().addClass('has-error').find('.server-msg').text(message).show();
            });
          });
          miniToaster.fire({
            type:'error',
            title: response.message
          });
          resetBtn.removeClass('loading')
        }
      })
    }
  });
});

function previewFile(imgFile, previewContainer) {
  let file = imgFile.files[0];
  let reader = new FileReader();
  reader.onloadend = function () {
    previewContainer.css('backgroundImage', `url(${reader.result})`);
    previewContainer.find('label .fa').removeClass('fa-camera').addClass('fa-times');
    previewContainer.find('label').addClass('reset-file');
  };
  if (file) {
    reader.readAsDataURL(file);
  } else {

    previewContainer.css('background', '');
    previewContainer.find('label .fa').removeClass('fa-times').addClass('fa-camera');
    previewContainer.find('label').removeClass('reset-file');
  }
}