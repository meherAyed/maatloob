$(document).ready(function () {
  let chatWrapper = $('#chat-wrapper');
  let discussionList = $('#discussions-list');
  chatWrapper.niceScroll();
  discussionList.niceScroll();
  if ($('.no-chat-message').length === 0)
    chatWrapper.getNiceScroll(0).doScrollTop(chatWrapper[0].scrollHeight);
  let messages = [];
  let defaultAvatar = '/assets/img/default-user.png';
  $('.message-img').lightGallery({
    share: false,
    autoplayControls: false,
    autoplay: false
  });
  /** Load discussion messages */
  $(document).on('click', '.discussion', function () {
    $('.discussion.active').removeClass('active');
    $(this).addClass('active');
    let discussionId = $(this).data('id');
    let posterId = $(this).data('poster');
    $.ajax({
      url: window.location.href + '/' + discussionId + '/messages',
      beforeSend: function () {
        chatWrapper.html("");
        app.block(chatWrapper, {
          state: 'success'
        });
      },
      success: function (res) {
        messages = res.data;
        loadMessages(messages.splice(0, 5), posterId);
        if (messages.length > 0) {
          chatWrapper.prepend(`<div class="col-12 text-center mt-10">
                                                <button class="btn btn-theme-primary btn-round" data-poster="${posterId}" id="load-more-messages" >${showMoreText}</button>
                                                </div>`);
        }
        app.unblock(chatWrapper);
        $.each(chatWrapper.find('.message-img'),(i,block) => {
          if ($(block).find('img')) {
            $(block).lightGallery({
              share: false,
              autoplayControls: false,
              autoplay: false
            });
          }
        });
        setTimeout(function () {
          chatWrapper.getNiceScroll(0).doScrollTop(chatWrapper.height());
        }, 50);
      }
    })
  });
  $(document).on('click', '#load-more-messages', function () {
    let posterId = $(this).data('poster');
    loadMessages(messages.splice(0, 5), posterId);
    if (messages.length === 0)
      $(this).remove();
    else
      $(this).parent().prependTo(chatWrapper)
  });

  /** Discussion load & search */
  discussionList.scroll(function () {
    if (discussionList.hasClass('has-more') && !discussionList.hasClass('loading-data')) {
      if ($(this)[0].scrollHeight < $(this).scrollTop() + $(this).height() + 10) {
        let fetchPage = discussionList.data('page') + 1;
        loadDiscussions(fetchPage, $('#search-discussion').val(), false);
      }
    }
  });
  let currentSearchRequest = null;
  let searchDiscussion = function () {
    loadDiscussions(1, $(this).val(), true)
  };
  let loadDiscussions = (page, search, init) => {
    if (currentSearchRequest)
      currentSearchRequest.abort();
    currentSearchRequest = $.ajax({
      data: {'page': page, search: search},
      beforeSend: function () {
        if (init)
          discussionList.find('.discussion').remove();
        $('#discussion-load').removeClass('invisible');
        discussionList.addClass('loading-data');
      },
      success: function (res) {
        discussionList.removeClass('loading-data');
        if (res.hasMore) {
          discussionList.data('page', page);
          discussionList.addClass('has-more');
        } else {
          discussionList.removeClass('has-more');
        }
        res.discussions.forEach(discussion => {
          let taskStatus = "";
          switch (taskStatus) {
            case 2:
              taskStatus = '<span class="badge badge-info">' + taskStatusAssigned + '</span>';
              break;
            case 3:
              taskStatus = '<span class="badge badge-info">' + taskStatusWaitingPayment + '</span>';
              break;
            case 4:
              taskStatus = '<span class="badge badge-info">' + taskStatusCompleted + '</span>';
              break;
            case 5:
              taskStatus = '<span class="badge badge-info">' + taskStatusCompleted + '</span>';
              break;
            case 6:
              taskStatus = '<span class="badge badge-info">' + taskStatusCancelled + '</span>';
              break;
          }
          discussionList.append(`
                                     <li class="row discussion p-10 pr-10" data-id="${discussion.id}" data-poster="${discussion.to.id}">
                                        <div class="col-3 avatars pl-0">
                                            <div class="avatar-icon" data-toggle="tooltip"
                                                 title="${discussion.from.first_name + ' ' + discussion.from.last_name}">
                                                <img src="${discussion.from.picture ? discussion.from.picture.path : '/assets/admin/images/default-user.png'}"
                                                     alt="">
                                            </div>
                                            <div class="avatar-icon ml--20" data-toggle="tooltip"
                                                 title="${discussion.to.first_name + ' ' + discussion.to.last_name}">
                                                <img src="${discussion.to.picture ? discussion.to.picture.path : '/assets/admin/images/default-user.png'}"
                                                     alt="">
                                            </div>
                                        </div>
                                        <div class="col-9 pl-0">
                                            <div class="subject">${discussion.task.title}</div>
                                            <small class="mr-10">
                                                <i class="far fa-clock"></i> ${moment(discussion.task.created_at).format('DD-MM-YYYY HH:mm')}</small>
                                                ${taskStatus}
                                        </div>
                                    </li>`);
        });
        $('#discussion-load').addClass('invisible').appendTo(discussionList);
      }
    });
  };
  $('#search-discussion').on({
    change: searchDiscussion,
    keyup: $.debounce(300, searchDiscussion)
  });
  let loadMessages = (messages, posterId) => {
    messages.forEach(message => {
      if (message.user.id === posterId) {
        chatWrapper.prepend(`
                                    <div class="chat-box-wrapper col-12 row">
                                        <div class="avatar-icon avatar-icon-lg rounded" style="width: 60px"
                                        data-toggle="tooltip" title="${message.user.first_name + ' ' + message.user.last_name}">
                                            <img class="m-auto" onerror="loadDefaultAvatar(this)"  src="${message.user.picture ? message.user.picture.path : defaultAvatar}" alt="">
                                            <div class="text-center position-absolute">${poster}</div>
                                        </div>
                                        <div class="mr-auto ml-5 p-0 mt-5" style="max-width: 75%;">
                                            <div class="chat-box">
                                                ${message.text}
                                                <ul class="message-img">
                                                    ${message.files.map(file => fileDisplay(file.path)).join('')}
                                                </ul>
                                            </div>
                                            <small class="opacity-6 pl-10">
                                                <i class="fa fa-calendar-alt mr-1"></i>
                                                ${moment(message.created_at).format('hh:mm | D MMM YYYY')}
                                            </small>
                                        </div>
                                    </div>
                                `);
      } else {
        chatWrapper.prepend(`
                                    <div class="float-right col-12">
                                        <div class="chat-box-wrapper chat-box-wrapper-right row">
                                            <div class="ml-auto mr-5 p-0 mt-5" style="max-width: 75%;">
                                                <div class="chat-box">
                                                    ${message.text}
                                                    <ul class="message-img">
                                                        ${message.files.map(file => fileDisplay(file.path)).join('')}
                                                    </ul>
                                                </div>
                                                <small class="opacity-6 pl-5">
                                                    <i class="fa fa-calendar-alt mr-1"></i>
                                                    ${moment(message.created_at).format('hh:mm | D MMM YYYY')}
                                                </small>
                                            </div>
                                            <div class="avatar-icon avatar-icon-lg rounded" style="width: 60px" data-toggle="tooltip" title="${message.user.first_name + ' ' + message.user.last_name}">
                                                <img class="m-auto" onerror="loadDefaultAvatar(this)"  src="${message.user.picture ? message.user.picture.path : defaultAvatar}" alt="">
                                                <div class="text-center position-absolute">${tasker}</div>
                                            </div>
                                        </div>
                                    </div>
                                `);
      }
    });
  }
});

function fileDisplay(filePath) {
  if ((/\.(gif|jpe?g|tiff|png|webp|bmp)$/i).test(filePath))
    return `<li data-src="${filePath}"><img src="${filePath}"></li>`;

  return `<li><a href="${filePath}" download"><i class="far fa-file-alt fa-2x indigo"></i></a></li>`
}