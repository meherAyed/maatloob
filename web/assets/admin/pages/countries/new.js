$(document).ready(function () {
  $('#country-selector').on('change', function () {
    if ($(this).val()) {
      $('#country-container').removeClass('hidden');
      let selectedOption = $(this).find('option:selected');
      $('#flag-preview').attr('src',selectedOption.data('icon-path'));
      $('#country_flag').val(selectedOption.data('icon-path'));
      $('#country_identifier').val($(this).val());
      $('#country_name').val(selectedOption.text());
      $('#country_currencyCode').val(selectedOption.data('currency-code'));
      $('#country_currencyName').val(selectedOption.data('currency-name'));
      $('#country_currencySymbol').val(selectedOption.data('currency-symbol'));
      $('#country_tva').val("");
      /** Select default country language if exist*/
      let languages = selectedOption.data('languages').split(',');
      languages.forEach((language) => {
        if($(`#country_defaultLanguage option[value="${language}"]`).length>0){
          $('#country_defaultLanguage').val(language).trigger('change');
        }
      });
    } else {
      $('#messages-container').addClass('hidden');
      $('#country_flag').val("");
      $('#country_identifier').val("");
      $('#country_name').val("");
      $('#country_currencyCode').val("");
      $('#country_currencyName').val("");
      $('#country_currencySymbol').val("");
      $('#country_tva').val("");
    }
  });
  $('.btn-add-profits').on('click', function (e) {
    e.preventDefault();
    let index = $('.profit-config-block').length;
    let inputName = $(this).data('name');
    let block = `
                   <div class="form-line d-flex profit-config-block mb-10">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">${$(this).data('min')}</span>
                                </div>
                                <input class="form-control number-validate config-input required" type="number" name="${inputName}[${index}][min]"
                                       value="0" data-min="0">
                            </div>
                            <span class="error-msg">${$(this).data('required-msg')}</span>
                        </div>
                        <div class="form-group ml-10">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">${$(this).data('max')}</span>
                                </div>
                                <input class="form-control number-validate config-input required" type="number" name="${inputName}[${index}][max]"
                                       value="0" data-min="0">
                            </div>
                            <span class="error-msg">${$(this).data('required-msg')}</span>
                        </div>
                        <div class="form-group ml-10">
                            <div class="input-group">
                                <input class="form-control number-validate config-input required" type="number" name="${inputName}[${index}][percent]"
                                       value="0" data-min="0" data-max="100">
                                <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                            <span class="error-msg">${$(this).data('required-msg')}</span>
                        </div>
                        <div class="ml-10">
                            <button class="btn  btn-sm btn-icon btn-round waves-effect btn-flat btn-clean waves-classic
                                             waves-effect waves-classic btn-remove-profit" data-toggle="tooltip">
                                <i class="fa fa-trash-alt font-size-16"></i>
                            </button>
                        </div>
                    </div>
                `;
    $('#profits-container').append($(block));
  });
  $(document).on('click','.btn-remove-profit',function (e) {
    e.preventDefault();
    $(this).closest('.profit-config-block').remove();
  });
  tinymce.init({
    selector: '.invoice-template',
    plugins: 'print preview searchreplace autolink directionality  visualblocks visualchars fullscreen  link  table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools  contextmenu colorpicker textpattern help',
    toolbar1: 'formatselect | bold italic underline strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | numlist bullist outdent indent  | removeformat',
    height: 500,
    menubar: "file | edit | view| table | format | insert",
    toolbar: "print",
    directionality : $('html').attr('dir'),
    language: $('html').attr('lang'),
  });
});