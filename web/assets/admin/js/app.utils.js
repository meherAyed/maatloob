const app = {
    block: (target, options) => {
        let el = $(target);
        options = $.extend(true, {
            opacity: 0.05,
            overlayColor: '#000000',
            type: '',
            size: '',
            state: 'brand',
            centerX: true,
            centerY: true,
            message: '',
            shadow: true,
            width: 'auto'
        }, options);
        let html;
        let state = options.state ? 'spinner-' + options.state : '';
        let size = options.size ? 'spinner-' + options.size : '';
        let spinner = '<div class="loading-spinner ' + state + ' ' + size + '"></div';

        if (options.message && options.message.length > 0) {
            let classes = 'blockui ' + (options.shadow === false ? 'blockui' : '');

            html = '<div class="' + classes + '"><span>' + options.message + '</span><span>' + spinner + '</span></div>';

            let el = document.createElement('div');
            $('body').prepend(el);
            $(el).addClass(el, classes);
            el.innerHTML = '<span>' + options.message + '</span><span>' + spinner + '</span>';
            options.width = $(el).width() + 10;
            $(el).remove();

            if (target == 'body') {
                html = '<div class="' + classes + '" style="margin-left:-' + (options.width / 2) + 'px;"><span>' + options.message + '</span><span>' + spinner + '</span></div>';
            }
        } else {
            html = spinner;
        }

        let params = {
            message: html,
            centerY: options.centerY,
            centerX: options.centerX,
            css: {
                top: '30%',
                left: '50%',
                border: '0',
                padding: '0',
                backgroundColor: 'none',
                width: options.width
            },
            overlayCSS: {
                backgroundColor: options.overlayColor,
                opacity: options.opacity,
                cursor: 'wait',
                zIndex: '10'
            },
            onUnblock: function () {
                if (el && el[0]) {
                    $(el[0]).css('position', '');
                    $(el[0]).css('zoom', '');
                }
            }
        };

        if (target == 'body') {
            params.css.top = '50%';
            $.blockUI(params);
        } else {
            let el = $(target);
            el.block(params);
        }
    },
    unblock: (target) => {
        if (target && target != 'body') {
            $(target).unblock();
        } else {
            $.unblockUI();
        }
    }
};