<?php

namespace AppBundle\EventListener;

use AppBundle\AppEvents;
use AppBundle\Services\UserMailer;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EmailConfirmationListener implements EventSubscriberInterface
{
    private $userMailer;
    private $tokenGenerator;
    private $router;
    private $session;

    /**
     * EmailConfirmationListener constructor.
     *
     * @param UserMailer $userMailer
     * @param TokenGeneratorInterface $tokenGenerator
     * @param UrlGeneratorInterface $router
     * @param SessionInterface $session
     */
    public function __construct(UserMailer $userMailer, TokenGeneratorInterface $tokenGenerator, UrlGeneratorInterface $router, SessionInterface $session)
    {
        $this->userMailer = $userMailer;
        $this->tokenGenerator = $tokenGenerator;
        $this->router = $router;
        $this->session = $session;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            AppEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
        );
    }

    /**
     * @param FormEvent $event
     */
    public function onRegistrationSuccess(FormEvent $event)
    {
        $user = $event->getForm()->getData();
        if(!$user->isAdmin()){
            $user->setEnabled(false);
        }
        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }
        $this->userMailer->sendConfirmationEmailMessage($user);
        $this->session->set('fos_user_send_confirmation_email/email', $user->getEmail());
    }
}
