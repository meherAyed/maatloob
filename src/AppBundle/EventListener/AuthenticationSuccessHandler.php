<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 29/09/2019
 * Time: 22:50
 */

namespace AppBundle\EventListener;

use  Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use AppBundle\Entity\Country;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\HttpUtils;

class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    public function __construct(HttpUtils $httpUtils, array $options = [])
    {
        parent::__construct($httpUtils, $options);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        /** @var User $user */
        $user = $token->getUser();
        if($user->isAdmin()){
            $request->getSession()->set('region',$user->isSuperAdmin()?Country::Default:$user->getAssignedCountries()->first()->getIdentifier());
        }else{
            $request->getSession()->set('region',$token->getUser()->getCountry()->getIdentifier());
        }
        return $this->httpUtils->createRedirectResponse($request, $this->determineTargetUrl($request));
    }
}
