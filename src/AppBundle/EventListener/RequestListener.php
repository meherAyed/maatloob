<?php


namespace AppBundle\EventListener;


use AppBundle\Entity\Country;
use AppBundle\Entity\User;
use Components\Helper;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class RequestListener
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event): void
    {
        if(($token = $this->tokenStorage->getToken()) && $token->isAuthenticated()){
            /** @var User $user */
            $user = $token->getUser();
            if(($user instanceof User) && $user->isAdmin()) {
                $region = $event->getRequest()->getSession()->get('region',$user->isSuperAdmin()?Country::Default:$user->getAssignedCountries()->first()->getIdentifier());
                Helper::setCountryTimeZone($region);
            }
        }
    }

}
