<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 27/08/2019
 * Time: 00:05
 */

namespace AppBundle\EventListener;


use AppBundle\Entity\File;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\UserWallet;
use AppBundle\Entity\WalletHistoric;
use AppBundle\Services\FileUploader;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;

class DoctrineTransactionListener implements EventSubscriber
{


    private $fileUploader;

    public function __construct(FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::preRemove,
            Events::onFlush
        ];
    }

    public function preRemove(LifecycleEventArgs $args){
        $entity = $args->getEntity();
        if($entity instanceof File){
            $this->fileUploader->deleteFile($entity);
        }
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $unitOfWork = $em->getUnitOfWork();
        $updatedEntities = $unitOfWork->getScheduledEntityUpdates();
        foreach ($updatedEntities as $updatedEntity) {
            if ($updatedEntity instanceof UserWallet) {
                $changeSet = $unitOfWork->getEntityChangeSet($updatedEntity);
                foreach ($changeSet as $field => $change){
                    $oldValue = $change[0] ?? null;
                    $newValue = $change[1] ?? null;
                    if ($oldValue != $newValue) {
                        $walletHistoric = new WalletHistoric();
                        $walletHistoric->setWallet($updatedEntity);
                        $walletHistoric->setChanges(json_encode($changeSet));
                        $em->persist($walletHistoric);
                        $metaData = $em->getClassMetadata(WalletHistoric::class);
                        $unitOfWork->computeChangeSet($metaData, $walletHistoric);
                        break;
                    }
                }
            }
        }
    }
}