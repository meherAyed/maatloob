<?php
namespace AppBundle\EventListener;

use AppBundle\Entity\Notification;
use AppBundle\Services\NotifyUser;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Translation\Translator;

class NotificationTrigger implements EventSubscriber
{

    private $notifyUser;
    private $translator;

    public function __construct(NotifyUser $notifyUser,Translator $translator)
    {
        $this->notifyUser = $notifyUser;
        $this->translator = $translator;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof Notification && $entity->getType() && (($entity->getExtra()['type']??'') !== Notification::TYPE_NEWS)) {
            $this->notifyUser->notify($entity);
        }
    }

}