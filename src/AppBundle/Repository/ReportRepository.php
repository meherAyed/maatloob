<?php

namespace AppBundle\Repository;

use Doctrine\ORM\QueryBuilder;

/**
 * ReportRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ReportRepository extends CustomEntityRepository
{

    public function findReports($search, $status, $orderBy, $order): QueryBuilder
    {
        $query = $this->createQueryBuilder('r')
                    ->join('r.reportedUser','u')
                    ->orWhere('u.firstName like :search')
                    ->orWhere('u.lastName like :search')
                    ->orWhere("(CONCAT(u.firstName,' ',u.lastName) LIKE :search )")
                    ->setParameter('search', '%' . $search . '%');
        if ($status !== 'all') {
            $query->andWhere('r.status = :status')->setParameter('status', $status);
        }

        switch ($orderBy) {
            case 'user':
                $query->orderBy("CONCAT(u.firstName,' ',u.lastName)", $order);
                break;
            case 'reports':
                $query->orderBy('count(r.reporters)', $order);
                break;
            case 'type':
                $query->orderBy('r.type', $order);
                break;
            case 'status':
                $query->orderBy('r.status', $order);
                break;
            default:
                $query->orderBy('r.id', $order);
                break;
        }
        return $query;
    }
}
