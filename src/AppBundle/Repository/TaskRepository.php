<?php

namespace AppBundle\Repository;
use AppBundle\Entity\Offer;
use AppBundle\Entity\Report;
use AppBundle\Entity\ReportType;
use AppBundle\Entity\Task;
use AppBundle\Entity\User;
use AppBundle\Entity\UserReview;
use Doctrine\ORM\QueryBuilder;

/**
 * TaskRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TaskRepository extends CustomEntityRepository
{

    /**
     * @param $user
     * @param null $startDate
     * @param null $endDate
     * @return mixed
     * @throws \Exception
     */
    public function findOpenForOffer($user,$startDate=null,$endDate=null){
        $query = $this->createQueryBuilder('t')
            ->where('t.status = 1')
            ->andWhere('t.date > :date')
            ->andWhere('t.user = :user')
            ->setParameters(['user'=>$user,'date'=>new \DateTime()]);
        if($startDate){
            $query->andWhere('t.createdAt >= :startDate')->setParameter('startDate',new \DateTime($startDate));
        }
        if($endDate){
            $query->andWhere('t.createdAt <= :endDate')->setParameter('endDate',new \DateTime($endDate.' 23:59:59'));
        }
        return $query->getQuery()->getResult();
    }

    /**
     * @param $user
     * @param null $startDate
     * @param null $endDate
     * @return mixed
     */
    public function findAssigned($user,$startDate=null,$endDate=null){
        $query = $this->createQueryBuilder('t')
            ->where('t.status = :statusAssigned')
            ->andWhere('t.user = :user')
            ->setParameters([
                'user'=>$user,
                'statusAssigned' => Task::STATUS_ASSIGNED
            ]);
        if($startDate){
            $query->andWhere('t.createdAt >= :startDate')->setParameter('startDate',new \DateTime($startDate));
        }
        if($endDate){
            $query->andWhere('t.createdAt <= :endDate')->setParameter('endDate',new \DateTime($endDate.' 23:59:59'));
        }
        return $query->getQuery()->getResult();
    }

    /**
     * @param $user
     * @param null $startDate
     * @param null $endDate
     * @return mixed
     * @throws \Exception
     */
    public function findOverdue($user,$startDate=null,$endDate=null){
        $query = $this->createQueryBuilder('t')
            ->where('t.status = :statusAssigned')
            ->andWhere('t.date < :date')
            ->andWhere('t.user = :user')
            ->setParameters([
                'user'=>$user,
                'statusAssigned' => Task::STATUS_ASSIGNED,
                'date'=>new \DateTime()
            ]);
        if($startDate){
            $query->andWhere('t.createdAt >= :startDate')->setParameter('startDate',new \DateTime($startDate));
        }
        if($endDate){
            $query->andWhere('t.createdAt <= :endDate')->setParameter('endDate',new \DateTime($endDate.' 23:59:59'));
        }
        return $query->getQuery()->getResult();
    }

    /**
     * @param $user
     * @param null $paymentType
     * @param null $startDate
     * @param null $endDate
     * @return mixed
     */
    public function findAwaitingPayment($user,$paymentType = null,$startDate=null,$endDate=null){
        $query = $this->createQueryBuilder('t')
            ->where('t.status = :statusWaitingPayment')
            ->andWhere('t.user = :user')
            ->setParameters([
                'user'=>$user,
                'statusWaitingPayment'=>Task::STATUS_WAITING_PAYMENT
            ]);
             if(in_array($paymentType, [Task::PAYMENT_TYPE_CASH, Task::PAYMENT_TYPE_WALLET], true)){
                 $query->andWhere('t.paymentType = :paymentType')->setParameter('paymentType',$paymentType);
             }
        if($startDate){
            $query->andWhere('t.createdAt >= :startDate')->setParameter('startDate',new \DateTime($startDate));
        }
        if($endDate){
            $query->andWhere('t.createdAt <= :endDate')->setParameter('endDate',new \DateTime($endDate.' 23:59:59'));
        }
        return $query->getQuery()->getResult();
    }

    /**
     * @param $user
     * @param null $paymentType
     * @param null $startDate
     * @param null $endDate
     * @return mixed
     */
    public function findCompleted($user,$paymentType = null,$startDate=null,$endDate=null){
        $query = $this->createQueryBuilder('t')
            ->where('t.status in (:status)')
            ->andWhere('t.user = :user')
            ->setParameters([
                'user'=>$user,
                'status'=> [Task::STATUS_COMPLETED,Task::STATUS_PAID]
            ]);
         if(in_array($paymentType, [Task::PAYMENT_TYPE_CASH, Task::PAYMENT_TYPE_WALLET], true)){
             $query->andWhere('t.paymentType = :paymentType')->setParameter('paymentType',$paymentType);
         }
        if($startDate){
            $query->andWhere('t.createdAt >= :startDate')->setParameter('startDate',new \DateTime($startDate));
        }
        if($endDate){
            $query->andWhere('t.createdAt <= :endDate')->setParameter('endDate',new \DateTime($endDate.' 23:59:59'));
        }
        return $query->getQuery()->getResult();
    }

    public function findTotal(User $user,$startDate=null,$endDate=null){
        $query = $this->createQueryBuilder('t')
            ->where('t.status <> :statusDraft')
            ->andWhere('t.user = :user')
            ->setParameters([
                'user'=>$user->getId(),
                'statusDraft' => Task::STATUS_DRAFT
            ]);
        if($startDate){
            $query->andWhere('t.createdAt >= :startDate')->setParameter('startDate',new \DateTime($startDate));
        }
        if($endDate){
            $query->andWhere('t.createdAt <= :endDate')->setParameter('endDate',new \DateTime($endDate.' 23:59:59'));
        }
        return $query->getQuery()->getResult();
    }

    public function findAwaitingReview($user,$startDate=null,$endDate=null){
        $query = $this->createQueryBuilder('t')
            ->leftJoin(UserReview::class,'ur','with','ur.task = t and ur.createdBy != :user')
            ->where('t.status = :underReview')
            ->setParameters([
                'underReview' => Task::STATUS_PAID,
                'user' => $user
            ]);
        if($startDate){
            $query->andWhere('t.createdAt >= :startDate')->setParameter('startDate',new \DateTime($startDate));
        }
        if($endDate){
            $query->andWhere('t.createdAt <= :endDate')->setParameter('endDate',new \DateTime($endDate.' 23:59:59'));
        }
        return $query->getQuery()->getResult();
    }

    public function getTotalUnpaidTask($user){
        return $this->createQueryBuilder('t')
                ->select('count(t.price) as priceToPay')
                ->where('t.user = :user and t.status = :status')
                ->setParameters(['user'=>$user,'status'=>Task::STATUS_WAITING_PAYMENT])
                ->getQuery()->getSingleResult()['priceToPay'];
    }

    /**
     * Add max distance allowed
     * @param QueryBuilder $query
     * @param array $distance
     */
    public function addDistanceCheck(QueryBuilder $query,array $distance): void
    {
        if(isset($distance['latitude'],$distance['longitude'],$distance['max'])){
            $query->andWhere('111.111 * DEGREES(ACOS(LEAST(COS(RADIANS('.$distance['latitude'].'))  * 
                 COS(RADIANS(a.latitude)) * COS(RADIANS('.$distance['longitude'].' - a.longitude)) + 
                 SIN(RADIANS('.$distance['latitude'].')) * SIN(RADIANS(a.latitude)), 1.0))) <= '.$distance['max']);
        }
    }

    /**
     * @param $distance
     * @return QueryBuilder
     * @throws \Exception
     */
    public function findTaskList($distance): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->where('t.date >= :currentDate')
            ->andWhere('t.status not in (:status)')
            ->setParameters(['currentDate'=>(new \DateTime())->format('Y-m-d'),'status'=>[Task::STATUS_DRAFT,Task::STATUS_CANCELLED]])
            ->orderBy('t.id','DESC');
    }

    public function findTasks($search,$status,$orderBy,$order): QueryBuilder
    {

        $query=$this->createQueryBuilder('t')
            ->join('t.user','u')
            ->where('t.title like :search')
            ->orWhere("( CONCAT(u.firstName,' ',u.lastName) LIKE :search )")
            ->setParameter('search','%'.$search.'%');
        if(array_key_exists($status, self::getStatusNames())){
            $query->andWhere('t.status in (:status) ')
                ->setParameter('status', self::getStatusNames()[$status]);
        }
        if($status==='overdue'){
            $query->andWhere('t.date < :dateNow')
                  ->andWhere('t.status = :status')
            ->setParameter('status', Task::STATUS_ASSIGNED)
            ->setParameter('dateNow' , new \DateTime());
        }
        switch ($orderBy){
            case 'title': $query->orderBy('t.title',$order);break;
            case 'price': $query->orderBy('t.price',$order);break;
            case 'status': $query->orderBy('t.status',$order);break;
            case 'poster': $query->orderBy("CONCAT(u.firstName,' ',u.lastName)",$order);break;
            case 'due_date': $query->orderBy('t.date',$order);break;
            case 'maatloob_tax': $query->orderBy('t.maatloobTax',$order);break;
            default: $query->orderBy('t.id',$order);break;
        }
        return $query;
    }

    public static function getStatusNames(){
        $statusNames['draft'] = Task::STATUS_DRAFT;
        $statusNames['published'] = Task::STATUS_PUBLISHED;
        $statusNames['assigned'] = Task::STATUS_ASSIGNED;
        $statusNames['waiting_payment'] = Task::STATUS_WAITING_PAYMENT;
        $statusNames['completed'] = [Task::STATUS_PAID,Task::STATUS_COMPLETED];
        $statusNames['cancelled'] = Task::STATUS_CANCELLED;
        return $statusNames;
    }

    public function getDashboardStat($year,$country){
        $firstDateThisYear = $year.'-01-01';
        $lastYearThisDay = (new \DateTime())->modify('-1 year');
        $stats['totalTask'] = count($this->createQueryBuilder('t')
            ->join('t.user','user')
            ->where('t.isDeleted = false')
            ->andWhere('user.country = :country')
            ->setParameter('country',$country)
            ->getQuery()->getResult());
        $stats['lastYearTasks'] =count($this->createQueryBuilder('t')
            ->join('t.user','user')
            ->where('t.isDeleted = false')
            ->andWhere('user.country = :country')
            ->andWhere('t.createdAt >= :minDate and t.createdAt <= :maxDate')
            ->setParameters([
                'country'=>$country,
                'minDate'=>$lastYearThisDay->format('Y').'-01-01',
                'maxDate'=>$lastYearThisDay->format('Y-m-d')
            ])
            ->getQuery()->getResult());
        $stats['newTasks'] = count($this->createQueryBuilder('t')
            ->join('t.user','user')
            ->where('t.isDeleted = false')
            ->andWhere('user.country = :country')
            ->andWhere('t.createdAt > :date')
            ->setParameters(['date'=>$firstDateThisYear,'country'=>$country])
            ->getQuery()->getResult());
        $stats['taskEvolution'] = $this->createQueryBuilder('t')
            ->join('t.user','user')
            ->select('COUNT(t) as totalTasks , MONTH(t.createdAt) month')
            ->where('t.isDeleted = false')
            ->andWhere('user.country = :country')
            ->andWhere('t.createdAt >= :date')
            ->setParameters(['date'=>$firstDateThisYear,'country'=>$country])
            ->groupBy('month')
            ->getQuery()->getResult();
        return $stats;
    }

    public function getCompletedTaskStats($country){
        try{
            return $this->createQueryBuilder('t')
                ->select('Count(t) as totalTasks','SUM(case when t.status>3 then offers.price else 0 end) as receipts')
                ->where('t.status in (:status)')
                ->join('t.offers','offers','with','offers.status = :offerAccepted')
                ->join('t.user','user')
                ->andWhere('user.country = :country')
                ->setParameters([
                    'status' => [Task::STATUS_PAID,Task::STATUS_COMPLETED],
                    'offerAccepted' => Offer::STATUS_ACCEPTED,
                    'country' => $country
                ])
                ->getQuery()->getSingleResult();
        }catch (\Exception $e){
            return ['totalTasks'=>0,'receipts' => 0];
        }

    }
    public function getCancelledTaskStats($country){
            return $this->createQueryBuilder('t')
                ->select('Count(t) as totalTasks','SUM(t.maatloobTax) as losses')
                ->where('t.status = :status')
                ->join('t.user','user')
                ->andWhere('user.country = :country')
                ->setParameters(['status' => Task::STATUS_CANCELLED,'country' => $country])
                ->getQuery()->getSingleResult();


    }

    /** Queries for stats
     * @param $country
     * @return mixed
     * @throws \Exception
     */
    public function findNewTasks($country)
    {
        return $this->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->where('u.country = :country')
            ->andWhere('t.status <> :statusDraft')
            ->andWhere('t.createdAt >= :currentMonth')
            ->setParameter('statusDraft', Task::STATUS_DRAFT)
            ->setParameter('country', $country)
            ->setParameter('currentMonth', new \DateTime('first day of this month'))
            ;
    }

    public function findOpenTasks($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->where('u.country = :country')
            ->andWhere('t.status = :statusPublished  and t.date > :currentDate ')
            ->setParameters([
                'country' => $country,
                'currentDate' => new \DateTime(),
                'statusPublished' => Task::STATUS_PUBLISHED,]);
    }

    public function findAssignedTasks($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->where('u.country = :country')
            ->andWhere('t.status = :statusAssigned')
            ->setParameters([
                'country' => $country,
                'statusAssigned' => Task::STATUS_ASSIGNED
            ]);
    }

    public function findDelayedTasks($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->where('u.country = :country')
            ->andWhere('(t.status = :statusAssigned and t.date < :currentDate) or (t.doneDate > t.date)')
            ->setParameters([
                'country' => $country,
                'currentDate' => new \DateTime(),
                'statusAssigned' => Task::STATUS_ASSIGNED
            ]);
    }

    public function findCompletedTasks($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->where('u.country = :country')
            ->andWhere('t.status in (:statusCompleted)')
            ->setParameters([
                'country' => $country,
                'statusCompleted' => [Task::STATUS_COMPLETED, Task::STATUS_PAID]
            ]);
    }

    public function findCancelledTasks($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->where('u.country = :country')
            ->andWhere('t.status = :statusCancelled')
            ->setParameters([
                'country' => $country,
                'statusCancelled' => Task::STATUS_CANCELLED
            ]);
    }

    public function findDeletedTasks($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->where('u.country = :country')
            ->andWhere('t.isDeleted = :deleted')
            ->setParameters([
                'deleted' => 1,
                'country' => $country,
            ]);
    }

    public function findExpiredTasks($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->where('u.country = :country')
            ->andWhere('t.status = :statusPublished')
            ->andWhere('t.date < :currentDate')
            ->setParameters([
                'country' => $country,
                'statusPublished' => Task::STATUS_PUBLISHED,
                'currentDate' => new \DateTime()
            ]);
    }

    /**
     * @param null $country
     * @return QueryBuilder
     */
    public function findAwaitingPaymentTasks($country = null): QueryBuilder
    {
        $query =  $this->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->andWhere('t.status = :statusAwaitingPayment')
            ->setParameter('statusAwaitingPayment' , Task::STATUS_WAITING_PAYMENT);
        if($country){
            $query->andWhere('u.country = :country')->setParameter('country', $country);
        }
        return $query;
    }

    public function findPaymentCashTasks($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->where('u.country = :country')
            ->andWhere('t.paymentType = :paymentCash and t.status <> 0')
            ->setParameters([
                'country' => $country,
                'paymentCash' => Task::PAYMENT_TYPE_CASH
            ]);
    }

    public function findPaymentWalletTasks($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->where('u.country = :country and t.status <> 0')
            ->andWhere('t.paymentType = :paymentWallet')
            ->setParameters([
                'country' => $country,
                'paymentWallet' => Task::PAYMENT_TYPE_WALLET
            ]);
    }

    public function findTasksWithProblematic($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->join(Report::class, 'r', 'with', 'r.objectId = t.id and r.section = :taskSection')
            ->join('t.user', 'u')
            ->where('r.status = :statusAccepted')
            ->andWhere('u.country = :country and t.status <> 0')
            ->setParameters([
                'statusAccepted' => Report::STATUS_TREATED,
                'taskSection' => ReportType::SECTION_TASK,
                'country' => $country
            ]);
    }


    /**
     * @param $country
     * @return QueryBuilder
     */
    public function findLockedBalanceTasks($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
                    ->join('t.user','u')
                    ->where('u.country = :country')
                    ->andWhere('t.paymentType = :paymentWallet')
                    ->andWhere('t.status in (:status)')
                    ->setParameters([
                        'country' => $country,
                        'paymentWallet' => Task::PAYMENT_TYPE_WALLET,
                        'status'=> [Task::STATUS_PUBLISHED,Task::STATUS_ASSIGNED,Task::STATUS_WAITING_PAYMENT]
                    ]);
    }

    /**
     * @param $user
     * @return array
     */
    public function findUserLockedBalanceTasks($user): array
    {
        return $this->createQueryBuilder('t')
                    ->where('t.user = :user')
                    ->andWhere('t.paymentType = :paymentWallet')
                    ->andWhere('t.status in (:status)')
                    ->setParameters([
                        'user' => $user,
                        'paymentWallet' => Task::PAYMENT_TYPE_WALLET,
                        'status'=> [Task::STATUS_PUBLISHED,Task::STATUS_ASSIGNED,Task::STATUS_WAITING_PAYMENT]
                    ])->getQuery()->getResult();
    }

    public function findOngoingTask($country): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->join('t.acceptedOffer','ao')
            ->join('ao.user','u')
            ->where('u.country = :country')
            ->andWhere('t.status in (:status)')
            ->andWhere('t.maatloobTax > 0')
            ->setParameters([
                'country' => $country,
                'status'=> [Task::STATUS_PUBLISHED,Task::STATUS_ASSIGNED,Task::STATUS_WAITING_PAYMENT]
            ]);
    }
}
