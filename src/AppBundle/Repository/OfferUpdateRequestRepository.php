<?php

namespace AppBundle\Repository;

use AppBundle\Entity\OfferUpdateRequest;

/**
 * OfferUpdateRequestRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OfferUpdateRequestRepository extends CustomEntityRepository
{

    public function findTopFiveDecreasingTasker($country){
        return $this->createQueryBuilder('r')
            ->select('u as user','count(r) as totalRequests','SUM(r.oldPrice - r.newPrice) as totalDecrease')
            ->join('r.offer','o')
            ->join('o.user','u')
            ->where('r.newPrice < r.oldPrice')
            ->andWhere('u.country = :country')
            ->groupBy('u')
            ->setParameter('country',$country)
            ->setMaxResults(5)
            ->orderBy('totalRequests','DESC')
            ->addOrderBy('totalDecrease','DESC')
            ->getQuery()->getResult();

    }

    public function findTopFiveDecreasingPoster($country){
        return $this->createQueryBuilder('r')
            ->select('u as user','count(r) as totalRequests','SUM(r.oldPrice - r.newPrice) as totalDecrease')
            ->join('r.offer','o')
            ->join('o.task','t')
            ->join('t.user','u')
            ->where('r.status = :accepted')
            ->andWhere('u.country = :country')
            ->andWhere('r.newPrice < r.oldPrice')
            ->groupBy('u')
            ->setParameter('accepted',OfferUpdateRequest::STATUS_ACCEPTED)
            ->setParameter('country',$country)
            ->setMaxResults(5)
            ->orderBy('totalRequests','DESC')
            ->addOrderBy('totalDecrease','DESC')
            ->getQuery()->getResult();
    }
}
