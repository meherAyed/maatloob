<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Address;
use AppBundle\Entity\UserBan;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;

class CustomQueryBuilder extends QueryBuilder {

    public function getQuery($deleted=0)
     {
      $this->andWhere($this->getRootAlias().'.isDeleted = :deleted')
                ->setParameter('deleted', $deleted);   
      return parent::getQuery();
     }

    /**
     * Add max distance allowed
     * @param CustomQueryBuilder $query
     * @param array $distance
     */
    public function addDistanceCheck(CustomQueryBuilder $query,array $distance){
        if(isset($distance['latitude'],$distance['longitude'],$distance['max'])){
            $query->join('t.address','adr')
                ->andWhere('111.111 * DEGREES(ACOS(LEAST(COS(RADIANS('.$distance['latitude'].'))  * 
                 COS(RADIANS(adr.latitude)) * COS(RADIANS('.$distance['longitude'].' - adr.longitude)) + 
                 SIN(RADIANS('.$distance['latitude'].')) * SIN(RADIANS(adr.latitude)), 1.0))) <= '.$distance['max']);
        }
    }

    /**
     * @param string $userAlias
     * @return QueryBuilder
     * @throws \Exception
     */
    public function removeBannedUser($userAlias='u'): QueryBuilder
    {
        $ban = $this->getEntityManager()->createQueryBuilder()
            ->from(UserBan::class, 'ub')
            ->select('MAX(ub.date)')
            ->where('ub.user = ' .$userAlias);

        $this->leftJoin($userAlias. '.ban', 'ban', 'with', $this->expr()->eq('ban.date', '(' . $ban->getDQL() . ')'))
            ->andWhere('ban.id is null or ban.banLifted = 1 or (ban.permanent = 0 and ban.unbanDate < :today)')
            ->setParameter('today',new \DateTime());
        return $this;

    }
    /**
     * Sets a collection of query parameters.
     *
     * @param ArrayCollection|array $parameters
     *
     * @return static This query instance.
     */
    public function appendParameters(array $parameters)
    {
        if ($this->getParameters()->count()===0) {
            $this->setParameters($parameters);
        }else{
            foreach ($parameters as $key => $value) {
                $this->setParameter($key,$value);
            }
        }
        return $this;
    }

}
