<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 14/07/2019
 * Time: 21:08
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Task;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class TransactionRepository extends EntityRepository
{

    public function getNetTransaction($user)
    {
        $netOutGoing = $this->createQueryBuilder('t')
            ->select('SUM(t.fromPrice) as netOutGoing')
            ->where('t.fromUser = :user')
            ->andWhere('t.status = :completed')
            ->setParameter('user', $user)
            ->setParameter('completed', Transaction::STATUS_COMPLETED)
            ->getQuery()
            ->getSingleResult();

        $netEarned = $this->createQueryBuilder('t')
            ->select('SUM(t.price) as netEarned')
            ->where('t.toUser = :user')
            ->andWhere('t.status = :completed')
            ->setParameter('user', $user)
            ->setParameter('completed', Transaction::STATUS_COMPLETED)
            ->getQuery()
            ->getSingleResult();
        return array_merge($netOutGoing, $netEarned);
    }

    /**
     * @param $type
     * @param $search
     * @param $orderBy
     * @param $order
     * @return QueryBuilder
     */
    public function findTransactions($search, $type, $orderBy, $order): QueryBuilder
    {
        $query = $this->createQueryBuilder('t')
            ->leftJoin(User::class, 'fromUser', 'with', 'fromUser.id = t.fromUser')
            ->leftJoin(User::class, 'toUser', 'with', 'toUser.id = t.toUser')
            ->leftJoin(Task::class, 'task', 'with', 'task.id = t.task')
            ->orWhere('fromUser.firstName like :search')
            ->orWhere('fromUser.lastName like :search')
            ->orWhere("(CONCAT(fromUser.firstName,' ',fromUser.lastName) LIKE :search )")
            ->orWhere('toUser.firstName like :search')
            ->orWhere('toUser.lastName like :search')
            ->orWhere("(CONCAT(toUser.firstName,' ',toUser.lastName) LIKE :search )")
            ->orWhere('task.title like :search')
            ->setParameter('search', '%' . $search . '%');
        if ($type && $type !== 'all') {
            $query->andWhere('t.type = :type')->setParameter('type', $type);
        }
        switch ($orderBy) {
            case 'from':
                $query->orderBy("CONCAT(fromUser.firstName,' ',fromUser.lastName)", $order);
                break;
            case 'for':
                $query->orderBy("CONCAT(toUser.firstName,' ',toUser.lastName)", $order);
                break;
            case 'price':
                $query->orderBy('t.price', $order);
                break;
            case 'task':
                $query->orderBy('task.title', $order);
                break;
            default:
                $query->orderBy('t.id', $order);
                break;
        }
        return $query;
    }

    public function getTransactionStat($year, $country)
    {
        $firstDateThisYear = $year . '-01-01';
        $lastYearThisDay = (new DateTime())->modify('-1 year');
        $stats['totalTransactions'] = count($this->createQueryBuilder('t')
            ->leftJoin('t.fromUser', 'fromUser')
            ->leftJoin('t.toUser', 'toUser')
            ->andWhere('toUser.country = :country or fromUser.country = :country')
            ->setParameters([
                'country' => $country,
            ])
            ->getQuery()->getResult());
        $stats['lastYearTransactions'] = count($this->createQueryBuilder('t')
            ->leftJoin('t.fromUser', 'fromUser')
            ->leftJoin('t.toUser', 'toUser')
            ->where('toUser.country = :country or fromUser.country = :country')
            ->andWhere('t.createdAt >= :minDate and t.createdAt <= :maxDate')
            ->setParameters([
                'minDate' => $lastYearThisDay->format('Y') . '-01-01',
                'maxDate' => $lastYearThisDay->format('Y-m-d'),
                'country' => $country,
            ])
            ->getQuery()->getResult());
        $stats['newTransactions'] = count($this->createQueryBuilder('t')
            ->leftJoin('t.fromUser', 'fromUser')
            ->leftJoin('t.toUser', 'toUser')
            ->where('toUser.country = :country or fromUser.country = :country')
            ->andWhere('t.createdAt > :date')
            ->setParameters([
                'date' => $firstDateThisYear,
                'country' => $country,
            ])
            ->getQuery()->getResult());
        $stats['transactionEvolution'] = $this->createQueryBuilder('t')
            ->leftJoin('t.fromUser', 'fromUser')
            ->leftJoin('t.toUser', 'toUser')
            ->select('COUNT(t) as totalTransactions , MONTH(t.createdAt) month')
            ->where('toUser.country = :country or fromUser.country = :country')
            ->andWhere('t.createdAt >= :date')
            ->setParameters([
                'date' => $firstDateThisYear,
                'country' => $country,
            ])
            ->groupBy('month')
            ->getQuery()->getResult();
        return $stats;
    }

    public function getRevenueStat($year, $country)
    {
        $firstDateThisYear = $year . '-01-01';
        $lastYearThisDay = (new DateTime())->modify('-1 year');
        $stats['totalRevenue'] = (float)$this->createQueryBuilder('t')
            ->select('SUM(t.price) as revenue')
            ->where('t.type in (:type)')
            ->join('t.fromUser', 'user')
            ->andWhere('user.country = :country')
            ->setParameters([
                'type' => [Transaction::TYPE_FEES,Transaction::TYPE_DEAL_CANCELLED],
                'country' => $country
            ])
            ->getQuery()->getSingleResult()['revenue'];
        $stats['lastYearRevenue'] = (float)$this->createQueryBuilder('t')
            ->select('SUM(t.price) as revenue')
            ->where('t.type in (:type)')
            ->join('t.fromUser', 'user')
            ->andWhere('user.country = :country')
            ->andWhere('t.createdAt >= :minDate and t.createdAt <= :maxDate')
            ->setParameters([
                'country' => $country,
                'type' => [Transaction::TYPE_FEES,Transaction::TYPE_DEAL_CANCELLED],
                'minDate' => $lastYearThisDay->format('Y') . '-01-01',
                'maxDate' => $lastYearThisDay->format('Y-m-d')
            ])
            ->getQuery()->getSingleResult()['revenue'];
        $stats['newRevenue'] = (float)$this->createQueryBuilder('t')
            ->select('SUM(t.price) as revenue')
            ->where('t.type in (:type)')
            ->join('t.fromUser', 'user')
            ->andWhere('user.country = :country')
            ->andWhere('t.createdAt > :date')
            ->setParameters([
                'type' => [Transaction::TYPE_FEES,Transaction::TYPE_DEAL_CANCELLED],
                'date' => $firstDateThisYear,
                'country' => $country
            ])
            ->getQuery()->getSingleResult()['revenue'];
        $stats['revenueEvolution'] = $this->createQueryBuilder('t')
            ->select('SUM(t.price) as revenue , MONTH(t.createdAt) month')
            ->where('t.type in (:type)')
            ->join('t.fromUser', 'user')
            ->andWhere('user.country = :country')
            ->andWhere('t.createdAt >= :date')
            ->setParameters([
                'type' => [Transaction::TYPE_FEES,Transaction::TYPE_DEAL_CANCELLED],
                'date' => $firstDateThisYear,
                'country' => $country])
            ->groupBy('month')
            ->getQuery()->getResult();
        return $stats;
    }

    public function getProfitsStats($country='en')
    {
        $stats['year'] = $this->createQueryBuilder('t')
            ->select('MONTH(t.createdAt) as month', 'SUM(t.price) as totalPrice',
                'SUM(case when t.type = :tax then t.price else 0 end) as tax')
            ->join('t.fromUser','fromUser')
            ->where('t.type in (:types)')
            ->andWhere('t.createdAt between :startDate and :endDate')
            ->andWhere('fromUser.country = :country')
            ->setParameters(['types' => [
                Transaction::TYPE_COUNTRY_TAX,
                Transaction::TYPE_FEES,
                Transaction::TYPE_DEAL_CANCELLED],
                'tax' => Transaction::TYPE_COUNTRY_TAX,
                'startDate' => new DateTime('first day of january'),
                'endDate' => new DateTime(),
                'country' => $country
            ])
            ->groupBy('month')
            ->getQuery()->getResult();
        $stats['month'] = $this->createQueryBuilder('t')
            ->select('DAY(t.createdAt) as day', 'SUM(t.price) as totalPrice',
                'SUM(case when t.type = :tax then t.price else 0 end) as tax')
            ->join('t.fromUser','fromUser')
            ->where('t.type in (:types)')
            ->andWhere('t.createdAt between :startDate and :endDate')
            ->andWhere('fromUser.country = :country')
            ->setParameters(['types' => [
                Transaction::TYPE_COUNTRY_TAX,
                Transaction::TYPE_FEES,
                Transaction::TYPE_DEAL_CANCELLED],
                'tax' => Transaction::TYPE_COUNTRY_TAX,
                'startDate' => new DateTime('first day of this month'),
                'endDate' => new DateTime(),
                'country' => $country
            ])
            ->groupBy('day')
            ->getQuery()->getResult();
        $stats['week'] = $this->createQueryBuilder('t')
            ->select('WEEKDAY(t.createdAt) as day', 'SUM(t.price) as totalPrice',
                'SUM(case when t.type = :tax then t.price else 0 end) as tax')
            ->join('t.fromUser','fromUser')
            ->where('t.type in (:types)')
            ->andWhere('t.createdAt between :startDate and :endDate')
            ->andWhere('fromUser.country = :country')
            ->setParameters(['types' => [
                Transaction::TYPE_COUNTRY_TAX,
                Transaction::TYPE_FEES,
                Transaction::TYPE_DEAL_CANCELLED],
                'tax' => Transaction::TYPE_COUNTRY_TAX,
                'startDate' => new DateTime('monday this week'),
                'endDate' => new DateTime(),
                'country' => $country
            ])
            ->groupBy('day')
            ->getQuery()->getResult();
        $stats['day'] = $this->createQueryBuilder('t')
            ->select('HOUR(t.createdAt) as hour', 'SUM(t.price) as totalPrice',
                'SUM(case when t.type = :tax then t.price else 0 end) as tax')
            ->join('t.fromUser','fromUser')
            ->where('t.type in (:types)')
            ->andWhere('t.createdAt between :startDate and :endDate')
            ->andWhere('fromUser.country = :country')
            ->setParameters(['types' => [
                Transaction::TYPE_COUNTRY_TAX,
                Transaction::TYPE_FEES,
                Transaction::TYPE_DEAL_CANCELLED],
                'tax' => Transaction::TYPE_COUNTRY_TAX,
                'startDate' => (new DateTime())->format('Y-m-d'),
                'endDate' => new DateTime(),
                'country' => $country
            ])
            ->groupBy('hour')
            ->getQuery()->getResult();

        return $stats;
    }

    /**
     * Return all user Incoming/Outgoing transactions
     * @param User $user
     * @param $orderBy
     * @param $order
     * @param null $type
     * @return QueryBuilder
     */
    public function getUserTransactions(User $user, $orderBy, $order, $type=null): QueryBuilder
    {

        $query = $this->createQueryBuilder('t')
            ->leftJoin('t.toUser','toUser')
            ->leftJoin('t.fromUser','fromUser')
            ->where('t.fromUser = :user or t.toUser = :user')
            ->andWhere('t.status = 2')
            ->orderBy('t.createdAt','desc')
            ->setParameter('user',$user);
        if ($type) {
            $query->andWhere('t.type = :type')->setParameter('type', $type);
        }

        switch ($orderBy) {
            case 'from':
                $query->orderBy("CONCAT(fromUser.firstName,' ',fromUser.lastName)", $order);
                break;
            case 'for':
                $query->orderBy("CONCAT(toUser.firstName,' ',toUser.lastName)", $order);
                break;
            case 'price':
                $query->orderBy('t.price', $order);
                break;
            case 'type':
                $query->orderBy('t.type', $order);
                break;
            default:
                $query->orderBy('t.id', $order);
                break;
        }
        return $query;
    }

    /**
     * @param $user
     * @return array
     */
    public function findLockedBalance($user): array
    {
        return $this->createQueryBuilder('t')
                    ->where('t.fromUser = :user')
                    ->andWhere('t.type = :withdraw')
                    ->andWhere('t.status in (:waiting)')
                    ->setParameters([
                        'user' => $user,
                        'withdraw' => Transaction::TYPE_WITHDRAW,
                        'waiting' => [Transaction::STATUS_PENDING,Transaction::STATUS_PROCESSING],
                    ])->getQuery()->getResult();
    }

    public function findLockedInvoiceBalance($user){
        return $this->createQueryBuilder('t')
            ->where('t.fromUser = :user')
            ->andWhere('t.type = :invoice')
            ->andWhere('t.status = :status')
            ->setParameters([
                'user' => $user,
                'invoice' => Transaction::TYPE_TASK_INVOICE,
                'status' => Transaction::STATUS_CAPTURED,
            ])->getQuery()->getResult();
    }
}