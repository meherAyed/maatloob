<?php

namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class CustomEntityRepository extends EntityRepository
{
    /** @var $query QueryBuilder */
    protected  $query;

    /**
     * @param string $alias
     * @param null $indexBy
     * @return CustomQueryBuilder|\Doctrine\ORM\QueryBuilder
     */
    public function createQueryBuilder($alias, $indexBy = null)
    {
        $this->query = new CustomQueryBuilder($this->_em);
        $this->query->select($alias)
            ->from($this->_entityName, $alias, $indexBy);

        return $this->query;
    }
}
