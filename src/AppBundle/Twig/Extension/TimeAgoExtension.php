<?php

namespace AppBundle\Twig\Extension;

use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToTimestampTransformer;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\TwigFilter;

class TimeAgoExtension extends \Twig_Extension
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;
    /**
     * Constructor method
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('distance_of_time_in_words', [$this, 'distanceOfTimeInWordsFilter']),
            new TwigFilter('time_ago_in_words', [$this, 'timeAgoInWordsFilter']),
        ];
    }
    /**
     * Like distance_of_time_in_words, but where to_time is fixed to timestamp()
     *
     * @param $from_time String or DateTime
     * @param bool $include_seconds
     * @param bool $include_months
     *
     * @return mixed
     */
    public function timeAgoInWordsFilter($from_time, $include_seconds = false, $include_months = false)
    {
        return $this->distanceOfTimeInWordsFilter($from_time, new \DateTime('now'), $include_seconds, $include_months);
    }
    /**
     * Reports the approximate distance in time between two times given in seconds
     * or in a valid ISO string like.
     * For example, if the distance is 47 minutes, it'll return
     * "about 1 hour". See the source for the complete wording list.
     *
     * Integers are interpreted as seconds. So, by example to check the distance of time between
     * a created user and their last login:
     * {{ user.createdAt|distance_of_time_in_words(user.lastLoginAt) }} returns "less than a minute".
     *
     * Set include_seconds to true if you want more detailed approximations if distance < 1 minute
     * Set include_months to true if you want approximations in months if days > 30
     *
     * @param string|\DateTimeInterface $from_time
     * @param string|\DateTimeInterface $to_time
     * @param bool             $include_seconds True to return distance in seconds when it's lower than a minute.
     * @param bool             $include_months
     *
     * @return string
     */
    public function distanceOfTimeInWordsFilter(
        $from_time,
        $to_time = null,
        $include_seconds = false,
        $include_months = false
    ) {
        $datetime_transformer = new DateTimeToStringTransformer(null, null, 'Y-m-d H:i:s');
        $timestamp_transformer = new DateTimeToTimestampTransformer();
        // Transform “from” to timestamp
        if ($from_time instanceof \DateTimeInterface) {
            $from_time = $timestamp_transformer->transform($from_time);
        } elseif (!is_numeric($from_time)) {
            $from_time = $datetime_transformer->reverseTransform($from_time);
            $from_time = $timestamp_transformer->transform($from_time);
        }
        $to_time = empty($to_time) ? new \DateTime('now') : $to_time;
        // Transform “to” to timestamp
        if ($to_time instanceof \DateTimeInterface) {
            $to_time = $timestamp_transformer->transform($to_time);
        } elseif (!is_numeric($to_time)) {
            $to_time = $datetime_transformer->reverseTransform($to_time);
            $to_time = $timestamp_transformer->transform($to_time);
        }
        $distance_in_minutes = round((abs($to_time - $from_time))/60);
        $distance_in_seconds = round(abs($to_time - $from_time));
        if ($distance_in_minutes <= 1) {
            if ($include_seconds && $distance_in_seconds < 20) {
                return $this->translator->trans('date.less_then_second_ago', ['%second%' => 20]);
            }
            return $this->translator->trans('date.less_then_minute_ago');

        }
        if ($distance_in_minutes <= 45) {
            return $this->translator->trans('date.minutes_ago', ['%minutes%' => $distance_in_minutes]);
        }
        if ($distance_in_minutes <= 90) {
            return $this->translator->trans('date.about_one_hour');
        }
        if ($distance_in_minutes <= 1440) {
            return $this->translator->trans(
                ($distance_in_minutes/60)>1 && ($distance_in_minutes/60)<11?'date.hours_ago':'date.hours_ago_ar',
                ['%hours%' => round($distance_in_minutes/60)]
            );
        }
        if ($distance_in_minutes <= 2880) {
            return $this->translator->trans('date.one_day_ago');
        }
        $distance_in_days = round($distance_in_minutes/1440);
        if (!$include_months || $distance_in_days <= 30) {
            return $this->translator->trans(
                $distance_in_days>1 && $distance_in_days<11?'date.days_ago':'date.days_ago_ar',
                ['%days%' => round($distance_in_days)]);
        }
        if ($distance_in_days < 345) {
            return $this->translator->trans(
                ($distance_in_days/30)>1 && ($distance_in_minutes/60)<11?'date.month_ago':'date.month_ago_ar',
                ['%months%' => round($distance_in_days/30),'%s%'=> round($distance_in_days/30)>1?'s':'']
            );
        }
        return (new \DateTime($from_time))->format('d-m-Y H:i');
    }
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'time_ago_extension';
    }
}