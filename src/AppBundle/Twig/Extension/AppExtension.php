<?php

namespace AppBundle\Twig\Extension;

use ApiBundle\Services\DBTranslator;
use AppBundle\Entity\Country;
use AppBundle\Entity\DocumentTypeTranslation;
use AppBundle\Entity\Language;
use AppBundle\Entity\LanguageMessage;
use AppBundle\Entity\Notification;
use AppBundle\Entity\ReportType;
use AppBundle\Entity\ReportTypeTranslation;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskCategory;
use AppBundle\Entity\TaskCategoryTranslation;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBan;
use Components\Helper;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\DocumentType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class AppExtension
 * @package AppBundle\Twig\Extension
 */
class AppExtension extends AbstractExtension
{
    /** @var EntityManagerInterface */
    private $em;
    private $tokenStorage;
    private $translator;
    private $dbTranslator;
    private $session;
    private $generator;

    /**
     * AppExtension constructor.
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface $tokenStorage
     * @param TranslatorInterface $translator
     * @param DBTranslator $dbTranslator
     * @param SessionInterface $session
     * @param UrlGeneratorInterface $generator
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        TranslatorInterface $translator,
        DBTranslator $dbTranslator,
        SessionInterface $session,
        UrlGeneratorInterface $generator
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
        $this->translator = $translator;
        $this->dbTranslator = $dbTranslator;
        $this->session = $session;
        $this->generator = $generator;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('notifications', [$this, 'getNotifications']),
            new TwigFunction('newNotifications', [$this, 'getNewNotifications']),
            new TwigFunction('translationsCompletionRate', [$this, 'translationsCompletionRate']),
            new TwigFunction('supportedCountries', [$this, 'getSupportedCountries']),
            new TwigFunction('currentRegion', [$this, 'currentRegion']),
            new TwigFunction('menuStats', [$this, 'getMenuStats']),
            new TwigFunction('updateUrl', [$this, 'updateUrl']),
            new TwigFunction('instanceOfTask', [$this, 'instanceOfTask']),
            new TwigFunction('numberGenerator', static function ($length, $format) {
                return Helper::generateStrongPassword($length, false, $format = 'd');
            }),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('file_size_format', [$this, 'fileSizeFormatter']),
            new TwigFilter('slugify', [$this, 'slugify']),
            new TwigFilter('transDb', [$this, 'databaseTranslationFilter']),
            new TwigFilter('json_decode', [$this, 'jsonDecode'])

        ];
    }

    /**
     * @param $object
     * @return bool
     */
    public function instanceOfTask($object): bool
    {
        return  $object instanceof Task;
    }

    public function jsonDecode($str)
    {
        return json_decode($str);
    }

    /**
     * @return Notification[]
     */
    public function getNotifications()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $notifications = $this->em->getRepository(Notification::class)
            ->findBy(['toUser' => $user], ['createdAt' => 'DESC'], 10);
        foreach ($notifications as $notification) {
            $notification->setTitle($this->translator->trans($notification->getTitle()));
            $notification->setBody($this->translator->trans($notification->getBody(), $notification->getParams()));
        }
        return $notifications;
    }


    /**
     * @return Notification[]
     */
    public function getNewNotifications()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $notifications = $this->em->getRepository(Notification::class)
            ->findBy(['toUser' => $user, 'shownOn' => false]);
        return $notifications;
    }

    /**
     * @param $bytes
     * @return string
     */
    public function fileSizeFormatter($bytes)
    {
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        $terabyte = $gigabyte * 1024;

        if ($bytes < $kilobyte)
            $size = $bytes . ' B';
        elseif ($bytes < $megabyte)
            $size = number_format(($bytes / $kilobyte), 0, '.', ' ') . ' KiB';
        elseif ($bytes < $gigabyte)
            $size = number_format(($bytes / $megabyte), 0, '.', ' ') . ' MiB';
        elseif ($bytes < $terabyte)
            $size = number_format(($bytes / $gigabyte), 0, '.', ' ') . ' GiB';
        else
            $size = number_format(($bytes / $terabyte), 0, '.', ' ') . ' TiB';
        return $size;
    }

    public function slugify($string)
    {
        if (!trim($string))
            return "user";

        return preg_replace(
            '/[^a-z0-9]/',
            '_',
            strtolower(trim(strip_tags($string)))
        );
    }

    public function translationsCompletionRate(Language $language)
    {
        /** All entities that should be translated  */
        $reports = $this->em->getRepository(ReportType::class)->findAll();
        $translatedReports = $this->em->createQueryBuilder()
            ->select('rtt')
            ->from(ReportTypeTranslation::class, 'rtt')
            ->where('rtt.locale = :language')
            ->andWhere("rtt.name is not null and rtt.name <> ''")
            ->setParameter('language', $language->getIdentifier())
            ->getQuery()->getResult();
        $translatedMessages = 0;
        /** @var LanguageMessage $message */
        foreach ($language->getMessages() as $message) {
            if ($message->getMessage() != "" && $message->getMessage() != null) {
                $translatedMessages++;
            }
        }
        $translatedMessages +=  count($translatedReports);
        $totalMessages = count($reports)  + $language->getMessages()->count();
        return round(($translatedMessages / $totalMessages) * 100, 2);
    }

    public function getSupportedCountries()
    {
        return $this->em->getRepository(Country::class)->findAll();
    }

    public function currentRegion()
    {
        $region = $this->session->get('region', Country::Default);
        return $this->em->getRepository(Country::class)->find($region);
    }

    /**
     * Database translator filter
     * @param $messageKey
     * @param array $params
     * @param string $locale
     * @return string
     */
    public function databaseTranslationFilter($messageKey, $params = [], $locale = null): string
    {
        if(!$locale){
            $locale = $this->translator->getLocale();
        }
        return $this->dbTranslator->transDb($messageKey, $params,$locale);
    }

    public function getMenuStats(): array
    {
        /** Current browsing country */
        $country = $this->session->get('region');
        $totalCancelRequest = $this->em->getRepository(Task::class)->createQueryBuilder('t')
            ->join('t.cancelRequest', 'cr', 'with', 'cr.closed = 0')
            ->join('t.user', 'user')
            ->where('user.country = :country')
            ->setParameter('country', $country)->getQuery()->getResult();

        return [
            'usersCount' => $this->userCounts(),
            'cancelRequest' => count($totalCancelRequest)
        ];
    }

    public function userCounts(): array
    {
        /** Current browsing country */
        $country = $this->session->get('region');
        $activeUsers = count($this->em->getRepository(User::class)->getActiveUsersByCountry($country)->getQuery()->getResult());
        $inactivatedAccount = count($this->em->getRepository(User::class)
            ->getInactiveUsersByCountry($country)->getQuery()->getResult());
        $bannedAccount = count($this->em->getRepository(User::class)->getBannedUsers($country)->getQuery()->getResult());
        return [
            'active' => $activeUsers,
            'inactive' => $inactivatedAccount,
            'banned' => $bannedAccount
        ];
    }


    public function updateUrl(Request $request, $params)
    {
        $route = $request->attributes->get('_route');
        $urlPrams = $request->attributes->get('_route_params');
        $queryParams = $request->query->all();
        $params = array_merge($urlPrams, $queryParams, $params);
        /** remove empty params */
        $params = array_filter($params, static function ($elem){
            return $elem!=='';
        });
        return $this->generator->generate($route,$params);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app';
    }
}
