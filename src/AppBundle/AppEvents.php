<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 03/08/2019
 * Time: 12:09
 */

namespace AppBundle;


final class AppEvents
{
    const REGISTRATION_SUCCESS = "app.registration.success";
}