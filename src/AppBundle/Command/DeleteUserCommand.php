<?php

namespace AppBundle\Command;

use AppBundle\Entity\Notification;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBan;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteUserCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:delete_user')
            ->setDescription('Hello PhpStorm')
            ->addArgument('p', InputArgument::REQUIRED, 'Please enter a password')
            ->addArgument('email', InputArgument::REQUIRED, 'Please enter user email');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        if ((int)$input->getArgument('p') === 123456789) {
            /** @var User $user */
            $user = $em->getRepository(User::class)->findOneBy(['email' => $input->getArgument('email')]);
            if ($user) {
                try {
                    foreach ($user->getDocuments() as $document) {
                        $em->remove($document);
                    }
                    $em->flush();
                    foreach ($user->getNotificationConfig() as $not) {
                        $em->remove($not);
                    }
                    $em->flush();
                    foreach ($user->getBadges() as $badge) {
                        $em->remove($badge);
                    }
                    $em->flush();
                    $em->remove($user);
                    $em->flush();
                } catch (\Exception $e) {
                    $output->writeln('Error : ' . $e->getMessage());
                }

                $output->writeln(' user has been deleted');
            }
        }
    }
}
