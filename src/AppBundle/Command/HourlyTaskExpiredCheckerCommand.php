<?php

namespace AppBundle\Command;

use AppBundle\Entity\Country;
use AppBundle\Entity\Notification;
use AppBundle\Entity\Task;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBan;
use AppBundle\Entity\UserReview;
use Components\Helper;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HourlyTaskExpiredCheckerCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:hourly_task_checker')
            ->setDescription('Check published taskExpired');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $countries = $em->getRepository(Country::class)->findAll();
        /** @var Country $country */
        foreach ($countries as $country){
            Helper::setCountryTimeZone($country->getIdentifier());
            $tasks = $em->getRepository(Task::class)->createQueryBuilder('t')
                ->join('t.user','u')
                ->where('u.country = :country')
                ->andWhere('t.date <= :dateNow')
                ->andWhere('t.status = :published')
                ->setParameters([
                    'country' => $country->getIdentifier(),
                    'dateNow' => (new \DateTime())->format('Y-m-d H:i'),
                    'published' => Task::STATUS_PUBLISHED
                ])->getQuery()->getResult();
            /** @var Task $task */
            foreach ($tasks as $task){
                $task->setStatus(Task::STATUS_DRAFT);
                $task->setNotPublishedReason(Task::NOT_PUBLISHED_EXPIRED);
                /** unlock task price if payment from wallet */
                if($task->getPaymentType() === Task::PAYMENT_TYPE_WALLET){
                    $userWallet = $task->getUser()->getWallet();
                    /** update task locked price */
                    $userWallet->unlockCredit($task->getUsedCredit());
                    $userWallet->unlockBalance($task->getPrice() - $task->getUsedCredit());
                    $task->setUsedCredit(0);
                    $em->persist($userWallet);
                }
                $em->persist($task);
                $notification = new Notification();
                $notification->setType(Notification::ACTION_TASK_UPDATE);
                $notification->setToUser($task->getUser());
                $notification->setTitle('notification.task_expired_draft.title');
                $notification->setBody('notification.task_expired_draft.body');
                $notification->setParams([
                    '#task#' => $task->getTitle()
                ]);
                $notification->setExtra([
                    'type' => Notification::TYPE_TASK,
                    'task_id' => $task->getId()
                ]);
                $em->persist($notification);
            }
            $em->flush();
            $output->writeln($country->getName().' : '.count($tasks).' tasks expired !');
        }
        $output->writeln('Hourly task expiration checker done');
    }
}
