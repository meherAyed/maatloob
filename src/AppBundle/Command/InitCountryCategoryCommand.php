<?php

namespace AppBundle\Command;

use AppBundle\Entity\Country;
use AppBundle\Entity\CountryCategory;
use AppBundle\Entity\TaskCategory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitCountryCategoryCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:init_category')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $categories = $em->getRepository(TaskCategory::class)->findAll();
        $countries = $em->getRepository(Country::class)->findAll();
        foreach ($categories as $category){
            /** @var Country $country */
            foreach ($countries as $country){
                $countryCategory = $em->getRepository(CountryCategory::class)->findOneBy(['country'=>$country,'category'=>$category]);
                if(!$countryCategory){
                    $countryCategory = new CountryCategory();
                    $countryCategory->setCountry($country);
                    $countryCategory->setCategory($category);
                    $countryCategory->setPosition(count($country->getCategories())+1);
                    $em->persist($countryCategory);
                }
            }
        }
        $em->flush();
        $output->writeln('all countries are initialized with categories');
    }
}
