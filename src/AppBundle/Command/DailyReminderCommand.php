<?php

namespace AppBundle\Command;

use AppBundle\Entity\Notification;
use AppBundle\Entity\Task;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBan;
use AppBundle\Entity\UserReview;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DailyReminderCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:daily_reminder')
            ->setDescription('Remind poster to pay to tasker & reviews');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->remindPosterToPayToTasker($output);
        $this->remindTaskReview($output);
        $output->writeln('Daily reminder done !');
    }

    public function remindPosterToPayToTasker(OutputInterface $output){
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $tasks = $em->getRepository(Task::class)->findAwaitingPaymentTasks()->getQuery()->getResult();
        /** @var Task $task */
        foreach ($tasks as $task){
            $notification = new Notification();
            $notification->setTitle('notification.poster_pay_reminder.title');
            $notification->setBody('notification.poster_pay_reminder.body');
            $notification->setType(Notification::ACTION_TASK_REMINDER);
            $notification->setToUser($task->getUser());
            $notification->setParams([
                '#task#' => $task->getTitle(),
                '#price#' => $task->getPrice().$task->getUser()->getCountry()->getCurrencySymbol(),
                '#user#' => $task->getTasker()->getFullName()
            ]);
            $notification->setExtra([
                'type' => Notification::TYPE_TASK,
                'task_id' => $task->getId()
            ]);
            $em->persist($notification);
            $output->writeln('User reminded to pay is : '.$task->getUser()->getFullName());
        }
        $em->flush();
        $output->writeln('Posters are reminded to pay for tasker');
    }

    private function remindTaskReview(OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $trans = $this->getContainer()->get('app.database_translator');
        $tasks = $em->getRepository(Task::class)->findBy(['status'=>Task::STATUS_PAID]);
        /** @var Task $task */
        foreach ($tasks as $task){
            $date = (clone $task->getUpdatedAt())->modify('+1 day');
            $closeDate = (clone $task->getUpdatedAt())->modify('+15 day');
            $dateNow = new \DateTime();
            if($date < $dateNow && $closeDate > $dateNow ){
                $review = $em->getRepository(UserReview::class)->findOneBy(['task'=>$task]);
                if($review){
                    $users = [$task->getUser()->getId() !== $review->getUser()->getId()?$task->getTasker():$task->getUser()];
                }else{
                    $users = [$task->getUser(),$task->getTasker()];
                }
                /** @var User $user */
                foreach ($users as $user){
                    $notification = new Notification();
                    $notification->setToUser($user);
                    $notification->setType(Notification::ACTION_TASK_REMINDER);
                    $notification->setTitle('notification.review_reminder.title');
                    $notification->setBody('notification.review_reminder.body');
                    $notification->setParams([
                        '#task#' => $task->getTitle(),
                        '#user#' => $task->getUser()->getId() === $user->getId()?$task->getTasker()->getFullName():$task->getUser()->getFullName(),
                        '#time#' => $closeDate->diff($dateNow)->d .' '. $trans->transDb('unit_day',[],$user->getLanguage())
                    ]);
                    $notification->setExtra([
                        'task_id' => $task->getId(),
                        'type' => Notification::TYPE_TASK
                    ]);
                    $em->persist($notification);
                    $output->writeln('User reminded to review is : '.$user->getFullName());
                }
            }elseif ($closeDate<$dateNow){
                $task->setStatus(Task::STATUS_COMPLETED);
                $em->persist($task);
            }
        }
        $em->flush();
        $output->writeln('Users has been reminded to review tasks');
    }
}
