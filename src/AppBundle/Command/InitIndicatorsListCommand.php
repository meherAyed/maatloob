<?php

namespace AppBundle\Command;

use AppBundle\Entity\Indicator;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitIndicatorsListCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:init_indicators')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $admins = $em->getRepository(User::class)->findAdminsByRoles([User::ROLE_ADMIN,User::ROLE_DASHBOARD_USER,
            User::ROLE_SUPER_ADMIN,User::ROLE_ACCOUNTANT, User::ROLE_SUPERVISOR]);
        foreach ($admins as $admin){
            foreach (Indicator::getSupportedLists() as $list){
                $indicator = $em->getRepository(Indicator::class)->findOneBy(['list'=>$list,'admin'=>$admin]);
                if(!$indicator){
                    $indicator = new Indicator();
                    $indicator->setAdmin($admin);
                    $indicator->setList($list);
                    $indicator->setItems([]);
                    $em->persist($indicator);
                }
            }
        }

        $em->flush();

        $output->writeln('Operation done all indicators are initialized');
    }
}
