<?php

namespace AppBundle\Command;

use AppBundle\Entity\Badge;
use AppBundle\Entity\Indicator;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBadge;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitBadgesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:init_badges')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $badges = [Badge::Facebook,Badge::Google,Badge::LinkedIn,Badge::Mobile,Badge::Twitter,Badge::Apple];
        $users = $em->getRepository(User::class)->findBy(['enabled' => true]);
        foreach ($badges as $badgeName){
            $badge = $em->getRepository(Badge::class)->findOneBy(['name'=>$badgeName]);
            if(!$badge){
                $badge = new Badge($badgeName);
                $badge->setIdentification(false);
                $em->persist($badge);
                $em->flush();
                /** init badge for all users */
                foreach ($users as $user){
                    $userBadge = new UserBadge();
                    $userBadge->setUser($user);
                    $userBadge->setBadge($badge);
                    $em->persist($userBadge);
                }
            }
        }
        $em->flush();
        $output->writeln('Operation done all badges are initialized');
    }
}
