<?php

namespace AppBundle\Command;

use AppBundle\Entity\Notification;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBan;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DailyWalletCheckerCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:daily_wallet_checker_command')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $users = $em->getRepository(User::class)->createQueryBuilder('u')
                            ->join('u.wallet','w')
                            ->join('u.country','c')
                            ->join('c.config','cc')
                            ->leftJoin('u.ban','b','with','b.permanent = 1 and b.banLifted = 0')
                            ->where('b.id is NULL')
                            ->andWhere('w.balance+w.lockedBalance<cc.minAllowedMaatloobBalance')
                            ->getQuery()->getResult();

        $appService = $this->getContainer()->get('app_service');
        $translator = $this->getContainer()->get('app.database_translator');
        /** @var User $user */
        foreach ($users as $user){
            /** Check if user has been already warned */
            if($user->isWarnedForBan()){
                $userBan = new UserBan();
                $userBan->setUser($user);
                $userBan->setBanReason($translator->transDb('ban.reason.negative_wallet',
                    ['#amount#'=> $user->getCountry()->getConfig()->getMinAllowedMaatloobBalance().$user->getCountry()->getCurrencySymbol()],
                    $user->getLanguage()));
                $userBan->setPermanent(true);
                $userBan->setUnbanPenalty($user->getWallet()->getTotalBalance()*-1);
                $appService->sendBanNotification($userBan);
                $em->persist($userBan);
                $em->flush();
            }else{
                $user->setWarnedForBan(true);
                $notification = new Notification();
                $notification->setTitle('notification.warn_negative_wallet_balance.title');
                $notification->setBody('notification.warn_negative_wallet_balance.body');
                $notification->setParams(['#price#'=>($user->getWallet()->getBalance()*-1).$user->getCountry()->getCurrencySymbol()]);
                $notification->setToUser($user);
                $notification->setType(Notification::ACTION_HELP_INFO);
                $notification->setExtra(['type'=>Notification::TYPE_FILL_WALLET]);
                $em->persist($notification);
                $em->persist($user);
            }
        }
        $em->flush();
        $output->writeln(count($users).' users has been checked');
    }
}
