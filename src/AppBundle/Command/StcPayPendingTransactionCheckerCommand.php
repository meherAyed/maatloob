<?php


namespace AppBundle\Command;


use AppBundle\Entity\Transaction;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StcPayPendingTransactionCheckerCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('app:stcpay_pending_transaction_checker')
            ->setDescription('Check if there is an stcpay payout informed but not completed');
    }


    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager.abstract');
        $transactions = $em->getRepository(Transaction::class)->createQueryBuilder('t')
                            ->where('t.type = :type')
                            ->andWhere('t.status = :status')
                            ->andWhere('t.extras like :method')
                            ->setParameters([
                                'type'=>Transaction::TYPE_WITHDRAW,
                                'status' => Transaction::STATUS_PROCESSING,
                                'method' => '%'.Transaction::WITHDRAW_TYPE_STC_PAY.'%'
                            ]);
        $payoutService = $this->getContainer()->get('payout');
        /** @var Transaction $transaction */
        foreach ($transactions as $transaction){
            $user = $transaction->getFromUser();
            try{
                $result = $payoutService->getStcPayStatus($transaction->getExtras()['paymentReference'],$transaction->getCheckoutId());
                if((int)$result['Payments'][0]['Status'] === 5){
                    $transaction->setStatus(Transaction::STATUS_COMPLETED);
                    $transaction->setPaymentInformation(json_encode($result));
                    $user->getWallet()->unlockBalance($transaction->getPrice());
                    $user->getWallet()->withdraw($transaction->getPrice());
                    $em->persist($user->getWallet());
                    $em->persist($transaction);
                    $em->flush();
                    return 'payment.withdraw.created';
                }elseif ((int)$result['Payments'][0]['Status']=== 9){
                    $transaction->setStatus(Transaction::STATUS_FAILED);
                    $user->getWallet()->unlockBalance($transaction->getPrice());
                    $em->persist($user->getWallet());
                    $em->persist($transaction);
                    $em->flush();
                }elseif(new \DateTime() > (new \DateTime($transaction->getExtras()['createdAt']))->modify('+1 hour')){
                    if($payoutService->cancelStcTransaction($transaction->getExtras()['paymentReference'],$transaction->getCheckoutId())){
                        $transaction->setStatus(Transaction::STATUS_CANCELLED);
                        $user->getWallet()->unlockBalance($transaction->getPrice());
                        $em->persist($user->getWallet());
                        $em->persist($transaction);
                        $em->flush();
                    }
                }
            }catch (\Exception $e){
                $this->getContainer()->get('logger')->addError('failed check stc pay payout transaction : '.$transaction->getId());
            }

        }
        $output->writeln('System language has been initialized');
    }
}
