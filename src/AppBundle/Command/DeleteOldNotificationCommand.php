<?php

namespace AppBundle\Command;

use AppBundle\Entity\Notification;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteOldNotificationCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:delete_old_notification_command')
            ->setDescription('Delete notification that has more than 1 month');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $em->getRepository(Notification::class)
                            ->createQueryBuilder('n')
                            ->delete()
                            ->where('n.createdAt < :lastMonth')
                            ->setParameter('lastMonth',(new \DateTime())->modify('-1 month'))
                            ->getQuery()->execute();
        $output->writeln('old notification successfully deleted');
    }
}
