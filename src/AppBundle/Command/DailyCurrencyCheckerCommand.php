<?php

namespace AppBundle\Command;

use AppBundle\Entity\Country;
use AppBundle\Entity\CountryExchangeCurrency;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DailyCurrencyCheckerCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:daily_currency_checker')
            ->setDescription('Check currency rate conversion');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $currencyConverter = $this->getContainer()->get('app.currency.converter');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $countries = $em->getRepository(Country::class)->findAll();
        /** @var Country $country */
        foreach ($countries as $country){
            if(count($countries) > count($country->getCurrencyExchange()) ){
                $this->initCountryCurrenciesConvert($country,$countries);
            }
            foreach ($country->getCurrencyExchange() as $toCountry){
                /** @var CountryExchangeCurrency $toCountry */
                if($country->getCurrencyCode()!== $toCountry->getToCurrency()){
                    $rate = $currencyConverter->convert($country->getCurrencyCode(),$toCountry->getToCurrency());
                    if($rate){
                        $toCountry->setRate($rate??1);
                        $em->persist($toCountry);
                        $output->writeln('From '.$country->getCurrencyCode().' To '.$country->getCurrencyCode().' : '.$rate);
                    }
                }
            }
        }
        $em->flush();
        $output->writeln('Daily currency checker done !');
    }

    /**
     * @param Country $country
     * @param $countries
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function initCountryCurrenciesConvert(Country $country,$countries): void
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var Country $toCountry */
        foreach ($countries as $toCountry){
            $currencyConversion = $country->getCurrencyExchange($toCountry->getCurrencyCode());
            if(!$currencyConversion){
                $currencyConversion = new CountryExchangeCurrency();
                $currencyConversion->setCountry($country);
                $currencyConversion->setRate(1);
                $currencyConversion->setToCurrency($toCountry->getCurrencyCode());
                $em->persist($currencyConversion);
            }
        }
        $em->flush();
    }
}
