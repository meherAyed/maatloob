<?php

namespace AppBundle\Command;

use AppBundle\Entity\CancelTaskRequest;
use AppBundle\Entity\Country;
use AppBundle\Entity\CountryExchangeCurrency;
use Components\Helper;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DailyCancelTaskRequestCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('app:daily_cancel_task_request')
            ->setDescription('Cancel ignored cancel task request');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $cancelTaskRequests = $em->getRepository(CancelTaskRequest::class)->findBy(['status'=>CancelTaskRequest::STATUS_WAITING]);
        /** @var CancelTaskRequest $cancelTaskRequest */
        foreach ($cancelTaskRequests as $cancelTaskRequest){
            Helper::setCountryTimeZone($cancelTaskRequest->getFromUser()->getCountry()->getIdentifier());
            if($cancelTaskRequest->getUpdatedAt()->modify('+1 day') < new \DateTime()){
                $cancelTaskRequest->setStatus(CancelTaskRequest::STATUS_REFUSED);
                $em->persist($cancelTaskRequest);
            }
        }
        $em->flush();
        $output->writeln('Daily cancel task request checker done !');
    }
}
