<?php
/**
 * Created by PhpStorm.
 * User: mda
 * Date: 04/12/19
 * Time: 03:52 م
 */

namespace AppBundle\Services;

use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\Configurator\ParametersConfigurator;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use Xendit\Xendit;

class Payout
{
    public const BASE_PATH = 'https://b2btest.stcpay.com.sa/B2B.MerchantTransactionsWebApi/MerchantTransactions/v3';

    private $container;

    /**
     * HyperPay constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        Xendit::setApiKey($this->container->getParameter('xendit_payment_secret'));
    }


    /**
     * @param User $user
     * @param Transaction $transaction
     * @return mixed
     */
    public function createPayout(User $user, Transaction $transaction) : array
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $dbTranslator = $this->container->get('app.database_translator');
        if ($user->getCountry()->getIdentifier() === 'id') {
            if(!$user->getBankAccount()->isValid('id')){
                return [false,$dbTranslator->transDb('payment.withdraw.invalid_bank_account',[],$user->getLanguage())];
            }
            $params = [
                'external_id' => $transaction->getCheckoutId(),
                'amount' => $transaction->getPrice(),
                'email_to' => [$user->getEmail()],
                'bank_code' => $user->getBankAccount()->getBankCode(),
                'account_holder_name' => $user->getBankAccount()->getHolderName(),
                'account_number' => $user->getBankAccount()->getAccountNumber(),
                'description' => 'withdraw from your maatloob wallet'
            ];
            try{
                $result = \Xendit\Disbursements::create($params);
                $transaction->addExtras('createdAt',(new \DateTime())->format('Y-m-d h:i:s'));
                $transaction->addExtras('disbursementId',$result['id']);
                $em->persist($transaction);
                $em->flush();
                $success = true;
                $msg = $dbTranslator->transDb('payment.withdraw.created');
            }catch (\Exception $e){
                $msg = $e->getMessage();
                $success = false;
            }
            return [$success,$msg];
        } else {
            try{
                $result = $this->sendWithStcPay($user->getPhone(),  $transaction->getCheckoutId(), $transaction->getPrice());
                $reference = $result['PaymentOrderResponseMessage']['PaymentOrderReference'];
                $result = $this->getStcPayStatus($reference,$transaction->getId());
                if(isset($result['Payments'][0]['Status']) && $result['Payments'][0]['Status'] === 5){
                    $transaction->setStatus(Transaction::STATUS_COMPLETED);
                    $transaction->setPaymentInformation(json_encode($result));
                    $em->persist($transaction);
                    $em->flush();
                    $success = true;
                    $msg = $dbTranslator->transDb('payment.withdraw.created');
                }elseif ($result['Payments'][0]['Status'] && (int) $result['Payments']['Status'] === 2){
                    $transaction->addExtras('paymentReference',$reference);
                    $transaction->addExtras('createdAt',(new \DateTime())->format('Y-m-d h:i:s'));
                    $msg =$dbTranslator->transDb('payment.withdraw.stcpay.informed');
                    $success = true;
                }else{
                    $msg = $dbTranslator->transDb('payment.withdraw.stcpay.failed');
                    $success = false;
                }
                return [$success,$msg];
            }catch (\Exception $e){
                return [false,$dbTranslator->transDb('payment.withdraw.stcpay.failed')];
            }
        }
    }

    /**
     * @param $userPhone
     * @param $customerReference
     * @param $amount
     * @return mixed
     * @throws \Exception
     */
    private function sendWithStcPay($userPhone, $customerReference, $amount)
    {
        $data['PaymentOrderRequestMessage'] = [
            'CustomerReference' => $customerReference,
            'Description' => 'balance withdraw',
            'ValueDate' => (new \DateTime())->format('YMD'),
            'Payments' => [
                'MobileNumber' => $userPhone,
                'Amount' => $amount
            ]
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::BASE_PATH . '/PaymentOrderPay');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'X-ClientCode:' . $this->container->getParameter('stcpay_client_code')));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if (curl_error($ch) || empty($responseData)) {
            throw new \Exception($ch, 400);
        }
        return json_decode($responseData, true);
    }

    public function getStcPayStatus($paymentOrderReference , $customerReference)
    {
        $data = [
            'PaymentOrderInquiryRequestMessage' => [
                'PaymentOrderReference' => $paymentOrderReference,
                'CustomerReference' => $customerReference
            ]
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::BASE_PATH . '/PaymentOrderInquiry');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'X-ClientCode:' . $this->container->getParameter('stcpay_client_code')));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new \Exception($ch, 400);
        }
        curl_close($ch);
        return json_decode($responseData, true)['PaymentOrderInquiryResponseMessage'];
    }

    public function cancelStcTransaction($paymentOrderReference , $customerReference): bool
    {
        $data = [
            'PaymentOrderCancelRequestMessage' => [
                'PaymentOrderReference' => $paymentOrderReference,
                'CustomerReference' => $customerReference
            ]
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::BASE_PATH . '/PaymentOrderCancel');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'X-ClientCode:' . $this->container->getParameter('stcpay_client_code')));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if (curl_errno($ch)) {
            return false;
        }
        curl_close($ch);
        $data = json_decode($responseData, true);
        return isset($data['PaymentOrderCancelResponseMessage']['Payments']['Status']) && (int)$data['PaymentOrderCancelResponseMessage']['Payments']['Status'] === 0;
    }
}
