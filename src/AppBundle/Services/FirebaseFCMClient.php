<?php
/**
 * Created by PhpStorm.
 * User: mda
 * Date: 02/07/19
 * Time: 03:10 م
 */

namespace AppBundle\Services;


use Monolog\Logger;
use Psr\Log\LogLevel;

class FirebaseFCMClient
{
    protected $server_key;
    protected $message;
    public const FCM_URL = 'https://fcm.googleapis.com/fcm/send';

    public function __construct($server_key, Logger $logger)
    {
        $this->server_key = $server_key;
        $this->message['channelId'] = 'Matloob general channel';
        $this->message['icon'] = 'http://dev.maatloob.com/assets/img/logo/logo_v2.png';
        $this->logger = $logger;
    }

    public function createMessage($message)
    {
        if (array_key_exists('registration_ids', $message)) {
            $this->message['registration_ids'] = $message['registration_ids'];
        } elseif (array_key_exists('to', $message)) {
            $this->message['to'] = $message['to'];
        } else {
            return false;
        }
        if (array_key_exists('title', $message)) {
            $this->message['notification']['title'] = $message['title'];
        }
        if (array_key_exists('body', $message)) {
            $this->message['notification']['body'] = $message['body'];
        }
        if (array_key_exists('icon', $message)) {
            $this->message['notification']['icon'] = $message['icon'];
        }
        if (array_key_exists('image', $message)) {
            $this->message['notification']['image'] = $message['image'];
        }
        if (array_key_exists('data', $message) && !empty($message['data'])) {
            $this->message['data'] = $message['data'];
        }
        return $this;
    }

    public function sendMessage()
    {
        $content = json_encode($this->message);
        if (array_key_exists('registration_ids', $this->message) || array_key_exists('to', $this->message)) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, self::FCM_URL);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-type:application/json',
                'Authorization:key=' . $this->server_key
            ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
            $json_response = curl_exec($curl);
            $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($status != 200) {
                $this->logger->log(LogLevel::CRITICAL, 'Notification fail : ' . curl_error($curl) . ' content ' . $content);
            }
            curl_close($curl);
            $response = json_decode($json_response, true);
            return $response;
        }else{
            $this->logger->log(LogLevel::CRITICAL, 'Notification fail not destination token , content ' . $content);
        }
    }
}