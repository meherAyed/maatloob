<?php


namespace AppBundle\Services;

use Exception;

class WhatsappApi
{

    public const TYPE_MESSAGE = 1;
    public const TYPE_MEDIA = 2;
    public const TYPE_VOICE = 3;


    public const URL = 'https://hiwhats.com/API/send';

    private $instanceId;
    /**
     * @var int
     */
    private $mobile;
    /**
     * @var string
     */
    private $password;

    public function __construct($mobile,$password,$instanceId)
    {
        $this->mobile = $mobile;
        $this->password = $password;
        $this->instanceId = $instanceId;
    }

    /**
     * @param $message
     * @param $numbers
     * @param int $type
     * @return mixed
     * @throws Exception
     */
    public function sendMessage($message,$numbers,$type=self::TYPE_MESSAGE){
        if(is_array($numbers)){
            $numbers = implode(',',$numbers);
        }
        $message = urlencode($message);
        $url = self::URL."?mobile=$this->mobile&password=$this->password&instanceid=$this->instanceId&message=$message&numbers=$numbers&type=$type";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,5);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        $responseData = curl_exec($ch);
        if(($error = curl_errno($ch)) || (strpos($responseData,'sent')===false)) {
            throw new Exception("Send whatsapp message failed to : $numbers", 400);
        }
        curl_close($ch);
        return json_decode($responseData,true);
    }
}