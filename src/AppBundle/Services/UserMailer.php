<?php
/**
 * Date: 11/13/18
 * Time: 10:24 AM
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Services;

use Components\Helper;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

/**
 * Class UserMailer
 * @package AppBundle\Services
 */
class UserMailer
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var UrlGeneratorInterface
     */
    protected $router;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var array
     */
    protected $parameters;

    protected $supportEmail;

    /**
     * Mailer constructor.
     *
     * @param \Swift_Mailer         $mailer
     * @param UrlGeneratorInterface $router
     * @param EngineInterface       $templating
     * @param array                 $parameters
     */
    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface  $router, EngineInterface $templating, $supportEmail,array $parameters)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->parameters = $parameters;
        $this->supportEmail = $supportEmail;
    }

    /**
     * @param UserInterface $user
     * @param bool $admin
     */
    public function sendResettingEmailMessage(UserInterface $user,$admin=false)
    {
        $template = $this->parameters['template']['resetting'];

        if ($admin) {
            $url = $this->router->generate('admin_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        }else{
            $url = $this->router->generate('user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        }

        $rendered = $this->templating->render($template, array(
            'user' => $user,
            'confirmationUrl' => $url,
        ));
        $this->sendEmailMessage($rendered, [$this->supportEmail => 'Maatloob'], (string)$user->getEmail());
    }


    /**
     * {@inheritdoc}
     */
    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['template']['confirmation'];
        $url = $this->router->generate('user_registration_confirm', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        $params = ['user'=>$user,'confirmationUrl'=>$url];
        if(!$user->getPassword()){
            $user->setPlainPassword(Helper::generateStrongPassword());
            $params['password']=$user->getPlainPassword();
        }
        $rendered = $this->templating->render($template,$params);
        $this->sendEmailMessage($rendered, [$this->supportEmail => 'Maatloob'], (string) $user->getEmail());
    }

    /**
     * @param string $renderedTemplate
     * @param array|string $fromEmail
     * @param array|string $toEmail
     * @param null $subject
     * @param null $bcc
     */
    public function sendEmailMessage($renderedTemplate, $fromEmail, $toEmail,$subject=null,$bcc=null): void
    {
        if(!$fromEmail){
            $fromEmail = [$this->supportEmail => 'Maatloob'];
        }
        if(!$subject){
            // Render the email, use the first line as the subject, and the rest as the body
            $renderedTemplate = explode("\n", trim($renderedTemplate));
            $subject = array_shift($renderedTemplate);
            $renderedTemplate = implode("\n", $renderedTemplate);
        }
        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail)
            ->setBcc($bcc)
            ->setBody($renderedTemplate, 'text/html');

        $this->mailer->send($message);
    }
}
