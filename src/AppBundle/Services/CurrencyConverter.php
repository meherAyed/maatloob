<?php


namespace AppBundle\Services;


use AppBundle\Entity\Country;
use Exception;

class CurrencyConverter
{
    private  $url = 'https://free.currconv.com/api/v7/convert?compact=ultra';
    public function __construct($apiKey)
    {
        $this->url .= "&apiKey=${apiKey}";
    }

    public function convert($from,$to='USD')
    {
        $query = "${from}_${to}";
        $response_json = file_get_contents($this->url . "&q=${query}");
        if (false !== $response_json) {
            try {
                $response = json_decode($response_json,false);
                return number_format($response->$query,6,'.','');
            } catch (Exception $e) {
            }
        }
        return null;
    }

    /**
     * @param float $money
     * @param Country $from
     * @param Country $to
     * @return mixed
     */
    public function convertMoney(float $money,Country $from,Country $to){
        return number_format($from->getCurrencyExchange($to->getCurrencyCode())->getRate()* $money,2,'.','');
    }

    /**
     * @param $num
     * @return float
     */
    public function convertFloat($num): float
    {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

        if (!$sep) {
            return (float)preg_replace('/\D/', '', $num);
        }

        return (float)(preg_replace('/\D/', '', substr($num, 0, $sep)) . '.' .
            preg_replace('/\D/', '', substr($num, $sep + 1, strlen($num))));
    }
}
