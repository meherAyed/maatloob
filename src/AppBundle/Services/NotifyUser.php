<?php
/**
 * Created by PhpStorm.
 * User: naceur
 * Date: 03/07/19
 * Time: 11:14 ص
 */

namespace AppBundle\Services;


use ApiBundle\Services\DBTranslator;
use AppBundle\Entity\Notification;
use AppBundle\Entity\NotificationConfig;
use AppBundle\Entity\User;
use Components\Helper;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use Psr\Log\LogLevel;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Templating\EngineInterface;

class NotifyUser
{
    /** @var FirebaseFCMClient $fcm */
    private $fcmClient;

    /** @var \Swift_Mailer $mailer */
    private $mailer;

    /** @var DBTranslator $trans */
    private $trans;

    /** @var EngineInterface  */
    private $twig;

    /** @var EntityManagerInterface $em */
    private $em;

    /** @var string */
    private $supportMail;
    /**
     * @var Logger
     */
    private $logger;

    private $webDir;

    private $serverPath;

    public function __construct(EntityManagerInterface $em,FirebaseFCMClient $fcmClient,DBTranslator $dbTranslator,
                                \Swift_Mailer $mailer,EngineInterface $twig,$supportMail,$webDir,Logger $logger,$serverPath)
    {
        $this->em = $em;
        $this->fcmClient = $fcmClient;
        $this->trans = $dbTranslator;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->supportMail = $supportMail;
        $this->webDir = $webDir;
        $this->logger = $logger;
        $this->serverPath = $serverPath;
    }

    public function notify(Notification $notification)
    {
        $userToNotify = $notification->getToUser();
        /** @var NotificationConfig $userNotificationConfig */
        $userNotificationConfig = $this->em->getRepository(NotificationConfig::class)
            ->findOneBy(['user' => $userToNotify, 'action' => $notification->getType()]);
        if($userNotificationConfig){
            if ($userNotificationConfig->getPush() && !$userToNotify->getActiveBan()) {
                $this->pushNotify($userToNotify, $notification);
            }
            if ($userNotificationConfig->getSms()) {
                $this->smsNotify($userToNotify, $notification);
            }
            if ($userNotificationConfig->getEmail()) {
                $this->emailNotify($userToNotify, $notification);
            }
        }
    }

    /**
     * @param User $user
     * @param Notification $notification
     */
    public function pushNotify(User $user, Notification $notification): void
    {
        $message['title'] = $this->trans->transDb($notification->getTitle(), $notification->getParams(),$user->getLanguage());
        $message['body'] = $this->trans->transDb($notification->getBody(), $notification->getParams(),$user->getLanguage());
        $message['to'] = $user->getAndroidFcmToken() ?? $user->getIosFcmToken();
        $message['data'] = $notification->getExtra()??[];
        $message['icon'] = $this->serverPath.'/assets/img/logo/logo_v3.png';
        if(!empty($message['to'])){
            $this->fcmClient->createMessage($message)->sendMessage();
        }
    }

    public function smsNotify(User $user, Notification $notification)
    {
        // Configure after to send email
    }

    /**
     * @param User $user
     * @param Notification $notification
     */
    public function emailNotify(User $user, Notification $notification): void
    {
        $message = (new \Swift_Message($this->trans->transDb($notification->getTitle(),  $notification->getParams(),$user->getLanguage())))
            ->setFrom([$this->supportMail => 'Maatloob'])
            ->setTo($user->getEmail())
            ->setBody(
                $this->twig->render(
                    ':emails:notification.html.twig',
                    [
                        'title' => $this->trans->transDb($notification->getTitle(),  $notification->getParams(),$user->getLanguage()),
                        'name' => $user->getFullName(),
                        'lang' => $user->getLanguage(),
                        'body' => $this->trans->transDb($notification->getBody(), $notification->getParams(),$user->getLanguage()),
                        'extra' => $notification->getExtra()
                    ]
                ),
                'text/html'
            );

        $response = $this->mailer->send($message);
        $this->logger->log(LogLevel::INFO,'Sending email to user = '.$user->getEmail().' response is :'.$response);
    }
}