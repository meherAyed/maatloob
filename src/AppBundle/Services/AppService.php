<?php


namespace AppBundle\Services;

use AppBundle\Entity\Notification;
use AppBundle\Entity\Task;
use AppBundle\Entity\UserBan;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AppService
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function sendBanNotification(UserBan $userBan): void
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $body = 'notification.account_reactivated';
        if($userBan->getUser()->isBanned()){
            $body = $userBan->isPermanent()?'notification.account_banned_permanently':'notification.account_banned_period';
        }
        $notification = new Notification();
        $notification->setToUser($userBan->getUser());
        $notification->setTitle('notification.account_suspension.title');
        $notification->setBody($body);
        $notification->setType(Notification::ACTION_HELP_INFO);
        $notification->setParams([
            '#date#' => !$userBan->isPermanent()?$userBan->getUnbanDate()->format('Y-m-d H:i'):''
        ]);
        $notification->setExtra(['banReason'=>$userBan->getBanReason()]);
       $em->persist($notification);
       $em->flush();
    }
}