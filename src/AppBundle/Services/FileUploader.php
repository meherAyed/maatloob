<?php

namespace AppBundle\Services;

use AppBundle\Entity\File;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileUploader
{
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param $dir
     * @param \Symfony\Component\HttpFoundation\File\File $file
     * @param $user
     * @return File|null
     */
    public function uploadFile($dir, $file, User $user = null): ?File
    {
        $extension = $file->guessExtension();
        $name = $file->getFilename()??'file_'.time();
        $fileName = md5(uniqid('', true)) . '.' . $extension;
        $size = $file->getSize();
        try {
            $file->move(
                $dir,
                $fileName
            );
            $fileObject = new File();
            $fileObject->setName($name);
            if($user){
                $fileObject->setUser($user);
            }
            $fileObject->setExtension($extension);
            $fileObject->setSize($size);
            $fileObject->setPath(str_replace('../web', '', $dir) . $fileName);
            return $fileObject;
        } catch (FileException $e) {
            return null;
        }
    }

    public function deleteFile(File $file)
    {
        if(!empty($file->getPath())){
            $filePath = '../web'.$file->getPath();
            $filesystem = new Filesystem();
            if ($filesystem->exists($filePath))
                $filesystem->remove($filePath);
        }
        return $this;
    }
}