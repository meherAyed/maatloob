<?php
/**
 * Created by PhpStorm.
 * User: mda
 * Date: 04/12/19
 * Time: 03:52 م
 */

namespace AppBundle\Services\HyperPay;

use AppBundle\Entity\Country;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Services\CurrencyConverter;
use Doctrine\ORM\EntityManager;

class HyperPay
{
    private $path;
    private $entityId;
    private $authToken;
    private $testMode;

    public const TEST_PATH = 'https://test.oppwa.com/v1';
    public const PROD_PATH = 'https://oppwa.com/v1';
    /**
     * @var CurrencyConverter
     */
    private $currencyConverter;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * HyperPay constructor.
     * @param $authToken
     * @param $entityId
     * @param CurrencyConverter $currencyConverter
     * @param EntityManager $entityManager
     * @param bool $testMode
     */
    public function __construct($authToken, $entityId,CurrencyConverter $currencyConverter,EntityManager $entityManager,$testMode = true)
    {
        $this->path = $testMode ? self::TEST_PATH : self::PROD_PATH;
        $this->em = $entityManager;
        $this->authToken = $authToken;
        $this->entityId = $entityId;
        $this->testMode = $testMode;
        $this->currencyConverter = $currencyConverter;
    }


    /**
     * @param $transactionId
     * @param $amount
     * @param User $user
     * @param $redirectionUrl
     * @param array $registrationsId
     * @return mixed
     * @throws HyperPayException
     */
    public function createCheckout($transactionId,$amount,User $user,$redirectionUrl,$registrationsId = [])
    {
        /** Convert money from user country to sar currency*/
        /** @var Country $saudiCountry */
        $saudiCountry = $this->em->getRepository(Country::class)->find('sa');
        $convertedAmount = $this->currencyConverter->convertMoney($amount,$user->getCountry(),$saudiCountry);
        $data = 'entityId=' .$this->entityId. '&amount=' .$convertedAmount. '&currency=SAR&paymentType=DB&customer.email=' .
            $user->getEmail(). '&merchantTransactionId=' .$transactionId;
        /** Add notification url*/
        $data .= '&shopperResultUrl=' . $redirectionUrl;
        foreach ($registrationsId as $key => $regId){
            $data.="&registrations[$key].id=$regId";
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->path . '/checkouts');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer ' . $this->authToken));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, !$this->testMode);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new HyperPayException($ch, 400);
        }
        curl_close($ch);
        return json_decode($responseData, true);
    }


    /**
     * @param $checkoutId
     * @return mixed
     */
    public function getPaymentStatus($checkoutId)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->path . '/checkouts/' . $checkoutId . '/payment?entityId=' . $this->entityId);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer ' . $this->authToken));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, !$this->testMode);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if (curl_errno($ch)) {
            return [];
        }
        curl_close($ch);
        return json_decode($responseData, true);
    }

    /**
     * @param $paymentId
     * @param $amount
     * @param $currency
     * @return array|mixed
     */
    public function refundPayment($paymentId, $amount, $currency='SAR')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->path . '/payments/' . $paymentId);
        $entityId = $this->entityId;
        $data = "entityId=${entityId}&amount=${amount}&currency=${currency}&paymentType=RF";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer ' . $this->authToken));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, !$this->testMode);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if (curl_errno($ch)) {
            return [];
        }
        curl_close($ch);
        return json_decode($responseData, true);
    }
}
