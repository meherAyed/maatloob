<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 04/12/2019
 * Time: 20:41
 */

namespace AppBundle\Services;


use AppBundle\Entity\Notification;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class NotifyAdmin
{

    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function notifyAdmin(Notification $notification): void
    {
        $user = $notification->getFromUser();
        $adminsResponsibleForUser = $this->em->getRepository(User::class)
            ->createQueryBuilder('u')
            ->leftJoin('u.assignedCountries','ac')
            ->where('u.isAdmin = 1')
            ->andWhere('u.roles like :superAdmin or ac.country = :country')
            ->setParameters([
                'superAdmin' => '%'.User::ROLE_SUPER_ADMIN.'%',
                'country' => $user->getCountry()
            ])
            ->getQuery()->getResult();
        foreach ($adminsResponsibleForUser as $admin){
            $notif = clone $notification;
            $notif->setId(null);
            $notif->setToUser($admin);
            $this->em->persist($notif);
        }
        $this->em->flush();
    }
}
