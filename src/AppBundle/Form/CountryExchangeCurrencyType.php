<?php

namespace AppBundle\Form;

use AppBundle\Entity\CountryExchangeCurrency;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CountryExchangeCurrencyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('rate',MoneyType::class,['label' => false,'attr'=>['class'=>'form-control req'],'currency'=> false,'scale'=>6]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
        'data_class' => CountryExchangeCurrency::class));

    }
}
