<?php

namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'label' => 'users.email'
        ])
            ->add('firstName', TextType::class, [
                'label' => 'users.firstname',
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('lastName', TextType::class, [
                'label' => 'users.lastname',
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('picture', FileType::class, [
                'required' => false,
            ])
            ->add('phone', TelType::class, [
                'required' => false,
                'label' => 'users.address.phone',
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'users.role',
                'choices' => [
                    'users.role_admin' =>  User::ROLE_ADMIN ,
                    'users.role_supervisor' =>  User::ROLE_SUPERVISOR ,
                    'users.role_accountant' =>  User::ROLE_ACCOUNTANT ,
                    'users.role_dashboard_user' =>  User::ROLE_DASHBOARD_USER ,
                ],
            ])
            ->add('assignedCountries',EntityType::class,[
                'label' => 'system.administrator.assigned_countries',
                'class'  => Country::class,
                'query_builder' => static function (EntityRepository $er){
                    return $er->createQueryBuilder('c')
                        ->where('c.enabled = 1');
                },
                'choice_label' => 'name',
                'multiple' => true
            ]);
            $builder->get('roles')->addModelTransformer(new CallbackTransformer(
                static function ($array) {
                    return is_array($array)?$array[0]:'';
                },
                static function ($value) {
                    return  [$value];
                }
            ));
    }

    /**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
