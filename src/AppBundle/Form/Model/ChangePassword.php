<?php
/**
 * Created by PhpStorm.
 * User: chayma
 * Date: 09/04/18
 * Time: 10:06 ص
 */

namespace AppBundle\Form\Model;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

class ChangePassword
{
    /**
     * @Assert\NotBlank(message = "Please complete mandatory fields")
     * @SecurityAssert\UserPassword(
     *     message = "update_password.wrong_current_pwd"
     * )
     */
    protected $oldPassword;

    protected $newPassword;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getSalt()
    {
        return $this->user->getSalt();
    }

    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
    }

    public function getNewPassword()
    {
        return $this->newPassword;
    }

    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }
}
