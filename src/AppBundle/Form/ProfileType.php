<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\File;

class ProfileType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'label' => 'users.email'
             ])
            ->add('firstName', TextType::class, [
                'label' => 'users.firstname',
            ])
            ->add('phone', TextType::class,[
                'label' => 'users.address.phone',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'users.lastname',
            ])->add('picture', FileType::class, [
                'required' => false,
            ])
            ->add('coverPicture', FileType::class, [
                'required' => false,
            ]);
    }
}
