<?php

namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\CountryExchangeCurrency;
use AppBundle\Entity\Language;
use AppBundle\Entity\SystemConfig;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CountryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('identifier')
            ->add('flag', TextType::class, ['label' => 'system.country.table.flag'])
            ->add('name', TextType::class, ['label' => 'system.country.table.name'])
            ->add('currencyCode', TextType::class, ['label' => 'system.country.currency_code'])
            ->add('currencyName', TextType::class, ['label' => 'system.country.currency_name'])
            ->add('currencySymbol', TextType::class, ['label' => 'system.country.currency_symbol'])
            ->add('defaultLanguage', EntityType::class, [
                'label' => 'system.country.table.default_language',
                'class' => Language::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->createQueryBuilder('l')
                        ->where('l.active = 1');
                },
                'choice_label' => 'name',
                'preferred_choices' => ['en']
            ])
            ->add('config', SystemConfigType::class)
            ->add('currencyExchange', CollectionType::class, [
                'entry_type' => CountryExchangeCurrencyType::class,
                'label' => 'system.country.currency_exchange_usd',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Country::class,
            'allow_extra_fields' => true
        ));
    }
}
