<?php

namespace AppBundle\Form;

use AppBundle\Entity\CountryCategory;
use AppBundle\Entity\DocumentType;
use AppBundle\Entity\TaskCategory;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CountryCategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $country = $options['country'];
        $builder
            ->add('enabled',CheckboxType::class)
            ->add('requiredDocuments',EntityType::class,[
                'label' => 'system.category.table.required_documents',
                'class' => DocumentType::class,
                'query_builder' => static function (EntityRepository $er) use ($country) {
                    return $er->createQueryBuilder('dt')
                        ->where('dt.country = :country')
                        ->setParameter('country',$country);
                },
                'choice_label' => static function(DocumentType $documentType){
                    return $documentType->translate()->getName();
                },
                'multiple' => true,
                'required' => false
            ]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CountryCategory::class,
            'allow_extra_fields' => true,
            'country' => 'sa'
        ));
    }
}
