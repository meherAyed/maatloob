<?php

namespace AppBundle\Form;

use AppBundle\Entity\File as EntityFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class FileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('file', \Symfony\Component\Form\Extension\Core\Type\FileType::class, [
            'label' => ' ',
            'constraints' => [
                new File([
                    'maxSize' => '2024k',
                    'mimeTypes' => [
                        'image/*',
                    ],
                    'mimeTypesMessage' => 'update_profile.invalid_format',
                    'maxSizeMessage' => 'update_profile.invalid_filesize'
                ])
            ],
            'attr' => ['accept' => "image/*"],
            'label_attr' => ['class' => 'fas fa-camera']
        ]);
    }

    /**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => EntityFile::class
        ));
    }
}
