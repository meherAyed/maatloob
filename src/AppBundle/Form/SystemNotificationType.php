<?php

namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\CountryExchangeCurrency;
use AppBundle\Entity\Language;
use AppBundle\Entity\Notification;
use AppBundle\Entity\SystemConfig;
use AppBundle\Entity\SystemNotification;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SystemNotificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title',TextType::class,['label'=>'system.notification.form.title'])
                ->add('body',TextareaType::class,['label'=>'system.notification.form.body'])
                ->add('type',ChoiceType::class,[
                    'label' => 'system.notification.form.type',
                    'choices' => [
                        'system.notification.types.'.Notification::ACTION_UPDATE_NEWS => Notification::ACTION_UPDATE_NEWS,
                        'system.notification.types.'.Notification::ACTION_HELP_INFO => Notification::ACTION_HELP_INFO,
                    ]
                ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => SystemNotification::class,
            'allow_extra_fields' => true
        ));
    }
}
