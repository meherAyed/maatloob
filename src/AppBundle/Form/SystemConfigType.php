<?php

namespace AppBundle\Form;

use AppBundle\Entity\SystemConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SystemConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $paymentBrands = [
            'MASTER' => 'MASTER',
            'VISA' => 'VISA',
        ];
        if($options['country']==='id'){
            $paymentBrands = array_merge($paymentBrands,['OVO'=>'OVO','DANA'=>'DANA','LINKAJA'=>'LINKAJA']);
        }else{
            $paymentBrands = array_merge($paymentBrands,['MADA'=>'MADA','STC_PAY'=>'STC_PAY','PAYPAL' => 'PAYPAL']);
        }


        $builder->add('id')
            ->add('taskProfits', null, [
                'label' => 'system.general.maatloob_profits',
                'allow_extra_fields' => true,
                'mapped' => false
            ])
            ->add('maxTaskProfits', TextType::class, ['label' => 'system.general.max_allowed_profits'])
            ->add('includedTax', CheckboxType::class, ['label' => 'system.general.included_tax'])
            ->add('enableMaatloobProfitsAfterDays', TextType::class, ['label' => 'system.general.enable_profits_after'])
            ->add('countryTax', TextType::class, ['label' => 'system.general.country_tax'])
            ->add('showReviewsAfter', TextType::class, ['label' => 'system.general.show_reviews_after'])
            ->add('allowedTaskOfferBeforeVerification', TextType::class, ['label' => 'system.general.max_task_offer_before_verification'])
            ->add('minAcceptedTaskBudget', TextType::class, ['label' => 'system.general.min_accepted_task_budget'])
            ->add('minAllowedBalanceRecharge', TextType::class, ['label' => 'system.general.min_allowed_balance_recharge'])
            ->add('allowWithdrawBalanceFrom', TextType::class, ['label' => 'system.general.allow_withdraw_balance_from'])
            ->add('minMandatoryDocumentToVerifyAccount', TextType::class, ['label' => 'system.general.min_mandatory_document_account_verification'])
            ->add('filterDistance', null, ['label' => 'system.general.task_filter_distance'])
            ->add('filterPrice', null, ['label' => 'system.general.task_filter_price'])
            ->add('referralToUserLimit', TextType::class, ['label' => 'system.general.referral_to_user_limit'])
            ->add('referralCredit', TextType::class, ['label' => 'system.general.referral_credit'])
            ->add('referringCredit', TextType::class, ['label' => 'system.general.referring_credit'])
            ->add('maxAllowedTaskCancel', TextType::class, ['label' => 'system.general.max_allowed_task_cancel'])
            ->add('taskCancelBanDuration', TextType::class, ['label' => 'system.general.task_cancel_ban_duration'])
            ->add('minAllowedMaatloobBalance', TextType::class, ['label' => 'system.general.min_allowed_maatloob_balance'])
            ->add('taxIdentifier', TextType::class, ['label' => 'system.general.tax_identifier', 'required' => false])
//            ->add('invoiceTemplate', TextType::class, ['label' => 'system.general.invoice_template', 'required' => false])
            ->add('transferFeeBank', TextType::class, ['label' => 'system.general.transfer_fee_bank', 'required' => false])
            ->add('transferFeeEWallet', TextType::class, ['label' => 'system.general.transfer_fee_e-wallet', 'required' => false])
            ->add('supportedPaymentBrand', ChoiceType::class, [
                'label' => 'system.general.supported_payment_brand',
                'choices' => $paymentBrands,
                'multiple'  => true,
                'choice_attr' => ['label'=>'mr-35'],
            ])
            ->add('taskCompleteTips', CollectionType::class, [
                'label' => 'system.general.task_complete_tips',
                'required' => false,
                'entry_type' => TextType::class
                ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => SystemConfig::class,
            'allow_extra_fields' => true,
            'method' => 'post',
            'country' => 'sa'
        ));
    }
}
