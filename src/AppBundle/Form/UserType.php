<?php

namespace AppBundle\Form;

use ApiBundle\Form\SkillsType;
use AppBundle\Entity\Country;
use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'label' => 'users.email'
        ])
            ->add('firstName', TextType::class, [
                'label' => 'users.firstname',
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('lastName', TextType::class, [
                'label' => 'users.lastname',
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('location', TextType::class, [
                'label' => 'users.address.location',
                'required' => false
            ])
            ->add('suburb', TextType::class, [
                'label' => 'users.address.Suburb',
                'required' => false
            ])
            ->add('phone', TextType::class, [
                'label' => 'users.address.phone',
                'required' => false
            ])
            ->add('picture', FileType::class, [
                'required' => false,
            ])
            ->add('coverPicture', FileType::class, [
                'required' => false,
            ]);
        if ($options['admin']) {
            $builder->add('roles', ChoiceType::class, [
                'label' => 'users.role',
                'choices' => [
                    'users.role_admin' =>  User::ROLE_ADMIN ,
                    'users.role_supervisor' =>  User::ROLE_SUPERVISOR ,
                    'users.role_accountant' =>  User::ROLE_ACCOUNTANT ,
                    'users.role_dashboard_user' =>  User::ROLE_DASHBOARD_USER ,
                ],
            ]);
            $builder->get('roles')->addModelTransformer(new CallbackTransformer(
                static function ($array) {
                    array_pop($array);
                },
                static function ($value) {
                    return  [$value];
                }
            ));
        }else{
            $builder->add('skills', SkillsType::class)
                ->add('portfolio', CollectionType::class, [
                    'entry_type' => FileType::class,
                    'prototype' => true,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'required' => false,
                    'label' => false,
                ])->add('tagLine', TextType::class, [
                    'label' => 'users.other.tag_line',
                    'required' => false
                ])
                ->add('about', TextareaType::class, [
                    'label' => 'users.other.about',
                    'required' => false
                ])
                ->add('birth_date', TextType::class, ['label' => 'users.birthday']);
            $builder->get('birth_date')->addModelTransformer(new CallbackTransformer(
                static function (?\DateTime $date) {
                    if($date)
                        return $date->format('d-m-Y');
                    return '';
                },
                static function ($date) {
                    return new \DateTime($date);
                }
            ));
        }

        if ($options['data']->getId() && !$options['admin']) {
            $builder->get('email')->setDisabled(true);
        }

    }

    /**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'admin' => false
        ));
    }
}
