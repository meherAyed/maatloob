<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 26/08/2019
 * Time: 21:43
 */

namespace AppBundle\Security;


use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Translation\TranslatorInterface;

class UserChecker implements UserCheckerInterface
{

    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Checks the user account before authentication.
     *
     * @throws AccountStatusException
     */
    public function checkPreAuth(UserInterface $user)
    {
        if ($user->isSimpleUser() || $user->getisDeleted() || !$user->isEnabled()) {
            throw new AuthenticationException(
                $this->translator->trans('security.login.error.invalid_credentials',[],'FOSUserBundle'));
        }
    }

    /**
     * Checks the user account after authentication.
     *
     * @throws AccountStatusException
     */
    public function checkPostAuth(UserInterface $user)
    {
        // TODO: Implement checkPostAuth() method.
    }
}
