<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * TaskInvoice
 *
 * @ORM\Table(name="task_invoice")
 * @ORM\Entity()
 */
class TaskInvoice extends Timestampable
{
    public const STATUS_PENDING = 0;
    public const STATUS_PROCESSED = 1;
    public const STATUS_PAYED = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"invoice_details"})
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\File", mappedBy="taskInvoice",cascade={"persist", "remove"},orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @JMS\Groups({"invoice_details"})
     */
    private $files;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    private $user;

    /**
     * @var float
     * @ORM\Column(name="price",type="decimal",precision=15, scale=2,options={"default":0})
     * @JMS\Groups({"invoice_details"})
     */
    private $price = 0;

    /**
     * @var int
     * @ORM\Column(name="status", type="smallint",options={"default" : 0})
     * @JMS\Groups({"invoice_details"})
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(type="string", length=64 , name="checkout_id",nullable=true)
     */
    private $checkoutId;

    /**
     * @var string
     * @ORM\Column(type="text" , name="payment_information",nullable=true)
     */
    private $paymentInformation;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Task",inversedBy="invoices")
     * @ORM\JoinColumn(nullable=false,referencedColumnName="id")
     */
    private $task;

    public function __construct()
    {
        parent::__construct();
        $this->status = self::STATUS_PENDING;
        $this->files = new ArrayCollection();
    }

    /**
     * @param int $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files): void
    {
        $this->files = $files;
    }

    public function addFile(File $file){
        $file->setTaskInvoice($this);
        $this->files->add($file);
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getCheckoutId(): string
    {
        return $this->checkoutId;
    }

    /**
     * @param string $checkoutId
     */
    public function setCheckoutId(string $checkoutId): void
    {
        $this->checkoutId = $checkoutId;
    }

    /**
     * @return string
     */
    public function getPaymentInformation(): string
    {
        return $this->paymentInformation;
    }

    /**
     * @param string $paymentInformation
     */
    public function setPaymentInformation(string $paymentInformation): void
    {
        $this->paymentInformation = $paymentInformation;
    }

    /**
     * @return Task
     */
    public function getTask(): ?Task
    {
        return $this->task;
    }

    /**
     * @param mixed $task
     */
    public function setTask($task): void
    {
        $this->task = $task;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}

