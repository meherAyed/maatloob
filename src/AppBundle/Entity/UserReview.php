<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * UserReview
 *
 * @ORM\Table(name="user_review")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserReviewRepository")
 */
class UserReview extends Timestampable
{

    const REVIEW_AS_POSTER = 0;
    const REVIEW_AS_TASKER = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"user_reviews"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"user_reviews"})
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false,fieldName="for_user")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Task")
     * @ORM\JoinColumn(nullable=true)
     * @JMS\Groups({"user_reviews"})
     */
    private $task;

    /**
     * @var int
     *
     * @ORM\Column(name="as_a", type="integer", length=255,nullable=true,options={"comment":"type=1 tasker, type=0 poster"})
     */
    private $asA;

    /**
     * @var string
     * @ORM\Column(name="description", type="text")
     * @JMS\Groups({"user_reviews"})
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="note",type="integer", options={"default":0})
     * @JMS\Groups({"user_reviews"})
     */
    private $note;


    /**
     * Set description
     *
     * @param string $description
     *
     * @return UserReview
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set note
     *
     * @param int $note
     *
     */
    public function setNote(int $note)
    {
        $this->note = $note;
    }

    /**
     * Get note
     *
     * @return int
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return UserReview
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set task
     *
     * @param Task $task
     *
     * @return UserReview
     */
    public function setTask(Task $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set asA
     *
     * @param int $asA
     *
     */
    public function setAsA($asA)
    {
        $this->asA = $asA;
    }

    /**
     * Get asA
     *
     * @return int
     */
    public function getAsA()
    {
        return $this->asA;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy): void
    {
        $this->createdBy = $createdBy;
    }
}
