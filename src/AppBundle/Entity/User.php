<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user", indexes={@ORM\Index(name="user_index", columns={"email", "enabled"})})
 * @UniqueEntity("email", message="email alerady exists.")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    public const TYPE_TASKER = 1;
    public const TYPE_POSTER = 0;
    public const SOURCE_WEB = 0;
    public const SOURCE_ANDROID = 1;
    public const SOURCE_IPHONE = 2;

    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_SUPERVISOR = 'ROLE_SUPERVISOR';
    public const ROLE_ACCOUNTANT = 'ROLE_ACCOUNTANT';
    public const ROLE_DASHBOARD_USER = 'ROLE_DASHBOARD_USER';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"user_info","user_data"})
     */
    protected $id;

    /**
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     * )
     * @JMS\Groups({"user_data"})
     */
    protected $email;
    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255,nullable=true)
     * @JMS\Groups({"user_info","user_data"})
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_completion", type="integer", nullable=false,options={"default" : 0})
     */
    private $profileCompletion = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255,nullable=true)
     * @JMS\Groups({"user_info","user_data"})
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="tagline", type="string", length=255,nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $tagLine;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255,nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="suburb", type="string", length=255,nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $suburb;

    /**
     * @var string
     *
     * @ORM\Column(name="about", type="text",nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $about;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255,nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $location;


    /**
     * @var Country
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country")
     * @ORM\JoinColumn(referencedColumnName="identifier")
     * @JMS\Groups({"user_data","user_info"})
     */
    private $country;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="date",nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $birthDate;

    /**
     * @var Skills
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Skills",cascade={"persist", "remove"},orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id",fieldName="skills_id",nullable=true,onDelete="CASCADE")
     * @JMS\Groups({"user_data"})
     */
    private $skills;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\NotificationConfig",mappedBy="user",cascade={"remove"},orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $notificationConfig;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Address",mappedBy="user")
     */
    private $addresses;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserReview",mappedBy="user")
     *
     */
    private $reviews;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\File",cascade={"persist", "remove"},orphanRemoval=true)
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id",onDelete="CASCADE")
     * @JMS\Groups({"user_info","user_data"})
     */
    private $picture;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\File", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id",onDelete="SET NULL")
     * @JMS\Groups({"user_data"})
     */
    private $coverPicture;

    /**
     * @var int
     *
     * @ORM\Column(name="source", type="smallint",options={"comment":"0 web 1 android 2 iphone","default": 0})
     */
    private $source;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime",nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $createdAt;

    /**
     * @var int
     *
     * @ORM\Column(name="userType", type="integer",nullable=true,options={"comment":"type=1 tasker, type=0 poster"})
     * @JMS\Groups({"user_data"})
     */
    private $userType;

    /**
     * @var string
     *
     * @ORM\Column(name="android_fcm_token", type="string", length=255,nullable=true)
     */
    private $androidFcmToken;

    /**
     * @var string
     *
     * @ORM\Column(name="ios_fcm_token", type="string", length=255,nullable=true)
     */
    private $iosFcmToken;

    /**
     * @JMS\Groups({"user_data"})
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\File",mappedBy="userPortfolio",cascade={"persist", "remove"})
     */
    private $portfolio;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserBadge",mappedBy="user",cascade={"persist", "remove"})
     */
    private $badges;

    /**
     * @var boolean
     *
     * @ORM\Column(name="verified", type="boolean",options={"comment":"Is the account verified by the admin"})
     * @JMS\Groups({"user_data"})
     */
    private $verified;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_admin", type="boolean")
     */
    private $isAdmin = false;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserDocument",mappedBy="user",cascade={"persist","remove"},orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserBan",mappedBy="user")
     */
    private $ban;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserOldPhones",mappedBy="user")
     */
    private $oldPhones;

    /**
     *
     * @ORM\Column(name="is_deleted", type="boolean")
     */
    private $isDeleted = false;

    /**
     * @JMS\Groups({"user_data"})
     */
    protected $lastLogin;

    /**
     * @JMS\Groups({"user_data"})
     */
    protected $enabled;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\UserWallet", cascade={"persist","remove"},orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @JMS\Groups({"user_wallet"})
     */
    private $wallet;

    /**
     * @var string $language
     * @ORM\Column(name="language", type="string",length=2,nullable=true,options={"default":"en"})
     */
    private $language;

    /**
     * @var int
     *
     * @ORM\Column(name="blocked_stars", type="smallint",options={"comment":"total user review stars blocked","default": 0})
     */
    private $blockedStars;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\UserBankAccount",cascade={"persist","remove"},orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @JMS\Groups({"user_data"})
     */
    private $bankAccount;

    /**
     * @var string
     * @ORM\Column(name="referral_code", type="string",length=50,unique=true,nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $referralCode;

    /**
     * @var bool
     * @ORM\Column(name="warned_for_ban", type="boolean",options={"default": 0})
     */
    private $warnedForBan;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Task",mappedBy="user")
     */
    private $tasks;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Offer",mappedBy="user")
     */
    private $offers;

    /**
     * @var array
     * @ORM\Column(name="links", type="array",nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $links;


    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Country" )
     * @ORM\JoinTable(name="admin_assigned_countries",
     *     joinColumns={@JoinColumn(name="admin_id", referencedColumnName="id")},
     *     inverseJoinColumns={@JoinColumn(name="country_id", referencedColumnName="identifier")})
     */
    private $assignedCountries;

    /**
     * User constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->createdAt = new \DateTime();
        $this->notificationConfig = new ArrayCollection();
        $this->badges = new ArrayCollection();
        $this->tasks = new ArrayCollection();
        $this->offers = new ArrayCollection();
        $this->oldPhones = new ArrayCollection();
        $this->verified = false;
        $this->ban = new ArrayCollection();
        $this->roles = ['ROLE_USER'];
        $this->portfolio = new ArrayCollection();
        $this->blockedStars = 0;
        $this->warnedForBan = false;
        $this->reviews = new ArrayCollection();
        $this->source = self::SOURCE_WEB;
        $this->assignedCountries = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set tagLine
     *
     * @param string $tagLine
     *
     * @return User
     */
    public function setTagLine($tagLine)
    {
        $this->tagLine = $tagLine;

        return $this;
    }

    /**
     * Get tagLine
     *
     * @return string
     */
    public function getTagLine()
    {
        return $this->tagLine;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return User
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return User
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set picture
     *
     * @param \AppBundle\Entity\File $picture
     *
     * @return User
     */
    public function setPicture($picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return \AppBundle\Entity\File
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set coverPicture
     *
     * @param File $coverPicture
     *
     * @return User
     */
    public function setCoverPicture(File $coverPicture = null)
    {
        $this->coverPicture = $coverPicture;

        return $this;
    }

    /**
     * Get coverPicture
     *
     * @return File
     */
    public function getCoverPicture()
    {
        return $this->coverPicture;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set suburb
     *
     * @param string $suburb
     *
     * @return User
     */
    public function setSuburb($suburb)
    {
        $this->suburb = $suburb;

        return $this;
    }

    /**
     * Get suburb
     *
     * @return string
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set about
     *
     * @param string $about
     *
     * @return User
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set userType
     *
     * @param integer $userType
     *
     * @return User
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * Get userType
     *
     * @return integer
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Set profileCompletion
     *
     * @param integer $profileCompletion
     *
     * @return User
     */
    public function setProfileCompletion($profileCompletion)
    {
        $this->profileCompletion = $profileCompletion;

        return $this;
    }

    /**
     * Get profileCompletion
     *
     * @return int
     */
    public function getProfileCompletion()
    {
        return $this->profileCompletion;
    }

    /**
     * @return string
     */
    public function getAndroidFcmToken(): ?string
    {
        return $this->androidFcmToken;
    }

    /**
     * @param string $androidFcmToken
     */
    public function setAndroidFcmToken(?string $androidFcmToken)
    {
        $this->androidFcmToken = $androidFcmToken;
    }

    /**
     * @return string
     */
    public function getIosFcmToken(): ?string
    {
        return $this->iosFcmToken;
    }

    /**
     * @param string $iosFcmToken
     */
    public function setIosFcmToken(?string $iosFcmToken)
    {
        $this->iosFcmToken = $iosFcmToken;
    }

    /**
     * @return mixed
     */
    public function getPortfolio()
    {
        return $this->portfolio;
    }

    /**
     * @param mixed $portfolio
     */
    public function setPortfolio($portfolio)
    {
        $this->portfolio = $portfolio;
    }

    /**
     * @param null $type
     * @return ArrayCollection|NotificationConfig
     */
    public function getNotificationConfig($type = null)
    {
        if($type){
            /** @var NotificationConfig $notificationConfig */
            foreach ($this->notificationConfig as $notificationConfig){
                if($notificationConfig->getAction() === $type) {
                    return $notificationConfig;
                }
            }
        }
        return $this->notificationConfig;
    }

    public function addPortfolio(File $file)
    {
        $file->setUserPortfolio($this);
        $this->portfolio->add($file);
    }

    /**
     * @param ArrayCollection $notificationConfig
     */
    public function setNotificationConfig(ArrayCollection $notificationConfig)
    {
        $this->notificationConfig = $notificationConfig;
    }

    /**
     * @return Skills
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param Skills $skills
     */
    public function setSkills(Skills $skills)
    {
        $this->skills = $skills;
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getName()
    {
        return $this->firstName . ' ' . substr($this->lastName, 0, 2);
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->verified;
    }

    /**
     * @param bool $verified
     */
    public function setVerified(bool $verified): void
    {
        $this->verified = $verified;
    }

    /**
     * @return mixed
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param mixed $documents
     */
    public function setDocuments($documents): void
    {
        $this->documents = $documents;
    }


    /**
     * @return UserWallet
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * @param UserWallet $Wallet
     */
    public function setWallet($Wallet): void
    {
        $this->wallet = $Wallet;
    }

    /**
     * @return mixed
     */
    public function getisDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param mixed $isDeleted
     */
    public function setIsDeleted($isDeleted): void
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return ArrayCollection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * @param ArrayCollection $addresses
     */
    public function setAddresses(ArrayCollection $addresses): void
    {
        $this->addresses = $addresses;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"user_data","user_info"})
     */
    public function getLogoName()
    {
        $logoName = (mb_substr($this->firstName, 0, 1, 'utf8')) . (mb_substr($this->lastName, 0, 1, 'utf8'));
        return strtoupper($logoName);
    }

    /**
     * Check a specific user document is verified or not
     * @param $documentIdentifier
     * @return bool
     */
    public function isDocumentVerified($documentIdentifier)
    {
        /** @var UserDocument $document */
        foreach ($this->documents as $document) {
            if ($document->getType()->getIdentifier() === $documentIdentifier) {
                return $document->isVerified();
            }
        }
        return false;
    }

    public function getReviewAsaPoster()
    {
        $reviews = [];
        /** @var UserReview $review */
        foreach ($this->reviews as $review) {
            if ($review->getAsA() === self::TYPE_POSTER) {
                $reviews[] = $review;
            }
        }
        return new ArrayCollection($reviews);
    }

    public function getReviewAsaTasker()
    {
        $reviews = [];
        /** @var UserReview $review */
        foreach ($this->reviews as $review) {
            if ($review->getAsA() === self::TYPE_TASKER) {
                $reviews[] = $review;
            }
        }
        return new ArrayCollection($reviews);
    }


    /**
     *
     * @JMS\VirtualProperty()
     * @JMS\Groups("user_info")
     *
     */
    public function getCompletionRatePoster() : float {
        $completedTasks = 0;
        $totalTasks = 0;
        /** @var Task $task */
        foreach ($this->tasks as $task){
            if(in_array($task->getStatus(), [Task::STATUS_PAID, Task::STATUS_COMPLETED], true)){
                $completedTasks ++;
                $totalTasks++;
            }elseif($task->is(Task::STATUS_CANCELLED) && $task->getCancelRequest() && $task->getCancelRequest()->getGuilty() &&
                $task->getCancelRequest()->getGuilty()->getId()===$this->getId()){
                $totalTasks++;
            }
        }
        return (float) number_format($totalTasks === 0 ? 0 : $completedTasks/$totalTasks,2,'.','');
    }


    /**
     *
     * @JMS\VirtualProperty()
     * @JMS\Groups("user_info")
     *
     */
    public function getCompletionRateTasker() :float{
        $completedTasks = 0;
        $totalTasks = 0;
        /** @var Offer $offer */
        foreach ($this->offers as $offer){
            if($offer->getStatus() === Offer::STATUS_ACCEPTED){
                $task = $offer->getTask();
                if(in_array($task->getStatus(), [Task::STATUS_PAID, Task::STATUS_COMPLETED], true)){
                    $completedTasks ++;
                    $totalTasks++;
                }elseif($task->is(Task::STATUS_CANCELLED) && $task->getCancelRequest() && $task->getCancelRequest()->getGuilty() &&
                    $task->getCancelRequest()->getGuilty()===$this->getId()){
                    $totalTasks++;
                }
            }
        }
        return (float) number_format($totalTasks === 0 ? 0 : $completedTasks/$totalTasks,2,'.','');
    }

    /**
     *
     * @JMS\VirtualProperty()
     * @JMS\Groups("user_info")
     *
     */
    public function getReviewNoteAsaPoster() : float
    {
        $note = 0;
        $reviews = $this->getReviewAsaPoster();
        if ($reviews->count() > 0) {
            /** @var UserReview $review */
            foreach ($reviews as $review) {
                $note += $review->getNote();
            }
            $note -= $this->blockedStars;
            $note = $note < 0 ?0:$note;
            $note = (float) number_format($note / $reviews->count(),1,'.','');
        }
        return $note;
    }

    /**
     *
     * @JMS\VirtualProperty()
     * @JMS\Groups("user_info")
     *
     */
    public function getReviewNoteAsaTasker() : float
    {
        $note = 0;
        $reviews = $this->getReviewAsaTasker();
        if ($reviews->count() > 0) {
            /** @var UserReview $review */
            foreach ($reviews as $review) {
                $note += $review->getNote();
            }
            $note -= $this->blockedStars;
            $note = $note < 0 ?0:$note;
            $note = (float) number_format($note / $reviews->count(),1,'.','');
        }
        return $note;
    }

    public function getNote()
    {
        if ($this->userType === self::TYPE_TASKER) {
            return $this->getReviewNoteAsaTasker();
        } else {
            return $this->getReviewNoteAsaPoster();
        }

    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        if(!$this->language) {
            return $this->getCountry()->getDefaultLanguage()->getIdentifier();
        }
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

    /**
     * @return Country
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry(?Country $country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getBan()
    {
        return $this->ban;
    }

    /**
     * @param mixed $ban
     */
    public function setBan($ban): void
    {
        $this->ban = $ban;
    }

    /**
     * CHeck if user is banned
     * @return bool
     */
    public function isBanned() : bool
    {
        return $this->getActiveBan() !== null;
    }

    /**
     * Get user active ban
     * @return UserBan|null
     */
    public function getActiveBan(): ?UserBan
    {
        if ($this->getBan()->count() > 0) {
            /** @var UserBan $ban */
            foreach ($this->getBan() as $ban) {
                if (!$ban->isBanLifted() && ($ban->isPermanent() || $ban->getUnbanDate() > new \DateTime())) {
                    return $ban;
                }
            }
        }
        return null;
    }

    public function setEmail($email)
    {
        $this->setUsername($email);
        return parent::setEmail($email);
    }

    /**
     * @return UserBankAccount|null
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * @param UserBankAccount $bankAccount
     */
    public function setBankAccount($bankAccount): void
    {
        $this->bankAccount = $bankAccount;
    }


    public function blockStars(): void
    {
        $this->blockedStars++;
    }

    /**
     * @param int $blockedStars
     */
    public function setBlockedStars(int $blockedStars): void
    {
        $this->blockedStars = $blockedStars;
    }

    /**
     * @return string
     */
    public function getReferralCode(): ?string
    {
        return $this->referralCode;
    }

    /**
     * @param string $referralCode
     */
    public function setReferralCode(string $referralCode): void
    {
        $this->referralCode = $referralCode;
    }

    /**
     * @return bool
     */
    public function isWarnedForBan(): bool
    {
        return $this->warnedForBan;
    }

    /**
     * @param bool $warnedForBan
     */
    public function setWarnedForBan(bool $warnedForBan): void
    {
        $this->warnedForBan = $warnedForBan;
    }

    /**
     * @return ArrayCollection
     */
    public function getTasks(): ArrayCollection
    {
        return $this->tasks;
    }

    /**
     * @param ArrayCollection $tasks
     */
    public function setTasks(ArrayCollection $tasks): void
    {
        $this->tasks = $tasks;
    }

    /**
     * @return ArrayCollection
     */
    public function getOffers(): ArrayCollection
    {
        return $this->offers;
    }

    /**
     * @param ArrayCollection $offers
     */
    public function setOffers(ArrayCollection $offers): void
    {
        $this->offers = $offers;
    }

    /**
     * @return mixed
     */
    public function getBadges()
    {
        return $this->badges;
    }

    /**
     * @param mixed $badges
     */
    public function setBadges($badges): void
    {
        $this->badges = $badges;
    }

    /**
     * @param $badge
     */
    public function addBadge($badge): void
    {
        $this->badges->add($badge);
    }
    /**
     * @param $type
     * @return UserBadge|null
     */
    public function getBadgeByType($type): ?UserBadge
    {
        /** @var UserBadge $badge */
        foreach ($this->badges as $badge){
            if($badge->getBadge()->getName() === $type){
                return $badge;
            }
        }
        return null;
    }

    /**
     * @return bool
     */
    public function isSimpleUser() :bool
    {
        return (in_array(self::ROLE_DEFAULT, $this->getRoles(), true) && count($this->getRoles())===1);
    }

    /**
     * @return int
     */
    public function getBlockedStars(): int
    {
        return $this->blockedStars??0;
    }

    /**
     * @return int
     */
    public function getSource(): int
    {
        return $this->source;
    }

    /**
     * @param int $source
     */
    public function setSource(int $source): void
    {
        $this->source = $source;
    }

    /**
     * @return array
     */
    public function getLinks(): array
    {
        return $this->links??[];
    }

    /**
     * @param array $links
     */
    public function setLinks(array $links): void
    {
        $this->links = $links;
    }

    /**
     * @return mixed
     */
    public function getOldPhones()
    {
        return $this->oldPhones;
    }

    /**
     * @param mixed $oldPhones
     */
    public function setOldPhones($oldPhones): void
    {
        $this->oldPhones = $oldPhones;
    }

    /**
     * @return mixed
     */
    public function getAssignedCountries()
    {
        return $this->assignedCountries;
    }

    /**
     * @param mixed $assignedCountries
     */
    public function setAssignedCountries($assignedCountries): void
    {
        $this->assignedCountries = $assignedCountries;
    }

    /**
     * @param Country $country
     * @return bool
     */
    public function isAssignedToCountry(Country $country): bool
    {
        /** @var Country $assignedCountry */
        foreach ($this->assignedCountries as $assignedCountry){
            if ($country->getIdentifier() === $assignedCountry->getIdentifier()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    /**
     * @param bool $isAdmin
     */
    public function setIsAdmin(bool $isAdmin): void
    {
        $this->isAdmin = $isAdmin;
    }
}
