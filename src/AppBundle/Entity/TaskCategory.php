<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as JMS;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * TaskCategory
 *
 * @ORM\Table(name="task_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskCategoryRepository")
 */
class TaskCategory extends Timestampable
{
    use ORMBehaviors\Translatable\Translatable;
    public const DELIVERY_TYPE = 1;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"task_list","task_details"})
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\File")
     */
    private $icon;

    /**
     * @var int
     * @ORM\Column(name="type",type="integer", options={"comment":"1 delivery","default" : 0})
     * @JMS\Groups({"task_list","task_details"})
     */
    private $type;

    public function __construct()
    {
        parent::__construct();
        $this->type = 0;
    }

    /**
     * Set icon
     *
     * @param File $icon
     * @return TaskCategory
     */
    public function setIcon(File $icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * Get icon
     * @return File|null
     */
    public function getIcon(): ?File
    {
        return $this->icon;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isDelivery(): bool
    {
        return $this->type === self::DELIVERY_TYPE;
    }
}
