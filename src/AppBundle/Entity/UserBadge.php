<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserBadge
 *
 * @ORM\Table(name="user_badge")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserBadgeRepository")
 */
class UserBadge
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Badge
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Badge")
     */
    private $badge;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",inversedBy="badges")
     */
    private $user;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="activationDate", type="datetime",nullable=true)
     */
    private $activationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="string", length=255,nullable=true)
     */
    private $data;


    public function __construct()
    {
        $this->active = false;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set badge.
     *
     * @param string $badge
     *
     * @return UserBadge
     */
    public function setBadge($badge)
    {
        $this->badge = $badge;

        return $this;
    }

    /**
     * Get badge.
     *
     * @return Badge|null
     */
    public function getBadge(): ?Badge
    {
        return $this->badge;
    }

    /**
     * Set user.
     *
     * @param string $user
     *
     * @return UserBadge
     */
    public function setUser($user)
    {
        $this->user = $user;
        $this->user->addBadge($this);
        return $this;
    }

    /**
     * Get user.
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return UserBadge
     */
    public function setActive($active)
    {
        if(!$this->active && $active){
            $this->setActivationDate(new \DateTime());
        }
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set activationDate.
     *
     * @param \DateTime $activationDate
     *
     * @return UserBadge
     */
    public function setActivationDate($activationDate)
    {
        $this->activationDate = $activationDate;

        return $this;
    }

    /**
     * Get activationDate.
     *
     * @return \DateTime
     */
    public function getActivationDate()
    {
        return $this->activationDate;
    }

    /**
     * Set data.
     *
     * @param string $data
     *
     * @return UserBadge
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }
}
