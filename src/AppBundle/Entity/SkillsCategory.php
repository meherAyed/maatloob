<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SkillsCategory
 *
 * @ORM\Table(name="skills_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SkillsCategoryRepository")
 * @UniqueEntity("name", message="Category name exists.")
 */
class SkillsCategory extends Timestampable
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="arabicName", type="text")
     */
    private $arabicName;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SkillsCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set arabicName
     *
     * @param string $arabicName
     *
     * @return SkillsCategory
     */
    public function setArabicName($arabicName)
    {
        $this->arabicName = $arabicName;

        return $this;
    }

    /**
     * Get arabicName
     *
     * @return string
     */
    public function getArabicName()
    {
        return $this->arabicName;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
