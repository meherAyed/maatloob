<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * TaskAlert
 *
 * @ORM\Table(name="task_alerts")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskAlertRepository")
 */
class TaskAlert extends Timestampable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"address_details"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(name="remote", type="boolean",options={"default":false})
     * @JMS\Groups({"address_details"})
     */
    private $remote;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="keyword", type="string")
     * @JMS\Groups({"address_details"})
     */
    private $keyword;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Address",orphanRemoval=true,cascade={"persist","remove"})
     * @ORM\JoinColumn(name="address_id",referencedColumnName="id",nullable=true,onDelete="CASCADE")
     * @JMS\Groups({"address_details"})
     */
    private $address;


    /**
     * @var int
     *
     * @ORM\Column(name="radius", type="integer", nullable=true , options={"comment":"unit is kilometer"})
     * @JMS\Groups({"address_details"})
     */

    private $radius;

    public function __construct()
    {
        parent::__construct();
        $this->remote = false;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function isRemote()
    {
        return $this->remote;
    }

    /**
     * @param mixed $remote
     */
    public function setRemote($remote): void
    {
        $this->remote = $remote;
    }

    /**
     * @return int
     */
    public function getRadius(): ?int
    {
        return $this->radius;
    }

    /**
     * @param int $radius
     */
    public function setRadius(int $radius): void
    {
        $this->radius = $radius;
    }

    /**
     * @return Address
     */
    public function getAddress(): ?Address
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }


}
