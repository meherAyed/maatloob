<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * CancelTaskRequest
 *
 * @ORM\Table(name="cancel_task_request")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CancelTaskRequestRepository")
 */
class CancelTaskRequest extends Timestampable
{
    public const STATUS_WAITING = 0;
    public const STATUS_ACCEPTED = 1;
    public const STATUS_REFUSED = 2;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"task_details"})
     */
    private $fromUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $forUser;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Task",inversedBy="cancelRequest")
     * @ORM\JoinColumn(nullable=false)
     */
    private $task;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint",options={"comment":"0 waiting ,1 accepted ,2 refused"})
     * @JMS\Groups({"task_details"})
     */
    private $status;

    /**
     * @var string
     *@ORM\Column(name="reason", type="text", nullable=true)
     * @JMS\Groups({"task_details"})
     */
    protected $reason;

    /**
     * @var bool
     * @ORM\Column(name="closed", type="boolean",options={"default":0})
     */
    private $closed;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $guilty;

    /**
     * @var float
     * @ORM\Column(name="compensation",type="decimal",precision=15, scale=2,options={"default":0})
     */
    private $compensation;




    public function __construct()
    {
        parent::__construct();
        $this->closed = false;
        $this->status = self::STATUS_WAITING;
        $this->compensation = 0;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set status.
     *
     * @param int $status
     *
     * @return CancelTaskRequest
     */
    public function setStatus($status): CancelTaskRequest
    {
        $this->status = $status;
        if($status === self::STATUS_ACCEPTED){
            $this->closed = true;
            $this->guilty = $this->fromUser;
        }
        return $this;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return Task
     */
    public function getTask(): Task
    {
        return $this->task;
    }

    /**
     * @param Task $task
     */
    public function setTask(Task $task): void
    {
        $this->task = $task;
    }

    /**
     * @return string
     */
    public function getReason(): ?string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason(string $reason): void
    {
        $this->reason = $reason;
    }

    /**
     * @return User
     */
    public function getFromUser(): ?User
    {
        return $this->fromUser;
    }

    /**
     * @param mixed $fromUser
     */
    public function setFromUser($fromUser): void
    {
        $this->fromUser = $fromUser;
    }

    /**
     * @return User
     */
    public function getForUser(): ?User
    {
        return $this->forUser;
    }

    /**
     * @param mixed $forUser
     */
    public function setForUser($forUser): void
    {
        $this->forUser = $forUser;
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->closed;
    }

    /**
     * @param bool $closed
     */
    public function setClosed(bool $closed): void
    {
        $this->closed = $closed;
    }

    /**
     * @return float
     */
    public function getCompensation(): float
    {
        return $this->compensation;
    }

    /**
     * @param float $compensation
     */
    public function setCompensation(float $compensation): void
    {
        $this->compensation = $compensation;
    }

    /**
     * @return User|null
     */
    public function getGuilty(): ?User
    {
        return $this->guilty;
    }

    /**
     * @param User $guilty
     */
    public function setGuilty(User $guilty): void
    {
        $this->guilty = $guilty;
    }
}
