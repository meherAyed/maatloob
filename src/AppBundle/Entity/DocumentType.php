<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * DocumentType
 *
 * @ORM\Table(name="document_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DocumentTypeRepository")
 */
class DocumentType
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"user_documents"})
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(name="disabled",type="boolean")
     */
    private $disabled;

    /**
     * @var boolean
     * @ORM\Column(name="required_for_verification",type="boolean")
     * @JMS\Groups({"user_documents"})
     */
    private $requiredForVerification;

    /**
     * @var Country
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country",inversedBy="countryDocuments")
     * @ORM\JoinColumn(name="country_identifier",referencedColumnName="identifier")
     */
    private $country;

    /**
     * @var  \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    public function __construct()
    {
        $this->disabled = false;
        $this->createdAt = new \DateTime();
        $this->requiredForVerification = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * @param bool $disabled
     */
    public function setDisabled(bool $disabled): void
    {
        $this->disabled = $disabled;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return bool
     */
    public function isRequiredForVerification(): bool
    {
        return $this->requiredForVerification;
    }

    /**
     * @param bool $requiredForVerification
     */
    public function setRequiredForVerification(bool $requiredForVerification): void
    {
        $this->requiredForVerification = $requiredForVerification;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"user_documents"})
     */
    public function getName(){
        return $this->translate()->getName();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"user_documents"})
     */
    public function getDescription(){
        return $this->translate()->getDescription();
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }
}

