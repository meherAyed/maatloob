<?php


namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;


/**
 * UserDocument
 *
 * @ORM\Table(name="country_category")
 * @ORM\Entity()
 */
class CountryCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(name="enabled",type="boolean")
     */
    private $enabled;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country",inversedBy="countryCategories")
     * @ORM\JoinColumn(name="country",referencedColumnName="identifier")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TaskCategory")
     */
    private $category;

    /**
     * @var int
     * @ORM\Column(name="position",type="smallint")
     */
    private $position;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\DocumentType")
     * @ORM\JoinTable(name="document_required_for_category")
     */
    private $requiredDocuments;


    public function __construct()
    {
        $this->enabled = true;
        $this->position = 1;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    /**
     * @return mixed
     */
    public function getRequiredDocuments()
    {
        return $this->requiredDocuments;
    }

    /**
     * @param mixed $requiredDocuments
     */
    public function setRequiredDocuments($requiredDocuments): void
    {
        $this->requiredDocuments = $requiredDocuments;
    }
}