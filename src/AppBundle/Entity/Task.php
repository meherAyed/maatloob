<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Task
 *
 * @ORM\Table(name="task", indexes={@ORM\Index(name="task_index", columns={"title", "status", "isOnlineWork"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskRepository")
 */
class Task extends Timestampable
{
    public const STATUS_DRAFT = 0;
    public const STATUS_PUBLISHED = 1;
    public const STATUS_ASSIGNED = 2;
    public const STATUS_WAITING_PAYMENT = 3;
    public const STATUS_PAID = 4;
    public const STATUS_COMPLETED = 5;
    public const STATUS_CANCELLED = 6;

    public const PRICE_ALL_AMOUNT = 1;
    public const PRICE_PER_HOUR = 2;

    public const PAYMENT_TYPE_CASH = 1;
    public const PAYMENT_TYPE_WALLET = 2;
    public const PAYMENT_TYPE_CARD = 3;

    public const TIME_BEFORE_MORNING = 'BM10';
    public const TIME_MIDDAY = 'M10-2';
    public const TIME_EVENING = 'E2-6';
    public const TIME_AFTER_NIGHT = 'AN6';

    public const NOT_PUBLISHED_UNCOMPLETED = 1;
    public const NOT_PUBLISHED_NO_BALANCE = 2;
    public const NOT_PUBLISHED_EXPIRED = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"report","task_list","task_details","user_reviews","task_discussion"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer",options={"comment":"0 draft ,1 published ,2 affected , 3 completed, 4 paid ,5 reviewed,6 cancelled "})
     * @JMS\Groups({"task_list","task_details","user_reviews","task_discussion"})
     */
    private $status;

    /**
     * @var string
     * @Assert\NotBlank(message="task.add.empty_title")
     * @ORM\Column(name="title", type="string",length=255)
     * @JMS\Groups({"report","task_list","task_details","user_reviews","task_discussion"})
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank(message="task.add.empty_description")
     * @ORM\Column(name="description", type="text")
     * @JMS\Groups({"task_list","task_details","user_reviews"})
     */
    private $description;


    /**
     * @var bool
     *
     * @ORM\Column(name="isOnlineWork", type="boolean",options={"default" : 0} )
     * @JMS\Groups({"task_list","task_details"})
     */
    private $isOnlineWork = false;


    /**
     * @var DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     * @JMS\Groups({"task_list","task_details"})
     */
    private $date;


    /**
     * @var DateTime
     *
     * @ORM\Column(name="published_at", type="datetime", nullable=true)
     */
    private $publishedAt;

    /**
     * duration unit is minutes
     * @ORM\Column(name="duration", type="integer" , nullable=true)
     * @JMS\Groups({"task_list","task_details"})
     */
    private $duration;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address",cascade={"persist"})
     * @JMS\Groups({"task_list","task_details"})
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address",cascade={"persist"})
     * @JMS\Groups({"task_list","task_details"})
     */
    private $deliveryAddress;

    /**
     * @var array
     * @ORM\Column(name="times", type="array",nullable=true)
     * @JMS\Groups({"task_list","task_details"})
     */
    private $times;

    /**
     * @var float
     * @ORM\Column(name="price",type="decimal",precision=15, scale=2,options={"default":0})
     * @JMS\Groups({"task_list","task_details"})
     */
    private $price = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="pricingType", type="integer",options={"comment":"1 all amount, etat=2 per hour"})
     * @JMS\Groups({"task_list","task_details"})
     */

    private $pricingType;

    /**
     * @var int
     *
     * @ORM\Column(name="payment_type", type="integer",options={"comment":"1 cash, 2 from wallet"})
     * @JMS\Groups({"task_list","task_details"})
     */
    private $paymentType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payment_type_updated", type="boolean",options={"default":"0"})
     * @JMS\Groups({"task_details"})
     */
    private $paymentTypeUpdated;

    /**
     * @var int
     *
     * @ORM\Column(name="hoursCount", type="integer",options={"default":0})
     * @JMS\Groups({"task_list","task_details"})
     */

    private $hoursCount;

    /**
     * @var int
     *
     * @ORM\Column(name="tasksCount", type="integer",options={"default":1})
     * @JMS\Groups({"task_list","task_details"})
     */

    private $tasksCount;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",inversedBy="tasks" )
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"task_list","task_details"})
     */
    private $user;

    /**
     * @var TaskCategory
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TaskCategory")
     * @ORM\JoinColumn(nullable=true)
     * @JMS\Groups({"task_list","task_details"})
     */
    private $category;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TaskQuestion", mappedBy="task",cascade={"persist"})
     * @JMS\Groups({"task_details"})
     */
    private $questions;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Offer", mappedBy="task",cascade={"persist"})
     * @JMS\Groups({"task_list","task_details"})
     * @ORM\OrderBy({"status" = "ASC"})
     */
    private $offers;

    /**
     * @var TaskDiscussion
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\TaskDiscussion", mappedBy="task",cascade={"persist"})
     * @ORM\JoinColumn(name="discussion_id",referencedColumnName="id",nullable=true)
     * @JMS\Groups({"task_details"})
     */
    private $discussion;

    /**
     * @var Offer
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Offer")
     * @ORM\JoinColumn(name="accepted_offer_id",referencedColumnName="id",nullable=true)
     */
    private $acceptedOffer;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\File", mappedBy="task",cascade={"persist"})
     * @JMS\Groups({"task_list","task_details"})
     */
    private $files;

    /**
     * @var array
     * @ORM\Column(name="must_have", type="array", nullable=true)
     * @JMS\Groups({"task_details"})
     */
    protected $mustHave;

    /**
     * @var string
     * @ORM\Column(name="cancel_reason", type="text", nullable=true)
     */
    protected $cancelReason;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="done_date", type="datetime", nullable=true)
     */
    private $doneDate;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TaskInvoice",mappedBy="task")
     * @JMS\Groups({"task_details"})
     */
    private $invoices;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\CancelTaskRequest",mappedBy="task")
     * @JMS\Groups({"task_details"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $cancelRequest;


    private $offerUpdateRequest = null;

    /**
     * @var float
     * @ORM\Column(name="maatloob_tax",type="decimal",precision=15, scale=2,options={"default":0})
     */
    private $maatloobTax = 0;

    /**
     * @var float
     * @ORM\Column(name="country_tax",type="decimal",precision=15, scale=2,options={"default":0})
     */
    private $countryTax = 0;

    /**
     * @var float
     * @ORM\Column(name="used_credit",type="decimal",precision=15, scale=2,options={"default":0})
     */
    private $usedCredit = 0;

    /**
     * @var int
     * @ORM\Column(name="not_published_reason", type="smallint",options={"default" : 1} )
     * @JMS\Groups({"task_list","task_details"})
     */
    private $notPublishedReason;


    public function __construct()
    {
        parent::__construct();
        $this->files = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->paymentType = self::PAYMENT_TYPE_CASH;
        $this->paymentTypeUpdated = false;
        $this->tasksCount = 1;
        $this->status = self::STATUS_DRAFT;
        $this->hoursCount = 0;
        $this->pricingType = self::PRICE_ALL_AMOUNT;
        $this->notPublishedReason = self::NOT_PUBLISHED_UNCOMPLETED;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Task
     */
    public function setStatus($status)
    {
        $this->status = $status;
        if ($status === self::STATUS_PUBLISHED) {
            $this->publishedAt = new DateTime();
        }elseif ($status === self::STATUS_PAID){
            $this->doneDate = new \DateTime();
        }
        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Task
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Task
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isOnlineWork
     *
     * @param boolean $isOnlineWork
     *
     * @return Task
     */
    public function setIsOnlineWork($isOnlineWork)
    {
        $this->isOnlineWork = $isOnlineWork;

        return $this;
    }

    /**
     * Get isOnlineWork
     *
     * @return boolean
     */
    public function getIsOnlineWork()
    {
        return $this->isOnlineWork;
    }

    /**
     * Set date
     *
     * @param DateTime $date
     *
     * @return Task
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return DateTime
     */
    public function getDate()
    {
        if($this->date){
            $dueDateHourFormat = $this->date->format('H:i:s');
            if($dueDateHourFormat === '00:00:00'){
                $this->date->modify('+23 hour')->modify('+59 minute')->modify('+59 second');
            }
        }
        return $this->date;
    }

    /**
     * Set times
     *
     * @param array $time
     *
     */
    public function setTimes(array $times)
    {
        $this->times = $times;
    }

    /**
     * Get times
     *
     * @return array
     */
    public function getTimes()
    {
        return $this->times ?? [];
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Task
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @param Country $country
     * @return float
     */
    public function getPrice(Country $country=null) : float
    {
        if($country && ($country->getCurrencyCode() !== $this->user->getCountry()->getCurrencyCode())){
            return (float)number_format($this->user->getCountry()->getCurrencyExchange($country->getCurrencyCode())->getRate() * $this->price, 2,'.','');
        }
        return (float)number_format($this->price, 2,'.','');
    }

    /**
     * Set pricingType
     *
     * @param integer $pricingType
     *
     * @return Task
     */
    public function setPricingType($pricingType)
    {
        $this->pricingType = $pricingType;

        return $this;
    }

    /**
     * Get pricingType
     *
     * @return integer
     */
    public function getPricingType()
    {
        return $this->pricingType;
    }

    /**
     * Set hoursCount
     *
     * @param integer $hoursCount
     *
     * @return Task
     */
    public function setHoursCount($hoursCount)
    {
        $this->hoursCount = $hoursCount;

        return $this;
    }

    /**
     * Get hoursCount
     *
     * @return integer
     */
    public function getHoursCount()
    {
        return $this->hoursCount;
    }

    /**
     * Set tasksCount
     *
     * @param integer $tasksCount
     *
     * @return Task
     */
    public function setTasksCount($tasksCount)
    {
        $this->tasksCount = $tasksCount;

        return $this;
    }

    /**
     * Get tasksCount
     *
     * @return integer
     */
    public function getTasksCount()
    {
        return $this->tasksCount;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Task
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set category
     *
     * @param TaskCategory $category
     *
     * @return Task
     */
    public function setCategory(TaskCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return TaskCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add question
     *
     * @param TaskQuestion $question
     *
     * @return Task
     */
    public function addQuestion(TaskQuestion $question)
    {
        $this->questions->add($question);

        return $this;
    }

    /**
     * Remove question
     *
     * @param TaskQuestion $question
     */
    public function removeQuestion(TaskQuestion $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add offer
     *
     * @param Offer $offer
     *
     * @return Task
     */
    public function addOffer(Offer $offer)
    {
        $this->offers->add($offer);

        return $this;
    }

    /**
     * Remove offer
     *
     * @param Offer $offer
     */
    public function removeOffer(Offer $offer)
    {
        $this->offers->removeElement($offer);
    }


    /**
     * @return ArrayCollection
     */
    public function getOffers()
    {
        return $this->offers;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param array $mustHave
     */
    public function setMustHave(array $mustHave)
    {
        $this->mustHave = $mustHave;
    }

    /**
     * @return array
     */
    public function getMustHave()
    {
        return $this->mustHave;
    }

    /**
     * @return ArrayCollection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param ArrayCollection $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    public function addFile(File $file)
    {
        $file->setTask($this);
        $this->files->add($file);
    }

    /**
     * @return int
     */
    public function getPaymentType(): int
    {
        return $this->paymentType;
    }

    /**
     * @param int $paymentType
     */
    public function setPaymentType(?int $paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return string
     */
    public function getCancelReason(): ?string
    {
        return $this->cancelReason;
    }

    /**
     * @param string $cancelReason
     */
    public function setCancelReason(string $cancelReason)
    {
        $this->cancelReason = $cancelReason;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * @param mixed $deliveryAddress
     */
    public function setDeliveryAddress($deliveryAddress): void
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * @param ArrayCollection $questions
     */
    public function setQuestions(ArrayCollection $questions): void
    {
        $this->questions = $questions;
    }

    /**
     * @param ArrayCollection $offers
     */
    public function setOffers(ArrayCollection $offers): void
    {
        $this->offers = $offers;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return ArrayCollection
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * @param mixed $invoices
     */
    public function setInvoices($invoices): void
    {
        $this->invoices = $invoices;
    }

    /**
     * @param TaskInvoice $invoice
     */
    public function addInvoice(TaskInvoice $invoice): void
    {
        $invoice->setTask($this);
        $this->invoices->add($invoice);
    }

    /**
     * @return DateTime
     */
    public function getDoneDate(): ?DateTime
    {
        return $this->doneDate;
    }

    /**
     * @param DateTime $doneDate
     */
    public function setDoneDate(DateTime $doneDate): void
    {
        $this->doneDate = $doneDate;
    }

    /**
     * Check if task is delayed
     * @return bool
     * @throws Exception
     */
    public function isDelayed()
    {
        return ($this->status == self::STATUS_ASSIGNED && $this->date <= new DateTime())
            || ($this->doneDate > $this->date);
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups("task_details")
     */
    public function getOfferRequest()
    {
        return $this->offerUpdateRequest;
    }

    /**
     * @param OfferUpdateRequest $OfferUpdateRequest
     */
    public function setOfferRequest(OfferUpdateRequest $OfferUpdateRequest)
    {
        $this->offerUpdateRequest = $OfferUpdateRequest;
    }

    /** Check task status equal
     * @param int $status
     * @return bool
     */
    public function is(int $status): bool
    {
        return $this->status === $status;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration ?? 0;
    }

    /**
     * @param int $duration
     */
    public function setDuration($duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return bool
     */
    public function isPaymentTypeUpdated(): bool
    {
        return $this->paymentTypeUpdated;
    }

    /**
     * @param bool $paymentTypeUpdated
     */
    public function setPaymentTypeUpdated(bool $paymentTypeUpdated): void
    {
        $this->paymentTypeUpdated = $paymentTypeUpdated;
    }

    /**
     * @param Country|null $country
     * @return float
     */
    public function getMaatloobTax(Country $country=null): ?float
    {
        if($country && ($country->getCurrencyCode() !== $this->user->getCountry()->getCurrencyCode())){
            return $this->user->getCountry()->getCurrencyExchange($country->getCurrencyCode())->getRate() * $this->maatloobTax;
        }
        return $this->maatloobTax;
    }

    /**
     * @param float $maatloobTax
     */
    public function setMaatloobTax(float $maatloobTax): void
    {
        $this->maatloobTax = $maatloobTax;
    }

    /**
     * @param Country|null $country
     * @return float
     */
    public function getCountryTax(Country $country=null): ?float
    {
        if($country && ($country->getCurrencyCode() !== $this->user->getCountry()->getCurrencyCode())){
            return $this->user->getCountry()->getCurrencyExchange($country->getCurrencyCode())->getRate() * $this->countryTax;
        }
        return $this->countryTax;
    }

    /**
     * @param float $countryTax
     */
    public function setCountryTax(float $countryTax): void
    {
        /** Check if Country tax included in maatloob tax */
        $isIncluded = $this->getUser()->getCountry()->getConfig()->isIncludedTax();
        if($isIncluded){
            $this->maatloobTax -=$countryTax;
        }
        $this->countryTax = $countryTax;
    }

    /**
     * @return CancelTaskRequest
     */
    public function getCancelRequest(): ?CancelTaskRequest
    {
        return $this->cancelRequest;
    }

    /**
     * @param CancelTaskRequest $cancelRequest
     */
    public function setCancelRequest(CancelTaskRequest $cancelRequest): void
    {
        $this->cancelRequest = $cancelRequest;
    }

    /**
     * @return DateTime
     */
    public function getPublishedAt(): DateTime
    {
        return $this->publishedAt;
    }

    /**
     * @param DateTime $publishedAt
     */
    public function setPublishedAt(DateTime $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }

    /**
     * @return User|null
     */
    public function getTasker(): ?User
    {
        return $this->getAcceptedOffer() ? $this->getAcceptedOffer()->getUser() : null;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups("task_details")
     */
    public function canBeCancelledWithoutRequest(): bool
    {
        return $this->is(self::STATUS_PUBLISHED) ||
            ($this->is(self::STATUS_ASSIGNED) &&
                $this->category->isDelivery() &&
                $this->getAcceptedOffer()->getAcceptedAt()->modify('+10 minute') > new DateTime());
    }

    /**
     * @return float
     */
    public function getUsedCredit(): ?float
    {
        return $this->usedCredit;
    }

    /**
     * @param float $usedCredit
     */
    public function setUsedCredit(float $usedCredit): void
    {
        $this->usedCredit = $usedCredit;
    }

    /**
     * @return TaskDiscussion
     */
    public function getDiscussion(): ?TaskDiscussion
    {
        return $this->discussion;
    }

    /**
     * @param TaskDiscussion $discussions
     */
    public function setDiscussion(TaskDiscussion $discussions): void
    {
        $this->discussion = $discussions;
    }

    /**
     * @param Offer $acceptedOffer
     * @return Task
     */
    public function setAcceptedOffer(Offer $acceptedOffer): Task
    {
        $this->setStatus(self::STATUS_ASSIGNED);
        $this->acceptedOffer = $acceptedOffer;
        return $this;
    }


    /**
     * @return Offer|null
     */
    public function getAcceptedOffer(): ?Offer
    {
       return $this->acceptedOffer;
    }
     /**
     * @return int
     */
    public function getNotPublishedReason(): int
    {
        return $this->notPublishedReason;
    }

    /**
     * @param int $notPublishedReason
     */
    public function setNotPublishedReason(int $notPublishedReason): void
    {
        $this->notPublishedReason = $notPublishedReason;
    }

    public function isTaskExpired() : bool
    {
        return $this->getDate()<new \DateTime();
    }

    public function isInvoicePayed(): bool
    {
        /** @var TaskInvoice $invoice */
        foreach ($this->invoices as $invoice){
            if($invoice->getPrice()>0 && $invoice->getStatus()===TaskInvoice::STATUS_PAYED) {
                return true;
            }
        }
        return false;
    }
}
