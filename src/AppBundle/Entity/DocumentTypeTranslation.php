<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * DocumentTypeTranslation
 * @ORM\Entity
 */
class DocumentTypeTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"user_documents"})
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     * @Groups({"user_documents"})
     */
    private $description;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }


    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }
}

