<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * LanguageMessage
 *
 * @ORM\Table(name="language_message",
 *       uniqueConstraints={
 *        @ORM\UniqueConstraint(name="message_unique", columns={"identifier", "language"})
 *    })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LanguageMessageRepository")
 */
class LanguageMessage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string")
     * @JMS\Groups({"app_language_messages"})
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text",nullable=true)
     * @JMS\Groups({"app_language_messages"})
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Language", inversedBy="messages",cascade={"remove"})
     * @ORM\JoinColumn(name="language", referencedColumnName="identifier")
     */
    private $language;


    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language): void
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier(string $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}
