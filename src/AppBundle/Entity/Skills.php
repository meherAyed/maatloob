<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Skills
 *
 * @ORM\Table(name="skills")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SkillsRepository")
 */
class Skills extends Timestampable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var array
     * @ORM\Column(name="good_at", type="array",nullable=true)
     * @JMS\Groups({"user_data"})
     * @JMS\Accessor(getter="getGoodAt")
     */
    private $goodAt;
    
     /**
     * @var array
     * @ORM\Column(name="get_around", type="array",nullable=true)
     * @JMS\Groups({"user_data"})
     * @JMS\Accessor(getter="getGetAround")
     */
    private $getAround;
    
    /**
     * @var array
     * @ORM\Column(name="languages", type="array",nullable=true)
     * @JMS\Groups({"user_data"})
     * @JMS\Accessor(getter="getLanguages")
     */
    private $languages;  
    
    /**
     * @var array
     * @ORM\Column(name="qualifications", type="array",nullable=true)
     * @JMS\Groups({"user_data"})
     * @JMS\Accessor(getter="getQualifications")
     */
    private $qualifications ;   
    
    /**
     * @var array
     * @ORM\Column(name="experience", type="array",nullable=true)
     * @JMS\Groups({"user_data"})
     * @JMS\Accessor(getter="getExperience")
     */
    private $experience ; 
    
    /**
     * @var array
     * @ORM\Column(name="education", type="array",nullable=true)
     * @JMS\Groups({"user_data"})
     * @JMS\Accessor(getter="getEducation")
     */
    private $education;
    
    /**
     * @var array
     * @ORM\Column(name="work", type="array",nullable=true)
     * @JMS\Groups({"user_data"})
     * @JMS\Accessor(getter="getWork")
     */
    private $work;
    
    /**
     * Set goodAt
     *
     * @param array $goodAt
     *
     * @return Skills
     */
    public function setGoodAt($goodAt)
    {
        $this->goodAt = $goodAt;

        return $this;
    }

    /**
     * Get goodAt
     *
     * @return array
     */
    public function getGoodAt()
    {
        return $this->goodAt??[];
    }

    /**
     * Set getAround
     *
     * @param array $getAround
     *
     * @return Skills
     */
    public function setGetAround($getAround)
    {
        $this->getAround = $getAround;

        return $this;
    }

    /**
     * Get getAround
     *
     * @return array
     */
    public function getGetAround()
    {
        return $this->getAround??[];
    }

    /**
     * Set languages
     *
     * @param array $languages
     *
     * @return Skills
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;

        return $this;
    }

    /**
     * Get languages
     *
     * @return array
     */
    public function getLanguages()
    {
        return $this->languages??[];
    }

    /**
     * Set qualifications
     *
     * @param array $qualifications
     *
     * @return Skills
     */
    public function setQualifications($qualifications)
    {
        $this->qualifications = $qualifications;

        return $this;
    }

    /**
     * Get qualifications
     *
     * @return array
     */
    public function getQualifications()
    {
        return $this->qualifications??[];
    }

    /**
     * Set experience
     *
     * @param array $experience
     *
     * @return Skills
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return array
     */
    public function getExperience()
    {
        return $this->experience??[];
    }


    /**
     * Set education
     *
     * @param array $education
     *
     * @return Skills
     */
    public function setEducation($education)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * Get education
     *
     * @return array
     */
    public function getEducation()
    {
        return $this->education??[];
    }

    /**
     * Set work
     *
     * @param array $work
     *
     * @return Skills
     */
    public function setWork($work)
    {
        $this->work = $work;

        return $this;
    }

    /**
     * Get work
     *
     * @return array
     */
    public function getWork()
    {
        return $this->work??[];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
