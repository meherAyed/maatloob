<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints\Count;

/**
 * SystemConfig
 *
 * @ORM\Table(name="system_config")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SystemConfigRepository")
 */
class SystemConfig
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Country",inversedBy="config")
     * @ORM\JoinColumn(referencedColumnName="identifier")
     */
    private $country;

    /**
     * @var array
     * @ORM\Column(name="task_profits",type="array")
     * @JMS\Groups({"app_config"})
     */
    private $taskProfits;

    /**
     * @var float
     * @ORM\Column(name="max_task_profits",type="decimal",options={"default":0})
     * @JMS\Groups({"app_config"})
     */
    private $maxTaskProfits;

    /**
     * @var float
     * @ORM\Column(name="country_tax",type="decimal",options={"default":0})
     * @JMS\Groups({"app_config"})
     */
    private $countryTax;

    /**
     * @var int
     * @ORM\Column(name="show_reviews_after",type="integer",options={"default":0})
     */
    private $showReviewsAfter;

    /**
     * @var int
     * @ORM\Column(name="allowed_task_offer_before_verification",type="integer",options={"default":0})
     */
    private $allowedTaskOfferBeforeVerification;

    /**
     * @var int
     * @ORM\Column(name="enable_maatloob_profits_after_days",type="integer",options={"default":0})
     */
    private $enableMaatloobProfitsAfterDays;

    /**
     * @var array
     * @ORM\Column(name="filter_price",type="array")
     * @JMS\Groups({"app_config"})
     */
    private $filterPrice;

    /**
     * @var array
     * @ORM\Column(name="filter_distance",type="array")
     * @JMS\Groups({"app_config"})
     */
    private $filterDistance;

    /**
     * @var int
     * @ORM\Column(name="min_accepted_task_budget",type="integer",options={"default":1})
     */
    private $minAcceptedTaskBudget;

    /**
     * @var float
     * @ORM\Column(name="min_allowed_balance_recharge",type="decimal",options={"default":1})
     */
    private $minAllowedBalanceRecharge;

    /**
     * @var float
     * @ORM\Column(name="allow_withdraw_balance_from",type="decimal",options={"default":1})
     */
    private $allowWithdrawBalanceFrom;

    /**
     * @var int
     * @ORM\Column(name="min_mandatory_document_to_verify_account",type="smallint",options={"default":2})
     */
    private $minMandatoryDocumentToVerifyAccount;


    /**
     * @var int
     * @ORM\Column(name="referral_to_user_limit",type="smallint",options={"default":15})
     */
    private $referralToUserLimit;

    /**
     * @var int
     * @ORM\Column(name="referral_credit",type="integer",options={"default":25})
     */
    private $referralCredit;

    /**
     * @var int
     * @ORM\Column(name="referring_credit",type="integer",options={"default":10})
     */
    private $referringCredit;

    /**
     * @var int
     * @ORM\Column(name="max_allowed_task_cancel",type="smallint",options={"default":3})
     */
    private $maxAllowedTaskCancel;

    /**
     * @var int
     * @ORM\Column(name="task_cancel_ban_duration",type="integer",options={"default":3})
     */
    private $taskCancelBanDuration;

    /**
     * @var int
     * @ORM\Column(name="min_allowed_maatloob_balance",type="integer",options={"default":-10})
     */
    private $minAllowedMaatloobBalance;

    /**
     * @var string|null
     * @ORM\Column(name="invoice_template", type="text", nullable=true)
     */
    private $invoiceTemplate;

    /**
     * @var boolean
     * @ORM\Column(name="included_tax", type="boolean", options={"default":0})
     */
    private $includedTax;

    /**
     * @var string
     * @ORM\Column(name="tax_identifier", type="string", length=100 , nullable=true)
     */
    private $taxIdentifier;

    /**
     * @var int
     * @ORM\Column(name="transfer_fee_bank",type="integer",options={"default":0})
     * @JMS\Groups({"app_config"})
     */
    private $transferFeeBank;

    /**
     * @var int
     * @ORM\Column(name="transfer_fee_ewallet",type="integer",options={"default":0})
     * @JMS\Groups({"app_config"})
     */
    private $transferFeeEWallet;

    /**
     * @var array
     * @ORM\Column(name="supported_payment_brand", type="array",nullable=true)
     * @JMS\Groups({"app_config"})
     */
    private $supportedPaymentBrand;

    /**
     * @var array
     * @ORM\Column(name="task_complete_tips", type="array",nullable=true)
     * @JMS\Groups({"app_config"})
     */
    private $taskCompleteTips;

    public function __construct()
    {
        $this->taskProfits = [];
        $this->showReviewsAfter = 10;
        $this->allowedTaskOfferBeforeVerification = 40;
        $this->enableMaatloobProfitsAfterDays = 180;
        $this->filterPrice = ['min'=>5 ,'max'=> 9999];
        $this->filterDistance = ['min'=>5 ,'max'=>100];
        $this->maxTaskProfits = 0;
        $this->countryTax = 0;
        $this->minMandatoryDocumentToVerifyAccount = 2;
        $this->minAcceptedTaskBudget = 1;
        $this->minAllowedBalanceRecharge = 1;
        $this->allowWithdrawBalanceFrom = 1;
        $this->includedTax = false;
        $this->transferFeeBank = 0;
        $this->transferFeeEWallet = 0;
        $this->supportedPaymentBrand = [];
        $this->taskCompleteTips = [];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return float
     */
    public function getCountryTax(): ?float
    {
        return $this->countryTax;
    }

    /**
     * @param float $countryTax
     */
    public function setCountryTax(float $countryTax): void
    {
        $this->countryTax = $countryTax;
    }


    /**
     * @return int
     */
    public function getAllowedTaskOfferBeforeVerification(): ?int
    {
        return $this->allowedTaskOfferBeforeVerification;
    }

    /**
     * @param int $allowedTaskOfferBeforeVerification
     */
    public function setAllowedTaskOfferBeforeVerification(int $allowedTaskOfferBeforeVerification): void
    {
        $this->allowedTaskOfferBeforeVerification = $allowedTaskOfferBeforeVerification;
    }

    /**
     * @return int
     */
    public function getEnableMaatloobProfitsAfterDays(): int
    {
        return $this->enableMaatloobProfitsAfterDays;
    }

    /**
     * @param int $enableMaatloobProfitsAfterDays
     */
    public function setEnableMaatloobProfitsAfterDays(int $enableMaatloobProfitsAfterDays): void
    {
        $this->enableMaatloobProfitsAfterDays = $enableMaatloobProfitsAfterDays;
    }

    /**
     * @return array
     */
    public function getFilterPrice(): array
    {
        return $this->filterPrice;
    }

    /**
     * @param array $filterPrice
     */
    public function setFilterPrice(array $filterPrice): void
    {
        $this->filterPrice = $filterPrice;
    }

    /**
     * @return array
     */
    public function getFilterDistance(): array
    {
        return $this->filterDistance;
    }

    /**
     * @param array $filterDistance
     */
    public function setFilterDistance(array $filterDistance): void
    {
        $this->filterDistance = $filterDistance;
    }

    /**
     * @return array
     */
    public function getTaskProfits(): array
    {
        return $this->taskProfits;
    }

    /**
     * @param array $taskProfits
     */
    public function setTaskProfits(array $taskProfits): void
    {
        $this->taskProfits = $taskProfits;
    }

    /**
     * @return float
     */
    public function getMaxTaskProfits(): ?float
    {
        return $this->maxTaskProfits;
    }

    /**
     * @param float $maxTaskProfits
     */
    public function setMaxTaskProfits(float $maxTaskProfits): void
    {
        $this->maxTaskProfits = $maxTaskProfits;
    }

    /**
     * @return int
     */
    public function getShowReviewsAfter(): ?int
    {
        return $this->showReviewsAfter;
    }

    /**
     * @param int $showReviewsAfter
     */
    public function setShowReviewsAfter(int $showReviewsAfter): void
    {
        $this->showReviewsAfter = $showReviewsAfter;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getAllowWithdrawBalanceFrom(): float
    {
        return $this->allowWithdrawBalanceFrom;
    }

    /**
     * @param float $allowWithdrawBalanceFrom
     */
    public function setAllowWithdrawBalanceFrom(float $allowWithdrawBalanceFrom): void
    {
        $this->allowWithdrawBalanceFrom = $allowWithdrawBalanceFrom;
    }

    /**
     * @return float
     */
    public function getMinAllowedBalanceRecharge(): float
    {
        return $this->minAllowedBalanceRecharge;
    }

    /**
     * @param float $minAllowedBalanceRecharge
     */
    public function setMinAllowedBalanceRecharge(float $minAllowedBalanceRecharge): void
    {
        $this->minAllowedBalanceRecharge = $minAllowedBalanceRecharge;
    }

    /**
     * @return int
     */
    public function getMinAcceptedTaskBudget(): int
    {
        return $this->minAcceptedTaskBudget;
    }

    /**
     * @param int $minAcceptedTaskBudget
     */
    public function setMinAcceptedTaskBudget(int $minAcceptedTaskBudget): void
    {
        $this->minAcceptedTaskBudget = $minAcceptedTaskBudget;
    }

    /**
     * @return int
     */
    public function getMinMandatoryDocumentToVerifyAccount(): int
    {
        return $this->minMandatoryDocumentToVerifyAccount;
    }

    /**
     * @param int $minMandatoryDocumentToVerifyAccount
     */
    public function setMinMandatoryDocumentToVerifyAccount(int $minMandatoryDocumentToVerifyAccount): void
    {
        $this->minMandatoryDocumentToVerifyAccount = $minMandatoryDocumentToVerifyAccount;
    }

    /**
     * @return int
     */
    public function getReferralToUserLimit(): ?int
    {
        return $this->referralToUserLimit;
    }

    /**
     * @param int $referralToUserLimit
     */
    public function setReferralToUserLimit(int $referralToUserLimit): void
    {
        $this->referralToUserLimit = $referralToUserLimit;
    }

    /**
     * @return int
     */
    public function getReferralCredit(): ?int
    {
        return $this->referralCredit;
    }

    /**
     * @param int $referralCredit
     */
    public function setReferralCredit(int $referralCredit): void
    {
        $this->referralCredit = $referralCredit;
    }

    /**
     * @return int
     */
    public function getReferringCredit(): ?int
    {
        return $this->referringCredit;
    }

    /**
     * @param int $referringCredit
     */
    public function setReferringCredit(int $referringCredit): void
    {
        $this->referringCredit = $referringCredit;
    }

    /**
     * @return int
     */
    public function getMaxAllowedTaskCancel(): ?int
    {
        return $this->maxAllowedTaskCancel;
    }

    /**
     * @param int $maxAllowedTaskCancel
     */
    public function setMaxAllowedTaskCancel(int $maxAllowedTaskCancel): void
    {
        $this->maxAllowedTaskCancel = $maxAllowedTaskCancel;
    }

    /**
     * @return int
     */
    public function getTaskCancelBanDuration(): ?int
    {
        return $this->taskCancelBanDuration;
    }

    /**
     * @param int $taskCancelBanDuration
     */
    public function setTaskCancelBanDuration(int $taskCancelBanDuration): void
    {
        $this->taskCancelBanDuration = $taskCancelBanDuration;
    }

    /**
     * @return int
     */
    public function getMinAllowedMaatloobBalance(): ?int
    {
        return $this->minAllowedMaatloobBalance;
    }

    /**
     * @param int $minAllowedMaatloobBalance
     */
    public function setMinAllowedMaatloobBalance(int $minAllowedMaatloobBalance): void
    {
        $this->minAllowedMaatloobBalance = $minAllowedMaatloobBalance;
    }

    /**
     * @return string|null
     */
    public function getInvoiceTemplate(): ?string
    {
        return $this->invoiceTemplate;
    }

    /**
     * @param string|null $invoiceTemplate
     */
    public function setInvoiceTemplate(?string $invoiceTemplate): void
    {
        $this->invoiceTemplate = $invoiceTemplate;
    }

    /**
     * @return bool
     */
    public function isIncludedTax(): bool
    {
        return $this->includedTax;
    }

    /**
     * @param bool $includedTax
     */
    public function setIncludedTax(bool $includedTax): void
    {
        $this->includedTax = $includedTax;
    }

    /**
     * @return string
     */
    public function getTaxIdentifier(): ?string
    {
        return $this->taxIdentifier;
    }

    /**
     * @param string $taxIdentifier
     */
    public function setTaxIdentifier(?string $taxIdentifier): void
    {
        $this->taxIdentifier = $taxIdentifier;
    }

    /**
     * @return array
     */
    public function getSupportedPaymentBrand(): ?array
    {
        return $this->supportedPaymentBrand??[];
    }

    /**
     * @param array $supportedPaymentBrand
     */
    public function setSupportedPaymentBrand(array $supportedPaymentBrand): void
    {
        $this->supportedPaymentBrand = $supportedPaymentBrand;
    }

    /**
     * @return int
     */
    public function getTransferFeeBank(): int
    {
        return $this->transferFeeBank;
    }

    /**
     * @param int $transferFeeBank
     */
    public function setTransferFeeBank(int $transferFeeBank): void
    {
        $this->transferFeeBank = $transferFeeBank;
    }

    /**
     * @return int
     */
    public function getTransferFeeEWallet(): int
    {
        return $this->transferFeeEWallet;
    }

    /**
     * @param int $transferFeeEWallet
     */
    public function setTransferFeeEWallet(int $transferFeeEWallet): void
    {
        $this->transferFeeEWallet = $transferFeeEWallet;
    }

    /**
     * @return array
     */
    public function getTaskCompleteTips(): array
    {
        return $this->taskCompleteTips??[0 => 0 , 1 => 0 , 2 => 0];
    }

    /**
     * @param array $taskCompleteTips
     */
    public function setTaskCompleteTips(array $taskCompleteTips): void
    {
        $this->taskCompleteTips = $taskCompleteTips;
    }

}

