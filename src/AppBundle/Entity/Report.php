<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Report
 *
 * @property ORM\Entity object
 * @ORM\Table(name="report")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReportRepository")
 */
class Report extends Timestampable
{
    public const STATUS_UNTREATED = 0;
    public const STATUS_TREATED = 1;
    public const STATUS_REFUSED = 2;

    public const ACTION_WARN= 'ACTION_WARN';
    public const ACTION_BAN = 'ACTION_BAN';
    public const ACTION_REMOVE_STAR = 'ACTION_REMOVE_STAR';
    public const ACTION_REFUSE = 'ACTION_REFUSE';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"report"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"report"})
     */
    private $reportedUser;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Reporter" , mappedBy="report",cascade={"persist","remove"})
     * @JMS\Groups({"report"})
     */
    private $reporters;


    /**
     * @var int
     *
     * @ORM\Column(name="object_id", type="integer")
     */
    private $objectId;

    /**
     * @var ReportType
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ReportType")
     * @JMS\Groups({"report"})
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(name="section", type="string")
     * @JMS\Groups({"report"})
     */
    private $section;

    /**
     * @var string
     * @ORM\Column(name="refuse_reason", type="string",length=255,nullable=true)
     */
    private $refuseReason;

    /**
     * @var int
     * @ORM\Column(name="status",type="smallint" ,options={"comment":"0 untreated,1 accpeted, 2 refused"})
     * @JMS\Groups({"report"})
     */
    private $status;

    /**
     * @var array
     * @ORM\Column(name="taken_actions", type="array",nullable=true)
     */
    private $takenActions;


    public function __construct()
    {
        parent::__construct();
        $this->status = Report::STATUS_UNTREATED;
        $this->reporters = new ArrayCollection();
    }

    /**
     * Set objectId
     *
     * @param integer $objectId
     *
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
    }

    /**
     * Get objectId
     *
     * @return integer
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ReportType
     */
    public function getType(): ReportType
    {
        return $this->type;
    }

    /**
     * @param ReportType $type
     */
    public function setType(ReportType $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSection(): string
    {
        return $this->section;
    }

    /**
     * @param string $section
     */
    public function setSection(string $section): void
    {
        $this->section = $section;
    }

    public function setObject($entity){
        $this->object = $entity;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups("report")
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @return User
     */
    public function getReportedUser()
    {
        return $this->reportedUser;
    }

    /**
     * @param mixed $reportedUser
     */
    public function setReportedUser($reportedUser): void
    {
        $this->reportedUser = $reportedUser;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }


    /**
     * @return mixed
     */
    public function getReporters()
    {
        return $this->reporters;
    }

    /**
     * @param mixed $reporters
     */
    public function setReporters($reporters): void
    {
        $this->reporters = $reporters;
    }

    /**
     * @return array
     */
    public function getTakenActions(): array
    {
        return $this->takenActions;
    }

    /**
     * @param array $takenActions
     */
    public function setTakenActions(array $takenActions): void
    {
        $this->takenActions = $takenActions;
    }

    /**
     * @return string
     */
    public function getRefuseReason(): string
    {
        return $this->refuseReason;
    }

    /**
     * @param string $refuseReason
     */
    public function setRefuseReason(string $refuseReason): void
    {
        $this->refuseReason = $refuseReason;
    }

}
