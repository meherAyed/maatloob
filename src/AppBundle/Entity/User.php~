<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @UniqueEntity("email", message="email alerady exists.")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
     protected $email;
    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255,nullable=true)
     */
    private $first_name;
    
     /**
     * @var string
     *
     * @ORM\Column(name="users_rate", type="integer", nullable=true,options={"default" : 0})
     */
    private $users_rate=0;
    
    /**
     * @var string
     *
     * @ORM\Column(name="profile_completion", type="integer", nullable=false,options={"default" : 0})
     */
    private $profile_completion=0;
    
     /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255,nullable=true)
     */
    private $last_name;
     /**
     * @var string
     *
     * @ORM\Column(name="tagline", type="string", length=255,nullable=true)
     */
    private $tagline;
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255,nullable=true)
     */
    private $phone;
   /**
     * @var string
     *
     * @ORM\Column(name="suburb", type="string", length=255,nullable=true)
     */
    private $suburb;
    
    /**
     * @var string
     *
     * @ORM\Column(name="about", type="text",nullable=true)
     */
    private $about;
   /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255,nullable=true)
     */
    private $location;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="datetime",nullable=true)
     */
    private $birth_date;
    
  /**
   * @ORM\ManyToOne(targetEntity="AppBundle\Entity\File")
   * @ORM\JoinColumn(nullable=true)
   */
    private $picture;
    /**
   * @ORM\ManyToOne(targetEntity="AppBundle\Entity\File")
   * @ORM\JoinColumn(nullable=true)
   */
    private $coverPicture;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime",nullable=true)
     */
    private $createdAt;
    /**
     * @var int
     *
     * @ORM\Column(name="userType", type="integer",nullable=true,options={"comment":"type=1 tasker, type=0 poster"})
     */
	
     private $userType;
    public function __construct()
    {
        $this->createdAt=new \DateTime();
        parent::__construct();
        // your own logic
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set tagline
     *
     * @param string $tagline
     *
     * @return User
     */
    public function setTagline($tagline)
    {
        $this->tagline = $tagline;

        return $this;
    }

    /**
     * Get tagline
     *
     * @return string
     */
    public function getTagline()
    {
        return $this->tagline;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return User
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return User
     */
    public function setBirthDate($birthDate)
    {
        $this->birth_date = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * Set picture
     *
     * @param \AppBundle\Entity\File $picture
     *
     * @return User
     */
    public function setPicture($picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return \AppBundle\Entity\File
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set coverPicture
     *
     * @param \AppBundle\Entity\File $coverPicture
     *
     * @return User
     */
    public function setCoverPicture(\AppBundle\Entity\File $coverPicture = null)
    {
        $this->coverPicture = $coverPicture;

        return $this;
    }

    /**
     * Get coverPicture
     *
     * @return \AppBundle\Entity\File
     */
    public function getCoverPicture()
    {
        return $this->coverPicture;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set suburb
     *
     * @param string $suburb
     *
     * @return User
     */
    public function setSuburb($suburb)
    {
        $this->suburb = $suburb;

        return $this;
    }

    /**
     * Get suburb
     *
     * @return string
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set about
     *
     * @param string $about
     *
     * @return User
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set userType
     *
     * @param integer $userType
     *
     * @return User
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * Get userType
     *
     * @return integer
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Set usersRate
     *
     * @param integer $usersRate
     *
     * @return User
     */
    public function setUsersRate($usersRate)
    {
        $this->users_rate = $usersRate;

        return $this;
    }

    /**
     * Get usersRate
     *
     * @return integer
     */
    public function getUsersRate()
    {
        return $this->users_rate;
    }

    /**
     * Set profileCompletion
     *
     * @param integer $profileCompletion
     *
     * @return User
     */
    public function setProfileCompletion($profileCompletion)
    {
        $this->profile_completion = $profileCompletion;

        return $this;
    }

    /**
     * Get profileCompletion
     *
     * @return integer
     */
    public function getProfileCompletion()
    {
        return $this->profile_completion;
    }
}
