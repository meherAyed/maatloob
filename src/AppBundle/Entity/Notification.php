<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NotificationRepository")
 */
class Notification
{
    public const ACTION_TRANSACTION = 'TRANS';
    public const ACTION_TASK_UPDATE = 'TSK_UPDT';
    public const ACTION_TASK_REMINDER = 'TSK_REM';
    public const ACTION_MAATLOOB_ALERT = 'M_ALERT';
    public const ACTION_TASK_RECOMMENDATION = 'TSK_RECOM';
    public const ACTION_HELP_INFO = 'HELP_INFO';
    public const ACTION_UPDATE_NEWS = 'UPDT_NEWS';

    public const ADMIN_NEW_REPORT = 'NEW_REPORT';
    public const ADMIN_NEW_DOCUMENT = 'NEW_DOCUMENT';
    public const ADMIN_NEW_WITHDRAW_REQUEST = 'NEW_WITHDRAW_REQUEST';

    public const TYPE_TASK = 'task';
    public const TYPE_TRANS = 'transaction';
    public const TYPE_MSG = 'message';
    public const TYPE_REVIEW = 'review';
    public const TYPE_DOCUMENT_VERIFY = 'document';
    public const TYPE_WITHDRAW = 'withdraw';
    public const TYPE_FILL_WALLET = 'fill_wallet';
    public const TYPE_NEWS = 'news_and_update';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"user_notifications"})
     */
    private $id;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     * @JMS\Groups({"user_notifications"})
     */
    private $fromUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $toUser;

    /**
     * @var bool
     *
     * @ORM\Column(name="read_on", type="boolean")
     * @JMS\Groups({"user_notifications"})
     */
    private $readOn;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     * @JMS\Groups({"user_notifications"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", length=255)
     * @JMS\Groups({"user_notifications"})
     */
    private $body;

    /**
     * @var array
     *
     * @ORM\Column(name="params", type="array", nullable=true,options={"comment":"param to generate notification body for different language"})
     */
    private $params;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255,nullable=true)
     * @JMS\Groups({"user_notifications"})
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255,nullable=true)
     * @JMS\Groups({"user_notifications"})
     */
    private $type;

    /**
     * @var bool
     *
     * @ORM\Column(name="shown_on", type="boolean")
     * @JMS\Groups({"user_notifications"})
     */
    private $shownOn;

    /**
     * @var array
     *
     * @ORM\Column(name="extra", type="array", nullable=true,options={"comment":"may contain id for related entities like offre_id task_id"})
     */
    private $extra;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="read_date", type="datetime", nullable=true)
     */
    private $readDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @JMS\Groups({"user_notifications"})
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255,nullable=true)
     * @JMS\Groups({"user_notifications"})
     */
    private $icon;

    public function __construct()
    {
        $this->readOn = false;
        $this->shownOn = false;
        $this->createdAt = new \DateTime();
        $this->setExtra(['type'=> '']);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * Set readOn
     *
     * @param boolean $readOn
     *
     * @return Notification
     */
    public function setReadOn($readOn)
    {
        $this->readOn = $readOn;

        return $this;
    }

    /**
     * Get readOn
     *
     * @return bool
     */
    public function isReadOn()
    {
        return $this->readOn;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Notification
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set link
     *
     * @param string $link
     *
     * @return Notification
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Notification
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set shownOn
     *
     * @param boolean $shownOn
     *
     * @return Notification
     */
    public function setShownOn($shownOn)
    {
        $this->shownOn = $shownOn;

        return $this;
    }

    /**
     * Get shownOn
     *
     * @return bool
     */
    public function isShownOn()
    {
        return $this->shownOn;
    }

    /**
     * Set readDate
     *
     * @param \DateTime $readDate
     *
     * @return Notification
     */
    public function setReadDate($readDate)
    {
        $this->readDate = $readDate;

        return $this;
    }

    /**
     * Get readDate
     *
     * @return \DateTime
     */
    public function getReadDate()
    {
        return $this->readDate;
    }

    /**
     * @return User
     */
    public function getFromUser()
    {
        return $this->fromUser;
    }

    /**
     * @param mixed $fromUser
     */
    public function setFromUser(User $fromUser)
    {
        $this->fromUser = $fromUser;
    }

    /**
     * @return User
     */
    public function getToUser()
    {
        return $this->toUser;
    }

    /**
     * @param User $toUser
     */
    public function setToUser(User $toUser)
    {
        $this->toUser = $toUser;
    }

    /**
     * @return string
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body)
    {
        $this->body = $body;
    }

    /**
     * @return array
     */
    public function getExtra(): ?array
    {
        return $this->extra;
    }

    /**
     * @param array $extra
     */
    public function setExtra(?array $extra)
    {
        $this->extra = $extra;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params??[];
    }

    /**
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @param string $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }
}

