<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Annotation as JMS;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CountryRepository")
 */
class Country
{
    public const Default = 'sa';
    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string",length=10,unique=true)
     * @ORM\Id
     * @JMS\Groups({"country_data","user_info"})
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Groups({"country_data","user_info"})
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(name="flag", type="string", length=255)
     * @JMS\Groups({"country_data"})
     */
    private $flag;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_code", type="string", length=10,options={"default" : "$"})
     * @JMS\Groups({"country_data"})
     */
    private $currencyCode;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_symbol", type="string", length=10,options={"default" : "$"})
     * @JMS\Groups({"country_data"})
     */
    private $currencySymbol;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_name", type="string", length=30)
     * @JMS\Groups({"country_data"})
     */
    private $currencyName;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\SystemConfig",mappedBy="country",cascade={"persist","remove"})
     */
    private $config;

    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Language")
     * @ORM\JoinColumn(referencedColumnName="identifier")
     * @JMS\Groups({"country_data"})
     */
    private $defaultLanguage;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CountryCategory",mappedBy="country")
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $countryCategories;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DocumentType",mappedBy="country")
     */
    private $countryDocuments;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CountryExchangeCurrency", mappedBy="country" , cascade={"all"}, orphanRemoval=true,fetch="EAGER")
     */
    private $currencyExchange;

    /**
     * @var boolean
     * @ORM\Column(name="enabled",type="boolean",options={"default" : "1"})
     */
    private $enabled;

    public function __construct()
    {
        $this->currencyExchange = new ArrayCollection();
        $this->countryCategories = new ArrayCollection();
        $this->enabled = true;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return Country
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }


    /**
     * Set flag.
     *
     * @param string $flag
     *
     * @return Country
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag.
     *
     * @return string
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * @return mixed
     */
    public function getDefaultLanguage()
    {
        return $this->defaultLanguage;
    }

    /**
     * @param mixed $defaultLanguage
     */
    public function setDefaultLanguage($defaultLanguage): void
    {
        $this->defaultLanguage = $defaultLanguage;
    }

    /**
     * @return string
     */
    public function getCurrencySymbol(): ?string
    {
        return $this->currencySymbol;
    }

    /**
     * @param string $currencySymbol
     */
    public function setCurrencySymbol(?string $currencySymbol): void
    {
        $this->currencySymbol = $currencySymbol;
    }

    /**
     * @return string
     */
    public function getCurrencyName(): ?string
    {
        return $this->currencyName;
    }

    /**
     * @param string $currencyName
     */
    public function setCurrencyName(?string $currencyName): void
    {
        $this->currencyName = $currencyName;
    }

    /**
     * @return SystemConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig(SystemConfig $config): void
    {
        $config->setCountry($this);
        $this->config = $config;
    }

    public function __toString()
    {
        return $this->getIdentifier();
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     */
    public function setCurrencyCode(?string $currencyCode): void
    {
        $this->currencyCode = $currencyCode;
    }

    /**
     * @param bool $onlyEnabled
     * @return array
     */
    public function getCategories($onlyEnabled=false): array
    {
        $categories = [];
        /** @var CountryCategory $countryCategory */
        foreach ($this->countryCategories as $countryCategory){
            if(!$onlyEnabled || $countryCategory->isEnabled()){
                $categories[] = $countryCategory->getCategory();
            }
        }
        return $categories;
    }


    /**
     * @param TaskCategory $category
     * @return CountryCategory
     */
    public function getCategory(TaskCategory $category): ?CountryCategory
    {
        /** @var CountryCategory $cat */
        foreach ($this->countryCategories as $cat){
            if($cat->getCategory()->getId() === $category->getId()){
                return $cat;
            }
        }
        return null;
    }

    /**
     * @param mixed $countryCategories
     */
    public function setCountryCategories($countryCategories): void
    {
        $this->countryCategories = $countryCategories;
    }

    /**
     * @return mixed
     */
    public function getCountryDocuments()
    {
        return $this->countryDocuments;
    }

    /**
     * @param mixed $countryDocuments
     */
    public function setCountryDocuments($countryDocuments): void
    {
        $this->countryDocuments = $countryDocuments;
    }

    /**
     * @return mixed
     */
    public function getCountryCategories()
    {
        return $this->countryCategories;
    }

    /**
     * @param null $currency
     * @return mixed|CountryExchangeCurrency
     */
    public function getCurrencyExchange($currency = null)
    {
        if($currency){
            foreach ($this->currencyExchange as $currencyExchange){
                if($currency === $currencyExchange->getToCurrency()) {
                    return $currencyExchange;
                }
            }
            return null;
        }
        return $this->currencyExchange;
    }

    /**
     * @param ArrayCollection $currencyExchange
     */
    public function setCurrencyExchange(ArrayCollection $currencyExchange): void
    {
        $this->currencyExchange = $currencyExchange;
    }

    /**
     * @param CountryExchangeCurrency $param
     */
    public function addCurrencyExchange(CountryExchangeCurrency $param): void
    {
        $this->currencyExchange->add($param);
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }
}
