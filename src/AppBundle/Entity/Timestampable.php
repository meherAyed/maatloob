<?php

/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 17/11/2017
 * Time: 11:54
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Timestampable class
 *
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class Timestampable {

    /**
     * @var \DateTime
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     * @JMS\Groups({"task_list","task_details","user_reviews","task_message","task_discussion"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     *
     * @ORM\Column(name="is_deleted", type="boolean")
     */
    private $isDeleted = false;

    /**
     * Timestampable constructor.
     */
    public function __construct() {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Timestampable
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return Timestampable
     */
    public function setIsDeleted($isDeleted) {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted() {
        return $this->isDeleted;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function PrePersist($args) {
        $this->updatedAt = new \DateTime();
    }

}
