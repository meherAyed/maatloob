<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="files")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FileRepository")
 */
class File extends Timestampable
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"file_info"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Exclude()
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     * @JMS\Groups({"file_info"})
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer",nullable=true)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=255,nullable=true)
     */
    private $extension;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Task", inversedBy="files")
     * @Serializer\Exclude()
     */
    private $task;

    /**
     * @ORM\ManyToOne(targetEntity="TaskInvoice", inversedBy="images")
     * @Serializer\Exclude()
     */
    private $taskInvoice;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="portfolio")
     */
    private $userPortfolio;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UserDocument", inversedBy="files")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id",onDelete="CASCADE",name="user_document_id")
     * @Serializer\Exclude()
     */
    private $userDocument;

    public $file;

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Set path
     *
     * @param string $path
     *
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set size
     *
     * @param integer $size
     *
     * @return File
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }


    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return File
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return File
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }


    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param mixed $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }


    /**
     * @return mixed
     */
    public function getUserDocument()
    {
        return $this->userDocument;
    }

    /**
     * @param mixed $userDocument
     */
    public function setUserDocument($userDocument): void
    {
        $this->userDocument = $userDocument;
    }

    /**
     * @param mixed $userPortfolio
     */
    public function setUserPortfolio($userPortfolio): void
    {
        $this->userPortfolio = $userPortfolio;
    }

    /**
     * @return mixed
     */
    public function getTaskInvoice()
    {
        return $this->taskInvoice;
    }

    /**
     * @param mixed $taskInvoice
     */
    public function setTaskInvoice($taskInvoice): void
    {
        $this->taskInvoice = $taskInvoice;
    }

}
