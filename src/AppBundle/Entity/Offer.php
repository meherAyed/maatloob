<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Offer
 *
 * @ORM\Table(name="offre")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfferRepository")
 */
class Offer extends Timestampable
{
    public Const STATUS_PUBLISHED = 1;
    public Const STATUS_ACCEPTED = 2;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"task_list","offer_details"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Task",inversedBy="offers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $task;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",inversedBy="offers")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"offer_details","task_list"})
     */
    private $user;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OfferReply", mappedBy="offer",cascade={"persist"})
     * @JMS\Groups({"task_details"})
     */
    private $replies;

    /**
     * @var int
     * @ORM\Column(name="status", type="smallint",options={"comment":"0 draft ,1 published ,2 accepted"})
     * @JMS\Groups({"offer_details","task_list"})
     */
    private $status;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="text", type="text")
     * @JMS\Groups({"offer_details"})
     */
    private $text;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\File",   cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @JMS\Groups({"offer_details"})
     */
    private $files;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="accepted_at", type="datetime", nullable=true)
     * @JMS\Groups({"offer_details"})
     */
    private $acceptedAt;

    /**
     * @var string
     * @ORM\Column(name="time", type="text",nullable=true)
     * @JMS\Groups({"offer_details"})
     */
    private $time;

    /**
     *
     * @ORM\Column(name="price",type="decimal",precision=15, scale=2,options={"default":0})
     * @JMS\Groups({"offer_details"})
     */
    private $price = 0;

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Offer
     */
    public function setStatus($status): Offer
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Offer
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }


    /**
     * Set time
     *
     * @param string $time
     *
     * @return Offer
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Offer
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @param Country|null $country
     * @return float
     */
    public function getPrice(Country $country=null): ?float
    {
        if($country && ($country->getCurrencyCode() !== $this->user->getCountry()->getCurrencyCode())){
            return (float)number_format($this->user->getCountry()->getCurrencyExchange($country->getCurrencyCode())->getRate() * $this->price, 2,'.','');
        }
        return (float)number_format($this->price, 2,'.','');
    }

    /**
     * Set task
     *
     * @param Task $task
     *
     * @return Offer
     */
    public function setTask(Task $task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Offer
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add file
     *
     * @param File $file
     *
     * @return Offer
     */
    public function addFile(File $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param File $file
     */
    public function removeFile(File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @return ArrayCollection
     */
    public function getReplies()
    {
        return $this->replies;
    }

    /**
     * @param ArrayCollection $replies
     */
    public function setReplies($replies)
    {
        $this->replies = $replies;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getAcceptedAt(): \DateTime
    {
        return $this->acceptedAt;
    }

    /**
     * @param \DateTime $acceptDate
     */
    public function setAcceptedAt(\DateTime $acceptDate): void
    {
        $this->acceptedAt = $acceptDate;
    }

}
