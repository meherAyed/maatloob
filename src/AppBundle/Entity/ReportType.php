<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * ReportType
 *
 * @ORM\Table(name="report_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReportTypeRepository")
 */
class ReportType
{
    use ORMBehaviors\Translatable\Translatable;

    public CONST SECTION_USER  = 'USER';
    public CONST SECTION_TASK  = 'TASK';
    public CONST SECTION_QUESTION = 'QUESTION';
    public CONST SECTION_OFFER = 'OFFER';
    public CONST SECTION_MESSAGE = 'MESSAGE';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"report"})
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="sections", type="array")
     */

    private $sections;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getSections(): array
    {
        return $this->sections;
    }

    /**
     * @param array $sections
     */
    public function setSections(array $sections): void
    {
        $this->sections = $sections;
    }

}

