<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Report
 *
 * @property ORM\Entity object
 * @ORM\Table(name="reporter")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReporterRepository")
 */
class Reporter extends Timestampable
{
    public const STATUS_UNTREATED = 0;
    public const STATUS_ACCEPTED = 1;
    public const STATUS_REFUSED = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"report"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @JMS\Groups({"report"})
     */
    private $user;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Report",inversedBy="reporters")
     * @JMS\Groups({"report"})
     */
    private $report;

    /**
     * @var string
     * @ORM\Column(name="message", type="text")
     * @JMS\Groups({"report"})
     */
    private $message;


    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param mixed $report
     */
    public function setReport($report): void
    {
        $this->report = $report;
    }

}
