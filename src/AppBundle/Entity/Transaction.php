<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Notification
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TransactionRepository")
 */
class Transaction
{
    public const TYPE_TASK_COMPLETE = 'task_complete';
    public const TYPE_TASK_TIP = 'task_tip';
    public const TYPE_WITHDRAW = 'withdraw';
    public const TYPE_DEPOSIT = 'deposit';
    public const TYPE_FEES = 'fees';
    public const TYPE_REFERRAL = 'referral';
    public const TYPE_DEAL_CANCELLED = 'deal_cancelled';
    public const TYPE_COMPENSATION = 'compensation';
    public const TYPE_COUNTRY_TAX = 'country_tax';
    public const TYPE_TASK_INVOICE = 'task_invoice';
    public const TYPE_SUPPORT_CORRECTION = 'support_correction';

    public CONST WITHDRAW_TYPE_MANUAL_BANK_TRANSFER = 'manual_bank_transfer';
    public CONST WITHDRAW_TYPE_STC_PAY = 'stc_pay_payout';
    public CONST WITHDRAW_TYPE_DISBURSEMENT = 'xendit_disbursement';

    public const STATUS_PENDING = 0;
    public const STATUS_PROCESSING = 1;
    public const STATUS_COMPLETED = 2;
    public const STATUS_CANCELLED = 3;
    public const STATUS_CAPTURED = 4;
    public const STATUS_FAILED = 5;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"user_withdrawal"})
     */
    private $id;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $fromUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $toUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Task")
     * @ORM\JoinColumn(nullable=true)
     */
    private $task;

    /**
     * @var string
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
     * @var float
     * @ORM\Column(name="from_price",type="decimal" ,precision=15,scale=2)
     * @JMS\Groups({"user_withdrawal"})
     */
    private $fromPrice;

    /**
     * @var float
     * @ORM\Column(name="price",type="decimal" ,precision=15,scale=2)
     * @JMS\Groups({"user_withdrawal"})
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @JMS\Groups({"user_withdrawal"})
     */
    private $createdAt;

    /**
     * @var string
     * @ORM\Column(type="string", length=64 , name="checkout_id",nullable=true,unique=true)
     */
    private $checkoutId;

    /**
     * @var string
     * @ORM\Column(type="text" , name="payment_information",nullable=true)
     */
    private $paymentInformation;

    /**
     * @var int
     *
     * @ORM\Column(name="payment_type", type="integer",options={"comment":"1 cash, 2 from wallet , 3 card"})
     */
    private $paymentType;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer",options={"comment":"0 pending;1 processing ; 2 completed "})
     * @JMS\Groups({"user_withdrawal"})
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $processedBy;

    /**
     * @var array
     * @ORM\Column(name="extras", type="array", nullable=true,options={"comment":"extra params"})
     */
    private $extras;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->status = self::STATUS_COMPLETED;

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return User
     */
    public function getFromUser()
    {
        return $this->fromUser;
    }

    /**
     * @param mixed $fromUser
     */
    public function setFromUser(User $fromUser)
    {
        $this->fromUser = $fromUser;
    }

    /**
     * @return User
     */
    public function getToUser()
    {
        return $this->toUser;
    }

    /**
     * @param User $toUser
     */
    public function setToUser(User $toUser)
    {
        $this->toUser = $toUser;
    }


    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Task|null
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param mixed $task
     */
    public function setTask($task): void
    {
        $this->task = $task;
    }


    /**
     * @param User|null $user
     * @return float
     */
    public function getPrice(User $user = null): ?float
    {
        if($this->fromUser && $user && $this->fromUser->getId() === $user->getId()){
            return $this->getFromPrice();
        }
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
        if(empty($this->fromPrice)){
            $this->fromPrice = $price;
        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCheckoutId(): string
    {
        return $this->checkoutId;
    }

    /**
     * @param string $checkoutId
     */
    public function setCheckoutId(string $checkoutId): void
    {
        $this->checkoutId = $checkoutId;
    }

    /**
     * @return array
     */
    public function getPaymentInformation(): array
    {
        return $this->paymentInformation?json_decode($this->paymentInformation, true):[];
    }

    /**
     * @param string $paymentInformation
     */
    public function setPaymentInformation(string $paymentInformation): void
    {
        $this->paymentInformation = $paymentInformation;
    }

    /**
     * @return File|null
     */
    public function getWithdrawInvoice()
    {
        return $this->withdrawInvoice;
    }

    /**
     * @param mixed $withdrawInvoice
     */
    public function setWithdrawInvoice($withdrawInvoice): void
    {
        $this->withdrawInvoice = $withdrawInvoice;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"user_withdrawal"})
     */
    public function isCash(): bool
    {
        return $this->type === self::TYPE_TASK_COMPLETE ? $this->task->getPaymentType() === Task::PAYMENT_TYPE_CASH : false;
    }

    /**
     * @return float
     */
    public function getFromPrice(): float
    {
        return $this->fromPrice;
    }

    /**
     * @param float $fromPrice
     */
    public function setFromPrice(float $fromPrice): void
    {
        $this->fromPrice = $fromPrice;
    }

    /**
     * @return mixed
     */
    public function getProcessedBy()
    {
        return $this->processedBy;
    }

    /**
     * @param mixed $processedBy
     */
    public function setProcessedBy($processedBy): void
    {
        $this->processedBy = $processedBy;
    }

    /**
     * @return array
     */
    public function getExtras(): array
    {
        return $this->extras??[];
    }

    /**
     * @param array $extras
     */
    public function setExtras(array $extras): void
    {
        $this->extras = $extras;
    }

    public function addExtras(string $key, $value)
    {
        $this->extras[$key] = $value;
    }

    public function getPaymentMethod(){
        return $this->extras['method']??'';
    }

    /**
     * @return int
     */
    public function getPaymentType(): ?int
    {
        return $this->paymentType;
    }

    /**
     * @param int $paymentType
     */
    public function setPaymentType(int $paymentType): void
    {
        $this->paymentType = $paymentType;
    }
}

