<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * OfferUpdateRequest
 *
 * @ORM\Table(name="offer_update_request")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfferUpdateRequestRepository")
 */
class OfferUpdateRequest extends Timestampable
{
    public Const STATUS_WAITING = 0;
    public Const STATUS_ACCEPTED = 1;
    public Const STATUS_REFUSED = 2;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"task_details"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fromUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Offer")
     * @ORM\JoinColumn(nullable=false)
     */
    private $offer;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer",options={"comment":"0 waiting ,1 accepted ,2 refused"})
     */
    private $status;

    /**
     *
     * @ORM\Column(name="old_price",type="decimal", scale=2,options={"default":0})
     */
    private $oldPrice = 0;

    /**
     *
     * @ORM\Column(name="new_price",type="decimal", scale=2,options={"default":0})
     * @JMS\Groups({"task_details"})
     */
    private $newPrice = 0;

    public function __construct()
    {
        parent::__construct();
        $this->status = self::STATUS_WAITING;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getNewPrice()
    {
        return $this->newPrice;
    }

    /**
     * @param mixed $newPrice
     */
    public function setNewPrice($newPrice): void
    {
        $this->newPrice = $newPrice;
    }

    /**
     * @return Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param mixed $offer
     */
    public function setOffer($offer): void
    {
        $this->oldPrice = $offer->getPrice();
        $this->offer = $offer;
        $this->fromUser = $offer->getUser();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getOldPrice(): int
    {
        return $this->oldPrice;
    }

    /**
     * @param int $oldPrice
     */
    public function setOldPrice(int $oldPrice): void
    {
        $this->oldPrice = $oldPrice;
    }

    /**
     * @return mixed
     */
    public function getFromUser()
    {
        return $this->fromUser;
    }

    /**
     * @param mixed $fromUser
     */
    public function setFromUser($fromUser): void
    {
        $this->fromUser = $fromUser;
    }

}
