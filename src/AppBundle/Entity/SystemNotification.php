<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * SystemNotification
 *
 * @ORM\Table(name="system_notification")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SystemNotificationRepository")
 */
class SystemNotification extends Timestampable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"system_news_updates"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @JMS\Groups({"system_news_updates"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     * @JMS\Groups({"system_news_updates"})
     */
    private $body;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\File",cascade={"persist", "remove"},orphanRemoval=true)
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id",onDelete="CASCADE",name="image_id")
     * @JMS\Groups({"system_news_updates"})
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $createdBy;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_send", type="datetime", nullable=true)
     */
    private $lastSend;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country")
     * @ORM\JoinColumn(nullable=false,referencedColumnName="identifier",name="country_id")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20)
     * @JMS\Groups({"system_news_updates"})
     */
    private $type;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return SystemNotification
     */
    public function setTitle($title): SystemNotification
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set body.
     *
     * @param string $body
     *
     * @return SystemNotification
     */
    public function setBody($body): SystemNotification
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body.
     *
     * @return string
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @return File
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return DateTime
     */
    public function getLastSend(): ?DateTime
    {
        return $this->lastSend;
    }

    /**
     * @param DateTime $lastSend
     */
    public function setLastSend(DateTime $lastSend): void
    {
        $this->lastSend = $lastSend;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
