<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserBan
 *
 * @ORM\Table(name="user_ban")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserBanRepository")
 */
class UserBan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    private $bannedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permanent", type="boolean")
     */
    private $permanent;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="unban_date", type="datetime",nullable=true)
     */
    private $unbanDate;

    /**
     * @var string
     *
     * @ORM\Column(name="ban_reason", type="text")
     */
    private $banReason;

    /**
     * @var float
     * @ORM\Column(name="unban_penalty",type="decimal",options={"default":0})
     */
    private $unbanPenalty;

    /**
     * @var boolean
     * @ORM\Column(name="unban_penalty_payed",type="boolean",options={"default":0})
     */
    private $unbanPenaltyPayed;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",inversedBy="ban")
     */
    private $user;

    /**
     * @var boolean
     * @ORM\Column(name="ban_lifted",type="boolean",options={"default":0})
     */
    private $banLifted;

    public function __construct()
    {
        $this->permanent = true;
        $this->unbanPenalty = 0;
        $this->unbanPenaltyPayed = false;
        $this->banLifted = false;
        $this->date = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBannedBy()
    {
        return $this->bannedBy;
    }

    /**
     * @param mixed $bannedBy
     */
    public function setBannedBy($bannedBy): void
    {
        $this->bannedBy = $bannedBy;
    }

    /**
     * @return bool
     */
    public function isPermanent(): bool
    {
        return $this->permanent;
    }

    /**
     * @param bool $permanent
     */
    public function setPermanent(bool $permanent): void
    {
        $this->permanent = $permanent;
    }


    /**
     * @return string
     */
    public function getBanReason(): string
    {
        return $this->banReason;
    }

    /**
     * @param string $banReason
     */
    public function setBanReason(string $banReason): void
    {
        $this->banReason = $banReason;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime|null
     */
    public function getUnbanDate(): ?\DateTime
    {
        return $this->unbanDate;
    }

    /**
     * @param \DateTime $unbanDate
     */
    public function setUnbanDate(\DateTime $unbanDate): void
    {
        $this->permanent = false;
        $this->unbanDate = $unbanDate;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getUnbanPenalty(): float
    {
        return $this->unbanPenalty;
    }

    /**
     * @param float $unbanPenalty
     */
    public function setUnbanPenalty(float $unbanPenalty): void
    {
        $this->unbanPenalty = $unbanPenalty;
    }

    /**
     * @return bool
     */
    public function isUnbanPenaltyPayed(): bool
    {
        return $this->unbanPenaltyPayed;
    }

    /**
     * @param bool $unbanPenaltyPayed
     */
    public function setUnbanPenaltyPayed(bool $unbanPenaltyPayed): void
    {
        $this->unbanPenaltyPayed = $unbanPenaltyPayed;
    }

    /**
     * @return bool
     */
    public function isBanLifted(): bool
    {
        return $this->banLifted;
    }

    /**
     * @param bool $banLifted
     */
    public function setBanLifted(bool $banLifted): void
    {
        $this->banLifted = $banLifted;
    }

}
