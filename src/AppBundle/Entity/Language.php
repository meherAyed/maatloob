<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * languages
 *
 * @ORM\Table(name="language")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LanguageRepository")
 */
class Language
{
    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string",length=2,unique=true)
     * @ORM\Id
     * @JMS\Groups({"app_config","country_data"})
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Groups({"app_config","country_data"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255)
     * @JMS\Groups({"app_config"})
     */
    private $icon;

    /**
     * @var int
     *
     * @ORM\Column(name="version", type="integer")
     * @JMS\Groups({"app_config"})
     */
    private $version;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", options={"default" : 0,"comment":"1 all translation done for this language"})
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\LanguageMessage",mappedBy="language",cascade={"persist"})
     */
    private $messages;


    /**
     * language constructor.
     * @param null $identifier
     */
    public function __construct($identifier = null)
    {
        $this->identifier = $identifier;
        $this->messages = new ArrayCollection();
        $this->version = 1;
        $this->active = false;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return language
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set icon.
     *
     * @param string $icon
     *
     * @return language
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon.
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set version.
     *
     * @param int $version
     *
     * @return language
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version.
     *
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }


    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages): void
    {
        $this->messages = $messages;
    }

    public function addMessage(LanguageMessage $languageMessage)
    {
        $languageMessage->setLanguage($this);
        $this->messages->add($languageMessage);
    }

    /**
     * @return string
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier(string $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function translationCompletionRate()
    {
        $rate = 0;
        /** @var LanguageMessage $message */
        foreach ($this->getMessages() as $message) {
            if ($message->getMessage() != "" && $message->getMessage() != null) {
                $rate += 1 / $this->getMessages()->count();
            }
        }
        return round($rate * 100, 2);
    }

}
