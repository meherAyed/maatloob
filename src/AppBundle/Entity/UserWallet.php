<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * UserWallet
 *
 * @ORM\Table(name="user_wallet")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserWalletRepository")
 */
class UserWallet extends Timestampable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="balance",type="decimal",precision=15, scale=2,options={"default":0})
     * @JMS\Groups({"user_wallet"})
     */
    private $balance;


    /**
     * @var float
     *
     * @ORM\Column(name="locked_balance",type="decimal",precision=15, scale=2,options={"default":0},nullable=true)
     * @JMS\Groups({"user_wallet"})
     */
    private $lockedBalance;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\WalletHistoric",mappedBy="wallet",cascade={"persist","remove"})
     */
    private $historic;


    /**
     * @var float
     *
     * @ORM\Column(name="credit",type="decimal",precision=15, scale=2,options={"default":0})
     * @JMS\Groups({"user_wallet"})
     */
    private $credit;

    /**
     * @var float
     *
     * @ORM\Column(name="locked_credit",type="decimal",precision=15, scale=2,options={"default":0})
     * @JMS\Groups({"user_wallet"})
     */
    private $lockedCredit;

    /**
     * @var array
     * @ORM\Column(name="registration_card_id", type="array",nullable=true)
     */
    private $registrationCardId;



    public function __construct()
    {
        parent::__construct();
        $this->historic = new ArrayCollection();
        $this->balance = 0;
        $this->lockedBalance = 0;
        $this->credit = 0;
        $this->lockedCredit = 0;
        $this->registrationCardId = [];
    }

    /**
     * Set balance
     *
     * @param float $balance
     *
     */
    public function setBalance(float $balance)
    {
        $this->balance = $balance;
    }

    /**
     * Get balance
     *
     * @return float
     */
    public function getBalance() :float
    {
        return $this->balance??0;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param float $price
     * @return UserWallet
     */
    public function withdraw(float $price) : UserWallet
    {
        $this->balance -= $price;
        return $this;
    }

    /**
     * @param $price
     * @return UserWallet
     */
    public function deposit($price) : UserWallet
    {
        $this->balance += $price;
        return $this;
    }

    /**
     * @return float
     */
    public function getLockedBalance(): float
    {
        return $this->lockedBalance??0;
    }

    /**
     * @param float $lockedBalance
     */
    public function setLockedBalance(float $lockedBalance): void
    {
        $this->lockedBalance = $lockedBalance;
    }

    /**
     * @return mixed
     */
    public function getHistoric()
    {
        return $this->historic;
    }

    /**
     * @param mixed $historic
     */
    public function setHistoric($historic): void
    {
        $this->historic = $historic;
    }

    /**
     * @param $amount
     */
    public function lockBalance($amount) : void
    {
        $this->lockedBalance += $amount;
        $this->balance -= $amount;
    }

    /**
     * @param $amount
     */
    public function unlockBalance($amount) : void
    {
        $this->lockedBalance -= $amount;
        $this->balance += $amount;
    }

    public function totalBalance(){
        return $this->balance + $this->lockedBalance;
    }

    /**
     * @return float
     */
    public function getCredit(): float
    {
        return $this->credit;
    }

    /**
     * @param float $credit
     */
    public function setCredit(float $credit): void
    {
        $this->credit = $credit;
    }

    /**
     * @param float $credit
     */
    public function addCredit(float $credit): void
    {
        $this->credit += $credit;
    }

    /**
     * @return float|int
     */
    public function getTotalBalance(){
        return $this->balance+$this->lockedBalance;
    }

    /**
     * @return float
     */
    public function getLockedCredit(): float
    {
        return $this->lockedCredit;
    }

    /**
     * @param float $lockedCredit
     */
    public function setLockedCredit(float $lockedCredit): void
    {
        $this->lockedCredit = $lockedCredit;
    }

    /**
     * @param $amount
     * @return float|int
     */
    public function useCredit($amount)
    {
        if($amount>$this->credit){
            $usedCredit = $this->credit;
            $this->lockedCredit += $this->credit;
            $this->credit = 0;
            return $usedCredit;
        }
        $this->lockedCredit += $amount;
        $this->credit -= $amount;
        return $amount;
    }

    /**
     * @param $amount
     */
    public function unlockCredit($amount) : void
    {
        $this->lockedCredit -= $amount;
        $this->credit += $amount;
    }

    /**
     * Lock task price and return used credit
     * @param $price
     * @return float|int
     */
    public function lockTaskPrice($price){
        $creditUsed = $this->useCredit($price);
        $this->lockBalance($price - $creditUsed);
        return $creditUsed;
    }

    /**
     * @return float|int
     */
    public function getAvailableBalance(){
        return $this->credit + $this->balance;
    }

    /**
     * @return array
     */
    public function getRegistrationCardId(): array
    {
        return $this->registrationCardId??[];
    }

    /**
     * @param array $registrationCardId
     */
    public function setRegistrationCardId(array $registrationCardId): void
    {
        $this->registrationCardId = $registrationCardId;
    }

    /**
     * @param $registrationCardId
     */
    public function addRegistrationCardId($registrationCardId): void
    {
        $this->registrationCardId[] = $registrationCardId;
    }
}
