<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CountryExchangeCurrency
 *
 * @ORM\Table(name="country_exchange_currency")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CountryExchangeCurrencyRepository")
 */
class CountryExchangeCurrency
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country",inversedBy="currencyExchange")
     * @ORM\JoinColumn(name="country",referencedColumnName="identifier")
     */
    private $country;


    /**
     * @var string
     *
     * @ORM\Column(name="to_currency", type="string", length=4)
     */
    private $toCurrency;

    /**
     * @var float
     *
     * @ORM\Column(name="rate",type="decimal",precision=15, scale=6,options={"default":1})
     */
    private $rate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getToCurrency(): string
    {
        return $this->toCurrency;
    }

    /**
     * @param string $toCurrency
     */
    public function setToCurrency(string $toCurrency): void
    {
        $this->toCurrency = $toCurrency;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @param Country $fromCountry
     */
    public function setCountry(Country $fromCountry): void
    {
        $this->country = $fromCountry;
        $fromCountry->addCurrencyExchange($this);
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     */
    public function setRate(float $rate): void
    {
        $this->rate = $rate;
    }


}
