<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TaskQuestion
 *
 * @ORM\Table(name="task_question")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskQuestionRepository")
 */
class TaskQuestion extends Timestampable
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"task_details"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Task")
     * @ORM\JoinColumn(nullable=false)
     */
    private $task;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"task_details"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TaskQuestion" ,mappedBy="replyFor")
     * @ORM\JoinColumn(nullable=true)
     * @JMS\Groups({"task_details"})
     */
    private $replies;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TaskQuestion",inversedBy="replyFor")
     * @ORM\JoinColumn(nullable=true)
     */
    private $replyFor;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="text", type="text")
     * @JMS\Groups({"task_details"})
     */
    private $text;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\File",   cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @JMS\Groups({"task_details"})
     */
    private $files;


    /**
     * Set text
     *
     * @param string $text
     *
     * @return TaskQuestion
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set task
     *
     * @param Task $task
     *
     * @return TaskQuestion
     */
    public function setTask(Task $task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \AppBundle\Entity\Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return TaskQuestion
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add file
     *
     * @param \AppBundle\Entity\File $file
     *
     * @return TaskQuestion
     */
    public function addFile(\AppBundle\Entity\File $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \AppBundle\Entity\File $file
     */
    public function removeFile(\AppBundle\Entity\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set replyFor
     *
     * @param \AppBundle\Entity\TaskQuestion $replyFor
     *
     * @return TaskQuestion
     */
    public function setReplyFor(\AppBundle\Entity\TaskQuestion $replyFor = null)
    {
        $this->replyFor = $replyFor;

        return $this;
    }

    /**
     * Get replyFor
     *
     * @return \AppBundle\Entity\TaskQuestion
     */
    public function getReplyFor()
    {
        return $this->replyFor;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getReplies()
    {
        return $this->replies;
    }

    /**
     * @param mixed $replies
     */
    public function setReplies($replies): void
    {
        $this->replies = $replies;
    }
}
