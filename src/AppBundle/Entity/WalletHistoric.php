<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserWallet
 *
 * @ORM\Table(name="wallet_historic")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WalletHistoricRepository")
 */
class WalletHistoric extends Timestampable
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="changes",type="text")
     */
    private $changes;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UserWallet",inversedBy="historic")
     */
    private $wallet;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * @param mixed $wallet
     */
    public function setWallet($wallet): void
    {
        $this->wallet = $wallet;
    }

    /**
     * @return string
     */
    public function getChanges(): string
    {
        return $this->changes;
    }

    /**
     * @param string $changes
     */
    public function setChanges(string $changes): void
    {
        $this->changes = $changes;
    }
}
