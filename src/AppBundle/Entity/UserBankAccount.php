<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * UserBankAccount
 *
 * @ORM\Table(name="user_bank_account")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserBankAccountRepository")
 */
class UserBankAccount
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="holderName", type="string", length=150)
     * @JMS\Groups({"user_data"})
     */
    private $holderName;

    /**
     * @var int
     *
     * @ORM\Column(name="accountNumber", type="bigint")
     * @JMS\Groups({"user_data"})
     */
    private $accountNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="routingNumber", type="bigint",nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $routingNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_code", type="string", length=50)
     * @JMS\Groups({"user_data"})
     */
    private $bankCode;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_name", type="string" ,length=150,nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $bankName;


    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50,nullable=true)
     * @JMS\Groups({"user_data"})
     */
    private $phone;


    /**
     * @return string
     */
    public function getBankName(): ?string
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName(string $bankName): void
    {
        $this->bankName = $bankName;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set holderName.
     *
     * @param string $holderName
     *
     * @return UserBankAccount
     */
    public function setHolderName($holderName)
    {
        $this->holderName = $holderName;

        return $this;
    }

    /**
     * Get holderName.
     *
     * @return string
     */
    public function getHolderName()
    {
        return $this->holderName;
    }

    /**
     * Set accountNumber.
     *
     * @param int $accountNumber
     *
     * @return UserBankAccount
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber.
     *
     * @return int
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set routingNumber.
     *
     * @param int $routingNumber
     *
     * @return UserBankAccount
     */
    public function setRoutingNumber($routingNumber)
    {
        $this->routingNumber = $routingNumber;

        return $this;
    }

    /**
     * Get routingNumber.
     *
     * @return int
     */
    public function getRoutingNumber()
    {
        return $this->routingNumber;
    }

    /**
     * @return string
     */
    public function getBankCode(): ?string
    {
        return $this->bankCode;
    }

    /**
     * @param string $bankCode
     */
    public function setBankCode(string $bankCode): void
    {
        $this->bankCode = $bankCode;
    }

    public function isValid($country = 'sa'): bool
    {
        if($country === 'id') {
            return !empty($this->bankCode) && !empty($this->accountNumber) && !empty($this->holderName);
        }
        return !empty($this->accountNumber) && !empty($this->holderName) && !empty($this->routingNumber);
    }
}
