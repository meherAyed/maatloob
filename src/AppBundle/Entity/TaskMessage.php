<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * TaskMessage
 *
 * @ORM\Table(name="task_message")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskMessageRepository")
 */
class TaskMessage extends Timestampable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"task_message"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TaskDiscussion" , inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"task_message_discussion"})
     */
    private $discussion;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"task_message"})
     */
    private $user;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\File",   cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @JMS\Groups({"task_message"})
     */
    private $files;

    /**
     * @var string
     *
     * @ORM\Column(name="isShowed", type="boolean",options={"default" : 0} )
     * @JMS\Groups({"task_message"})
     */
    private $isShowed = false;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="text", type="text")
     * @JMS\Groups({"task_message"})
     */
    private $text;

    /**
     * Set text
     *
     * @param string $text
     *
     * @return TaskMessage
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return TaskMessage
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add file
     *
     * @param File $file
     *
     * @return TaskMessage
     */
    public function addFile(File $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param File $file
     */
    public function removeFile(File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set discussion
     *
     * @param TaskDiscussion $discussion
     *
     * @return TaskMessage
     */
    public function setDiscussion(TaskDiscussion $discussion)
    {
        $this->discussion = $discussion;

        return $this;
    }

    /**
     * Get discussion
     *
     * @return TaskDiscussion
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * Set isShowed
     *
     * @param boolean $isShowed
     *
     * @return TaskMessage
     */
    public function setIsShowed($isShowed)
    {
        $this->isShowed = $isShowed;

        return $this;
    }

    /**
     * Get isShowed
     *
     * @return boolean
     */
    public function getIsShowed()
    {
        return $this->isShowed;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
