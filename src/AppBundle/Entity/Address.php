<?php

namespace AppBundle\Entity;

use Components\Helper;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AddressRepository")
 */
class Address extends Timestampable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"address_details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=255)
     * @JMS\Groups({"task_address","address_details"})
     */
    private $place;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     * @JMS\Groups({"address_details"})
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="postal_code", type="integer", length=10,nullable=true)
     * @JMS\Groups({"task_address","address_details"})
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=18, scale=12)
     * @JMS\Groups({"task_address","address_details"})
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=18, scale=12)
     * @JMS\Groups({"task_address","address_details"})
     */
    private $latitude;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",inversedBy="addresses")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;


    /**
     * @var string
     * @ORM\Column(name="area", type="string",nullable=true)
     * @JMS\Groups({"task_list","task_details","address_details"})
     */
    private $area;

    /**
     * @var string
     * @ORM\Column(name="street", type="string",nullable=true)
     * @JMS\Groups({"task_list","task_details","address_details"})
     */
    private $street;


    /**
     * @var string
     * @ORM\Column(name="address_info", type="string",nullable=true)
     * @JMS\Groups({"task_list","task_details","address_details"})
     */
    private $addressInfo;


    /**
     * @var bool
     *
     * @ORM\Column(name="hide_address", type="boolean",options={"default" : 0} )
     * @JMS\Groups({"task_list","task_details","address_details"})
     */
    private $hideAddress;

    public function __construct()
    {
        parent::__construct();
        $this->hideAddress = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return Address
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Address
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set postalCode
     *
     * @param int $postalCode
     *
     * @return Address
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return int
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Address
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Address
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param Address $address
     * @return float|int
     */
    public function distance(Address $address){
        return Helper::distance($this->getLatitude(),$this->longitude,$address->getLatitude(),$address->getLongitude());
    }

    /**
     * @return string
     */
    public function getArea(): ?string
    {
        return $this->area;
    }

    /**
     * @param string $area
     * @return Address
     */
    public function setArea(string $area): Address
    {
        $this->area = $area;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return Address
     */
    public function setStreet(string $street): Address
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressInfo(): ?string
    {
        return $this->addressInfo;
    }

    /**
     * @param string $addressInfo
     * @return Address
     */
    public function setAddressInfo(string $addressInfo): Address
    {
        $this->addressInfo = $addressInfo;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHideAddress(): bool
    {
        return $this->hideAddress;
    }

    /**
     * @param bool $hideAddress
     * @return Address
     */
    public function setHideAddress(bool $hideAddress): Address
    {
        $this->hideAddress = $hideAddress;
        return $this;
    }
}

