<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Accessor;

/**
 * UserDocument
 *
 * @ORM\Table(name="user_document")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserDocumentRepository")
 */
class UserDocument
{
    public const STATUS_WAITING_FOR_VERIFICATION = 1;
    public const STATUS_VERIFIED = 2;
    public const STATUS_REFUSED = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"user_documents"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     * @Serializer\Groups({"user_documents"})
     */
    private $description;

    /**
     * @var DocumentType
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DocumentType")
     * @Serializer\Groups({"user_documents"})
     */
    private $type;


    /**
     * @var
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\File",mappedBy="userDocument",cascade={"persist", "remove"},orphanRemoval=true)
     * @Serializer\Groups({"user_documents"})
     * @Accessor("getFiles")
     */
    private $files;

    /**
     * @var boolean
     * @ORM\Column(name="verified",type="boolean")
     * @Serializer\Groups({"user_documents"})
     */
    private $verified;

    /**
     * @var \DateTime
     * @ORM\Column(name="verification_date",type="datetime",nullable=true)
     * @Serializer\Groups({"user_documents"})
     */
    private $verificationDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="request_verification_date",type="datetime",nullable=true)
     */
    private $requestVerificationDate;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="documents")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(name="status",type="string" ,options={"comment":"0 initial,1 Waiting for verification,2 verified, 3 refused"})
     * @Serializer\Groups({"user_documents"})
     */
    private $status;

    public function __construct()
    {
        $this->verified = false;
        $this->files = new ArrayCollection();
        $this->status = 0;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return UserDocument
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }


    /**
     * @return mixed
     */
    public function getFiles()
    {
        /** @var File $file */
        foreach ($this->files as $file){
            $fileName = str_replace('_','',$file->getName());
            $file->setPath("/protected/file/{$fileName}-{$file->getId()}.{$file->getExtension()}");
        }
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files): void
    {
        $this->files = $files;
    }

    /**
     * @param mixed $file
     */
    public function addFile(File $file): void
    {
        $file->setUserDocument($this);
        $this->files->add($file);
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->verified;
    }

    /**
     * @param bool $verified
     */
    public function setVerified(bool $verified): void
    {
        $this->verified = $verified;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return DocumentType
     */
    public function getType(): DocumentType
    {
        return $this->type;
    }

    /**
     * @param DocumentType $type
     */
    public function setType(DocumentType $type): void
    {
        $this->type = $type;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getVerificationDate(): ?\DateTime
    {
        return $this->verificationDate;
    }

    /**
     * @param \DateTime $verificationDate
     */
    public function setVerificationDate(?\DateTime $verificationDate): void
    {
        $this->verificationDate = $verificationDate;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getRequestVerificationDate(): ?\DateTime
    {
        return $this->requestVerificationDate;
    }

    /**
     * @param \DateTime $requestVerificationDate
     */
    public function setRequestVerificationDate(\DateTime $requestVerificationDate): void
    {
        $this->requestVerificationDate = $requestVerificationDate;
    }
}

