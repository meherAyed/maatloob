<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;
use Components\Validator\Constraints as AppAssert;

/**
 * PhoneVerificationRequest
 * @ORM\Embeddable()
 * @AppAssert\E164Number(groups={"E164"})
 * @Assert\GroupSequence({"PhoneVerificationRequest", "E164"})
 * @ORM\Table(name="phone_verification_request")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PhoneVerificationRequestRepository")
 */
class PhoneVerificationRequest
{
    public const CODE_LENGTH = 6;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=16, nullable=true)
     * @Assert\NotBlank()
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="verification_code", type="string", length=PhoneVerificationRequest::CODE_LENGTH, nullable=true)
     */
    private $verificationCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="requestedAt", type="datetime")
     */
    private $requestedAt;


    public function __construct()
    {
        $this->requestedAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set requestedAt.
     *
     * @return PhoneVerificationRequest
     */
    public function setRequestedAt()
    {
        $this->requestedAt = new \DateTime();

        return $this;
    }

    /**
     * Get requestedAt.
     *
     * @return \DateTime
     */
    public function getRequestedAt()
    {
        return $this->requestedAt;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getVerificationCode(): string
    {
        return $this->verificationCode;
    }

    public function setVerificationCode(): void
    {
        $this->verificationCode = sprintf('%0'.self::CODE_LENGTH.'d', random_int(1, str_repeat(9, self::CODE_LENGTH)));;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }
}
