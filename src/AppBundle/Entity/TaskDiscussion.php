<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * TaskDiscussion
 *
 * @ORM\Table(name="task_discussion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskDiscussionRepository")
 */
class TaskDiscussion extends Timestampable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"task_details","discussions"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"discussions"})
    */
    private $to; 
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"discussions"})
    */
    private $from; 
    
    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Task",inversedBy="discussion")
     * @ORM\JoinColumn(name="task_id",referencedColumnName="id")
     * @JMS\Groups({"discussions"})
    */
    private $task;

    /**
     * @var boolean
     * @ORM\Column(name="closed",type="boolean")
     */
    private $closed = false;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TaskMessage" , mappedBy="discussion",cascade={"persist","remove"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $messages;

    public function __construct()
    {
        parent::__construct();
        $this->messages = new ArrayCollection();
    }

    /**
     * Set to
     *
     * @param User $to
     *
     * @return TaskDiscussion
     */
    public function setTo(User $to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return User
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set from
     *
     * @param User $from
     *
     * @return TaskDiscussion
     */
    public function setFrom(User $from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return User
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set task
     *
     * @param Task $task
     *
     * @return TaskDiscussion
     */
    public function setTask(Task $task = null)
    {
        $task->setDiscussion($this);
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->closed;
    }

    /**
     * @param bool $closed
     */
    public function setClosed(bool $closed): void
    {
        $this->closed = $closed;
    }

    /**
     * @return ArrayCollection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages): void
    {
        $this->messages = $messages;
    }
}
