<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ReflectionClass;

/**
 * Indicator
 *
 * @ORM\Table(name="indicator")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IndicatorRepository")
 */
class Indicator
{
    public const LIST_USERS = 'LIST_USERS';
    public const LIST_TRANSACTIONS = 'LIST_TRANSACTIONS';
    public const LIST_TASKS = 'LIST_TASKS';
    public const LIST_REPORTS = 'LIST_REPORTS';
    public const LIST_WITHDRAW_REQUESTS = 'LIST_WITHDRAW_REQUESTS';
    public const LIST_USER_DOCUMENTS = 'LIST_USER_DOCUMENTS';
    public const LIST_TASKS_CANCEL_REQUEST = 'LIST_TASKS_CANCEL_REQUEST';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    private $admin;

    /**
     * @var string
     *
     * @ORM\Column(name="list", type="string", length=255)
     */
    private $list;

    /**
     * @var array|null
     *
     * @ORM\Column(name="items", type="array", nullable=true)
     */
    private $items;

    public function __construct()
    {
        $this->items = [];
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set list.
     *
     * @param string $list
     *
     * @return Indicator
     */
    public function setList($list): Indicator
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list.
     *
     * @return string
     */
    public function getList(): string
    {
        return $this->list;
    }

    /**
     * Set items.
     *
     * @param array|null $items
     *
     * @return Indicator
     */
    public function setItems($items = null): Indicator
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @param $item
     */
    public function addItem($item): void
    {
        $this->items[] = $item;
    }

    /**
     * @param $item
     */
    public function removeItem($item): void
    {
        $index = array_search($item, $this->items, true);
        if($index!==false) {
            unset($this->items[$index]);
        }
    }
    /**
     * Get items.
     *
     * @return array|null
     */
    public function getItems(): ?array
    {
        return $this->items;
    }

    public static function getSupportedLists(): array
    {
        $reflectionClass = new ReflectionClass(__CLASS__);
        return array_filter($reflectionClass->getConstants(),static function ($list){
            return strpos($list, 'LIST_')!==false;
        });
    }

    /**
     * @return User
     */
    public function getAdmin(): User
    {
        return $this->admin;
    }

    /**
     * @param User $admin
     */
    public function setAdmin(User $admin): void
    {
        $this->admin = $admin;
    }
}
