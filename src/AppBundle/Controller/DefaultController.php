<?php

namespace AppBundle\Controller;

use AppBundle\Entity\File;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(): Response
    {
        // replace this example code with whatever you need
        return $this->render('default/welcome.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    public function saveTablePageLengthAction(Request $request): JsonResponse
    {
        $data = $request->request->get('tablePageLength');
        $session = $this->get('session');
        $session->set('tableLength',$data );
        return $this->json([]);
    }


    public function protectedFileAccessAction($id): BinaryFileResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var File $file */
        $file = $this->getDoctrine()->getRepository(File::class)
                    ->findOneBy(['id'=>$id,'isDeleted'=>false]);
        if (null === $file || ($file->getUser()->getId() !== $user->getId() && !$user->isAdmin())) {
                throw new NotFoundHttpException('file not exists');
        }
        $file = $this->get('kernel')->getRootDir() . '/../web/' . $file->getPath();  // Path to the file on the server
        $fs = $this->get('filesystem');
        if(!$fs->exists($file)) {
            throw new NotFoundHttpException('file not exists');
        }
        $response = new BinaryFileResponse($file);

        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        return $response;
    }

}
