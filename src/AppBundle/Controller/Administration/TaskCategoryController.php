<?php

namespace AppBundle\Controller\Administration;

use AppBundle\Entity\Country;
use AppBundle\Entity\CountryCategory;
use AppBundle\Entity\Language;
use AppBundle\Entity\TaskCategory;
use AppBundle\Form\CountryCategoryType;
use AppBundle\Form\TaskCategoryType;
use Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * TaskCategory controller.
 *
 * @Route("/system/task-categories")
 */
class TaskCategoryController extends Controller
{
    /**
     * Lists all taskCategory entities.
     *
     * @Route("/", name="system_task_categories")
     */
    public function indexAction(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $country = $this->get('session')->get('region', 'sa');
        /** @var Country $country */
        $country = $em->getRepository(Country::class)->findOneBy(['identifier' => $country]);
        $categories = $country->getCountryCategories();

        return $this->render('@App/admin/system_config/taskCategory/category-config.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * Creates a new taskCategory entity.
     *
     * @Route("/new", name="tasks_category_new")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $country = $this->get('session')->get('region');
        /** @var Country $currentCountry */
        $currentCountry = $em->getRepository(Country::class)->findOneBy(['identifier' => $country]);
        $taskCategory = new TaskCategory();
        $countryCategory = new CountryCategory();
        $countryCategory->setCountry($currentCountry);
        $countryCategory->setCategory($taskCategory);
        $form = $this->createForm(CountryCategoryType::class, $countryCategory, ['country' => $country]);
        $form->handleRequest($request);
        $translator = $this->get('translator');
        if ($form->isSubmitted() && $form->isValid()) {
            /** Save all document translations */
            foreach ($request->get('category')['name'] as $language => $nameTranslation) {
                $taskCategory->translate($language)->setName($nameTranslation);
            }
            foreach ($request->get('category')['description'] as $language => $nameTranslation) {
                $taskCategory->translate($language)->setDescription($nameTranslation);
            }
            $taskCategory->mergeNewTranslations();
            $em->persist($taskCategory);
            /** Check if uploaded an icon */
            $file = $request->files->get('icon');
            if ($file) {
                $fileUploader = $this->get('file_uploader');
                $icon = $fileUploader->uploadFile(
                    $this->getParameter('categories_icons_directory'), $file
                );
                if ($icon) {
                    $em->persist($icon);
                    if ($taskCategory->getIcon()) {
                        $fileUploader->deleteFile($taskCategory->getIcon());
                    }
                    $taskCategory->setIcon($icon);
                }
            }
            $countryCategory->setPosition(count($currentCountry->getCategories()) + 1);
            $em->persist($countryCategory);
            /** duplicate category for other countries but as disabled */
            $countries = $em->getRepository(Country::class)->findAll();
            /** @var Country $country */
            foreach ($countries as $country) {
                if ($country->getIdentifier() !== $currentCountry->getIdentifier()) {
                    $newCountryCategory = new CountryCategory();
                    $newCountryCategory->setEnabled(false);
                    $newCountryCategory->setCategory($countryCategory->getCategory());
                    $newCountryCategory->setPosition($newCountryCategory->getPosition());
                    $newCountryCategory->setCountry($country);
                    $em->persist($newCountryCategory);
                }
            }
            $em->flush();
            $this->addFlash('success', $translator->trans('system.category.add.success'));
            return $this->redirect($this->generateUrl('system_task_categories'));
        }
        $languages = $em->getRepository(Language::class)->findAll();
        return $this->render('AppBundle:admin/system_config/taskCategory:new.category.html.twig', array(
            'form' => $form->createView(),
            'taskCategory' => $taskCategory,
            'languages' => $languages,
            'currentCountry' => $currentCountry
        ));
    }

    /**
     * Displays a form to edit an existing taskCategory entity.
     *
     * @Route("/{id}/edit", name="tasks_category_edit")
     * @param Request $request
     * @param TaskCategory $taskCategory
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, TaskCategory $taskCategory)
    {
        $em = $this->getDoctrine()->getManager();
        $country = $this->get('session')->get('region');
        /** @var Country $currentCountry */
        $currentCountry = $em->getRepository(Country::class)->findOneBy(['identifier' => $country]);
        $countryCategory = $em->getRepository(CountryCategory::class)->findOneBy(['country' => $country, 'category' => $taskCategory]);
        if (!$countryCategory) {
            $countryCategory = new CountryCategory();
            $countryCategory->setCountry($currentCountry);
            $countryCategory->setCategory($taskCategory);
        }
        $editForm = $this->createForm(CountryCategoryType::class, $countryCategory, ['country' => $country]);
        $editForm->handleRequest($request);
        $translator = $this->get('translator');
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            /** Save all document translations */
            foreach ($request->get('category')['name'] as $language => $nameTranslation) {
                $taskCategory->translate($language)->setName($nameTranslation);
            }
            foreach ($request->get('category')['description'] as $language => $nameTranslation) {
                $taskCategory->translate($language)->setDescription($nameTranslation);
            }
            $taskCategory->mergeNewTranslations();
            /** Check if updated icon */
            $file = $request->files->get('icon');
            if ($file) {
                $fileUploader = $this->get('file_uploader');
                $icon = $fileUploader->uploadFile(
                    $this->getParameter('categories_icons_directory'), $file
                );
                if ($icon) {
                    $em->persist($icon);
                    if ($taskCategory->getIcon()) {
                        $fileUploader->deleteFile($taskCategory->getIcon());
                    }
                    $taskCategory->setIcon($icon);
                }
            }
            $em->persist($taskCategory);
            $em->persist($countryCategory);
            $em->flush();
            $this->addFlash('success', $translator->trans('system.category.flash.update', ['%name%' => $taskCategory->translate()->getName()]));
            return $this->redirectToRoute('tasks_category_edit', ['id' => $taskCategory->getId()]);
        }
        $languages = $em->getRepository(Language::class)->findAll();
        return $this->render('@App/admin/system_config/taskCategory/edit.category.html.twig', array(
            'taskCategory' => $taskCategory,
            'form' => $editForm->createView(),
            'languages' => $languages,
            'currentCountry' => $currentCountry
        ));
    }

    /**
     * @Route("/update-positions", name="tasks_category_update_position")
     * @param Request $request
     * @return JsonResponse
     */
    public function updatePositionAction(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($request->get('positions') as $position) {
            /** @var CountryCategory $category */
            $category = $em->getRepository(CountryCategory::class)->find($position['id']);
            $category->setPosition($position['index']);
            $em->persist($category);
        }
        $em->flush();
        return new JsonResponse();
    }

    /**
     * @Route("/update-enable", name="tasks_category_update_enable")
     * @param Request $request
     * @return JsonResponse
     */
    public function configCategoryAction(Request $request): JsonResponse
    {
        $categoryId = $request->get('category');
        $em = $this->getDoctrine()->getManager();
        /** @var CountryCategory $category */
        $category = $em->getRepository(CountryCategory::class)->find($categoryId);
        $category->setEnabled(!$category->isEnabled());
        $em->persist($category);
        $em->flush();
        $msg = $this->get('translator')->trans('system.category.flash.update',
            ['%name%' => $category->translate()->getName()]);
        return $this->json(['message' => $msg]);
    }
}
