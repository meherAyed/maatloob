<?php

namespace AppBundle\Controller\Administration;

use ApiBundle\Controller\PaymentController;
use AppBundle\Entity\Indicator;
use AppBundle\Entity\Notification;
use AppBundle\Entity\Task;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * User controller.
 *
 * @Route("transactions")
 */
class TransactionController extends Controller
{
    /**
     * Finds and ban a user entity.
     *
     * @Route("", name="admin_transactions")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function transactionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $this->createIndexQuery($request);
        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the transactions list **/
        $transactions = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $pageLength
        );
        $indicator = $em->getRepository(Indicator::class)->findOneBy(['list'=>Indicator::LIST_TRANSACTIONS,'admin' => $this->getUser()]);
        try{
            $totalResult = $this->createIndexQuery($request)->select('coalesce(sum(t.price),0) as total')->getQuery()->getSingleResult();
        }catch (\Exception $e){
            $totalResult = ['total' => 0];
        }
        return $this->render('@App/admin/transactions/index.html.twig', [
            'transactions' => $transactions,
            'indicator' => $indicator,
            'totalResult' => $totalResult
        ]);
    }

    public function createIndexQuery(Request $request){
        /** Current browsing country */
        $country = $this->get('session')->get('region');

        $search = $request->query->get('search', '');
        $type = $request->query->get('type', 0);
        $orderBy = $request->query->get('sort_by', 'createdAt');
        $order = $request->query->get('order', 'DESC');

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Transaction::class)
            ->findTransactions($search, $type, $orderBy, $order)
            ->andWhere('fromUser.country = :country or toUser.country = :country')
            ->andWhere('t.status = 2')
            ->andWhere('t.type not in (:depWith)')
            ->setParameter('depWith',[Transaction::TYPE_DEPOSIT,Transaction::TYPE_WITHDRAW])
            ->setParameter('country', $country);

        /** Date range filter */
        if ($request->query->has('s_date')) {
            $query->andWhere('t.createdAt >= :startDate')
                ->setParameter('startDate', new \DateTime($request->get('s_date')));
        }
        if ($request->query->has('e_date')) {
            $query->andWhere('t.createdAt <= :endDate')
                ->setParameter('endDate', new \DateTime($request->get('e_date')));
        }
        return $query;
    }

    /**
     * Finds and ban a user entity.
     *
     * @Route("/export", name="admin_export_wallet_transactions")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function exportTransactionCsvAction(Request $request)
    {
        $transactions = $this->createIndexQuery($request)->getQuery()->getResult();
        $translator = $this->get('translator');
        $response = new StreamedResponse();
        $headers = [$translator->trans('transaction.table.date'), $translator->trans('transaction.table.from')
            , $translator->trans('transaction.table.to'), $translator->trans('transaction.table.task'),
            $translator->trans('transaction.table.type'),$translator->trans('transaction.table.payment_type'),
            $translator->trans('transaction.table.price')];
        $dbTranslator = $this->get('app.database_translator');
        $response->setCallback(function () use ($headers, $transactions, $translator,$dbTranslator) {
            $handle = fopen('php://output', 'w+');
            fputcsv($handle, array_values($headers), ',');
            /** @var Transaction $transaction */
            foreach ($transactions as $transaction) {
                $currency = $transaction->getToUser()?$transaction->getToUser():$transaction->getFromUser();
                $currency = $currency->getCountry()->getCurrencySymbol();
                $resultExport[$translator->trans('transaction.table.date')] = $transaction->getCreatedAt() ? $transaction->getCreatedAt()->format('d-m-Y') : '';
                $resultExport[$translator->trans('transaction.table.from')] = $transaction->getFromUser() ? $transaction->getFromUser()->getFullName() : '-';
                $resultExport[$translator->trans('transaction.table.to')] = $transaction->getToUser() ? $transaction->getToUser()->getFullName() : '-';
                $resultExport[$translator->trans('transaction.table.task')] = $transaction->getTask()?$transaction->getTask()->getTitle():'-';
                $resultExport[$translator->trans('transaction.table.type')] = $dbTranslator->transDb('transaction.type.'.$transaction->getType()).$currency;
                $resultExport[$translator->trans('transaction.table.payment_type')] = $translator->trans('task.payment_type.'.
                    str_replace([1,2,3],['cash','wallet','card'],$transaction->getPaymentType()));
                $resultExport[$translator->trans('transaction.table.price')] = number_format($transaction->getPrice(), 2, '.', ' ');
                fputcsv($handle, $resultExport, ',');
            }
            fclose($handle);
        });
        $response->headers->set('Content-Encoding', 'UTF-8');
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="Maatloob_transactions.csv"');
        echo "\xEF\xBB\xBF";
        return $response;
    }


    /**
     *
     * @Route("/deposit", name="admin_transactions_deposit")
     * @param Request $request
     * @return Response
     */
    public function depositAction(Request $request): Response
    {
        $query = $this->createDepositQuery($request);
        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the deposits list **/
        $deposits = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $pageLength
        );
        try{
            $totalResult = $this->createDepositQuery($request)->select('coalesce(sum(t.price),0) as total')->getQuery()->getSingleResult();
        }catch (\Exception $e){
            $totalResult = ['total' => 0];
        }
        return $this->render('@App/admin/transactions/deposit.html.twig', [
            'deposits' => $deposits,
            'totalResult' => $totalResult
        ]);
    }

    public function createDepositQuery(Request $request){
        /** Current browsing country */
        $country = $this->get('session')->get('region');

        $search = $request->query->get('search', '');
        $orderBy = $request->query->get('sort_by', 'createdAt');
        $order = $request->query->get('order', 'DESC');
        $query = $this->getDoctrine()->getRepository(Transaction::class)
            ->findTransactions($search, Transaction::TYPE_DEPOSIT, $orderBy, $order)
            ->andWhere('t.status = 2')
            ->andWhere('toUser.country = :country')
            ->setParameter('country', $country);
        /** Date range filter */
        if ($request->query->has('s_date')) {
            $query->andWhere('t.createdAt >= :startDate')
                ->setParameter('startDate', new \DateTime($request->get('s_date')));
        }
        if ($request->query->has('e_date')) {
            $query->andWhere('t.createdAt <= :endDate')
                ->setParameter('endDate', new \DateTime($request->get('e_date')));
        }
        return $query;
    }

    /**
     *
     * @Route("/withdrawal-request", name="admin_withdrawal_request")
     * @param Request $request
     * @return Response
     */
    public function withdrawalRequestAction(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $query = $this->createWithdrawQuery($request);
        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the requests list **/
        $requests = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $pageLength
        );
        $indicator = $em->getRepository(Indicator::class)->findOneBy(['list'=>Indicator::LIST_WITHDRAW_REQUESTS,'admin' => $this->getUser()]);
        try{
            $totalResult = $this->createWithdrawQuery($request)->select('coalesce(sum(t.price),0) as total')->getQuery()->getSingleResult();
        }catch (\Exception $e){
            $totalResult = ['total' => 0];
        }
        return $this->render('@App/admin/transactions/withdraw-request.html.twig', ['requests' => $requests,'indicator'=>$indicator,'totalResult'=>$totalResult]);
    }

    public function createWithdrawQuery(Request $request){
        /** Current browsing country */
        $country = $this->get('session')->get('region');

        $search = $request->query->get('search', '');
        $orderBy = $request->query->get('sort_by', 'createdAt');
        $order = $request->query->get('order', 'DESC');
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Transaction::class)
            ->findTransactions($search, Transaction::TYPE_WITHDRAW, $orderBy, $order)
            ->andWhere('fromUser.country = :country')
            ->setParameter('country', $country);

        $user = $this->getUser();
        if(!in_array(User::ROLE_SUPER_ADMIN, $user->getRoles(), true)){
            $query->andWhere('t.processedBy = :user or t.processedBy is null')
                    ->setParameter('user',$user);
        }


        if ($request->query->has('status')) {
            switch ($request->query->get('status')) {
                case 'pending' :
                    $query->andWhere('t.status = :status')->setParameter('status', Transaction::STATUS_PENDING);
                    break;
                case 'processing' :
                    $query->andWhere('t.status = :status')->setParameter('status', Transaction::STATUS_PROCESSING);
                    break;
                case 'completed' :
                    $query->andWhere('t.status = :status')->setParameter('status', Transaction::STATUS_COMPLETED);
                    break;
            }
        }

        /** Date range filter */
        if ($request->query->has('s_date')) {
            $query->andWhere('t.createdAt >= :startDate')
                ->setParameter('startDate', new \DateTime($request->get('s_date')));
        }
        if ($request->query->has('e_date')) {
            $query->andWhere('t.createdAt <= :endDate')
                ->setParameter('endDate', new \DateTime($request->get('e_date')));
        }

        return $query;
    }


    /**
     *
     * @Route("/withdrawal-request/{id}/details", name="admin_withdraw_request_details")
     * @param Transaction $withdrawRequest
     * @param Request $request
     * @return Response
     */
    public function withdrawalRequestDetailsAction(Transaction $withdrawRequest, Request $request): Response
    {
        if ($request->isMethod(Request::METHOD_POST) && $withdrawRequest->getStatus() !== Transaction::STATUS_COMPLETED) {
            $em = $this->getDoctrine()->getManager();
            $action = $request->get('action');
            $user = $withdrawRequest->getFromUser();
            switch ($action) {
                case 'cancel':
                    {
                        $withdrawRequest->setProcessedBy($this->getUser());
                        $withdrawRequest->setStatus(Transaction::STATUS_CANCELLED);
                        $user->getWallet()->unlockBalance($withdrawRequest->getPrice());
                        $em->persist($user->getWallet());
                        $notification = new Notification();
                        $notification->setTitle('notification.transaction_withdraw.title');
                        $notification->setBody('notification.transaction_withdraw.cancelled_body');
                        $notification->setToUser($user);
                        $notification->setType(Notification::ACTION_TRANSACTION);
                        $notification->setExtra([
                            'type' => Notification::TYPE_WITHDRAW
                        ]);
                        $em->persist($notification);
                        $em->persist($withdrawRequest);
                        $em->flush();
                        $cancelMsg = $this->get('translator')->trans('transaction.withdraw.cancelled');
                        if($request->isXmlHttpRequest()){
                            return new JsonResponse(['code' => Response::HTTP_OK,'message'=>$cancelMsg]);
                        }
                        $this->addFlash('success',$cancelMsg);
                        return $this->redirectToRoute('admin_withdrawal_request');
                    };
                    break;
                case 'process':
                    {
                        $withdrawRequest->setProcessedBy($this->getUser());
                        $withdrawRequest->setStatus(Transaction::STATUS_PROCESSING);
                        $em->persist($withdrawRequest);
                        $em->flush();
                        $this->addFlash('success',$this->get('translator')->trans('transaction.withdraw.processing'));
                        return $this->redirectToRoute('admin_withdraw_request_details',['id'=>$withdrawRequest->getId()]);
                    }break;
                case 'complete':{
                    $invoice = $request->files->get('invoice');
                    if(!$invoice){
                        $this->addFlash('error',$this->get('translator')->trans('transaction.withdraw.details.invoice.required'));
                        return $this->render('@App/admin/transactions/withdraw-request-details.html.twig',
                            ['request' => $withdrawRequest,'error' => true]);
                    }

                    $fileObject = $this->get('file_uploader')->uploadFile($this->getParameter('invoice_directory'),$invoice, $user);
                    $em->persist($fileObject);
                    $withdrawRequest->setWithdrawInvoice($fileObject);
                    $withdrawRequest->setStatus(Transaction::STATUS_COMPLETED);
                    $user->getWallet()->unlockBalance($withdrawRequest->getPrice());
                    $user->getWallet()->withdraw($withdrawRequest->getPrice());
                    $em->persist($user->getWallet());
                    $notification = new Notification();
                    $notification->setTitle('notification.transaction_withdraw.title');
                    $notification->setBody('notification.transaction_withdraw.completed_body');
                    $notification->setToUser($user);
                    $notification->setType(Notification::ACTION_TRANSACTION);
                    $notification->setExtra([
                        'type' => Notification::TYPE_TRANS,
                        'direction' => 'OUTGOING'
                    ]);
                    $em->persist($notification);
                    $em->persist($withdrawRequest);
                    $em->flush();
                    $this->addFlash('success',$this->get('translator')->trans('transaction.withdraw.completed'));
                    return $this->redirectToRoute('admin_withdraw_request_details',['id'=>$withdrawRequest->getId()]);
                }break;
            }
        }elseif ($request->isXmlHttpRequest()){
            return new JsonResponse(['code' => Response::HTTP_BAD_REQUEST,'message'=>$this->get('translator')->trans('invalid_request')]);
        }
        return $this->render('@App/admin/transactions/withdraw-request-details.html.twig', ['request' => $withdrawRequest]);
    }

    /**
     *
     * @Route("/deposit/{id}/details", name="admin_deposit_details")
     * @param Transaction $deposit
     * @param Request $request
     * @return Response
     */
    public function depositDetailsAction(Transaction $deposit, Request $request): Response
    {
        return $this->render('@App/admin/transactions/deposit-details.html.twig', ['deposit' => $deposit]);
    }
}
