<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 13/07/2019
 * Time: 17:44
 */

namespace AppBundle\Controller\Administration;


use AppBundle\AppEvents;
use AppBundle\Entity\Country;
use AppBundle\Entity\Indicator;
use AppBundle\Entity\Language;
use AppBundle\Entity\Page;
use AppBundle\Entity\User;
use AppBundle\Form\AdminType;
use AppBundle\Form\PageType;
use AppBundle\Form\SystemConfigType;
use Components\Helper;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SystemController
 * @package AppBundle\Controller\Administration
 * @Route("/system")
 */
class SystemController extends Controller
{

    /**
     * @Route("/", name="system_general")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function configAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $country = $this->get('session')->get('region','sa');
        /** @var Country $country */
        $country = $em->getRepository(Country::class)->findOneBy(['identifier'=>$country]);
        $countryConfig = $country->getConfig();
        $configForm = $this->createForm(SystemConfigType::class,$countryConfig,['country'=>$country->getIdentifier()]);
        $configForm->handleRequest($request);
        if ($configForm->isSubmitted() && $configForm->isValid()) {
            $countryConfig->setTaskProfits($request->request->get('system_config')['taskProfits']);
            $em->persist($countryConfig);
            $em->flush();
            $this->addFlash('success',$this->get('translator')->trans('system.general.flash.success'));
            return $this->redirectToRoute('system_general');
        }
        return $this->render('@App/admin/system_config/general-config.html.twig',
            [
                'config' => $country->getConfig(),
                'configForm' => $configForm->createView()
            ]);
    }

    /**
     * @Route("/pages", name="system_pages")
     * @return Response
     */
    public function pagesAction(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $pages = $this->getDoctrine()->getRepository(Page::class)
            ->findBy(['isDeleted' => false]);
        $languages = $em->getRepository(Language::class)->findAll();
        return $this->render('@App/admin/system_config/page/page-config.html.twig',[
            'pages' => $pages,
            'languages' => $languages
        ]);
    }

    /**
     * Update page
     * @Route("/pages/{identifier}/update", name="system_page_update", methods={"Get","Post"})
     * @param $identifier
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function updatePageAction($identifier, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Page $page */
        $page = $this->getDoctrine()->getRepository(Page::class)
            ->findOneBy(['isDeleted' => false, 'identifier' => $identifier]);
        $form = $this->createForm(PageType::class,$page);
        $form->handleRequest($request);
        $language = $request->query->get('lang','en');
        if ($form->isSubmitted() && $form->isValid()) {
            $page->translate($language)->setTitle($request->get('page_title'));
            $page->translate($language)->setContent($request->get('page_content'));
            $page->mergeNewTranslations();
            $em->persist($page);
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('pages.updated_success'));
            return $this->redirect($this->generateUrl('system_page_update', ['identifier' => $identifier,'lang'=>$language]));
        }
        $availableLanguages = $em->getRepository(Language::class)->findAll();
        return $this->render('@App/admin/system_config/page/page.edit.html.twig', [
            'form' => $form->createView(),
            'page' => $page,
            'language' => $language,
            'availableLanguages' => $availableLanguages
        ]);
    }

    /**
     * Show administrator page
     * @Route("/administrators", name="system_administrators", methods={"Get","Post"})
     * @return RedirectResponse|Response
     */
    public function administratorsAction(){
        $em = $this->getDoctrine()->getManager();
        $administrators = $em->getRepository(User::class)->findAdminsByRoles([
            User::ROLE_SUPERVISOR,User::ROLE_ACCOUNTANT,User::ROLE_ADMIN,User::ROLE_DASHBOARD_USER
        ]);
        return $this->render('@App/admin/system_config/administrators/index.html.twig',['administrators'=> $administrators]);
    }

    /**
     * delete administrator action
     * @Route("/administrators/{id}/delete", name="system_delete_administrator", methods={"Delete"})
     * @param User $admin
     * @return RedirectResponse|Response
     */
    public function deleteAdministratorAction(User $admin){
        if($admin->getId() !== $this->getUser()->getId()){
            $em = $this->getDoctrine()->getManager();
            $admin->setIsDeleted(true);
            $em->persist($admin);
            $em->flush();
        }
        return $this->json(['success'=>true,'message' => $this->get('translator')->trans('system.administrator.flash.delete_success',['#user#'=>$admin->getFullName()])]);
    }

    /**
     * Add new administrator to current country
     * @Route("/administrators/new", name="system_new_administrator", methods={"Get","Post"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function newAdministratorAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $userManager = $this->get('fos_user.user_manager');
        $admin = $userManager->createUser();
        $admin->setIsAdmin(true);
        $adminForm = $this->createForm(AdminType::class,$admin);
        $adminForm->handleRequest($request);
        if ($adminForm->isSubmitted() && $adminForm->isValid()) {
            if ($pictureFile = $adminForm->get('picture')->getData()) {
                $picture = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'),$pictureFile->file
                );
                $admin->setPicture($picture);
            }
            $eventDispatcher = $this->get('event_dispatcher');
            $admin->setEnabled(true);
            $form = $this->get('form.factory')->create();
            $form->setData($admin);
            $event = new FormEvent($form, $request);
            $eventDispatcher->dispatch(AppEvents::REGISTRATION_SUCCESS, $event);
            /** init admin indicators */
            foreach (Indicator::getSupportedLists() as $list){
                $indicator = new Indicator();
                $indicator->setAdmin($admin);
                $indicator->setList($list);
                $indicator->setItems([]);
                $em->persist($indicator);
            }
            $userManager->updateUser($admin);
            $this->addFlash('success', $this->get('translator')->trans('system.administrator.flash.add_success'));
            return $this->redirectToRoute('system_administrators');
        }
        return $this->render('@App/admin/system_config/administrators/new.html.twig', array(
            'user' => $admin,
            'form' => $adminForm->createView(),
        ));
    }

    /**
     *
     * @Route("/administrators/{name}-{id}/edit", name="admin_edit",methods={"GET", "POST"})
     * @param Request $request
     * @param User $admin
     * @return RedirectResponse|Response
     */
    public function editAdministratorAction(Request $request, User $admin)
    {
        $editForm = $this->createForm(AdminType::class, $admin);
        $editForm->handleRequest($request);
        $em =$this->getDoctrine()->getManager();
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $pictureFile = $request->files->get('user')['picture'];
            if ($pictureFile['file']) {
                $picture = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'),$pictureFile['file']
                );
                if($admin->getPicture()) {
                    $this->get('file_uploader')->deleteFile($admin->getPicture());
                }
                $admin->setPicture($picture);
            }
            $em->persist($admin);
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('users.edit_user.flash.success',['#user#'=>$admin->getFullName()]));
            return $this->redirectToRoute('admin_edit', ['name' => Helper::slugify($admin->getFullName(),'_'), 'id' => $admin->getId()]);
        }
        return $this->render('@App/admin/system_config/administrators/edit.html.twig', array(
            'admin' => $admin,
            'form' => $editForm->createView(),
        ));
    }
}
