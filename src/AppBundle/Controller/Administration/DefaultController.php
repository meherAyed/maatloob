<?php

namespace AppBundle\Controller\Administration;

use AppBundle\Entity\Country;
use AppBundle\Entity\Indicator;
use AppBundle\Entity\Notification;
use AppBundle\Entity\Offer;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskCategory;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Entity\UserWallet;
use AppBundle\Form\Model\ChangePassword;
use AppBundle\Form\ProfileType;
use AppBundle\Form\ResetPasswordType;
use Components\Helper;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    public function indexAction(Request $request)
    {
        /** Current browsing country */
        $country = $this->get('session')->get('region');

        $year = (new \DateTime())->format('Y');
        $em = $this->getDoctrine()->getManager();
        $userStat = $em->getRepository(User::class)->getDashboardStat($year, $country);
        $taskStat = $em->getRepository(Task::class)->getDashboardStat($year, $country);
        $transactionStat = $em->getRepository(Transaction::class)->getTransactionStat($year, $country);
        $revenueStat = $em->getRepository(Transaction::class)->getRevenueStat($year, $country);
        $balancesStat = $em->getRepository(UserWallet::class)->getDashboardStat($country);
        /** System free storage in megabytes */
        $freeServerStorage = disk_free_space($this->getParameter('kernel.project_dir') . '/web/') / (1024 * 1024);
        $finder = new Finder();
        $userDataSize = 0;
        /** @var \SplFileInfo $file */
        foreach ($finder->in($this->getParameter('kernel.project_dir') . '/web/uploads')->files() as $file) {
            $userDataSize += $file->getSize() / (1024 * 1024);
        }
        /** @var \SplFileInfo $file */
        foreach ($finder->in($this->getParameter('kernel.project_dir') . '/protected')->files() as $file) {
            $userDataSize += $file->getSize() / (1024 * 1024);
        }
        $tasksPerDay = $em->getRepository(Task::class)
            ->createQueryBuilder('t')
            ->select('DATE_FORMAT(t.createdAt, \'%Y-%m-%d\') as x , count(t) as y')
            ->join('t.user','user')
            ->where('t.createdAt between :startDate and :endDate')
            ->andWhere('user.country = :country')
            ->groupBy('x')
            ->setParameters([
                'startDate' => new \DateTime('1st January this Year'),
                'endDate' => new \DateTime(),
                'country' => $country
            ])
            ->getQuery()->getResult();
        $offersPerDay = $em->getRepository(Offer::class)
            ->createQueryBuilder('o')
            ->select('DATE_FORMAT(o.createdAt, \'%Y-%m-%d\') as x,count(o) as y')
            ->join('o.user','user')
            ->where('o.createdAt between :startDate and :endDate')
            ->andWhere('user.country = :country')
            ->groupBy('x')
            ->setParameters([
                'startDate' => new \DateTime('1st January this Year'),
                'endDate' => new \DateTime(),
                'country' => $country
            ])
            ->getQuery()->getResult();

        $topCategories = $em->getRepository(TaskCategory::class)
            ->findTopCategories($country,$request->getLocale());
        $delayedTasks = $em->getRepository(Task::class)->createQueryBuilder('t')
            ->join('t.user', 'user')
            ->where('t.status = :status')
            ->andWhere('user.country = :country')
            ->andWhere('t.date < :dateNow')
            ->setParameters(['status' => Task::STATUS_ASSIGNED, 'dateNow' => new \DateTime(),'country'=>$country])
            ->getQuery()->getResult();
        $completedTasks = $em->getRepository(Task::class)->getCompletedTaskStats($country);
        $cancelledTasks = $em->getRepository(Task::class)->getCancelledTaskStats($country);
        $totalBannedUsers = count($em->getRepository(User::class)->findBannedUsers(null, null)
            ->andWhere('u.country = :country')
            ->setParameter('country', $country)
            ->getQuery()->getResult());

        $profits = $em->getRepository(Transaction::class)->getProfitsStats($country);
        $topFivePoster = $em->getRepository(User::class)->findTopFivePoster($country);
        $topFiveTasker = $em->getRepository(User::class)->findTopFiveTasker($country);
        $topFiveCanceller = $em->getRepository(User::class)->findTopFiveCanceller($country);
        $topFiveDecreasing = $em->getRepository(User::class)->findTopFiveDecreasing($country);
        $topFiveIncreasing = $em->getRepository(User::class)->findTopFiveIncreasing($country);

        return $this->render('@App/admin/dashboard.html.twig',
            [
                'totalServerStorage' => ($freeServerStorage + $userDataSize),
                'usersDataSize' => $userDataSize,
                'userStats' => $userStat,
                'taskStats' => $taskStat,
                'transactionStats' => $transactionStat,
                'revenueStats' => $revenueStat,
                'balanceStats' => $balancesStat,
                'offerPerDay' => $offersPerDay,
                'taskPerDay' => $tasksPerDay,
                'topCategories' => $topCategories,
                'delayedTasks' => $delayedTasks,
                'completedTasks' => $completedTasks,
                'cancelledTasks' => $cancelledTasks,
                'totalBannedUsers' => $totalBannedUsers,
                'profits' => $profits,
                'topFivePoster' => $topFivePoster,
                'topFiveTasker' => $topFiveTasker,
                'topFiveCanceller' => $topFiveCanceller,
                'topFiveDecreasing' => $topFiveDecreasing,
                'topFiveIncreasing' => $topFiveIncreasing
            ]);
    }

    /**
     * @return JsonResponse
     */
    public function markNotificationSeenAction(): JsonResponse
    {
        $user = $this->getUser();
        $this->getDoctrine()->getRepository(Notification::class)
            ->createQueryBuilder('n')
            ->update(Notification::class, 'notif')
            ->set('notif.shownOn', 1)
            ->where('notif.toUser = :user and notif.shownOn = 0')
            ->setParameter('user', $user)
            ->getQuery()->execute();
        return $this->json(['message' => 'notification status updated']);
    }

    public function profileAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $data = new ChangePassword($this->getUser());
        $formDetails = $this->createForm(ProfileType::class, $user);
        $formReset = $this->createForm(ResetPasswordType::class, $data, ['container' => $this->get('service_container')]);
        $formDetails->handleRequest($request);
        $formReset->handleRequest($request);
        if ($formDetails->isSubmitted() && $formDetails->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (isset($formDetails->get('picture')->getData()->file)) {
                $userPicture = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'),
                    $formDetails->get('picture')->getData()->file,
                    $user
                );
                $em->remove($user->getPicture());
                $em->persist($userPicture);
                $user->setPicture($userPicture);
            }
            if (isset($formDetails->get('coverPicture')->getData()->file)) {
                $userCoverPicture = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'),
                    $formDetails->get('coverPicture')->getData()->file,
                    $user
                );
                $em->remove($user->getCoverPicture());
                $em->persist($userCoverPicture);
                $user->setCoverPicture($userCoverPicture);
            }
            $this->addFlash('success', $this->get('translator')->trans('profile.flash.details_updated'));
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('admin_profile');
        }
        if ($formReset->isSubmitted()) {
            if ($formReset->isValid()) {
                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($user);
                $password = $encoder->encodePassword($data->getNewPassword(), $data->getSalt());
                $user->setPassword($password);
                $manager = $this->getDoctrine()->getManager();
                $manager->flush();
                return $this->json(['message' => $this->get('translator')->trans('profile.flash.password_updated')]);
            } else {
                return $this->json([
                        'message' => $this->get('translator')->trans('profile.flash.update_password_failed'),
                        'errors' => Helper::getErrorsFromForm($formReset)
                    ]
                    , Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }


        return $this->render('@App/admin/profile.html.twig', [
            'detailsForm' => $formDetails->createView(),
            'resetPasswordForm' => $formReset->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function getNotificationsAction(Request $request)
    {
        $translator = $this->get('translator');
        $query = $this->getDoctrine()->getRepository(Notification::class)
            ->createQueryBuilder('n')
            ->where('n.toUser = :user')
            ->orderBy('n.createdAt', 'DESC')
            ->setParameter('user', $this->getUser())
            ->getQuery();
        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the user list **/
        $notifications = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $pageLength
        );
        /** @var Notification $item */
        foreach ($notifications->getItems() as $item) {
            $item->setTitle($translator->trans($item->getTitle()));
            $item->setBody($translator->trans($item->getBody(), $item->getParams()));
        }
        if ($request->isXmlHttpRequest()) {
            $data = $this->get('jms_serializer')->toArray($notifications->getItems(), SerializationContext::create()
                ->setGroups(['user_notifications', 'user_info', 'file_info']));
            return $this->json(['notifications' => $data]);
        }
        return $this->render('@App/admin/notifications.html.twig', ['notifications' => $notifications]);
    }

    /**
     * Switch country
     * @param Country $country
     * @return JsonResponse
     */
    public function switchToRegionAction(Country $country): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        if($user->isSuperAdmin() || $user->isAssignedToCountry($country)){
            $this->get('session')->set('region', $country->getIdentifier());
            $this->addFlash('success',$this->get('translator')->trans('system.country.switched',['#country#' => $country->getName()]));
            return $this->json(['msg' => 'success'], Response::HTTP_OK);
        }
        return $this->json(['msg' => 'invalid request'], Response::HTTP_FORBIDDEN);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function itemIndicatorAction(Request $request): JsonResponse
    {
        $item = $request->get('item');
        $list = $request->get('list');
        $user = $this->getUser();
        if($item && in_array($list, Indicator::getSupportedLists(), true)){
            $em = $this->getDoctrine()->getManager();
            /** @var Indicator $indicator */
            $indicator = $em->getRepository(Indicator::class)->findOneBy(['list'=>$list,'admin'=>$user]);
            if(!$indicator){
                $indicator = new Indicator();
                $indicator->setAdmin($user);
                $indicator->setList($list);
            }
            if(!in_array($item, $indicator->getItems(), true)){
                $indicator->addItem($item);
            }else{
                $indicator->removeItem($item);
            }
            $em->persist($indicator);
            $em->flush();
        }

        return $this->json(['success'=>true]);
    }

}
