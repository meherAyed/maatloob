<?php

namespace AppBundle\Controller\Administration;

use AppBundle\AppEvents;
use AppBundle\Entity\Country;
use AppBundle\Entity\DocumentType;
use AppBundle\Entity\Indicator;
use AppBundle\Entity\Notification;
use AppBundle\Entity\Offer;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskDiscussion;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBan;
use AppBundle\Entity\UserDocument;
use AppBundle\Entity\UserReview;
use AppBundle\Form\UserType;
use Components\Helper;
use DateTime;
use Exception;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * User controller.
 *
 * @Route("users")
 */
class UserController extends Controller
{

    /**
     * Lists all active users.
     *
     * @Route("/", name="users")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function activeUsersAction(Request $request): Response
    {
        /** Current browsing country */
        $country = $this->get('session')->get('region');

        $search = $request->query->get('search', '');
        $status = $request->query->get('status');
        $orderBy = $request->query->get('sort_by', 'createdAt');
        $order = $request->query->get('order', 'DESC');
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(User::class)
            ->findActiveUsers($search, $status, $orderBy, $order)
            ->andWhere('u.country = :country')
            ->setParameter('country', $country);

        /** Date range filter */
        if ($request->query->has('s_date')) {
            $query->andWhere('u.createdAt >= :startDate')
                ->setParameter('startDate', new DateTime($request->get('s_date')));
        }
        if ($request->query->has('e_date')) {
            $query->andWhere('u.createdAt <= :endDate')
                ->setParameter('endDate', new DateTime($request->get('e_date')));
        }

        $verifiedUser = count($em->getRepository(User::class)
            ->createQueryBuilder('u')
            ->andWhere('u.verified = 1')
            ->andWhere('u.country = :country')
            ->setParameter('country', $country)
            ->getQuery()->getResult());
        $totalAccount = count($em->getRepository(User::class)
            ->createQueryBuilder('u')
            ->where('u.enabled = 1')
            ->andWhere('u.country = :country')
            ->setParameter('country', $country)
            ->getQuery()->getResult());
        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the user list **/
        $users = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $pageLength
        );
        $indicator = $em->getRepository(Indicator::class)->findOneBy(['list' => Indicator::LIST_USERS, 'admin' => $this->getUser()]);
        return $this->render('@App/admin/user/users.html.twig', array(
            'users' => $users,
            'verifiedUserNumber' => $verifiedUser,
            'totalAccount' => $totalAccount,
            'indicator' => $indicator
        ));
    }

    /**
     * Lists all active users.
     *
     * @Route("/inactive", name="inactive_users")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function inactiveUsersAction(Request $request): Response
    {
        /** Current browsing country */
        $country = $this->get('session')->get('region');
        $search = $request->query->get('search', '');
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(User::class)
            ->createQueryBuilder('u')
            ->where('u.enabled = false')
            ->andWhere('u.country = :country')
            ->setParameter('country', $country);

        if ($search) {
            $query->andWhere('u.email like :search')
                ->setParameter('search', '%' . $search . '%');
        }
        /** Date range filter */
        if ($request->query->has('s_date')) {
            $query->andWhere('u.createdAt >= :startDate')
                ->setParameter('startDate', new DateTime($request->get('s_date')));
        }
        if ($request->query->has('e_date')) {
            $query->andWhere('u.createdAt <= :endDate')
                ->setParameter('endDate', new DateTime($request->get('e_date')));
        }

        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the user list **/
        $users = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $pageLength
        );

        $indicator = $em->getRepository(Indicator::class)->findOneBy(['list' => Indicator::LIST_USERS, 'admin' => $this->getUser()]);
        return $this->render('@App/admin/user/inactive_users.twig', array(
            'users' => $users,
            'indicator' => $indicator
        ));
    }

    /**
     * Lists all user entities.
     *
     * @Route("/banned", name="user_banned")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function bannedAction(Request $request): Response
    {
        /** Current browsing country */
        $country = $this->get('session')->get('region');

        $search = $request->query->get('search', '');
        $status = $request->query->get('status');
        $orderBy = $request->query->get('sort_by', 'createdAt');
        $order = $request->query->get('order', 'DESC');
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(User::class)
            ->findBannedUsers($search, $status, $orderBy, $order)
            ->andWhere('u.country = :country')
            ->setParameter('country', $country);

        /** Date range filter */
        if ($request->query->has('s_date')) {
            $query->andWhere('u.createdAt >= :startDate')
                ->setParameter('startDate', new DateTime($request->get('s_date')));
        }
        if ($request->query->has('e_date')) {
            $query->andWhere('u.createdAt <= :endDate')
                ->setParameter('endDate', new DateTime($request->get('e_date')));
        }

        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the user list **/
        $bannedUser = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $pageLength
        );
        $indicator = $em->getRepository(Indicator::class)->findOneBy(['list' => Indicator::LIST_USERS, 'admin' => $this->getUser()]);
        return $this->render('AppBundle::admin/user/banned.html.twig', array(
            'users' => $bannedUser,
            'indicator' => $indicator
        ));
    }

    /**
     * Add new user
     * @Route("/new", name="user_add",methods={"GET", "POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** Current browsing country */
        $country = $this->get('session')->get('region','sa');
        $country = $em->getRepository(Country::class)->find($country);
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setCountry($country);
        $user->setRoles([User::ROLE_DEFAULT]);

        $userForm = $this->createForm(UserType::class, $user,['admin'=>false]);
        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $user->setUsername($user->getEmail());
            $pictureFile = $request->files->get('user')['picture'];
            if ($pictureFile['file']) {
                $picture = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'),$pictureFile['file'], $user
                );
                if($user->getPicture()) {
                    $this->get('file_uploader')->deleteFile($user->getPicture());
                }
                $user->setPicture($picture);
            }
            $coverFile = $request->files->get('user')['coverPicture'];
            if ($coverFile['file']) {
                $cover = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'),$coverFile['file'], $user
                );
                if($user->getCoverPicture()) {
                    $this->get('file_uploader')->deleteFile($user->getCoverPicture());
                }
                $user->setCoverPicture($cover);
            }
            $eventDispatcher = $this->get('event_dispatcher');
            $form = $this->get('form.factory')->create();
            $form->setData($user);
            $event = new FormEvent($form, $request);
            $eventDispatcher->dispatch(AppEvents::REGISTRATION_SUCCESS, $event);
            $userManager->updateUser($user);
            $this->addFlash('success', $this->get('translator')->trans('users.new_user.flash.success'));
            return $this->redirectToRoute('user_details', array('id' => $user->getId()));
        }
        return $this->render('AppBundle::admin/user/new.html.twig', array(
            'user' => $user,
            'form' => $userForm->createView(),
        ));
    }

    /**
     * Finds and displays a user entity and save modification if present.
     *
     * @Route("/{id}/details", name="user_details")
     * @param User $user
     * @return RedirectResponse|Response
     */
    public function userDetailsAction(User $user)
    {
        $doc = $this->getDoctrine();
        /** Reviews as tasker */
        $totalReviews = $doc->getRepository(UserReview::class)
            ->findBy(['asA' => User::TYPE_TASKER, 'user' => $user], ['createdAt' => 'DESC']);

        $totalReviewNote = 0;
        $taskerStat['reviews'] = $totalReviews;
        $taskerStat['reviewsNotes'] = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0];
        /** @var UserReview $review */
        foreach ($totalReviews as $review) {
            $totalReviewNote += $review->getNote();
            $taskerStat['reviewsNotes'][$review->getNote()]++;
        }
        if (count($totalReviews) > 0)
            $totalReviewNote = $totalReviewNote / count($totalReviews);
        $taskerStat['totalReviews'] = count($totalReviews);
        $taskerStat['totalReviewsNote'] = $totalReviewNote;
        $taskerStat['completedTasks'] = count($doc->getRepository(Offer::class)->findCompleted($user));
        /** Reviews as poster */
        $totalReviews = $doc->getRepository(UserReview::class)
            ->findBy(['asA' => User::TYPE_POSTER, 'user' => $user], ['createdAt' => 'DESC']);
        $totalReviewNote = 0;
        $posterStat['reviews'] = $totalReviews;
        $posterStat['reviewsNotes'] = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0];
        /** @var UserReview $review */
        foreach ($totalReviews as $review) {
            $totalReviewNote += $review->getNote();
            $posterStat['reviewsNotes'][$review->getNote()]++;
        }
        if (count($totalReviews) > 0)
            $totalReviewNote = $totalReviewNote / count($totalReviews);

        $posterStat['totalReviews'] = count($totalReviews);
        $posterStat['totalReviewsNote'] = $totalReviewNote;
        $posterStat['completedTasks'] = count($doc->getRepository(Task::class)->findCompleted($user));
        $haveToPay = $doc->getRepository(Task::class)
            ->getTotalUnpaidTask($user);
        return $this->render('AppBundle::admin/user/show.html.twig', array(
            'user' => $user,
            'haveToPay' => $haveToPay,
            'posterReviews' => $posterStat,
            'taskerReviews' => $taskerStat,
        ));
    }


    /**
     * Finds and displays a user entity and save modification if present.
     *
     * @Route("/{id}/edit", name="user_edit",methods={"GET", "POST"})
     * @param Request $request
     * @param User $user
     * @return RedirectResponse|Response
     */
    public function userEditAction(Request $request, User $user)
    {
        $editForm = $this->createForm(UserType::class, $user);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $pictureFile = $request->files->get('user')['picture'];
            if ($pictureFile['file']) {
                $picture = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'),$pictureFile['file'], $user
                );
                if($user->getPicture()) {
                    $this->get('file_uploader')->deleteFile($user->getPicture());
                }
                $user->setPicture($picture);
            }
            $coverFile = $request->files->get('user')['coverPicture'];
            if ($coverFile['file']) {
                $cover = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'),$coverFile['file'], $user
                );
                if($user->getCoverPicture()) {
                    $this->get('file_uploader')->deleteFile($user->getCoverPicture());
                }
                $user->setCoverPicture($cover);
            }
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', "User {$user->getFullName()} has been updated");
            $this->addFlash('success', $this->get('translator')->trans('users.edit_user.flash.success',['#user#'=>$user->getFullName()]));
            return $this->redirectToRoute('user_edit', ['id' => $user->getId()]);
        }
        return $this->render('AppBundle::admin/user/edit.html.twig', array(
            'user' => $user,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Finds and displays a documents.
     *
     * @Route("/documents", name="user_documents",methods={"POST","GET"})
     * @param Request $request
     * @return Response
     */
    public function userDocumentsAction(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        /** Current browsing country */
        $country = $this->get('session')->get('region');
        $search = $request->query->get('search', '');
        $status = $request->query->get('status','all');
        $type = $request->query->get('type','all');
        $orderBy = $request->query->get('sort_by', 'date');
        $order = $request->query->get('order', 'DESC');

        $query = $em->getRepository(UserDocument::class)
            ->findByCountryAndSearch($country,$search,$status,$type,$orderBy,$order);

        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the user list **/
        $documents = $paginator->paginate(
            $query->getQuery(),
            $request->query->getInt('page', 1),
            $pageLength
        );
        $indicator = $em->getRepository(Indicator::class)->findOneBy(['list' => Indicator::LIST_USER_DOCUMENTS, 'admin' => $this->getUser()]);
        $documentsTypes = $em->getRepository(DocumentType::class)->findBy(['country' => $country]);
        return $this->render('@App/admin/user/documents.html.twig', [
            'documents' => $documents,
            'indicator' => $indicator,
            'documentsTypes'=>$documentsTypes]);
    }


    /**
     * Admin user document verification
     * @Route("/documents/verification", name="user_document_verification" , methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function documentVerificationAction(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $verified = false;
        $translator = $this->get('translator');
        /** @var UserDocument $document */
        $document = $em->getRepository(UserDocument::class)
            ->findOneBy(['id' => $request->get('id'), 'verified' => false]);
        $user = $document->getUser();
        if ($document) {
            $verified = $request->get('verified');
            $document->setVerified($verified);
            $document->setVerificationDate(new \DateTime());
            $em->flush();
            if ($verified) {
                $document->setStatus(UserDocument::STATUS_VERIFIED);
                $msg = 'notification.document_verification.accepted';
                /** Verify if account has now minimum verified documents  */
                $totalVerifiedDocuments = $em->getRepository(UserDocument::class)
                    ->totalVerifiedDocument($user);
                if (!$user->isVerified() && $totalVerifiedDocuments >= $user->getCountry()->getConfig()->getMinMandatoryDocumentToVerifyAccount()) {
                    $user->setVerified(true);
                    $em->persist($user);
                    $em->flush();
                    $this->sendAccountVerifiedNotification($user);
                }
            } else {
                $document->setStatus(UserDocument::STATUS_REFUSED);
                $msg = 'notification.document_verification.refused';
            }
            $notification = new Notification();
            $notification->setTitle('notification.document_verification.title');
            $notification->setBody($msg);
            $notification->setToUser($user);
            $notification->setExtra([
                'type' => Notification::TYPE_DOCUMENT_VERIFY,
                'document_id' => $document->getId()
            ]);
            $notification->setParams([
                '#doc#' => $document->getType()->translate($user->getLanguage())->getName(),
                '#reason#' => $request->get('refuseReason')
            ]);
            $notification->setType(Notification::ACTION_HELP_INFO);
            $em->persist($notification);
            $em->flush();
        }
        return $this->json(['message' => $verified ? $translator->trans('system.documents.verification.verified')  : $translator->trans('system.documents.verification.cancelled') ]);
    }

    /**
     * Send document verification notification to user
     * @param User $user
     */
    private function sendAccountVerifiedNotification(User $user): void
    {
        $em = $this->getDoctrine()->getManager();
        $notification = new Notification();
        $notification->setTitle('notification.account_verified.title');
        $notification->setBody('notification.account_verified.body');
        $notification->setToUser($user);
        $notification->setType(Notification::ACTION_HELP_INFO);
        $notification->setExtra(['type'=>'']);
        $em->persist($notification);
        $em->flush();
    }

    /**
     * Finds and displays users discussions.
     *
     * @Route("/discussions/{id}", name="user_discussions",defaults={"id" = null})
     * @param Request $request
     * @param TaskDiscussion|null $discussion
     * @return Response
     */
    public function userDiscussionsAction(Request $request, TaskDiscussion $discussion = null): Response
    {
        $country = $this->get('session')->get('region', 'en');
        $search = $request->query->get('search', '');

        $discussionsQuery = $this->getDoctrine()->getRepository(TaskDiscussion::class)
            ->createQueryBuilder('d')
            ->join('d.from', 'fromUser')
            ->join('d.to', 'toUser')
            ->join('d.task', 'task')
            ->join('d.messages','m')
            ->where('fromUser.country = :country')
            ->orderBy('d.createdAt', 'DESC')
            ->setParameter('country', $country);
        /** Add search filter */
        if (!empty($search)) {
            $discussionsQuery->andWhere('task.title like :search')
                ->orWhere("fromUser.firstName like :search or fromUser.lastName like :search or concat(fromUser.firstName,' ',fromUser.lastName) like :search")
                ->orWhere("toUser.firstName like :search or toUser.lastName like :search or concat(toUser.firstName,' ',toUser.lastName) like :search")
                ->setParameter('search', '%' . $search . '%');
        }

        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the user list **/
        $page = $request->query->getInt('page', 1);
        $discussions = $paginator->paginate(
            $discussionsQuery->getQuery(),
            $page,
            $pageLength
        );

        if ($request->isXmlHttpRequest()) {
            $data = $this->get('jms_serializer')->toArray($discussions->getItems(), SerializationContext::create()
                ->setGroups(['discussions', 'user_info', 'task_discussion', 'file_info']));
            return $this->json(['discussions' => $data, 'hasMore' => $discussions->getPageCount() - $page > 0]);
        }

        return $this->render('@App/admin/user/discussions.html.twig', [
            'discussions' => $discussions,
            'activeDiscussion' => $discussion
        ]);
    }

    /**
     * Finds and displays users discussions.
     *
     * @Route("/discussions/{id}/messages", name="user_discussions_messages")
     * @param TaskDiscussion $discussion
     * @return Response
     */
    public function discussionMessagesAction(TaskDiscussion $discussion): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->toArray($discussion->getMessages(), SerializationContext::create()
            ->setGroups(['task_message', 'user_info', 'file_info']));
        return $this->json(['data' => $data]);
    }

    /**
     * Finds and ban a user entity.
     *
     * @Route("/{id}/ban", name="user_ban",methods={"POST"})
     * @param Request $request
     * @param User $user
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function banAction(Request $request, User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $userBan = self::banUser($request->request->all());
        if (!$userBan) {
            return $this->json(['message' => $translator->trans('invalid_request')], Response::HTTP_BAD_REQUEST);
        }
        $userBan->setBannedBy($this->getUser());
        $userBan->setUser($user);
        $em->persist($userBan);
        $em->flush();
        $appService = $this->container->get('app_service');
        $appService->sendBanNotification($userBan);
        return $this->json(['message' => $this->get('translator')->trans('users.modal.block_user.flash.success')]);
    }

    /**
     * @param $data
     * @return UserBan|bool
     * @throws Exception
     */
    public static function banUser($data)
    {
        /** First check if all input are valid */
        if (empty($data['reason'])) {
            return false;
        }
        $newUserBan = new UserBan();
        $newUserBan->setBanReason($data['reason']);
        if (isset($data['duration']) && ($unbanDate = new \DateTime($data['duration'])) > new \DateTime()) {
            $newUserBan->setUnbanDate($unbanDate);
        }
        if (isset($data['unbanPenalty']) && (float)$data['unbanPenalty'] > 0) {
            $newUserBan->setUnbanPenalty((float)$data['unbanPenalty']);
        }
        return $newUserBan;
    }

    /**
     * Finds and ban a user entity.
     *
     * @Route("/{id}/restore_access", name="user_restore_access",methods={"POST"})
     * @param User $user
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function restoreAccessAction(User $user, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $trans = $this->get('translator');
        $userBan = $user->getActiveBan();
        if (($penalty = (float)$request->get('penalty', 0)) > 0) {
            /** Check if the user has enough balance in his wallet*/
            if ($penalty > 0 && $penalty <= $user->getWallet()->getBalance()) {
                $transaction = new Transaction();
                $transaction->setType(Transaction::TYPE_FEES);
                $transaction->setFromUser($user);
                $transaction->setPrice($penalty);
                $user->getWallet()->withdraw($penalty);
                $em->persist($user->getWallet());
                $em->persist($transaction);
                $em->flush();
                $userBan->setUnbanPenaltyPayed(true);
            } else {
                return $this->json([
                    'message' => $trans->trans('users.banned.restore.error.no_enough_money')],
                    Response::HTTP_BAD_REQUEST);
            }
        }
        $userBan->setBanLifted(true);
        $userBan->getUser()->setBlockedStars(0);
        $em->persist($userBan->getUser());
        $em->persist($userBan);
        $em->flush();
        $appService = $this->container->get('app_service');
        $appService->sendBanNotification($userBan);
        return $this->json([
            'message' => $trans->trans('users.banned.restore.success')],
            Response::HTTP_OK
        );
    }


    /**
     * Set user balance.
     *
     * @Route("/{user}/set-balance", name="admin_set_balance",methods={"POST"})
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function setBalanceAction(Request $request, User $user): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $balance = (float)$request->get('balance', 0);
        if (!$balance) {
            $this->json(['message' => 'Invalid balance'], 400);
        }
        $operationType = $request->get('operation', 'deposit');
        $userWallet = $user->getWallet();
        $transaction = new Transaction();
        $transaction->setPrice($balance);
        $transaction->setType(Transaction::TYPE_SUPPORT_CORRECTION);
        if ($operationType === 'deposit') {
            $userWallet->deposit($balance);
            $transaction->setToUser($user);
        } else {
            $userWallet->withdraw($balance);
            $transaction->setFromUser($user);
        }
        $em->persist($userWallet);
        $em->persist($transaction);
        /** Create notification for user */
        $notification = new Notification();
        $notification->setToUser($user);
        $notification->setType(Notification::ACTION_TRANSACTION);
        $notification->setTitle('notification.wallet_support');
        $notification->setBody($operationType === 'deposit' ? 'notification.wallet_support_deposit' : 'notification.wallet_support_withdraw');
        $notification->setParams([
            '#price#' => $transaction->getPrice() . $user->getCountry()->getCurrencySymbol()
        ]);
        $notification->setExtra([
            'type' => Notification::TYPE_TRANS,
            'direction' => $operationType === 'deposit' ? 'INCOMING' : 'OUTGOING'
        ]);
        $em->persist($notification);
        $em->flush();
        return $this->json([
            'balance' => $userWallet->getBalance(),
            'message' => $this->get('translator')->trans('users.modal.update_balance.flash')
            ]);
    }


    /**
     * Unlock user blocked stars
     *
     * @Route("/{user}/unlock-stars", name="admin_unlock_user_stars",methods={"PATCH"})
     * @param User $user
     * @return JsonResponse
     */
    public function unlockUserStarsAction(User $user): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $user->setBlockedStars(0);
        $em->persist($user);
        $em->flush();
        return $this->json([
            'message' => $this->get('translator')->trans('users.unlock_stars.flash',['#user#'=> $user->getFullName()])
        ]);
    }

    /**
     * Delete inactive user account
     *
     * @Route("/{user}/delete", name="admin_delete_inactive_account",methods={"DELETE"})
     * @param User $user
     * @return RedirectResponse|Response
     */
    public function deleteInactiveAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        if (!$user->isEnabled()) {
            $em->remove($user);
            $em->flush();
            return $this->json(['message' => 'User has been deleted'], Response::HTTP_OK);
        }

        return $this->json(['message' => 'Cant delete activated user'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * Send activation email to inactive user
     *
     * @Route("/{user}/send_activation_email", name="admin_send_activation_email",methods={"POST"})
     * @param Request $request
     * @param User $user
     * @return RedirectResponse|Response
     */
    public function sendActivationEmailAction(Request $request, User $user)
    {
        $trans = $this->get('translator');
        if (!$user->isEnabled()) {
            $eventDispatcher = $this->get('event_dispatcher');
            $form = $this->get('form.factory')->create();
            $form->setData($user);
            $event = new FormEvent($form, $request);
            $eventDispatcher->dispatch(AppEvents::REGISTRATION_SUCCESS, $event);
            $this->get('fos_user.user_manager')->updateUser($user);
            return $this->json(['message' => $trans->trans('users.flash.activation_email_send')], Response::HTTP_OK);
        }

        return $this->json(['message' => $trans->trans('users.flash.account_already_activated')], Response::HTTP_BAD_REQUEST);
    }

    /**
     * Send activation email to inactive user
     *
     * @Route("/{task}/send_remember_delay_tasker", name="admin_send_delayed_task_email",methods={"POST"})
     * @param Request $request
     * @param Task $task
     * @return RedirectResponse|Response
     */
    public function sendRememberDelayTaskerAction(Request $request, Task $task)
    {
        $trans = $this->get('translator');
        if ($task->is(Task::STATUS_ASSIGNED)) {
            $tasker = $task->getTasker();
            $notification = new Notification();
            $notification->setToUser($tasker);
            $notification->setTitle('notification.remember_delay_tasker.title');
            $notification->setBody('notification.remember_delay_tasker.body');
            $notification->setType(Notification::ACTION_TASK_REMINDER);
            $notification->setParams(['#task#' => $task->getTitle()]);
            $notification->setExtra([
                'task_id' => $task->getId(),
                'type' => Notification::TYPE_TASK
            ]);
            $em = $this->getDoctrine()->getManager();
            $em->persist($notification);
            $em->flush();
        }
        return $this->json(['message' => $trans->trans('users.flash.remember_delay_email_send')], Response::HTTP_OK);
    }

    /**
     * Send activation email to inactive user
     *
     * @Route("/admin_send_lost_users_email", name="admin_send_lost_users_email",methods={"POST"})
     * @return RedirectResponse|Response
     */
    public function sendLostUsersEmailAction()
    {
        $trans = $this->get('translator');
        $transDb = $this->get('app.database_translator');
        /** Current browsing country */
        $country = $this->get('session')->get('region');
        $lostUsers = $this->getDoctrine()->getRepository(User::class)->getLostUsers($country)
            ->select('u.email')->getQuery()->getResult();
        if(count($lostUsers)){
            $lostUsers = array_map(static function($user){
                return $user['email'];
            },$lostUsers);
            $template = $this->renderView(':emails:notification.html.twig',[
                'body' => '<h2>'.$transDb->transDb('email.new_features').'</h2>'
            ]);
            $this->get('user.mailer')->sendEmailMessage($template,
                null,
                null,$transDb->transDb('email.new_features_subject'),$lostUsers);
        }
        return $this->json(['message' => $trans->trans('users.flash.lost_uses_email_send')], Response::HTTP_OK);
    }

    /**
     * Export active users data as csv
     *
     * @Route("/export/csv", name="admin_export_active_users_csv")
     * @return RedirectResponse|Response
     */
    public function exportUsersCsvAction()
    {
        $country = $this->get('session')->get('region');
        $users = $this->getDoctrine()->getRepository(User::class)
            ->findActiveUsers()->andWhere('u.country = :country')->setParameter('country',$country)->getQuery()->getResult();
        $translator = $this->get('translator');
        $response = new StreamedResponse();
        $headers = [$translator->trans('table.user.join_date'), $translator->trans('table.user.email'), $translator->trans('users.firstname'),
            $translator->trans('users.lastname'), $translator->trans('users.address.phone'), $translator->trans('table.user.location'),
            $translator->trans('table.user.balance'),$translator->trans('table.user.locked_balance')];
        $response->setCallback(static function () use ($headers, $users, $translator) {
            $handle = fopen('php://output', 'w+');
            fputcsv($handle, array_values($headers), ',');
            /** @var User $user */
            foreach ($users as $user) {
                $resultExport[$translator->trans('table.user.join_date')] = $user->getCreatedAt() ? $user->getCreatedAt()->format('d-m-Y') : '';
                $resultExport[$translator->trans('table.user.email')] = $user->getEmail();
                $resultExport[$translator->trans('users.firstname')] = $user->getFirstName();
                $resultExport[$translator->trans('users.lastname')] = $user->getLastName();
                $resultExport[$translator->trans('users.address.phone')] = $user->getPhone();
                $resultExport[$translator->trans('table.user.location')] = $user->getLocation();
                $resultExport[$translator->trans('table.user.balance')] = number_format($user->getWallet()->getBalance(),2,'.',' ');
                $resultExport[$translator->trans('table.user.locked_balance')] = number_format($user->getWallet()->getLockedBalance(),2,'.',' ');
                fputcsv($handle, $resultExport, ',');
            }
            fclose($handle);
        });
        $response->headers->set('Content-Encoding', 'UTF-8');
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="Maatloob_active_users.csv"');
        echo "\xEF\xBB\xBF";
        return $response;
    }

    /**
     * Show user transactions
     *
     * @Route("/{id}/transactions", name="user_transactions")
     * @param User $user
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function userTransactionsAction(User $user, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $type = $request->query->get('type', 0);
        $orderBy = $request->query->get('sort_by', 'createdAt');
        $order = $request->query->get('order', 'DESC');
        $query = $em->getRepository(Transaction::class)->getUserTransactions($user, $orderBy, $order, $type);

        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the user list **/
        $page = $request->query->getInt('page', 1);
        $transactions = $paginator->paginate(
            $query->getQuery(),
            $page,
            $pageLength
        );
        $lockedBalance = $em->getRepository(Task::class)->findUserLockedBalanceTasks($user);
        $withdrawRequest = $em->getRepository(Transaction::class)->findLockedBalance($user);
        $taskInvoices = $em->getRepository(Transaction::class)->findLockedInvoiceBalance($user);
        /** Sort */
        $lockedBalance = array_merge($lockedBalance, $withdrawRequest,$taskInvoices);
        usort($lockedBalance,static function ($a,$b){
            return $a->getCreatedAt()>$b->getCreatedAt()?1:-1;
        });
        return $this->render('@App/admin/user/user-transaction.html.twig', [
            'transactions' => $transactions,
            'user' => $user,
            'lockedBalance' => $lockedBalance
        ]);
    }

    /**
     * Show user transactions
     *
     * @Route("/{id}/transactions/export", name="user_transactions_export")
     * @param User $user
     * @return Response
     * @throws Exception
     */
    public function exportUserTransactionAction(User $user): Response
    {
        $em =  $this->getDoctrine();
        $allTransaction = $em->getRepository(Transaction::class)
            ->createQueryBuilder('t')
            ->where('t.fromUser = :user or t.toUser = :user')
            ->andWhere('t.status = 2')
            ->setParameter('user',$user)
            ->getQuery()
            ->getResult();

        $lockedBalance = $em->getRepository(Task::class)->findUserLockedBalanceTasks($user);
        $withdrawRequest = $em->getRepository(Transaction::class)->findLockedBalance($user);
        $taskInvoices = $em->getRepository(Transaction::class)->findLockedInvoiceBalance($user);
        /** Sort */
        $lockedBalance = array_merge($lockedBalance, $withdrawRequest,$taskInvoices);
        usort($lockedBalance,static function ($a,$b){
            return $a->getCreatedAt()>$b->getCreatedAt()?1:-1;
        });
         $view =  $this->renderView('@Api/payment-history.html.twig',[
            'transactions'=>$allTransaction,
            'lockedBalance' => $lockedBalance,
            'user' => $user
        ]);
        $mpdf = new \Mpdf\Mpdf(['orientation' => 'L','mode' => 'utf-8']);
        $mpdf->WriteHTML($view);
        $mpdf->Output($this->get('translator')->trans('users.transactions.user_pdf',['#user#'=>$user->getFullName()]).'.pdf',true);
        return new Response();
    }
}
