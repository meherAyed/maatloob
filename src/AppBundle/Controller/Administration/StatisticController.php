<?php
/**
 * Created by PhpStorm.
 * User: mda
 * Date: 10/11/19
 * Time: 05:04 م
 */

namespace AppBundle\Controller\Administration;


use AppBundle\Entity\Offer;
use AppBundle\Entity\Task;
use AppBundle\Entity\User;
use AppBundle\Repository\TaskRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Twig\Extension\AppExtension;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Statistic controller.
 *
 * @Route("/stats")
 */
class StatisticController extends Controller
{
    /**
     * Users statistics.
     *
     * @Route("/users", name="admin_stats_users")
     * @return Response
     * @throws \Exception
     */
    public function userStatsAction(): Response
    {
        /** Current browsing country */
        $country = $this->get('session')->get('region');

        $userRepository = $this->getDoctrine()->getRepository(User::class);
        /** @var AppExtension $appExtension */
        $appExtension = $this->get('twig')->getExtension(AppExtension::class);
        $stats = $appExtension->userCounts();
        $banStats = $userRepository->getBannedUsers($country)->select('count(case when ban.permanent = 1 then 1 else :null end) as permanent',
            'count(case when ban.permanent = 0 then 1 else :null end) as temporarily')->setParameter('null',null)->getQuery()->getSingleResult();
        $stats['banned_permanent'] = $banStats['permanent'];
        $stats['banned_temporarily'] = $banStats['temporarily'];
        $stats['lost'] = count($userRepository->getLostUsers($country)->getQuery()->getResult());
        $stats['new'] = count($userRepository->getNewUsers($country)->getQuery()->getResult());
        $stats['android'] = count($userRepository->getUsersBySource($country,User::SOURCE_ANDROID)->getQuery()->getResult());
        $stats['iphone'] = count($userRepository->getUsersBySource($country,User::SOURCE_IPHONE)->getQuery()->getResult());
        return $this->render('@App/admin/stats/user.stats.html.twig', [
            'stats' => $stats
        ]);
    }

    /**
     * Users statistics.
     *
     * @Route("/tasks", name="admin_stats_tasks")
     * @return Response
     * @throws \Exception
     */
    public function taskStatsAction(): Response
    {
        /** Current browsing country */
        $country = $this->get('session')->get('region');
        $taskRepository = $this->getDoctrine()->getRepository(Task::class);
        $stats['new'] = count($taskRepository->findNewTasks($country)->getQuery()->getResult());
        $stats['open'] = count($taskRepository->findOpenTasks($country)->getQuery()->getResult());
        $stats['assigned'] = count($taskRepository->findAssignedTasks($country)->getQuery()->getResult());
        $stats['delayed'] = count($taskRepository->findDelayedTasks($country)->getQuery()->getResult());
        $stats['completed'] = count($taskRepository->findCompletedTasks($country)->getQuery()->getResult());
        $stats['cancelled'] = count($taskRepository->findCancelledTasks($country)->getQuery()->getResult());
        $stats['deleted'] = count($taskRepository->findDeletedTasks($country)->getQuery(1)->getResult());
        $stats['expired'] = count($taskRepository->findExpiredTasks($country)->getQuery()->getResult());
        $stats['awaiting_payment'] = count($taskRepository->findAwaitingPaymentTasks($country)->getQuery()->getResult());
        $stats['payment_cash'] = count($taskRepository->findPaymentCashTasks($country)->getQuery()->getResult());
        $stats['payment_wallet'] = count($taskRepository->findPaymentWalletTasks($country)->getQuery()->getResult());
        $stats['problematic'] = count($taskRepository->findTasksWithProblematic($country)->getQuery()->getResult());
        $stats['expected_revenue'] = $taskRepository->findOngoingTask($country)
            ->select('COALESCE(sum(t.maatloobTax),0) as totalRevenue')
            ->getQuery()->getSingleResult()['totalRevenue'];
        $stats['locked_balance'] = $taskRepository->findLockedBalanceTasks($country)
            ->select('COALESCE(sum(t.price) - sum(t.usedCredit),0) as lockedBalance')
            ->getQuery()->getSingleResult()['lockedBalance'];
        return $this->render('@App/admin/stats/task.stats.html.twig', [
            'stats' => $stats
        ]);
    }

    /**
     * Revenue statistics.
     *
     * @Route("/tasks/{type}/details", name="admin_stats_task_details")
     * @param Request $request
     * @param $type
     * @return Response
     * @throws \Exception
     */
    public function taskStatsDetailsAction(Request $request, $type): Response
    {
        /** @var TaskRepository $taskRepository */
        $taskRepository = $this->getDoctrine()->getRepository(Task::class);
        $country = $this->get('session')->get('region');
        switch ($type) {
            case 'new':
                $query = $taskRepository->findNewTasks($country);
                break;
            case 'open':
                $query = $taskRepository->findOpenTasks($country);
                break;
            case 'assigned':
                $query = $taskRepository->findAssignedTasks($country);
                break;
            case 'delayed':
                $query = $taskRepository->findDelayedTasks($country);
                break;
            case 'completed':
                $query = $taskRepository->findCompletedTasks($country);
                break;
            case 'cancelled':
                $query = $taskRepository->findCancelledTasks($country);
                break;
            case 'deleted':
                $query = $taskRepository->findDeletedTasks($country);
                break;
            case 'expired':
                $query = $taskRepository->findExpiredTasks($country);
                break;
            case 'awaiting_payment':
                $query = $taskRepository->findAwaitingPaymentTasks($country);
                break;
            case 'payment_cash':
                $query = $taskRepository->findPaymentCashTasks($country);
                break;
            case 'payment_wallet':
                $query = $taskRepository->findPaymentWalletTasks($country);
                break;
            case 'problematic':
                $query = $taskRepository->findTasksWithProblematic($country);
                break;
            case 'expected_revenue':
                $query = $taskRepository->findOngoingTask($country);
                break;
            case 'locked_balance':
                $query = $taskRepository->findLockedBalanceTasks($country);
                break;
            default :
                $taskRepository->findNewTasks($country);
                break;
        }
        $paginator = $this->get('knp_paginator');
        //Get page length
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the client list **/
        $tasks = $paginator->paginate(
            $query->getQuery($type==='deleted'?1:0),
            $request->query->getInt('page', 1),
            $pageLength
        );

        return $this->render('@App/admin/stats/task.details.stats.html.twig', ['tasks' => $tasks]);
    }

    /**
     * Revenue statistics.
     *
     * @Route("/users/{type}/details", name="admin_stats_user_details")
     * @param Request $request
     * @param $type
     * @return Response
     */
    public function userStatsDetailsAction(Request $request, $type): Response
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $country = $this->get('session')->get('region');
        switch ($type) {
            case 'new':
                $query = $userRepository->getNewUsers($country);
                break;
            case 'banned':
                $query = $userRepository->getBannedUsers($country);
                break;
            case 'lost':
                $query = $userRepository->getLostUsers($country);
                break;
            case 'android':
                $query = $userRepository->getUsersBySource($country,User::SOURCE_ANDROID);
                break;
            case 'iphone':
                $query = $userRepository->getUsersBySource($country,User::SOURCE_IPHONE);
                break;
            case 'active':
                $query = $userRepository->getActiveUsersByCountry($country);
                break;
            case 'inactive':
                $query = $userRepository->getInactiveUsersByCountry($country);
                break;
            default :
                $query = $userRepository->getTotalUsersByCountry($country);
                break;
        }
        $paginator = $this->get('knp_paginator');
        //Get page length
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the client list **/
        $users = $paginator->paginate(
            $query->getQuery(),
            $request->query->getInt('page', 1),
            $pageLength
        );

        return $this->render('@App/admin/stats/user.details.stats.html.twig', ['users' => $users]);
    }

}