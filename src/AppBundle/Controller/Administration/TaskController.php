<?php

namespace AppBundle\Controller\Administration;

use AppBundle\Entity\CancelTaskRequest;
use AppBundle\Entity\Indicator;
use AppBundle\Entity\Notification;
use AppBundle\Entity\Task;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Xendit\Cards;
use Xendit\Xendit;

/**
 * Task controller.
 *
 * @Route("/tasks")
 */
class TaskController extends Controller
{
    /**
     * Lists all task entities.
     *
     * @Route("/", name="task_index")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function indexAction(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $query = $this->createQueryForTaskList($request);
        $paginator = $this->get('knp_paginator');
        //Get page length
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the client list **/
        $tasks = $paginator->paginate(
            $query->getQuery(),
            $request->query->getInt('page', 1),
            $pageLength
        );
        $indicator = $em->getRepository(Indicator::class)->findOneBy(['list' => Indicator::LIST_TASKS, 'admin' => $this->getUser()]);
        $responseParams = [
            'tasks' => $tasks,
            'indicator' => $indicator
        ];
        if (in_array($request->get('status'), ['completed', 'assigned', 'waiting_payment'])) {
            $responseParams = array_merge($this->createQueryForTaskList($request)
                ->select('coalesce(sum(t.maatloobTax),0) as maatloobTax', 'coalesce(sum(t.countryTax),0) as countryTax')
                ->join('t.acceptedOffer','ao')
                ->join('ao.user','tasker')
                ->andWhere('tasker.country = :country')
                ->getQuery()->getSingleResult(), $responseParams);

        }
        return $this->render('AppBundle::admin/task/index.html.twig', $responseParams);
    }

    /**
     * @param Request $request
     * @return \Doctrine\ORM\QueryBuilder
     * @throws Exception
     */
    private function createQueryForTaskList(Request $request): \Doctrine\ORM\QueryBuilder
    {
        /** Current browsing country */
        $country = $this->get('session')->get('region');

        $search = $request->query->get('search', '');
        $status = $request->query->get('status');
        $orderBy = $request->query->get('sort_by', 'createdAt');
        $order = $request->query->get('order', 'DESC');

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Task::class)
            ->findTasks($search, $status, $orderBy, $order)
            ->andWhere('u.country = :country')
            ->setParameter('country', $country);

        /** Date range filter */
        if ($request->query->has('s_date')) {
            $query->andWhere('t.createdAt >= :startDate')
                ->setParameter('startDate', new DateTime($request->get('s_date')));
        }
        if ($request->query->has('e_date')) {
            $query->andWhere('t.createdAt <= :endDate')
                ->setParameter('endDate', new DateTime($request->get('e_date')));
        }
        return $query;
    }

    /**
     * Finds and displays a task entity.
     *
     * @Route("/{id}/details", name="task_show")
     * @param Task $task
     * @return Response
     */
    public function showAction(Task $task): Response
    {
        return $this->render('AppBundle::admin/task/show.html.twig', array(
            'task' => $task,
        ));
    }

    /**
     * Find and delete a specific task entity.
     *
     * @Route("/{id}/delete", name="admin_delete_task",methods={"DELETE"})
     * @param Task $task
     * @return Response
     */
    public function deleteTaskAction(Task $task): Response
    {
        $em = $this->getDoctrine()->getManager();
        if(in_array($task->getStatus(), [Task::STATUS_DRAFT,Task::STATUS_PUBLISHED, Task::STATUS_ASSIGNED], true)){
            $task->setIsDeleted(true);
            if($task->getPaymentType()===Task::PAYMENT_TYPE_WALLET && in_array($task->getStatus(), [Task::STATUS_PUBLISHED, Task::STATUS_ASSIGNED], true)){
                $posterWallet = $task->getUser()->getWallet();
                $posterWallet->unlockCredit($task->getUsedCredit());
                $posterWallet->unlockBalance($task->getPrice() - $task->getUsedCredit());
                $task->setUsedCredit(0);
                $em->persist($posterWallet);
            }
            $em->persist($task);
            $em->flush();
        }
        return $this->json(['message' => $this->get('translator')->trans('task.delete_modal.flash')]);
    }


    /**
     * Find tasks with refused request cancel.
     *
     * @Route("/cancellation-issues", name="admin_task_with_issues")
     * @param Request $request
     * @return Response
     */
    public function getTaskWithIssuesAction(Request $request): Response
    {
        /** Current browsing country */
        $country = $this->get('session')->get('region');

        $search = $request->query->get('search', '');

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Task::class)->createQueryBuilder('t')
            ->join('t.cancelRequest', 'cr', 'with', 'cr.closed = 0')
            ->join('cr.forUser', 'forUser', 'with', 'forUser.country = :country')
            ->join('cr.fromUser', 'fromUser')
            ->where('t.title like :search')
            ->orWhere("( CONCAT(forUser.firstName,' ',forUser.lastName) LIKE :search )")
            ->orWhere("( CONCAT(fromUser.firstName,' ',fromUser.lastName) LIKE :search )")
            ->setParameter('country', $country)
            ->setParameter('search', "%$search%");

        $paginator = $this->get('knp_paginator');
        //Get page length
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the client list **/
        $tasks = $paginator->paginate(
            $query->getQuery(),
            $request->query->getInt('page', 1),
            $pageLength
        );

        $indicator = $em->getRepository(Indicator::class)->findOneBy(['list' => Indicator::LIST_TASKS_CANCEL_REQUEST, 'admin' => $this->getUser()]);
        return $this->render('@App/admin/task/with.issues.html.twig', array(
            'tasks' => $tasks,
            'indicator' => $indicator
        ));
    }


    /**
     * Process task cancel request.
     *
     * @Route("/cancellation-issues/process", name="admin_process_cancel_task_request",methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function cancelRequestProcessAction(Request $request): JsonResponse
    {
        $data = $request->request->all();
        $trans = $this->get('translator');
        $dbTranslator = $this->get('app.database_translator');
        $em = $this->getDoctrine()->getManager();
        /** Validate request data */
        try {
            if (!$data['id'] && !$data['cancelType']) {
                throw new \HttpInvalidParamException($trans->trans('invalid_request'));
            }
            /** @var CancelTaskRequest $cancelRequest */
            $cancelRequest = $em->getRepository(CancelTaskRequest::class)->find($data['id']);
            if (!$cancelRequest || $cancelRequest->isClosed()) {
                throw new \HttpInvalidParamException($trans->trans('invalid_request'), Response::HTTP_NOT_FOUND);
            }
            /** Cancel request */
            if ($data['cancelType'] === 'task') {
                $task = $cancelRequest->getTask();
                $poster = $task->getUser();
                $tasker = $task->getAcceptedOffer()->getUser();
                $task->setStatus(Task::STATUS_CANCELLED);
                if ($task->getPaymentType() !== Task::PAYMENT_TYPE_CASH || $task->isInvoicePayed()) {
                    $posterWallet = $poster->getWallet();
                    $posterWallet->unlockCredit($task->getUsedCredit());
                    $posterWallet->unlockBalance($task->getPrice() - $task->getUsedCredit());
                    $task->setUsedCredit(0);
                    $em->persist($posterWallet);
                    $transactions = $em->getRepository(Transaction::class)->findBy(['type'=>Transaction::TYPE_TASK_INVOICE,
                        'task'=>$task,'status'=>Transaction::STATUS_COMPLETED]);
                    /** @var Transaction $transaction */
                    foreach ($transactions as $transaction){
                        $wallet = $transaction->getFromUser()->getWallet();
                        $wallet->unlockCredit($transaction->getPrice());
                        $em->persist($wallet);
                    }
                }
                if ($data['guiltyUser'] !== 'none') {
                    if ($poster->getId() === (int)$data['guiltyUser']) {
                        $cancelRequest->setGuilty($poster);
                    } elseif ($tasker->getId() === (int)$data['guiltyUser']) {
                        $cancelRequest->setGuilty($tasker);
                    } else {
                        throw new \HttpInvalidParamException($trans->trans('invalid_request'), Response::HTTP_NOT_FOUND);
                    }
                    \ApiBundle\Controller\TaskController::cancelTaskPenalty($cancelRequest->getGuilty(), $task, $em, $dbTranslator, $this->get('app_service'));
                    /** Check if there is a compensation for the damaged user */
                    if (!empty($data['compensation']) && $data['compensation'] > 0) {
                        $cancelRequest->setCompensation((float)$data['compensation']);
                        $this->compensateDamageUser($cancelRequest);
                    }
                }
                $em->persist($task);
                $cancelRequest->setClosed(true);
                $em->persist($cancelRequest);
            }else{
                $em->remove($cancelRequest);
            }
            $em->flush();
            $code = Response::HTTP_OK;
            $msg = $this->get('translator')->trans('task_cancellation_done');
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }
        return $this->json(['message' => $msg], $code);
    }

    private function compensateDamageUser(CancelTaskRequest $cancelRequest): void
    {
        $em = $this->getDoctrine()->getManager();
        $userToCompensate = $cancelRequest->getFromUser()->getId() === $cancelRequest->getGuilty()->getId() ?
            $cancelRequest->getForUser() : $cancelRequest->getFromUser();
        $guiltyWallet = $cancelRequest->getGuilty()->getWallet();
        $currencyConverter = $this->get('app.currency.converter');
        $transaction = new Transaction();
        $convertedMoneyToCompensateGuilty = (float)$currencyConverter->convertMoney($cancelRequest->getCompensation(),
            $cancelRequest->getTask()->getUser()->getCountry(),$userToCompensate->getCountry());
        $convertedMoneyToCompensateInnocent = (float)$currencyConverter->convertMoney($cancelRequest->getCompensation(),
            $cancelRequest->getTask()->getUser()->getCountry(), $userToCompensate->getCountry());
        if ($guiltyWallet->getBalance() > $convertedMoneyToCompensateGuilty) {
            $guiltyWallet->withdraw($convertedMoneyToCompensateGuilty);
            $userToCompensate->getWallet()->deposit($convertedMoneyToCompensateInnocent);
            $em->persist($userToCompensate->getWallet());
            $em->persist($guiltyWallet);
        } else {
            $transaction->setStatus(Transaction::STATUS_PENDING);
        }

        /** Create the transaction between the two users  */
        $transaction->setFromUser($cancelRequest->getGuilty());
        $transaction->setToUser($userToCompensate);
        $transaction->setTask($cancelRequest->getTask());
        $transaction->setType(Transaction::TYPE_COMPENSATION);
        $transaction->setFromPrice($convertedMoneyToCompensateGuilty);
        $transaction->setPrice($convertedMoneyToCompensateInnocent);
        $em->persist($transaction);

        if ($transaction->getStatus() === Transaction::STATUS_COMPLETED) {
            /** Send notification for both users */
            $em->persist(self::sendCompensationNotification($transaction, $userToCompensate,$convertedMoneyToCompensateInnocent));
            $em->persist(self::sendCompensationNotification($transaction, $cancelRequest->getGuilty(),$convertedMoneyToCompensateGuilty));
        }
        $em->flush();
    }

    public static function sendCompensationNotification(Transaction $transaction, User $forUser,$price): Notification
    {
        $notification = new Notification();
        $notification->setToUser($forUser);
        $notification->setType(Notification::ACTION_TRANSACTION);
        $notification->setTitle('notification.compensation_cancelled_task.title');
        $notification->setBody('notification.compensation_cancelled_task_' . ($transaction->getFromUser()->getId() === $forUser->getId() ? 'from' : 'for') . '.body');
        $notification->setParams([
            '#price#' => number_format($price,2,'.','') . $forUser->getCountry()->getCurrencySymbol(),
            '#task#' => $transaction->getTask()->getTitle()
        ]);
        $notification->setExtra([
            'type' => Notification::TYPE_TRANS,
            'direction' => 'INCOMING'
        ]);
        return $notification;
    }
}
