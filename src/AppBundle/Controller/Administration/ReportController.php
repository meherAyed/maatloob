<?php

namespace AppBundle\Controller\Administration;

use ApiBundle\Services\DBTranslator;
use AppBundle\Entity\Indicator;
use AppBundle\Entity\Notification;
use AppBundle\Entity\Offer;
use AppBundle\Entity\Report;
use AppBundle\Entity\ReportType;
use AppBundle\Entity\TaskMessage;
use AppBundle\Entity\TaskQuestion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Task controller.
 *
 * @Route("/reports")
 */
class ReportController extends Controller
{

    /**
     * Lists all task entities.
     *
     * @Route("/", name="admin_reports")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function reportsAction(Request $request): Response
    {
        /** Current browsing country */
        $country = $this->get('session')->get('region');

        $search = $request->query->get('search', '');
        $status = $request->query->get('status', 'all');
        $orderBy = $request->query->get('sort_by', 'createdAt');
        $order = $request->query->get('order', 'DESC');

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Report::class)
            ->findReports($search, $status, $orderBy, $order)
            ->andWhere('u.country = :country')
            ->setParameter('country',$country);

        /** Date range filter */
        if($request->query->has('s_date')){
            $query->andWhere('r.createdAt >= :startDate')
                ->setParameter('startDate',new \DateTime($request->get('s_date')));
        }
        if($request->query->has('e_date')){
            $query->andWhere('r.createdAt <= :endDate')
                ->setParameter('endDate',new \DateTime($request->get('e_date')));
        }

        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        /** Paginate the user list **/
        $reports = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $pageLength
        );
        $indicator = $em->getRepository(Indicator::class)->findOneBy(['list'=>Indicator::LIST_REPORTS,'admin' => $this->getUser()]);
        return $this->render('@App/admin/report/reports.html.twig', ['reports' => $reports,'indicator'=>$indicator]);
    }

    /**
     * Get report details
     *
     * @Route("/{id}/details", name="admin_report_details")
     * @param Report $report
     * @param Request $request
     * @param DBTranslator $DBTranslator
     * @return Response
     * @throws \Exception
     */
    public function reportDetailAction(Report $report, Request $request,DBTranslator $DBTranslator): Response
    {
        $translator = $this->get('translator');
        if ($request->isXmlHttpRequest() && $request->isMethod(Request::METHOD_POST)) {
            if ($report->getStatus() === Report::STATUS_UNTREATED) {
                $em = $this->getDoctrine()->getManager();
                $actionType = $request->get('action');
                if(!in_array($actionType, [Report::ACTION_BAN, Report::ACTION_REFUSE, Report::ACTION_WARN, Report::ACTION_REMOVE_STAR], true))
                {
                    return $this->json(['message' => $translator->trans('invalid_request')],Response::HTTP_BAD_REQUEST);
                }
                $report->setStatus(Report::STATUS_TREATED);
                $takenActions = ['action' => $actionType];
                if($actionType === Report::ACTION_WARN){
                    $warningMessage = $request->get('warningMessage',null);
                    if(!$warningMessage){
                        return $this->json(['message' => $translator->trans('invalid_request')],Response::HTTP_BAD_REQUEST);
                    }
                    $message = (new \Swift_Message())
                        ->setSubject($DBTranslator->transDb('email.warning.subject'))
                        ->setFrom([$this->getParameter('mailer_user') => 'Maatloob'])
                        ->setTo($report->getReportedUser()->getEmail())
                        ->setBody($this->renderView(':emails:notification.html.twig',[
                            'user'=>$report->getReportedUser()->getFullName(),
                            'lang' => $report->getReportedUser()->getLanguage(),
                            'body' => $warningMessage
                        ]), 'text/html');

                    $this->get('swiftmailer.mailer')->send($message);
                }

                if($actionType === Report::ACTION_BAN){
                    $userBan = UserController::banUser($request->get('ban'));
                    if(!$userBan){
                        return $this->json(['message' => $translator->trans('invalid_request')],Response::HTTP_BAD_REQUEST);
                    }
                    $userBan->setBannedBy($this->getUser());
                    $userBan->setUser($report->getReportedUser());
                    $em->persist($userBan);
                    $takenActions['duration'] = !$userBan->isPermanent()?$userBan->getUnbanDate():'permanent';
                }

                if($actionType === Report::ACTION_REMOVE_STAR){
                    $user = $report->getReportedUser();
                    $user->blockStars();
                    $em->persist($user);
                }

                if($actionType === Report::ACTION_REFUSE){
                    $report->setStatus(Report::STATUS_REFUSED);
                }
                $report->setTakenActions($takenActions);
                $em->persist($report);
                $em->flush();
                return $this->json(
                    ['message' => $translator->trans('reports.table.status_treated')],
                    Response::HTTP_OK);
            }

            return $this->json(
                ['message' => $translator->trans('reports.details.treat.errors.already_treated')],
                Response::HTTP_BAD_REQUEST);
        }
        return $this->render('@App/admin/report/report.details.html.twig', [
            'report' => $report
        ]);
    }

    /**
     * Inspect reported object
     *
     * @Route("/{id}/details/inspect", name="admin_report_details_inspect")
     * @param Report $report
     * @return Response
     */
    public function inspectReportedObject(Report $report): ?Response
    {
        $em = $this->getDoctrine()->getManager();
        switch ($report->getSection()) {
            case ReportType::SECTION_TASK:
                {
                    return $this->redirectToRoute('task_show', ['id' => $report->getObjectId()]);
                }
            case ReportType::SECTION_OFFER:
                {
                    $offer = $em->getRepository(Offer::class)->find($report->getObjectId());
                    return $this->redirect($this->generateUrl('task_show',
                            ['id' => $offer->getTask()->getId()]) . '#offer-' . $report->getObjectId());
                }
            case ReportType::SECTION_QUESTION:
                {
                    $question = $em->getRepository(TaskQuestion::class)->find($report->getObjectId());
                    return $this->redirect($this->generateUrl('task_show',
                            ['id' => $question->getTask()->getId()]) . '#question-' . $report->getObjectId());
                }
            case ReportType::SECTION_USER:
                {
                    return $this->redirect($this->generateUrl('user_details', [
                        'id' => $report->getReportedUser()->getId(),
                    ]));
                }
            case ReportType::SECTION_MESSAGE:
                {
                    $message = $em->getRepository(TaskMessage::class)->find($report->getObjectId());
                    return $this->redirect($this->generateUrl('user_discussions', [
                        'id' => $message?$message->getDiscussion()->getId():null,
                    ]));
                }
        }
    }

    /**
     * Get report details
     *
     * @Route("/{id}/execute", name="admin_report_action",methods={"POST"})
     * @param Report $report
     * @param Request $request
     * @return Response
     */
    public function reportExecuteAction(Report $report, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        if ($report->getStatus() === Report::STATUS_UNTREATED) {
            $report->setStatus($request->get('status', Report::STATUS_UNTREATED));
            $report->setRefuseReason($request->get('refuseReason', ''));
            $em->persist($report);
            $em->flush();
            return $this->json(['status' => $report->getStatus()], Response::HTTP_OK);
        }

        return $this->json(['message' => 'Report already treated'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * Find and delete a specific task entity.
     *
     * @Route("/{id}/delete", name="admin_delete_report",methods={"DELETE"})
     * @param Report $report
     * @return Response
     */
    public function deleteReportAction(Report $report): Response
    {
        $em = $this->getDoctrine()->getManager();
        $report->setIsDeleted(true);
        $em->persist($report);
        $em->flush();
        return $this->json(['message' => $this->get('translator')->trans('reports.delete_modal.flush.success')]);
    }
}
