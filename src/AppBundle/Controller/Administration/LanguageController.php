<?php

namespace AppBundle\Controller\Administration;

use AppBundle\Entity\DocumentType;
use AppBundle\Entity\Language;
use AppBundle\Entity\LanguageMessage;
use AppBundle\Entity\ReportType;
use AppBundle\Entity\TaskCategory;
use AppBundle\Form\LanguageType;
use Components\Helper;
use Doctrine\ORM\Query\QueryException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LanguageController
 * @package AppBundle\Controller
 * @Route("/system/languages")
 */
class LanguageController extends Controller
{
    /**
     * @Route("/", name="system_languages")
     * @return Response
     */
    public function indexAction(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $languages = $em->getRepository(Language::class)->findAll();
        return $this->render('@App/admin/system_config/language/language-config.html.twig',
            ['languages' => $languages]);
    }
    /**
     * Add system language
     * @Route("/new", name="system_languages_add",methods={"GET","POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function addLanguageAction(Request $request)
    {
        $trans = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        /** @var language $defaultLanguage */
        $defaultLanguage = $em->getRepository(Language::class)->findOneBy(['identifier'=>'en']);
        $newLanguage = new Language();
        $defaultMessages = $defaultLanguage->getMessages();
        $englishMessages = [];
        /** All those entities should be translated also  */
        $reports = $em->getRepository(ReportType::class)->findAll();

        /** @var LanguageMessage $defaultMessage */
        foreach ($defaultMessages as $defaultMessage){
            $newMessage = new LanguageMessage();
            $englishMessages[$defaultMessage->getIdentifier()]=$defaultMessage->getMessage();
            $newMessage->setIdentifier($defaultMessage->getIdentifier());
            $newLanguage->addMessage($newMessage);
        }
        $form = $this->createForm(LanguageType::class,$newLanguage);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em->persist($newLanguage);
            $em->flush();
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $request->files->get('importFileMessages');
            $importedData = [];
            if($uploadedFile){
                $importedData = Helper::csvToArray($uploadedFile->getRealPath(),';','key');
            }
            $allTranslationDone = true;
            /** @var LanguageMessage $message */
            foreach ($newLanguage->getMessages() as $message){
                if(isset($importedData[$message->getIdentifier()][$newLanguage->getName()])){
                    $message->setMessage($importedData[$message->getIdentifier()][$newLanguage->getName()]);
                }
                if(!in_array($message->getMessage(), ['', null], true)){
                    $allTranslationDone = false;
                    break;
                }
            }

            /** Check if all reports type are translated with new language */
            $translatedReports = $request->get('reports_translations');
            foreach ($translatedReports as $translatedReport){
                /** @var ReportType $reportType */
                $reportType = $em->getRepository(ReportType::class)->find($translatedReport['id']);
                if($translatedReport['name']){
                    $reportType->translate($newLanguage->getIdentifier())->setName($translatedReport['name']);
                }else{
                    $allTranslationDone = false;
                }
                $reportType->mergeNewTranslations();
                $em->persist($reportType);
            }

            $newLanguage->setActive($allTranslationDone);
            $em->persist($newLanguage);
            $em->flush();
            $this->addFlash('success',$allTranslationDone?
                $trans->trans('system.language.saved_and_enabled'):$trans->trans('system.language.saved_not_enabled'));
            return $this->redirectToRoute('system_languages');
        }
        $usedLanguages = $em->getRepository(Language::class)->findAll();
        $availableLanguages = json_decode(file_get_contents($this->getParameter('web_dir').'/languages.json'), true);
        /** @var language $language */
        foreach ($usedLanguages as $language){
            if(isset($availableLanguages[$language->getIdentifier()]))
                unset($availableLanguages[$language->getIdentifier()]);
        }
        return $this->render('@App/admin/system_config/language/language.new.html.twig',
            [
                'form'=>$form->createView(),
                'availableLanguages' => $availableLanguages,
                'englishMessages' => $englishMessages,
                'reports' => $reports,
            ]);
    }

    /**
     * Update system language messages
     * @Route("/{identifier}/edit", name="system_languages_edit",methods={"GET","POST"})
     * @param Language $language
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws QueryException
     */
    public function editLanguageAction(Language $language,Request $request)
    {
        $trans = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(LanguageType::class,$language);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $request->files->get('importFileMessages');
            $importedData = [];
            if($uploadedFile){
                $importedData = Helper::csvToArray($uploadedFile->getRealPath(),';','key');
            }
            $allTranslationDone = true;
            /** @var LanguageMessage $message */
            foreach ($language->getMessages() as $message){
                if(isset($importedData[$message->getIdentifier()][$language->getName()])){
                    $message->setMessage($importedData[$message->getIdentifier()][$language->getName()]);
                }
                if(in_array($message->getMessage(), ['', null], true)){
                    $allTranslationDone = false;
                    break;
                }
            };
            /** Check if all reports type are translated with new language */
            $translatedReports = $request->get('reports_translations');
            foreach ($translatedReports as $translatedReport){
                /** @var ReportType $reportType */
                $reportType = $em->getRepository(ReportType::class)->find($translatedReport['id']);
                if($translatedReport['name']){
                    $reportType->translate($language->getIdentifier())->setName($translatedReport['name']);
                }else{
                    $allTranslationDone = false;
                }
                $reportType->mergeNewTranslations();
                $em->persist($reportType);
            }

            if($allTranslationDone || !$language->isActive()){
                if ($language->isActive()){
                    $language->setVersion($language->getVersion()+1);
                }
                $language->setActive($allTranslationDone);
                $em->persist($language);
                $em->flush();
                $this->addFlash('success',$trans->trans('system.language.language_updated'));
                return $this->redirectToRoute('system_languages');
            }

            $this->addFlash('error',$trans->trans('system.language.update_cancelled_missing_translation'));
        }
        $englishMessages = $em->getRepository(LanguageMessage::class)
            ->createQueryBuilder('lm')
            ->select('lm.identifier,lm.message')
            ->where('lm.language = :english')
            ->setParameter('english','en')
            ->indexBy('lm','lm.identifier')
            ->getQuery()->getResult();
        $reports = $em->getRepository(ReportType::class)->findAll();
        return $this->render('@App/admin/system_config/language/language.edit.html.twig',[
            'form'=>$form->createView(),
            'englishMessages' => $englishMessages,
            'language' => $language,
            'reports' => $reports,
        ]);
    }

    /**
     * @Route("/{identifier}/export", name="system_language_export")
     * @param Language $language
     * @return StreamedResponse
     */
    public function exportLanguageMessagesAction(Language $language): StreamedResponse
    {
        $response = new StreamedResponse();
        $response->setCallback(function () use ($language) {
            $headers = [0=>'key',1=>$language->getName()];
            if($language->getIdentifier()!=='en'){
                $headers[2] = $headers[1];
                $headers[1] = 'english';
            }
            $handle = fopen('php://output', 'w+');
            fputcsv($handle, array_values($headers), ',');
            /** @var LanguageMessage $message */
            foreach ($language->getMessages() as $message) {
                $resultExport['key'] = $message->getIdentifier();
                if($language->getIdentifier()!== 'en'){
                    $englishMessage = $this->getDoctrine()->getRepository(LanguageMessage::class)->findOneBy([
                        'identifier'=>$message->getIdentifier(),'language'=>'en']);
                    $resultExport['english'] = $englishMessage?$englishMessage->getMessage():'-';
                }
                $resultExport[$language->getName()] =  $message->getMessage();
                fputcsv($handle, $resultExport, ',');
            }
            fclose($handle);
        });
        $response->setStatusCode(200);
        $response->headers->set('Content-Encoding', 'UTF-8');
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="'.$language->getName().'_messages.csv"');
        echo "\xEF\xBB\xBF";
        return $response;
    }
}
