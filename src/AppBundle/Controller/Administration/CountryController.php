<?php

namespace AppBundle\Controller\Administration;

use AppBundle\Entity\Country;
use AppBundle\Entity\CountryCategory;
use AppBundle\Entity\SystemConfig;
use AppBundle\Entity\TaskCategory;
use AppBundle\Form\CountryType;
use Doctrine\ORM\Query\QueryException;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CountryController
 * @package AppBundle\Controller\Administration
 * @Route("/system/countries")
 */
class CountryController extends Controller
{
    /**
     * @Route("/", name="system_countries")
     * @return Response
     */
    public function indexAction(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $countries = $em->getRepository(Country::class)->findAll();
        return $this->render('@App/admin/system_config/country/country-config.html.twig',[
            'countries' => $countries
        ]);
    }

    /**
     * Add country to the system
     * @Route("/new", name="system_country_new", methods={"GET","POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws QueryException
     */
    public function addCountryAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $country = new Country();
        $country->setConfig(new SystemConfig());
        $countryList = json_decode(file_get_contents($this->getParameter('web_dir').'/country.json'), true);
        $countryForm = $this->createForm(CountryType::class,$country);
        $countryForm->handleRequest($request);
        if($countryForm->isSubmitted() && $countryForm->isValid()){
            $country->getConfig()->setTaskProfits($request->request->get('country')['config']['taskProfits']);
            $trans = $this->get('translator');
            $em->persist($country);
            $this->initTaskCategories($country);
            $em->flush();
            $this->addFlash('success',$trans->trans('system.country.update_country_success'));
            return $this->redirect($this->generateUrl('system_countries'));
        }
        $activeCountries = $em->getRepository(Country::class)->createQueryBuilder('c')
            ->select('c.identifier')->indexBy('c','c.identifier')->getQuery()->getResult();

        $this->initCurrenciesExchange();
        return $this->render('@App/admin/system_config/country/new.country.html.twig',[
            'form'=>$countryForm->createView(),
            'countryList' => $countryList,
            'activeCountries' => $activeCountries,
            'country' => $country
        ]);
    }

    /**
     * Edit country information
     * @Route("/{identifier}/edit", name="system_country_edit",methods={"GET","POST"})
     * @param Country $country
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function editCountryAction(Country $country,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $countryForm = $this->createForm(CountryType::class,$country);
        $countryForm->handleRequest($request);
        if($countryForm->isSubmitted() && $countryForm->isValid()){
            $country->getConfig()->setTaskProfits($request->request->get('country')['config']['taskProfits']);
            $trans = $this->get('translator');
            $em->persist($country);
            $em->flush();
            $this->addFlash('success',$trans->trans('system.country.update_country_success'));
            return $this->redirect($this->generateUrl('system_countries'));
        }
        return $this->render('@App/admin/system_config/country/edit.country.html.twig',[
            'form'=>$countryForm->createView(),
            'country' => $country
        ]);
    }

    private function initCurrenciesExchange(): void
    {
        $application = new Application($this->get('kernel'));
        $application->setAutoExit(false);
        $input = new ArrayInput([
            'command' => 'app:daily_currency_checker',
        ]);
        $application->run($input, new NullOutput());
    }

    /**
     * @param Country $country
     */
    private function initTaskCategories(Country $country): void
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(TaskCategory::class)->findAll();
        foreach ($categories as $category){
            $countryCategory = new CountryCategory();
            $countryCategory->setCountry($country);
            $countryCategory->setCategory($category);
            $countryCategory->setPosition(count($country->getCategories())+1);
            $em->persist($countryCategory);
        }
    }

    /**
     * @Route("/{identifier}/update-enable", name="system_country_update_enable",methods={"PATCH"})
     * @param Country $country
     * @return JsonResponse
     */
    public function enableDisableCountryAction(Country $country): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $country->setEnabled(!$country->isEnabled());
        $em->persist($country);
        $em->flush();
        $msg = $this->get('translator')->trans('system.country.flash.update');
        return $this->json(['message' => $msg]);
    }
}
