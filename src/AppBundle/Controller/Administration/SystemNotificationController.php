<?php

namespace AppBundle\Controller\Administration;

use AppBundle\Entity\Country;
use AppBundle\Entity\Notification;
use AppBundle\Entity\SystemConfig;
use AppBundle\Entity\SystemNotification;
use AppBundle\Entity\User;
use AppBundle\Form\CountryType;
use AppBundle\Form\SystemNotificationType;
use Doctrine\ORM\Query\QueryException;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SystemNotificationController
 * @package AppBundle\Controller\Administration
 * @Route("/system_notifications")
 */
class SystemNotificationController extends Controller
{
    /**
     * @Route("/", name="system_notifications")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        /** Current browsing country */
        $country = $this->get('session')->get('region','sa');
        $systemNotifications = $em->getRepository(SystemNotification::class)->findBy(['country'=>$country]);
        /** Paginate the user list **/
        $paginator = $this->get('knp_paginator');
        $pageLength = $this->get('session')->get('tableLength', $this->getParameter('table_length'));
        $systemNotifications = $paginator->paginate(
            $systemNotifications,
            $request->query->getInt('page', 1),
            $pageLength
        );
        return $this->render('@App/admin/notifications/index.html.twig',[
            'notifications' => $systemNotifications
        ]);
    }

    /**
     * Add new notification to the system
     * @Route("/new", name="system_notification_new", methods={"GET","POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $newSystemNotification = new SystemNotification();
        $newSystemNotification->setCreatedBy($this->getUser());
        /** Current browsing country */
        $country = $this->get('session')->get('region');
        $country = $em->getRepository(Country::class)->find($country);
        $newSystemNotification->setCountry($country);
        return $this->handleFormSubmit($newSystemNotification,$request);
    }

    /**
     * Edit system notification
     * @Route("/{id}/edit", name="system_notification_edit",methods={"GET","POST"})
     * @param SystemNotification $systemNotification
     * @param Request $request
     * @return Response
     */
    public function editAction(SystemNotification $systemNotification,Request $request): Response
    {
        return $this->handleFormSubmit($systemNotification,$request);
    }


    /**
     * @param SystemNotification $systemNotification
     * @param Request $request
     * @return Response
     */
    private function handleFormSubmit(SystemNotification $systemNotification,Request $request): Response
    {
        $form = $this->createForm(SystemNotificationType::class,$systemNotification);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $isNew = !$systemNotification->getId();
            /** Check if uploaded an image */
            /** @var File $file */
            $file = $request->files->get('image');
            if ($file && $file->getSize() <= 1024*1024) {
                $fileUploader = $this->get('file_uploader');
                $img = $fileUploader->uploadFile(
                    $this->getParameter('system_notification_images_directory'), $file
                );
                if ($img) {
                    if($systemNotification->getImage()){
                        $fileUploader->deleteFile($systemNotification->getImage());
                    }
                    $systemNotification->setImage($img);
                }
            }
            if($request->get('sendNotification',false)){
                $this->sendNotification($systemNotification);
            }
            $em->persist($systemNotification);
            $em->flush();
            $trans = $this->get('translator');
            $this->addFlash('success',$trans->trans($isNew?'system.notification.new_success':'system.notification.update_success'));
            return $this->redirect($this->generateUrl('system_notifications'));
        }
        return $this->render('@App/admin/notifications/form.notification.html.twig',[
            'form' => $form->createView(),
            'notification' => $systemNotification
        ]);
    }

    /**
     * Edit system notification
     * @Route("/{id}/delete", name="system_notification_delete",methods={"DELETE"})
     * @param SystemNotification $systemNotification
     * @return RedirectResponse|Response
     */
    public function deleteAction(SystemNotification $systemNotification)
    {
        $em = $this->getDoctrine()->getManager();
        if($this->isGranted(User::ROLE_SUPER_ADMIN) && $systemNotification->getCreatedBy()->getId() !== $this->getUser()->getId()){
            return $this->json(['message' => $this->get('translator')->trans('system.notification.delete_error_unauthorized')],Response::HTTP_UNAUTHORIZED);
        }
        $em->remove($systemNotification);
        $em->flush();
        return $this->json(['message' => $this->get('translator')->trans('system.notification.delete_success')]);
    }

    /**
     * Send system notification
     * @Route("/{id}/send", name="system_notification_send",methods={"POST"})
     * @param SystemNotification $systemNotification
     * @return RedirectResponse|Response
     */
    public function sendAction(SystemNotification $systemNotification)
    {
        $this->sendNotification($systemNotification);
        return $this->json(['message' => $this->get('translator')->trans('system.notification.send_success')]);
    }


    /**
     * @param SystemNotification $systemNotification
     */
    private function sendNotification(SystemNotification $systemNotification): void
    {
        $country = $this->get('session')->get('region');
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findActiveUsers()
                                    ->andWhere('u.country = :country')
                                    ->setParameter('country',$country)
                                    ->getQuery()->getResult();
        $registerEmails = [];
        $registeredDevices = [];
        /** @var User $user */
        foreach ($users as $user){
            $notificationConfig = $user->getNotificationConfig($systemNotification->getType());
            if(!$user->isBanned() && $notificationConfig->getPush() && ($user->getIosFcmToken() || $user->getAndroidFcmToken())){
                $registeredDevices[] = $user->getIosFcmToken()??$user->getAndroidFcmToken();
            }
            if($notificationConfig->getEmail()){
                $registerEmails[] = $user->getEmail();
            }
            $notification = new Notification();
            $notification->setTitle($systemNotification->getTitle());
            $notification->setBody($systemNotification->getBody());
            $notification->setToUser($user);
            $notification->setType($systemNotification->getType());
            $notification->setExtra([
                'type' => Notification::TYPE_NEWS,
                'id' => $systemNotification->getId(),
            ]);
            $notification->setIcon($systemNotification->getImage()?$systemNotification->getImage()->getPath():'');
            $em->persist($notification);
            $country = $user->getCountry();
        }
        if(!empty($registerEmails)){
            $email = (new \Swift_Message($systemNotification->getTitle()))
                ->setFrom([$this->getParameter('mailer_support') => 'Maatloob'])
                ->setBody(
                    $this->renderView(
                        ':emails:notification.html.twig',
                        [
                            'title' => $systemNotification->getTitle(),
                            'body' => $systemNotification->getBody(),
                            'lang' => $country->getDefaultLanguage()->getIdentifier(),
                            'image' => $systemNotification->getImage()?$systemNotification->getImage()->getPath():null
                        ]
                    ),
                    'text/html'
                );
            $email->setBcc($registerEmails);
            $this->get('mailer')->send($email);
        }
        $message['title'] = $systemNotification->getTitle();
        $message['body'] = $systemNotification->getBody();
        $message['registration_ids'] = $registeredDevices;
        $message['data'] = ['type' => ''];
        if($systemNotification->getImage()){
            $message['image'] = $this->getParameter('server_base_url').$systemNotification->getImage()->getPath();
        }
        $message['icon'] = $this->getParameter('server_base_url').'/assets/img/logo/logo_v3.png';
        $this->get('fcm_client')->createMessage($message)->sendMessage();
        $systemNotification->setLastSend(new \DateTime());
        $em->persist($systemNotification);
        $em->flush();
    }

}
