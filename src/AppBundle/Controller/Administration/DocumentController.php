<?php

namespace AppBundle\Controller\Administration;

use AppBundle\Entity\Country;
use AppBundle\Entity\DocumentType;
use AppBundle\Entity\Language;
use AppBundle\Entity\User;
use AppBundle\Entity\UserDocument;
use AppBundle\Form\DocumentTypeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DocumentController
 * @package AppBundle\Controller\Administration
 * @Route("/system/documents")
 */
class DocumentController extends Controller
{
    /**
     * Lists all documents entities.
     *
     * @Route("/", name="system_documents")
     */
    public function indexAction(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $country = $this->get('session')->get('region');
        $documentTypes = $em->getRepository(DocumentType::class)->findBy(['country' => $country]);
        return $this->render('@App/admin/system_config/document/document-config.html.twig',
            array('documentTypes' => $documentTypes));
    }

    /**
     * @Route("/new", name="system_document_new",methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function newDocumentTypeAction(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $countryIdentifier = $this->get('session')->get('region');
        /** @var Country $currentCountry */
        $currentCountry = $em->getRepository(Country::class)->findOneBy(['identifier'=>$countryIdentifier]);
        $documentType = new DocumentType();
        $documentType->setCountry($currentCountry);
        /** Find country languages */
        $languages[] = $currentCountry->getDefaultLanguage();
        if($currentCountry->getDefaultLanguage()->getIdentifier()!== 'en'){
            $languages[] = $em->getRepository(Language::class)->find('en');
        }
        $form = $this->createForm(DocumentTypeType::class, $documentType);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isValid()) {
            /** Save all document translations */
            foreach ($request->get('document')['name'] as $language => $nameTranslation) {
                $documentType->translate($language)->setName($nameTranslation);
            }
            foreach ($request->get('document')['description'] as $language => $nameTranslation) {
                $documentType->translate($language)->setDescription($nameTranslation);
            }
            $documentType->mergeNewTranslations();
            $em->persist($documentType);
            $em->flush();
            $this->addFlash('success', $translator->trans('system.documents.add.success'));
            $this->addDocumentToUsers($documentType);
            return $this->redirect($this->generateUrl('system_documents'));
        }
        return $this->render('@App/admin/system_config/document/new.document.html.twig', [
            'form' => $form->createView(),
            'languages' => $languages
        ]);
    }

    /**
     * @Route("/{id}/edit", name="system_document_edit",methods={"GET","POST"})
     * @param Request $request
     * @param DocumentType $documentType
     * @return Response
     */
    public function editDocumentTypeAction(Request $request, DocumentType $documentType): Response
    {
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $languages[] = $documentType->getCountry()->getDefaultLanguage();
        if($documentType->getCountry()->getDefaultLanguage()->getIdentifier()!== 'en'){
            $languages[] = $em->getRepository(Language::class)->find('en');
        }
        $form = $this->createForm(DocumentTypeType::class, $documentType);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isValid()) {
            /** Save all document translations */
            foreach ($request->get('document')['name'] as $language => $nameTranslation) {
                $documentType->translate($language)->setName($nameTranslation);
            }
            foreach ($request->get('document')['description'] as $language => $nameTranslation) {
                $documentType->translate($language)->setDescription($nameTranslation);
            }
            $documentType->mergeNewTranslations();
            $em->persist($documentType);
            $em->flush();
            $this->addFlash('success', $translator->trans('system.documents.edit.success'));
            return $this->redirect($this->generateUrl('system_documents'));
        }
        return $this->render('@App/admin/system_config/document/edit.document.html.twig', [
            'form' => $form->createView(),
            'languages' => $languages,
            'document' => $documentType
        ]);
    }

    /**
     * Edit document type to enabled or disabled
     * @Route("/switch-status", name="system_document_switch_status",methods={"PUT"})
     * @param Request $request
     * @return JsonResponse
     */
    public function documentTypeStatusAction(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->request->has('id')) {
            /** @var DocumentType $document */
            $document = $em->getRepository(DocumentType::class)->find($request->get('id'));
            $document->setDisabled(!$document->isDisabled());
            $em->persist($document);
            $em->flush();
            return $this->json(['message' => 'Document ' . $document->translate()->getName() . ' has been ' . ($document->isDisabled() ? 'Disabled' : 'Enabled')]);
        }
        return $this->json(['message' => 'Document not found'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * Init new document type for user after it's creation
     * @param DocumentType $documentType
     */
    private function addDocumentToUsers(DocumentType $documentType): void
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findBy(['country'=>$documentType->getCountry()]);
        /** @var User $user */
        foreach ($users as $user) {
            $userDocument = new UserDocument();
            $userDocument->setUser($user);
            $userDocument->setType($documentType);
            $em->persist($userDocument);
            $em->persist($user);
        }
        $em->flush();
    }
}
