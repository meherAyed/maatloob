<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\Page;
use AppBundle\Entity\SystemNotification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{

    /**
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('default/maintenance.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @param $pageName
     * @return Response
     */
    public function PageAction($pageName): Response
    {
        $page  = $this->getDoctrine()->getRepository(Page::class)
            ->findOneBy(['identifier'=>$pageName,'published'=>true]);
        if(!$page) {
            throw new NotFoundHttpException('Requested page could not be found');
        }
        return $this->render('@App/front/page.html.twig',['page'=>$page]);
    }

    /**
     * @param SystemNotification $systemNotification
     * @return Response
     */
    public function showNotificationNewsAction(SystemNotification $systemNotification){
        return $this->render('@App/front/news_and_updates.html.twig',['notification'=>$systemNotification]);
    }
}
