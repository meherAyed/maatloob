<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller\Front;

use AppBundle\Entity\Badge;
use AppBundle\Entity\DocumentType;
use AppBundle\Entity\NotificationConfig;
use AppBundle\Entity\Skills;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBadge;
use AppBundle\Entity\UserDocument;
use AppBundle\Entity\UserWallet;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RegistrationController
 * @package AppBundle\Controller\Front
 */
class RegistrationController extends Controller
{


    /**
     * Tell the user his account is now confirmed.
     * @param Request $request
     * @param $token
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response|null
     * @throws \Exception
     */
    public function confirmAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            return $this->render('@App/front/register/error_register_expiration_link.twig');
        }

        if($user->isEnabled()){
            $user->setConfirmationToken(null);
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('user_registration_confirmed');
        }

        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        self::completeUserData($user,$em);
        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);
        $userManager->updateUser($user);
        $em->flush();
        if (null === $response = $event->getResponse()) {
            $url = $this->generateUrl('user_registration_confirmed');
            $response = new RedirectResponse($url);
        }
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, $response));

        return $response;
    }

    /**
     * Render Confirmed registration page
     */
    public function confirmedAction(): Response
    {
        return $this->render('@App/front/register/confirmed.html.twig');
    }

    /**
     * @param User $user
     * @param ObjectManager $em
     */
    public static function completeUserData(User $user,ObjectManager $em): void
    {
        $user->setEnabled(true);
        /** Set Default user notification config */
        foreach (NotificationConfig::$actions as $action) {
            $config = new NotificationConfig();
            $config->setUser($user);
            $config->setAction(strtoupper($action));
            $config->setSms(false);
            $config->setPush(true);
            $config->setEmail(true);
            $em->persist($config);
        }
        /** Set Default user document */
        $userDocumentTypes = $em->getRepository(DocumentType::class)->findBy(['country'=>$user->getCountry()]);
        /** @var DocumentType $documentType */
        foreach ($userDocumentTypes as $documentType) {
            $userDocument = new UserDocument();
            $userDocument->setUser($user);
            $userDocument->setType($documentType);
            $em->persist($userDocument);
        }
        $user->setSkills(new Skills());
        $user->setConfirmationToken(null);
        $user->setWallet(new UserWallet());
        /** init badges */
        $badges = $em->getRepository(Badge::class)->findAll();
        foreach ($badges as $badge){
            $userBadge = new UserBadge();
            $userBadge->setUser($user);
            $userBadge->setBadge($badge);
            $em->persist($userBadge);
        }
    }
}
