<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LanguageMessage;
use AppBundle\Entity\Notification;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Xendit\Cards;
use Xendit\Xendit;

class DevController extends Controller
{
    /**
     * @Route("/dev/add-message", name="dev-add-message")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $messages = json_decode(file_get_contents($this->getParameter('web_dir') . '/languages-messages.json'), true);
        if ($request->isMethod(Request::METHOD_POST)) {
            $newMessageKey = $request->get('messageKey');
            $message = $em->getRepository(LanguageMessage::class)->findOneBy(['identifier' => $newMessageKey]);
            if ($message) {
                $this->addFlash('error', 'Message key exist !');
            } else {
                $newMessageArabic = $request->get('messageArabic');
                $newMessageEnglish = $request->get('messageEnglish');
                $newMessageIndonesian = $request->get('messageIndonesian');
                $fs = new Filesystem();
                $messages[] = [
                    'key' => $newMessageKey,
                    'english' => $newMessageEnglish,
                    'arabic' => $newMessageArabic,
                    'indonesian' => $newMessageIndonesian
                ];
                $fs->dumpFile($this->getParameter('web_dir') . '/languages-messages.json', json_encode($messages, JSON_UNESCAPED_UNICODE));
                $kernel = $this->get('kernel');
                $application = new Application($kernel);
                $application->setAutoExit(false);
                $input = new ArrayInput(array(
                    'command' => 'api:init_system_language_command'
                ));
                $output = new BufferedOutput();
                $application->run($input, $output);
                $this->addFlash('success', 'Message successfully added');
            }
        }
        return $this->render('@App/dev/add-message.html.twig');
    }

    /**
     * @Route("/dev/send-notif", name="dev-send-notif")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function sendNotification(Request $request){
        if($request->isMethod(Request::METHOD_POST)){
            if($notif = $request->query->get('notif_id',null)){
                $notification = $this->getDoctrine()->getRepository(Notification::class)->find($notif);
                $this->get('notify_user')->notify($notification);
            }
            $message['title'] = $request->get('title');
            $message['body'] = $request->get('message');
            $message['to'] = $request->get('token');
            $message['data'] = ['type'=>''];
            $message['icon'] = $this->getParameter('server_base_url').'/assets/img/logo/logo_v2.png';
            $message['image'] = $this->getParameter('server_base_url').'/assets/img/icon/money.png';
            $fcm = $this->get('fcm_client');
            $fcm->createMessage($message)->sendMessage();
        }
        return $this->render('@App/dev/send-notification.html.twig');
    }

}
