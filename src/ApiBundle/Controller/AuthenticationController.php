<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 29/06/2019
 * Time: 11:48
 */

namespace ApiBundle\Controller;


use Abraham\TwitterOAuth\TwitterOAuth;
use ApiBundle\Form\RegisterForm;
use AppBundle\AppEvents;
use AppBundle\Controller\Front\RegistrationController;
use AppBundle\Entity\Badge;
use AppBundle\Entity\Country;
use AppBundle\Entity\File;
use AppBundle\Entity\Referral;
use AppBundle\Entity\SystemConfig;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBadge;
use AppBundle\Services\AppService;
use AppleSignIn\ASDecoder;
use AppleSignIn\ASPayload;
use Components\Helper;
use DateTime;
use Exception;
use Facebook\Facebook;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken;
use Google_Client;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Nelmio\ApiDocBundle\Annotation\Model;
use Psr\Log\LogLevel;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AuthenticationController
 * @package ApiBundle\Controller
 * @SWG\Tag(name="Authentication")
 */
class AuthenticationController extends Controller
{
    /**
     * Login API
     * @Rest\Post("/login", name="login", methods={"POST"})
     * @Rest\View(
     *     serializerGroups={"user_data","file_info","country_data","user_wallet"}
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Credentials are valid return user information with JWT",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Successfully logged in"),
     *         @SWG\Property(property="data",type="object",
     *            @SWG\Property(property="token", type="string", example=""),
     *            @SWG\Property(property="user", type="object", ref=@Model(type=User::class, groups={"user_data","file_info"})))
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Error login invalid credentials",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=400),
     *         @SWG\Property(property="msg", type="string", example="Invalid email or password"),
     *      )
     * )
     *
     * @SWG\Response(
     *     response=401,
     *     description="Error login account account not confirmed",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=401),
     *         @SWG\Property(property="msg", type="string", example="Account not confirmed verify your email"),
     *      )
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="Error login account is banned",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=403),
     *         @SWG\Property(property="msg", type="string", example="This account is banned"),
     *      )
     * )
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Request Body Params",
     *     @Model(type=ApiBundle\Form\LoginForm::class)
     * )
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function loginAction(Request $request): array
    {
        $loginMethod = $request->get('loginMethod', 'simple');
        if ($loginMethod === 'simple') {
            $email = $request->get('email');
            $password = $request->get('password');
            /** @var User $user */
            $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneBy(['email' => $email]);
            if ($user) {
                $isValid = $this->get('security.password_encoder')
                    ->isPasswordValid($user, $password);
                if (!$isValid) {
                    $user = null;
                }
            }
        } else {
            $user = $this->socialLoginCheck($loginMethod, $request->get('accessToken'),$request->get('country',null));
        }
        $data = null;
        if (!$user) {
            $msg = 'authentication.error.invalid_credentials';
            $code = Response::HTTP_NOT_FOUND;
            $user = null;
        } else{
            /** Set country dateZone to get correct date */
            if($user->getCountry()){
                Helper::setCountryTimeZone($user->getCountry()->getIdentifier());
            }
            if (!$user->isEnabled()) {
                $code = Response::HTTP_FORBIDDEN;
                $msg = $user->isBanned() ?: 'authentication.error.disabled';
                $data = null;
            } elseif ($activeBan = $user->getActiveBan()) {
                $dbTranslator = $this->get('app.database_translator');
                $msg = $dbTranslator->transDb(
                    $activeBan->isPermanent() ?
                        'account_permanently_banned' :
                        'account_suspended_to_date'
                    , ['#date#' => $activeBan->getUnbanDate() ? $activeBan->getUnbanDate()->format('d-m-Y H:i') : '-']);
                $code = Response::HTTP_FORBIDDEN;
                $data = null;
            }else {
                $data = $this->loginSuccessData($user);
                $msg = 'Login success';
                $code = Response::HTTP_OK;
            }
        }
        return [
            'code' => $code,
            'msg' => $this->get('app.database_translator')->transDb($msg),
            'data' => $data,
        ];
    }

    private function loginSuccessData($user)
    {
        $jwtManager = $this->container->get('lexik_jwt_authentication.jwt_manager');
        $data['user'] = $user;
        $data['meta_data']['id_user'] = $user->getId();
        $data['meta_data']['token'] = $jwtManager->create($user);
        $data['meta_data']['token_expiration_date'] =
            (new DateTime())->modify('+ ' . $this->getParameter('jwt_expiration') . ' second');
        $event = new AuthenticationSuccessEvent(array(), $user, new Response());
        $this->get('gesdinet.jwtrefreshtoken.send_token')->attachRefreshToken($event);
        $data['meta_data']['refreshToken'] = $this->get('gesdinet.jwtrefreshtoken.refresh_token_manager')
            ->getLastFromUsername($user->getUsername())->getRefreshToken();
        return $data;
    }

    /**
     * Register API
     * @Rest\Post("/register", name="register", methods={"POST"})
     * @Rest\View(
     *     serializerGroups={"user_data","file_info","country_data","user_wallet"}
     * )
     * @SWG\Response(
     *     response=201,
     *     description="Account created and confirm email was sent",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=201),
     *         @SWG\Property(property="msg", type="string", example="Account created"),
     *      )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Account exist but not confirmed , confirm email was sent",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Verify your email"),
     *      )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Error register provided inputs not valid",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=400),
     *         @SWG\Property(property="msg", type="string", example="Require email and password"),
     *      )
     * )
     * @SWG\Response(
     *     response=409,
     *     description="Error register used email address",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=409),
     *         @SWG\Property(property="msg", type="string", example="Used email address"),
     *      )
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Request Body Params",
     *     @Model(type=ApiBundle\Form\RegisterForm::class)
     * )
     * @param Request $request
     * @return array
     */
    public function registerAction(Request $request): array
    {
        $dbTranslator = $this->get('app.database_translator');
        $registerMethod = $request->get('registerMethod', 'simple');
        if ($registerMethod === 'simple') {
            $userManager = $this->get('fos_user.user_manager');
            $newUser = $userManager->createUser();
            $form = $this->createForm(RegisterForm::class, $newUser);
            $data = $this->get('json.request')->getParameters();
            $form->submit($data);
            if ($form->isValid()) {
                /** @var User $user */
                $user = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findOneBy(['email' => $data['email']]);
                if ($user) {
                    if (!$user->isEnabled()) {
                        $this->get('user.mailer')->sendConfirmationEmailMessage($user);
                        $code = Response::HTTP_OK;
                        $msg = 'register.verify_email';
                    } else {
                        $code = Response::HTTP_CONFLICT;
                        $msg = 'register.email_used';
                    }
                } else {
                    $newUser->setUsername($newUser->getEmail());
                    /** Save referral to be the email because it is mandatory and unique */
                    $newUser->setReferralCode($newUser->getEmail());
                    $userManager->updateUser($newUser);
                    /** Check if registered user used a referral code of another member */
                    if (!empty($data['referral_code'])) {
                        $this->addReferral($newUser, $data['referral_code']);
                    }
                    $eventDispatcher = $this->get('event_dispatcher');
                    $form = $this->get('form.factory')->create();
                    $form->setData($newUser);
                    $event = new FormEvent($form, $request);
                    $eventDispatcher->dispatch(AppEvents::REGISTRATION_SUCCESS, $event);
                    $userManager->updateUser($newUser);
                    return [
                        'code' =>  Response::HTTP_OK,
                        'msg' => $dbTranslator->transDb('register.success'),
                    ];
                }
            }else{
                $code = Response::HTTP_BAD_REQUEST;
                $msg =  $dbTranslator->transDb('register.email_used');
            }
        } elseif ($country = $request->get('country')) {
            $em = $this->getDoctrine()->getManager();
            $country = $em->getRepository(Country::class)->find($country);
            if ($country) {
                $user = $this->socialRegister($registerMethod, $request->get('accessToken'),$country);
                if (!$user) {
                    return [
                        'code' => Response::HTTP_BAD_REQUEST,
                        'msg' => $dbTranslator->transDb('invalid_request'),
                    ];
                }
                $data = $this->loginSuccessData($user);
                return [
                    'code' =>  Response::HTTP_OK,
                    'msg' => $dbTranslator->transDb('register.success'),
                    'data' => $data
                ];
            }
            $code = Response::HTTP_BAD_REQUEST;
            $msg = 'invalid_request';
        } else {
            $code = Response::HTTP_BAD_REQUEST;
            $msg = 'invalid_request';
        }
        return [
            'code' => $code,
            'msg' => $dbTranslator->transDb($msg),
        ];
    }

    /**
     * @param User $user
     * @param string $referralCode
     */
    private function addReferral(User $user, string $referralCode): void
    {
        $em = $this->getDoctrine()->getManager();
        $referralCodeUser = $em->getRepository(User::class)->findOneBy(['referralCode' => $referralCode]);
        /** @var SystemConfig $systemConfig */
        $systemConfig = $em->getRepository(SystemConfig::class)->findOneBy(['country' => $user->getCountry()]);
        /** @var AppService $appService */
        $totalReferredFriends = count($em->getRepository(Referral::class)->findBy(['fromUser' => $referralCodeUser]));
        if ($referralCodeUser && $totalReferredFriends < $systemConfig->getReferralToUserLimit()) {
            $referralLink = new Referral();
            $referralLink->setFromUser($referralCodeUser);
            $referralLink->setForUser($user);
            $em->persist($referralLink);
            $em->flush();
        }
    }

    /**
     * Forget password API
     * @Rest\Post("/forgot_password", name="forget_password", methods={"POST"})
     * @Rest\View()
     * @SWG\Response(
     *     response=200,
     *     description="Reset password email succeeded and email was sent",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Verify your email"),
     *      )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Error sending reset password email invalid input",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=400),
     *         @SWG\Property(property="msg", type="string", example="Invalid email address"),
     *      )
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Request Body Params",
     *      @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="email", type="string",example="api_maatloob@gmail.com",description="Account email to reset password")
     *      )
     * )
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function sendResetPasswordEmailAction(Request $request): array
    {
        $username = $request->request->get('username');
        $translator = $this->get('app.database_translator');
        $validator = $this->get('validator');
        $errors = $validator->validate($username, new Assert\Email(['message' => $translator->transDb('forget_password.error.invalid_email')]));
        if (count($errors) > 0) {
            return [
                'code' => Response::HTTP_BAD_REQUEST,
                'msg' => $errors[0]
            ];
        }

        /** @var $user UserInterface */
        $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        if ($user) {
            /** set language to user last logged in language */
            $request->setLocale($user->getLanguage());
            /* Dispatch init event */
            $event = new GetResponseUserEvent($user, $request);
            $dispatcher->dispatch(FOSUserEvents::RESETTING_SEND_EMAIL_INITIALIZE, $event);
            $ttl = $this->container->getParameter('fos_user.resetting.token_ttl');
            if (null === $event->getResponse() && !$user->isPasswordRequestNonExpired($ttl)) {
                $event = new GetResponseUserEvent($user, $request);
                $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_REQUEST, $event);

                if (null === $user->getConfirmationToken()) {
                    /** @var $tokenGenerator TokenGeneratorInterface */
                    $tokenGenerator = $this->get('fos_user.util.token_generator');
                    $user->setConfirmationToken($tokenGenerator->generateToken());
                }

                /* Dispatch confirm event */
                $event = new GetResponseUserEvent($user, $request);
                $dispatcher->dispatch(FOSUserEvents::RESETTING_SEND_EMAIL_CONFIRM, $event);

                $this->get('user.mailer')->sendResettingEmailMessage($user);
                $user->setPasswordRequestedAt(new DateTime());
                $this->get('fos_user.user_manager')->updateUser($user);
                /* Dispatch completed event */
                $event = new GetResponseUserEvent($user, $request);
                $dispatcher->dispatch(FOSUserEvents::RESETTING_SEND_EMAIL_COMPLETED, $event);
            }
        }
        return [
            'code' => Response::HTTP_OK,
            'msg' => $translator->transDb('forget_password.success.verify_email')
        ];
    }

    /**
     * @Rest\Post("/logout", name="logout", methods={"POST"})
     * @Rest\View()
     * @SWG\Response(
     *     response=200,
     *     description="user logout",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="You have been successfully logged out"),
     *      )
     * )
     * @return array
     * @throws Exception
     */
    public function logoutAction(): array
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user) {
            $em = $this->getDoctrine()->getManager();
            /** Delete notification token */
            $user->setIosFcmToken(null);
            $user->setAndroidFcmToken(null);
            /**
             * Delete refresh token
             * @var RefreshToken $refreshToken
             */
            $refreshToken = $this->get('gesdinet.jwtrefreshtoken.refresh_token_manager')
                ->getLastFromUsername($user->getUsername());
            $em->remove($refreshToken);
            $em->persist($user);
            $em->flush();
        }
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'You have been successfully logged out'
        ];
    }

    /**
     * @param $loginMethod
     * @param $accessToken
     * @param null $country
     * @return User|null
     */
    private function socialLoginCheck($loginMethod, $accessToken,$country=null): ?User
    {
        if($country){
            $country = $this->getDoctrine()->getRepository(Country::class)->find($country);
        }
        if ($loginMethod && $accessToken) {
            switch ($loginMethod) {
                case 'facebook':
                {
                    return $this->getUserFromFacebookAccessToken($accessToken, (bool)$country,$country);
                }
                case 'twitter':
                    return $this->getUserFromTwitterAccessToken($accessToken, (bool)$country,$country);
                case 'google':
                    return $this->getUserFromGoogleAccessToken($accessToken, (bool)$country,$country);
                case 'appel':
                    return $this->getUserFromAppelAccessToken($accessToken, (bool)$country,$country);
                default:
                    return null;
            }
        }
        return null;
    }

    /**
     * @param $registerMethod
     * @param $accessToken
     * @param $country
     * @return User|null
     */
    private function socialRegister($registerMethod, $accessToken,$country): ?User
    {
        if($country){
            $country = $this->getDoctrine()->getRepository(Country::class)->find($country);
        }
        if ($registerMethod && $accessToken) {
            switch ($registerMethod) {
                case 'facebook':
                {
                    return $this->getUserFromFacebookAccessToken($accessToken, true,$country);
                }
                case 'twitter':
                    return $this->getUserFromTwitterAccessToken($accessToken, true,$country);
                case 'google':
                    return $this->getUserFromGoogleAccessToken($accessToken, true,$country);
                case 'apple':
                    return $this->getUserFromAppelAccessToken($accessToken, true,$country);
                default:
                    return null;
            }
        }
        return null;
    }

    /**
     * @param $accessToken
     * @param bool $register
     * @param null $country
     * @return User|null
     */
    private function getUserFromFacebookAccessToken($accessToken, $register = false, $country= null): ?User
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $fb = new Facebook([
                'app_id' => $this->getParameter('fb_app_id'),
                'app_secret' => $this->getParameter('fb_app_secret'),
            ]);
            $payload = $fb->get('/me?fields=id,first_name,last_name,email,birthday,picture,cover', $accessToken);
            $payload = json_decode($payload->getBody(), true);
            if (isset($payload['email'], $payload['id'])) {
                $fbBadge = $em->getRepository(Badge::class)->findOneBy(['name' => Badge::Facebook]);
                /**
                 * Check for user badge facebook
                 * @var UserBadge $userBadge
                 */
                $userBadge = $em->getRepository(UserBadge::class)->findOneBy(['badge' => $fbBadge, 'data' => $payload['id']]);
                if (!$userBadge) {
                    /** Check if user connected with this fb for first time search for the user by fb email */
                    $user = $em->getRepository(User::class)->findOneBy(['email' => $payload['email']]);
                    if (!$user && $register) {
                        /** Create the user account with information form his facebook */
                        $userManager = $this->get('fos_user.user_manager');
                        /** @var User $user */
                        $user = $userManager->createUser();

                        $user->setEmail($payload['email']);
                        $user->setUsername($payload['email']);
                        $user->setFirstName($payload['first_name'] ?? '');
                        $user->setLastName($payload['last_name'] ?? '');
                        $user->setCountry($country);
                        /** Generate a fake password because it is required for the system to work properly */
                        $user->setPlainPassword(Helper::generateStrongPassword(20));
                        /** Create picture for user from social */
                        $picture = new File();
                        $picture->setPath($payload['picture']['data']['url']);
                        $picture->setName($user->getFullName() . ' facebook account picture');
                        $em->persist($picture);
                        $user->setPicture($picture);
                        $user->setReferralCode(Helper::slugify($user->getFirstName() . '-' . mb_substr($user->getLastName(), 0, 1, 'utf8')) . '-' . $user->getId());
                        $userManager->updateUser($user);
                        RegistrationController::completeUserData($user, $em);
                    }
                    if ($user) {
                        /** Create facebook badge for this user */
                        $userBadge = $user->getBadgeByType(Badge::Facebook);
                        $userBadge->setData($payload['id']);
                        $userBadge->setActive(true);
                        $em->persist($userBadge);
                        $em->flush();
                    }
                    return $user;
                }
                return $userBadge->getUser();
            }
        } catch (Exception $e) {
        }
        return null;
    }

    /**
     * @param $accessToken
     * @param bool $register
     * @param null $country
     * @return User|null
     */
    public function getUserFromGoogleAccessToken($accessToken, $register = false, $country= null): ?User
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Google_Client([
            'client_id' => $this->getParameter('google_app_id_android'),
            'client_secret' => $this->getParameter('google_app_secret')
        ]);
        $payload = $client->verifyIdToken($accessToken);
        /** Try with google ios app id if android doesn't work */
        if(!$payload){
            $client->setClientId($this->getParameter('google_app_id_ios'));
            $payload = $client->verifyIdToken($accessToken);
        }
        if ($payload) {
            $googleBadge = $em->getRepository(Badge::class)->findOneBy(['name' => Badge::Google]);
            /**
             * Check for user badge google
             * @var UserBadge $userBadge
             */
            $userBadge = $em->getRepository(UserBadge::class)->findOneBy(['badge' => $googleBadge, 'data' => $payload['sub']]);
            if (!$userBadge) {
                /** Check if user connected with this google for first time search for the user by fb email */
                $user = $em->getRepository(User::class)->findOneBy(['email' => $payload['email']]);
                if (!$user && $register) {
                    /** Create the user account with information form his facebook */
                    $userManager = $this->get('fos_user.user_manager');
                    /** @var User $user */
                    $user = $userManager->createUser();
                    $user->setEmail($payload['email']);
                    $user->setUsername($payload['email']);
                    $user->setFirstName($payload['given_name'] ?? '');
                    $user->setLastName($payload['family_name'] ?? '');
                    $user->setCountry($country);
                    /** Create picture for user from social */
                    $picture = new File();
                    $picture->setPath($payload['picture']);
                    $picture->setName($user->getFullName() . ' google account picture');
                    $em->persist($picture);
                    $user->setPicture($picture);
                    /** Generate a fake password because it is required for the system to work properly */
                    $user->setPlainPassword(Helper::generateStrongPassword(20));
                    $userManager->updateUser($user);
                    $user->setReferralCode(Helper::slugify($user->getFirstName() . '-' . mb_substr($user->getLastName(), 0, 1, 'utf8')) . '-' . $user->getId());
                    $userManager->updateUser($user);
                    RegistrationController::completeUserData($user, $em);
                    $em->flush();
                }
                if ($user) {
                    /** Create google badge for this user */
                    $userBadge = $user->getBadgeByType(Badge::Google);
                    $userBadge->setData($payload['sub']);
                    $userBadge->setActive(true);
                    $em->persist($userBadge);
                    $em->flush();
                }
                return $user;
            }
            return $userBadge->getUser();
        }
        return null;
    }

    /**
     * @param $accessToken
     * @param bool $register
     * @param null $country
     * @return User|null
     */
    public function getUserFromTwitterAccessToken($accessToken, $register = false, $country= null): ?User
    {
        $em = $this->getDoctrine()->getManager();
        $connection = new TwitterOAuth($this->getParameter('twitter_app_id'), $this->getParameter('twitter_app_secret'), $accessToken);
        $payload = $connection->get('account/verify_credentials');
        if ($payload) {
            $twitterBadge = $em->getRepository(Badge::class)->findOneBy(['name' => Badge::Twitter]);
            /**
             * Check for user badge twitter
             * @var UserBadge $userBadge
             */
            $userBadge = $em->getRepository(UserBadge::class)->findOneBy(['badge' => $twitterBadge, 'data' => $payload['id']]);
            if (!$userBadge) {
                /** Check if user connected with this twitter for first time search for the user by twitter email */
                $user = $em->getRepository(User::class)->findOneBy(['email' => $payload['email']]);

                if (!$user && $register) {
                    /** Create the user account with information form his facebook */
                    $userManager = $this->get('fos_user.user_manager');
                    /** @var User $user */
                    $user = $userManager->createUser();
                    $user->setEmail($payload['email']);
                    $user->setUsername($payload['email']);
                    $user->setFirstName(explode(' ', $payload['name'])[0] ?? '');
                    $user->setLastName(explode(' ', $payload['name'])[1] ?? '');
                    $user->setCountry($country);
                    /** Create picture for user from social */
                    $picture = new File();
                    $picture->setPath($payload['profile_image_url_https']);
                    $picture->setName($user->getFullName() . ' twitter account picture');
                    $em->persist($picture);
                    $user->setPicture($picture);
                    /** Generate a fake password because it is required for the system to work properly */
                    $user->setPlainPassword(Helper::generateStrongPassword(20));
                    $user->setReferralCode(Helper::slugify($user->getFirstName() . '-' . mb_substr($user->getLastName(), 0, 1, 'utf8')) . '-' . $user->getId());
                    $userManager->updateUser($user);
                    RegistrationController::completeUserData($user, $em);
                }

                if ($user) {
                    /** Create google badge for this user */
                    $userBadge = $user->getBadgeByType(Badge::Twitter);
                    $userBadge->setData($payload['sub']);
                    $userBadge->setActive(true);
                    $em->persist($userBadge);
                    $em->flush();
                }
                return $user;
            }
            return $userBadge->getUser();
        }
        return null;
    }

    /**
     * @param $accessToken
     * @param bool $register
     * @param null $country
     * @return User|object|null
     */
    private function getUserFromAppelAccessToken($accessToken, bool $register, $country=null) : ?User
    {
        $em = $this->getDoctrine()->getManager();
        /** @var ASPayload $payload */
        $payload = ASDecoder::getAppleSignInPayload($accessToken);
        if ($payload) {
            $appelBadge = $em->getRepository(Badge::class)->findOneBy(['name' => Badge::Apple]);
            /**
             * Check for user badge twitter
             * @var UserBadge $userBadge
             */
            $userBadge = $em->getRepository(UserBadge::class)->findOneBy(['badge' => $appelBadge, 'data' => $payload->getUser()]);
            if (!$userBadge) {
                /** Check if user connected with this twitter for first time search for the user by twitter email */
                $user = $em->getRepository(User::class)->findOneBy(['email' => $payload['email']]);

                if (!$user && $register) {
                    /** Create the user account with information form his facebook */
                    $userManager = $this->get('fos_user.user_manager');
                    /** @var User $user */
                    $user = $userManager->createUser();
                    $user->setEmail($payload->getEmail());
                    $user->setUsername($payload->getEmail());
                    $user->setFirstName($payload->getEmail());
                    $user->setLastName($payload->getEmail());
                    $user->setCountry($country);
                    /** Generate a fake password because it is required for the system to work properly */
                    $user->setPlainPassword(Helper::generateStrongPassword(20));
                    $user->setReferralCode(Helper::slugify($user->getFirstName() . '-' . mb_substr($user->getLastName(), 0, 1, 'utf8')) . '-' . $user->getId());
                    $userManager->updateUser($user);
                    RegistrationController::completeUserData($user, $em);
                }
                if ($user) {
                    /** Create google badge for this user */
                    $userBadge = $user->getBadgeByType(Badge::Apple);
                    $userBadge->setData($payload->getUser());
                    $userBadge->setActive(true);
                    $em->persist($userBadge);
                    $em->flush();
                }
                return $user;
            }
            return $userBadge->getUser();
        }
        return null;
    }
}