<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 29/06/2019
 * Time: 07:55
 */

namespace ApiBundle\Controller;


use AppBundle\Entity\Notification;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskQuestion;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TaskQuestionController extends Controller
{

    /**
     * @Rest\View(
     *     serializerGroups={"task_question_details","user_info","file_info"}
     * )
     * @Rest\Post("/task/question/add")
     * @param Request $request
     * @return array
     */
    public function addQuestionAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('app.database_translator');
        try {
            /** @var User $user */
            $user = $this->getUser();
            $taskId = (int)$request->get('task_id');
            /** @var Task $task */
            $task = $em->getRepository(Task::class)
                ->findOneBy(array('id' => $taskId));

            if (!$task) {
                throw new \Exception($translator->transDb('task_not_found'), Response::HTTP_NOT_FOUND);
            }

            if ($task->getUser()->getId() === $user->getId()) {
                throw new \Exception($translator->transDb('invalid_request'), Response::HTTP_UNAUTHORIZED);
            }

            $text = trim($request->get('text'));
            $file = $request->files->get('file');

            if (empty($text) && !$file) {
                throw new \Exception($translator->transDb('invalid_request'), Response::HTTP_BAD_REQUEST);
            }

            $tq = new TaskQuestion();
            $tq->setUser($user);
            $tq->setTask($task);
            $tq->setText($text);
            if (is_object($file)) {
                $fileObject = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'),$file, $user);
                $em->persist($fileObject);
                $tq->addFile($fileObject);
            }
            $em->persist($tq);
            if ($user->getId() !== $task->getUser()->getId()) {
                /** Create notification */
                $notification = new Notification();
                $notification->setTitle('notification.new_question.title');
                $notification->setBody('notification.new_question.body');
                $notification->setToUser($task->getUser());
                $notification->setFromUser($user);
                $notification->setExtra([
                    'type' => Notification::TYPE_TASK,
                    'task_id' => $tq->getTask()->getId(),
                    'question_id' => $tq->getId()
                ]);
                $notification->setType(Notification::ACTION_TASK_UPDATE);
                $notification->setParams([
                    '#user#' => $user->getName(),
                    '#task#' => $tq->getTask()->getTitle(),
                    '#msg#' => $tq->getText(),
                ]);
                $em->persist($notification);
                $em->flush();
            }

            $data = $tq;
            $code = Response::HTTP_OK;
            $msg = $translator->transDb('question.add.success');
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }

        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"task_question_details","user_info","file_info"}
     * )
     * @Rest\Post("/task/question/reply")
     * @param Request $request
     * @return array
     */
    public function replyQuestionAction(Request $request): array
    {
        $msg = 'Reply to Question';
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('app.database_translator');
        try {
            /** @var User $user */
            $user = $this->getUser();
            $questionId = (int)$request->get('question_id');
            /** @var TaskQuestion $taskQuestion */
            $taskQuestion = $this->getDoctrine()->getRepository(TaskQuestion::class)
                ->find($questionId);
            if (!$taskQuestion) {
                throw new \Exception($translator->transDb('question_not_found'), Response::HTTP_NOT_FOUND);
            }
            $text = trim($request->get('text'));
            if (empty($text)) {
                throw new \Exception('Invalid text', Response::HTTP_BAD_REQUEST);
            }

            $newTaskQuestion = new TaskQuestion();
            $newTaskQuestion->setUser($user);
            $newTaskQuestion->setReplyFor($taskQuestion);
            $newTaskQuestion->setTask($taskQuestion->getTask());
            $newTaskQuestion->setText($text);
            $file = $request->files->get('file');
            if (is_object($file)) {
                $fileObject = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'),$file, $user
                );
                $em->persist($fileObject);
                $newTaskQuestion->addFile($fileObject);
            }
            $em->persist($newTaskQuestion);
            if ($user->getId() !== $taskQuestion->getUser()->getId()) {
                /** Create notification */
                $notification = new Notification();
                $notification->setTitle('notification.question_reply.title');
                $notification->setBody('notification.question_reply.body');
                $notification->setToUser($taskQuestion->getUser());
                $notification->setFromUser($user);
                $notification->setExtra([
                    'type' => Notification::TYPE_TASK,
                    'task_id' => $taskQuestion->getTask()->getId(),
                    'question_id' => $taskQuestion->getId()
                ]);
                $notification->setType(Notification::ACTION_TASK_UPDATE);
                $notification->setParams([
                    '#user#' => $user->getName(),
                    '#msg#' => $text,
                ]);
                $em->persist($notification);
            }
            $em->flush();
            $data = $newTaskQuestion;
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }

        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/question/delete")
     * @param Request $request
     * @return array
     */
    public function deleteQuestionAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('app.database_translator');
        try {
            $user = $this->getUser();
            $questionId = (int)$request->get('question_id');
            $tq = $this->getDoctrine()
                ->getRepository(TaskQuestion::class)
                ->find($questionId);
            if (!$tq) {
                throw new \Exception($translator->transDb('question_not_found'), Response::HTTP_NOT_FOUND);
            }
            if ($tq->getUser()->getId() !== $user->getId()) {
                throw new \Exception($translator->transDb('cant_proceed_request'), Response::HTTP_UNAUTHORIZED);
            }
            $tq->setIsDeleted(true);
            $em->persist($tq);
            $em->flush();
            $msg = $translator->transDb('question.delete.success');
            $code = Response::HTTP_OK;
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }

        return [
            'code' => $code,
            'msg' => $msg,
        ];
    }


}