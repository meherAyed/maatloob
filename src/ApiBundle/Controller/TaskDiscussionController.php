<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Notification;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskDiscussion;
use AppBundle\Entity\TaskMessage;
use AppBundle\Entity\User;
use Doctrine\ORM\Tools\Pagination\Paginator;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TaskDiscussionController
 * @package ApiBundle\Controller
 */
class TaskDiscussionController extends Controller
{

    /**
     * @Rest\View()
     * @Rest\Post("/user/discussions")
     * @param Request $request
     * @return array
     */
    public function getDiscussionsAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        $oh = $this->get('api.object_filter');
        /** @var User $user */
        $user = $this->getUser();
        $page = (int)$request->get('page',1);
        $nbMaxParPage = (int)$request->get('resultsPerPage',10);
        $discRepo = $em->getRepository(TaskDiscussion::class)
            ->createQueryBuilder('t');
        $q = $discRepo->andWhere('(t.from= :user or t.to= :user) ')->setParameter('user', $user);
        $query = $q->orderBy('t.id', 'DESC')->getQuery();
        $firstResult = ($page - 1) * $nbMaxParPage;
        $query->setFirstResult($firstResult)->setMaxResults($nbMaxParPage);
        $paginator = new Paginator($query);
        $total = $paginator->count();
        $totalPages = ceil($total / $nbMaxParPage);
        $result = [];
        foreach ($paginator as $discuss) {
            $taskMessages = $em->getRepository(TaskMessage::class)
                ->findBy(['discussion' => $discuss], ['createdAt' => 'Desc']);
            $dUser = $discuss->getFrom();
            if ($dUser->getId() === $user->getId()) {
                $dUser = $discuss->getTo();
            }

            $unseenMessages = $em->getRepository(TaskMessage::class)
                ->findBy(['discussion' => $discuss,'user'=>$dUser,'isShowed'=>false], ['createdAt' => 'Desc']);

            $data = [
                'id' => $discuss->getId(),
                'unseenMessages' => count($unseenMessages),
                'closed' => $discuss->isClosed(),
                'task' => $oh->getDataFromObject($discuss->getTask(), [
                    'id',
                    'title',
                    'status'
                ]),
                'user' => $oh->getDataFromObject($dUser, [
                    'id',
                    'firstName',
                    'lastName',
                    'picture',
                    'usersRate'
                ]),
            ];
            if (count($taskMessages) > 0) {
                /** @var TaskMessage $lastTaskMessage */
                $lastTaskMessage = $taskMessages[0];
                $data['lastMessage'] = $oh->getDataFromObject($lastTaskMessage, [
                    'id',
                    'text',
                    'createdAt',
                ]);
                $data['lastMessage']['me'] = $lastTaskMessage->getUser()->getId() === $user->getId();
                if ($taskMessages[0]->getUser()->getId() !== $user->getId()) {
                    $data['lastMessage']['isShowed'] = $lastTaskMessage->getIsShowed();
                } else {
                    $data['lastMessage']['isShowed'] = true;
                }
            }
            $result[] = $data;
        }
        $data = [
            'disussions' => $result,
            'totalResults' => $total,
            'totalPages' => $totalPages,
            'resultsPerPage' => $nbMaxParPage,
            'currentPage' => $page
        ];

        return [
            'code' =>Response::HTTP_OK,
            'msg' => 'user discussions',
            'data' => $data
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/user/discussions/messages/add")
     * @param Request $request
     * @return array
     */
    public function addMessageAction(Request $request): array
    {
        $msg = 'Add message';
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        $oh = $this->get('api.object_filter');

        try {
            $user = $this->getUser();
            if (!$user) {
                throw new \Exception('invalid_request', Response::HTTP_BAD_REQUEST);
            }
            $discussionId = $request->get('discussion_id');
            if (!$discussionId) {
                throw new \Exception('Invalid discussion ID', Response::HTTP_BAD_REQUEST);
            }
            /** @var TaskDiscussion $discussion */
            $discussion = $em->getRepository(TaskDiscussion::class)
                ->find($discussionId);
            if (!$discussion || !in_array($user->getId(), [$discussion->getFrom()->getId(), $discussion->getTo()->getId()], true)){
                throw new \Exception('invalid_request', Response::HTTP_BAD_REQUEST);
            }
            if(!in_array($discussion->getTask()->getStatus(), [Task::STATUS_ASSIGNED, Task::STATUS_WAITING_PAYMENT, Task::STATUS_PAID], true)){
                throw new \Exception('discussion.add_message.error_closed', Response::HTTP_UNAUTHORIZED);
            }
            $text = trim($request->get('text'));
            if (empty($text) && !$request->files->has('file')) {
                throw new \Exception('discussion.add_message.error_empty', Response::HTTP_UNAUTHORIZED);
            }

            $tm = new TaskMessage();
            $tm->setUser($user);
            $tm->setDiscussion($discussion);
            $tm->setText($text);
            $file = $request->files->get('file');
            if (is_object($file)) {
                $fileObject = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'), $file, $user
                );
                $em->persist($fileObject);
                $tm->addFile($fileObject);
            }
            $em->persist($tm);
            $em->flush();
            $data = $oh->getDataFromObject($tm, [
                'id',
                'text',
                'createdAt',
                'user' =>
                    [
                        'id',
                        'firstName',
                        'lastName',
                        'picture',
                        'usersRate'
                    ]
            ]);
            $toUser = $discussion->getTo()->getId() === $user->getId()?$discussion->getFrom():$discussion->getTo();
            $data['files'] = $oh->getDataFromArray($tm->getFiles(), [
                'name',
                'path',
                'extension']);
            $notification = new Notification();
            $notification->setTitle('notification.new_message.title');
            $notification->setBody('notification.new_message.body');
            $notification->setToUser($toUser);
            $notification->setFromUser($user);
            $notification->setExtra([
                'type' => Notification::TYPE_MSG,
                'discussion_id' => $discussion->getId()
            ]);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setParams([
                '#user#' => $user->getName(),
                '#msg#' => $text
            ]);
            $em->persist($notification);
            $em->flush();
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }

        return [
            'code' => $code,
            'msg' => $this->get('app.database_translator')->transDb($msg),
            'data' => $data
        ];
    }


    /**
     * @Rest\View()
     * @Rest\Post("/user/discussions/detail")
     * @param Request $request
     * @return array
     */
    public function getDetailAction(Request $request): array
    {
        $msg = 'Discussion messages';
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        $oh = $this->get('api.object_filter');
        try {
            $user = $this->getUser();
            $discussionId = $request->get('discussion_id');
            if (!$discussionId) {
                throw new \Exception('invalid_request', Response::HTTP_BAD_REQUEST);
            }
            /** @var TaskDiscussion $discussion */
            $discussion = $em->getRepository(TaskDiscussion::class)
                ->find($discussionId);
            if (!$discussion || !in_array($user->getId(), [$discussion->getFrom()->getId(), $discussion->getTo()->getId()], true)){
                throw new \Exception('invalid_request', Response::HTTP_BAD_REQUEST);
            }
            $page = (int)$request->get('page',1);
            $nbMaxParPage = (int)$request->get('resultsPerPage',10);
            $discRepo = $em->getRepository(TaskMessage::class)
                ->createQueryBuilder('t');
            $q = $discRepo->andWhere('(t.discussion= :discussion) ')->setParameter('discussion', $discussion);
            $query = $q->orderBy('t.id', 'ASC')->getQuery();
            $firstResult = ($page - 1) * $nbMaxParPage;
            $query->setFirstResult($firstResult)->setMaxResults($nbMaxParPage);
            $paginator = new Paginator($query);
            $total = $paginator->count();
            $totalPages = ceil($total / $nbMaxParPage);
            $messages = [];
            foreach ($paginator as $object) {
                $messages[] = $object;
            }
            $data = $oh->getDataFromArray($messages, [
                'id',
                'text',
                'createdAt',
                'isShowed',
                'user' =>
                    ['id',
                        'firstName',
                        'lastName',
                        'picture',
                        'usersRate'
                    ], 'files' =>
                    [
                        'id',
                        'name',
                        'path',
                        'extension'
                    ]
            ]);
            $userDiscussion = $discussion->getFrom() ===  $user ? $discussion->getTo() : $discussion->getFrom();
            $task = $discussion->getTask();
            $data = [
                'closed' => $discussion->isClosed(),
                'user' => $oh->getDataFromObject($userDiscussion, [
                    'firstName',
                    'lastName',
                    'picture',
                ]),
                'messages' => $data,
                'task' => $oh->getDataFromObject($task, [
                    'id',
                    'title',
                    'description',
                    'price',
                    'status'
                ]),
                'totalResults' => $total,
                'totalPages' => $totalPages,
                'resultsPerPage' => $nbMaxParPage,
                'currentPage' => $page
            ];
            $data['task']['price'] = $user->getId() === $task->getTasker()->getId() ? $task->getAcceptedOffer()->getPrice():$task->getPrice();
            /** Set all messages to be as showed */
            $em->getRepository(TaskMessage::class)->setShowedMessagedFor($discussionId,$user);
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }

        return [
            'code' => $code,
            'msg' => $this->get('app.database_translator')->transDb($msg),
            'data' => $data
        ];
    }


    /**
     * @Rest\View()
     * @Rest\Post("/user/discussions/showed")
     * @param Request $request
     * @return array
     */
    public function setShowedAction(Request $request): array
    {
        $msg = 'Discussion edited';
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        try {
            $user = $this->getUser();
            $discussionId = $request->get('discussion_id');
            if (!$discussionId) {
                throw new \Exception('invalid_request', Response::HTTP_BAD_REQUEST);
            }
            $discussion = $em->getRepository(TaskDiscussion::class)
                ->find($discussionId);
            if (!$discussion) {
                throw new \Exception('invalid_request', Response::HTTP_BAD_REQUEST);
            }
            if(!in_array($discussion->getTask()->getStatus(), [Task::STATUS_ASSIGNED, Task::STATUS_WAITING_PAYMENT, Task::STATUS_PAID], true)){
                throw new \Exception('You are not allowed to edit this discussion', Response::HTTP_UNAUTHORIZED);
            }

            $taskMessages = $em->getRepository(TaskMessage::class)
                ->findBy(array('discussion' => $discussion));

            /** @var TaskMessage $message */
            foreach ($taskMessages as $message) {
                if ($message->getUser()->getId() !== $user->getId()) {
                    $message->setIsShowed(true);
                    $em->persist($message);
                }
            }
            $em->flush();
            $data = null;
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }
        return [
            'code' => $code,
            'msg' => $this->get('app.database_translator')->transDb($msg),
            'data' => $data
        ];
    }


}
