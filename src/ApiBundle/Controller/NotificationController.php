<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Country;
use AppBundle\Entity\Notification;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

class NotificationController extends Controller
{
    /**
     * @Rest\View(
     *     serializerGroups={"user_info","file_info"}
     * )
     * @Rest\Get("/notification/list")
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request): array
    {
        /** @var User $user */
        $user = $this->getUser();
        $trans = $this->get('app.database_translator');
        $notifications = $this->getDoctrine()->getRepository(Notification::class)
            ->findBy(['toUser' => $user],['createdAt'=>'DESC']);

        $data = array_map(static function (Notification $notification) use ($trans,$request,$user){
            $userCountry = $user->getCountry();
            /** Fix country currency symbol for en */
            $params = $notification->getParams();
            if($request->getLocale()==='en' && $userCountry->getDefaultLanguage()->getIdentifier()==='ar'){
                foreach ($notification->getParams() as $key => $param){
                    if($key==='#price#' || $key === '#tax#' || $key === '#currency#'){
                        $params[$key] = str_replace(
                            $userCountry->getCurrencySymbol(),
                            ucfirst(strtolower($userCountry->getCurrencyCode())),
                            $param
                        );
                    }
                }
            }

            return [
                'id' => $notification->getId(),
                'title' => $trans->transDb($notification->getTitle(),$notification->getParams(),$request->getLocale()),
                'body' => $trans->transDb($notification->getBody(),$params,$request->getLocale()),
                'shownOn' => $notification->isShownOn(),
                'readOn' => $notification->isReadOn(),
                'extra' => $notification->getExtra(),
                'date' => $notification->getCreatedAt(),
                'from' => $notification->getFromUser(),
            ];
        },$notifications);

        return [
            'code' => Response::HTTP_OK,
            'msg' => 'notifications list',
            'data' => $data
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/notification/update")
     * @param Request $request
     * @return array
     */
    public function updateAction(Request $request): array
    {
        $msg = 'Update notification';
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        try {
            $user = $this->getUser();
            if (!$user) {
                throw new \Exception('Invalid User', Response::HTTP_BAD_REQUEST);
            }
            $notificationId = $request->get('notification_id');
            $notification = $em->getRepository(Notification::class)
                ->findOneBy(['toUser' => $user, 'id' => $notificationId]);
            if (!$notification) {
                throw new \Exception('Invalid notification', Response::HTTP_BAD_REQUEST);
            }
            if ($request->request->has('read_on')) {
                $notification->setReadOn(true);
                $notification->setReadDate(new \DateTime());
            }
            if ($request->request->has('shown_on')) {
                $notification->setShownOn(true);
                $notification->setReadDate(new \DateTime());
            }
            $em->persist($notification);
            $em->flush();
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }
        return [
            'code' => $code,
            'msg' => $this->get('translator')->trans($msg),
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/notification/delete")
     * @param Request $request
     * @return array
     */
    public function deleteAction(Request $request): array
    {
        $msg = 'Delete notification';
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        try {
            $user = $this->getUser();
            $notificationId = $request->get('notification_id');
            $notification = $em->getRepository(Notification::class)
                ->findOneBy(['toUser' => $user, 'id' => $notificationId]);
            if (!$notification) {
                throw new \Exception('Invalid notification', Response::HTTP_BAD_REQUEST);
            }
            $em->remove($notification);
            $em->flush();
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }
        return [
            'code' => $code,
            'msg' => $this->get('translator')->trans($msg),
        ];
    }

}
