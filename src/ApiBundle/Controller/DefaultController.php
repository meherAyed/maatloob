<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Country;
use AppBundle\Entity\Language;
use AppBundle\Entity\LanguageMessage;
use AppBundle\Entity\Notification;
use AppBundle\Entity\ReportType;
use AppBundle\Entity\SystemNotification;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskCategory;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package ApiBundle\Controller
 * @SWG\Tag(name="SystemConfig")
 */
class DefaultController extends Controller
{
    /**
     * @Rest\View()
     * @Rest\Get("/")
     */
    public function indexAction(): array
    {
        return ['code' => 200, 'msg' => 'APP_API_V1.0', 'data' => null];
    }


    /**
     * @Rest\View(
     *     serializerGroups={"app_config"}
     * )
     * @Rest\Get("/language/{identifier}/messages")
     * @SWG\Response(
     *     response=200,
     *     description="Return language translated messages",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Language messages"),
     *         @SWG\Property(property="data",type="object",ref=@Model(type=LanguageMessage::class, groups={"app_config"}))
     *      )
     * )
     * @param language $language
     * @return array
     */
    public function languageAction(Language $language): array
    {
        $data = [];
        /** @var LanguageMessage $message */
        foreach ($language->getMessages() as $message){
            $data[$message->getIdentifier()]=$message->getMessage();
        }
        return [
            'code' => 200,
            'msg' => 'Language Messages',
            'data' => $data
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"app_config"}
     * )
     * @Rest\Get("/app_config/country/{identifier}")
     * @SWG\Response(
     *     response=200,
     *     description="Return language translated messages",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="app config for a specific country"),
     *      )
     * )
     * @param Country $country
     * @return array
     */
    public function configAction(Country $country): array
    {
        $em = $this->getDoctrine()->getManager();

        $reportTypes = $em->getRepository(ReportType::class)
                        ->findAll();

        $data['report_config']=[];
        /** @var ReportType $reportType */
        foreach ($reportTypes as $reportType){
            $data['report_config'][] = [
                'id' => $reportType->getId(),
                'name' => $reportType->translate()->getName(),
                'section' => $reportType->getSections()
            ];
        }

        $data['delivery'] = TaskCategory::DELIVERY_TYPE;
        $data['times'] = [
            Task::TIME_BEFORE_MORNING,
            Task::TIME_MIDDAY,
            Task::TIME_EVENING,
            Task::TIME_AFTER_NIGHT,
        ];
        /** Return configured languages */
        $data['languages'][] = $country->getDefaultLanguage();
        if($country->getDefaultLanguage()->getIdentifier()!== 'en'){
            $data['languages'][] = $em->getRepository(Language::class)->find('en');
        }
        $data['general_config'] = $country->getConfig();
        $data['xendit_public_key'] = $this->getParameter('xendit_payment_public');
        return [
            'code' => 200,
            'msg' => 'APP_CONFIG',
            'data' => $data
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"country_data"}
     * )
     * @Rest\Get("/supported_countries")
     * @SWG\Response(
     *     response=200,
     *     description="Return app supported countries",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Country list"),
     *         @SWG\Property(property="data",type="object",ref=@Model(type=Country::class, groups={"app_config"}))
     *      )
     * )
     * @return array
     */
    public function supportedCountriesAction(): array
    {
        $countries = $this->getDoctrine()->getRepository(Country::class)->findBy(['enabled'=>true]);
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'Country list',
            'data' => $countries
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"system_news_updates","file_info"}
     * )
     * @Rest\Get("/news-and-updates")
     * @param Request $request
     * @return array
     */
    public function newsAndUpdatesAction(Request $request): array
    {
        $country = $request->query->get('country','sa');
        $news = $this->getDoctrine()->getRepository(SystemNotification::class)->createQueryBuilder('sn')
            ->where('sn.country = :country')
            ->andWhere('sn.lastSend is not null')
            ->setParameter('country',$country)
            ->getQuery()->getResult();

        return [
            'code' => Response::HTTP_OK,
            'msg' => 'news and updates list',
            'data' => $news
        ];
    }
}
