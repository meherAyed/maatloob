<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Country;
use AppBundle\Entity\TaskCategory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TaskCategoryController
 * @package ApiBundle\Controller
 */
class TaskCategoryController extends Controller
{
    /**
     * @Rest\View()
     * @Rest\Get("/category/list")
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request): array
    {
        $msg = 'List categories';
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        $country = $request->headers->get('country','sa');
        /** @var Country $country */
        $country = $em->getRepository(Country::class)->find($country);
        $categories = $country->getCategories(true);
        $data = [];
        /** @var TaskCategory $category */
        foreach ($categories as $category){
            $data[] = [
                'id' => $category->getId(),
                'name' => $category->translate()->getName(),
                'icon' => $category->getIcon()?$category->getIcon()->getPath():'',
                'type' => $category->getType()
            ];
        }
        return [
            'code' => $code,
            'msg' => $this->get('translator')->trans($msg),
            'data' => $data,
        ];
    }
}
