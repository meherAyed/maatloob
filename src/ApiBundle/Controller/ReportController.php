<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Notification;
use AppBundle\Entity\Offer;
use AppBundle\Entity\Report;
use AppBundle\Entity\Reporter;
use AppBundle\Entity\ReportType;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskQuestion;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Exception\InvalidParameterException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;

/**
 * Class ReportController
 * @package ApiBundle\Controller
 * @SWG\Tag(name="Reports")
 */
class ReportController extends Controller
{

    /**
     * @Rest\View()
     * @Rest\Post("/report/add")
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function addAction(Request $request): array
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('app.database_translator');
        $data = $request->request->all();
        /** @var User $reportedUser */
        $reportedUser = $em->getRepository(User::class)
            ->find($data['reportedUser']);
        try {
            if (!$reportedUser) {
                throw new InvalidParameterException($translator->transDb('invalid_request'));
            }
            if (empty(trim($data['message']))) {
                throw new InvalidParameterException($translator->transDb('invalid_request'));
            }
            /** @var ReportType $reportType */
            $reportType = $em->getRepository(ReportType::class)
                ->find($data['type']);
            if (!$reportType || !in_array($data['section'], $reportType->getSections(), true)) {
                throw new \Exception($translator->transDb('invalid_request'), 400);
            }

            $object = (int)$data['object'];
            $alreadyReported = $this->getDoctrine()
                ->getRepository(Reporter::class)
                ->createQueryBuilder('rpt')
                ->join('rpt.report','report')
                ->where('report.objectId = :object')
                ->andWhere('report.type = :type')
                ->andWhere('report.section = :section')
                ->andWhere('rpt.user = :user')
                ->andWhere('report.reportedUser = :reportedUser')
                ->setParameters([
                    'object'=>$object,
                    'section'=>$data['section'],
                    'user'=>$user,
                    'reportedUser'=>$reportedUser,
                    'type' => $reportType
                ])
                ->getQuery()->getResult();
            if ($alreadyReported) {
                throw new \Exception($translator->transDb('report.add.already_reported'), Response::HTTP_BAD_REQUEST);
            }

            /** Check if there is already a report of this user for this topic */
            $report = $em->getRepository(Report::class)->findOneBy([
                'reportedUser' => $reportedUser,
                'section' => $data['section'],
                'objectId' => $object,
                'type' => $reportType
            ]);
            if(!$report){
                $report = new Report();
                $report->setType($reportType);
                $report->setReportedUser($reportedUser);
                $report->setObjectId($object);
                $report->setType($reportType);
                $report->setSection($data['section']);
                $em->persist($report);
                $em->flush();
            }
            $reporter = new Reporter();
            $reporter->setUser($user);
            $reporter->setMessage($data['message']);
            $reporter->setReport($report);
            $em->persist($reporter);
            $em->flush();
            $notification = new Notification();
            $notification->setFromUser($user);
            $notification->setType(Notification::ADMIN_NEW_REPORT);
            $notification->setTitle('notification.admin.report_title');
            $notification->setBody('notification.admin.report_body');
            $notification->setParams(
                ['_user_'=>$user->getName()]
            );
            $notification->setLink($this->generateUrl('admin_reports'));
            $this->get('notify_admin')->notifyAdmin($notification);
            $msg = $translator->transDb('report.add.success');
            $code = Response::HTTP_OK;
        }catch (\Exception $e){
            $code = $e->getCode();
            $msg = $e->getMessage();
        }
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => null,
        ];
    }

    /**
     * User reports API
     * @Rest\Get("/user/reports")
     * @Rest\View(
     *     serializerGroups={"report","report_type","user_info","file_info"}
     * )
     * @SWG\Response(
     *     response=201,
     *     description="Repost list",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="User reports"),
     *         @SWG\Property(property="data", type="object", ref=@Model(type=Report::class, groups={"report","report_type","user_info","file_info"}))
     *      )
     * )
     *
     * @Security(name="Bearer")
     * @return array
     */
    public function getReportsAction(): array
    {
        /** @var User $user */
        $user = $this->getUser();
        $reporters = $this->getDoctrine()->getRepository(Reporter::class)
                    ->findBy(['user' => $user,'isDeleted'=>false]);
        $reports = [];
        /** @var Reporter $reporter */
        foreach ($reporters as $reporter){
            switch ($reporter->getReport()->getSection()){
                case ReportType::SECTION_TASK : $className = Task::class;break;
                case ReportType::SECTION_QUESTION : $className = TaskQuestion::class;break;
                case ReportType::SECTION_OFFER : $className = Offer::class;break;
                default : $className = User::class;break;
            }
            $report['section'] = $reporter->getReport()->getSection();
            $report['message'] = $reporter->getMessage();
            $report['object'] = $this->getDoctrine()->getRepository($className)
                ->find($reporter->getReport()->getObjectId());
            $report['type'] = $reporter->getReport()->getType();
            $report['status'] = $reporter->getReport()->getStatus();
            $report['reported_user'] = $reporter->getReport()->getReportedUser();
            $reports[] = $report;
        }
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'List of reports',
            'data' => $reports,
        ];
    }
}
