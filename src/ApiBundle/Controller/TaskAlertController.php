<?php


namespace ApiBundle\Controller;
use ApiBundle\Form\AddTaskAlertForm;
use AppBundle\Entity\TaskAlert;
use AppBundle\Entity\User;
use Components\Helper;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class TaskAlertController extends Controller
{

    /**
     * @Rest\View(
     *     serializerGroups={"task_alert","address_details"}
     * )
     * @Rest\Get("/user/task-alerts")
     */
    public function indexAction(): array
    {
        $taskAlerts = $this->getDoctrine()->getRepository(TaskAlert::class)
            ->findBy(['user' => $this->getUser()]);
        return [
            'code' => Response::HTTP_OK,
            'msg'  => 'task alert list',
            'data' => $taskAlerts,
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"task_alert","address_details"}
     * )
     * @Rest\Post("/user/task-alerts")
     * @return array
     */
    public function newAction(): array
    {
        $user = $this->getUser();
        $taskAlert = new TaskAlert();
        $taskAlert->setUser($user);
        $form = $this->createForm(AddTaskAlertForm::class, $taskAlert);
        $data = $this->get('json.request')->getParameters();
        $form->submit($data);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($taskAlert);
            $em->flush();
            return [
                'code' => Response::HTTP_OK,
                'msg' => 'Task has been created',
                'data' => $taskAlert
            ];
        }

        return [
            'code' => Response::HTTP_BAD_REQUEST,
            'msg' => Helper::getErrorsFromForm($form, false)[0]
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"task_alert","address_details"}
     * )
     * @Rest\Put("/user/task-alerts/{id}")
     * @param TaskAlert $taskAlert
     * @return array
     */
    public function updateAction(TaskAlert $taskAlert): array
    {
        /** @var User $user */
        $user = $this->getUser();
        if($taskAlert->getUser()->getId() !== $user->getId()){
            return [
                'code' => Response::HTTP_UNAUTHORIZED,
                'msg'  => $this->get('app.database_translator')->transDb('invalid_request')
            ];
        }
        $form = $this->createForm(AddTaskAlertForm::class, $taskAlert);
        $data = $this->get('json.request')->getParameters();
        $form->submit($data);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($taskAlert);
            $em->flush();
            return [
                'code' => Response::HTTP_OK,
                'msg' => $this->get('app.database_translator')->transDb('update_success'),
                'data' => $taskAlert
            ];
        }

        return [
            'code' => Response::HTTP_BAD_REQUEST,
            'msg' => Helper::getErrorsFromForm($form, false)[0]
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"task_alert"}
     * )
     * @Rest\Delete("/user/task-alerts/{id}")
     * @param $id
     * @return array
     */
    public function deleteAction($id): array
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('app.database_translator');
        $taskAlert = $em->getRepository(TaskAlert::class)->find($id);
        if(!$taskAlert || $taskAlert->getUser()->getId() !== $user->getId()){
            return [
                'code' => Response::HTTP_UNAUTHORIZED,
                'msg'  => $translator->transDb('invalid_request')
            ];
        }
        $em->remove($taskAlert);
        $em->flush();
        return [
            'code' => Response::HTTP_OK,
            'msg'  => $translator->transDb('task_alert.delete.success'),
        ];
    }
}