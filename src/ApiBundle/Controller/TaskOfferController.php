<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 29/06/2019
 * Time: 07:57
 */

namespace ApiBundle\Controller;


use AppBundle\Entity\CountryCategory;
use AppBundle\Entity\DocumentType;
use AppBundle\Entity\Notification;
use AppBundle\Entity\OfferUpdateRequest;
use AppBundle\Entity\OfferReply;
use AppBundle\Entity\Offer;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskDiscussion;
use AppBundle\Entity\TaskInvoice;
use AppBundle\Entity\User;
use AppBundle\Entity\UserDocument;
use Exception;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class TaskOfferController extends Controller
{

    /**
     * @Rest\View(
     *     serializerGroups={"offer_details","user_info","file_info"}
     * )
     * @Rest\Post("/task/offer/add" )
     * @SWG\Response(
     *     response=201,
     *     description="Offer successfully created",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=201),
     *         @SWG\Property(property="msg", type="string", example="Offer has been created"),
     *         @SWG\Property(property="data",type="object",ref=@Model(type=Offer::class,groups={"offer_details","user_info","file_info"}))
     *      )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Task not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=404),
     *         @SWG\Property(property="msg", type="string", example="Task not found"),
     *      )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Cant add an offer invalid request",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=400),
     *         @SWG\Property(property="msg", type="string", example="Invalid request"),
     *      )
     * )
     *
     * @SWG\Response(
     *     response=401,
     *     description="You cant add an offer for this task",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=401),
     *         @SWG\Property(property="msg", type="string", example="You cant add offer on this task it is already assigned"),
     *      )
     * )
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Request Body Params",
     *     @Model(type=ApiBundle\Form\AddOfferForm::class)
     * )
     * @Security(name="Bearer")
     * @param Request $request
     * @return array
     */
    public function addOfferAction(Request $request): array
    {
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('app.database_translator');
        try {
            /** @var User $user */
            $user = $this->getUser();
            $taskId = $request->get('task_id');
            /** @var Task $task */
            $task = $this->getDoctrine()
                ->getRepository(Task::class)
                ->findOneBy(['id'=>$taskId,'isDeleted'=>false]);

            if (!$task){
                throw new \Exception($translator->transDb('task_not_found'), Response::HTTP_NOT_FOUND);
            }
            $poster = $task->getUser();
            if(!$task->is(Task::STATUS_PUBLISHED) || $user->getId() === $poster->getId()){
                throw new \Exception($translator->transDb('offer.add.cant_add_offer'), Response::HTTP_UNAUTHORIZED);
            }
            $existOffer = $em->getRepository(Offer::class)->findOneBy(['task'=>$task,'user'=>$user]);
            if($existOffer){
                throw new \Exception($translator->transDb('offer.add.already_submit_offer'), Response::HTTP_CONFLICT);
            }
            $text = trim($request->get('text'));
            if (empty($text)){
                throw new \Exception($translator->transDb('offer.add.empty_text'), Response::HTTP_BAD_REQUEST);
            }
            $price = (float)$request->get('price');
            if(empty($price)){
                throw new \Exception($translator->transDb('offer.add.empty_price'), Response::HTTP_BAD_REQUEST);
            }
            $minBalance = $user->getCountry()->getConfig()->getMinAllowedMaatloobBalance();
            if($user->getWallet()->getBalance() < $minBalance){
                throw new \Exception($translator->transDb('offer.add.no_balance'),Response::HTTP_BAD_REQUEST);
            }
            /** Check task category require a verified documents */
            $category = $user->getCountry()->getCategory($task->getCategory());
            $requiredDocuments = $category?$category->getRequiredDocuments():[];
            $unverifiedDocuments = [];
            /** @var DocumentType $document */
            foreach ($requiredDocuments as $document){
                /** @var UserDocument $userDocument */
                $userDocument = $em->getRepository(UserDocument::class)->findOneBy(['type'=> $document,'user'=>$user]);
                if(!$userDocument->isVerified()){
                    $unverifiedDocuments[] = $document->translate()->getName();
                }
            }
            if(count($unverifiedDocuments)>0){
                throw new \Exception($translator->transDb('offer.add.required_document',['#documents#' => implode(' ',$unverifiedDocuments)]),Response::HTTP_BAD_REQUEST);
            }
            $offer = new Offer();
            $offer->setUser($user);
            $offer->setTask($task);
            $offer->setText($text);
            $offer->setPrice($price);
            $offer->setStatus(Offer::STATUS_PUBLISHED);
            $file = $request->files->get('file');
            if (is_object($file)) {
                $fileObject = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('picture_directory'), $file, $user
                );
                $em->persist($fileObject);
                $offer->addFile($fileObject);
            }
            $em->persist($offer);
            /** Create notification */
            $notification = new Notification();
            $notification->setTitle('notification.new_offer.title');
            $notification->setBody('notification.new_offer.body');
            $notification->setToUser($poster);
            $notification->setFromUser($offer->getUser());
            $notification->setExtra([
                'type' => Notification::TYPE_TASK,
                'task_id' => $offer->getTask()->getId(),
                'offer_id'=>$offer->getId()
            ]);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setParams([
                '#user#'=>$offer->getUser()->getName(),
                '#task#'=>$offer->getTask()->getTitle(),
                '#price#' => $offer->getPrice($poster->getCountry()),
                '#currency#' => $poster->getCountry()->getCurrencySymbol()
            ]);
            $em->persist($notification);
            $em->flush();
            $data = $offer;
            $msg = $translator->transDb('offer.add.success');
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }

        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/offer/accept" )
     * @param Request $request
     * @return array
     *
     * @SWG\Response(
     *     response=200,
     *     description="Offer successfully assigned",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Offer successfully assigned"),
     *      )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Offer not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=404),
     *         @SWG\Property(property="msg", type="string", example="Offer not found"),
     *      )
     * )
     *
     * @SWG\Response(
     *     response=401,
     *     description="You cant accept offer on this task",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=401),
     *         @SWG\Property(property="msg", type="string", example="You cant accept offer on this task"),
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Task assigned or offer cancelled of task date ended",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=400),
     *         @SWG\Property(property="msg", type="string", example="Invalid request"),
     *      )
     * )
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Request Body Params",
     *      @SWG\Schema(
     *         type="object",
     *          @SWG\Property(property="offre_id",type="string",example="4F8759AZ20",description="The id of the offer")
     *      )
     * )
     * @Security(name="Bearer")
     */
    public function acceptOfferAction(Request $request): array
    {
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('app.database_translator');
        try {
            /** @var User $user */
            $user = $this->getUser();
            $offerId = $request->get('offre_id');
            /** @var Offer $offer */
            $offer = $em->getRepository(Offer::class)->findOneBy(['id' => $offerId , 'isDeleted'=>false]);

            if (!$offer) {
                throw new \Exception($translator->transDb('offer.accept.not_found'), Response::HTTP_NOT_FOUND);
            }

            $task = $offer->getTask();
            $poster = $task->getUser();

            if ((int)$poster->getId() !== $user->getId()) {
                throw new \Exception($translator->transDb('invalid_request'), Response::HTTP_UNAUTHORIZED);
            }

            if((int)$offer->getStatus()!==Offer::STATUS_PUBLISHED) {
                throw new \Exception($translator->transDb('offer.accept.already_accepted'), Response::HTTP_BAD_REQUEST);
            }
            if($task->getPaymentType() === Task::PAYMENT_TYPE_WALLET){
                $userWallet = $user->getWallet();
                $priceDifference = $offer->getPrice($user->getCountry()) - $task->getPrice();
                if($priceDifference>0 && $userWallet->getAvailableBalance() < $priceDifference){
                    throw new \Exception($translator->transDb('offer.accept.not_enough_balance'),Response::HTTP_BAD_REQUEST);
                }
                /** update task locked price */
                $userWallet->unlockBalance($task->getPrice()-$task->getUsedCredit());
                $userWallet->unlockCredit($task->getUsedCredit());
                $usedCredit = $userWallet->lockTaskPrice($offer->getPrice($user->getCountry()));
                $task->setUsedCredit($usedCredit);
                $em->persist($userWallet);
            }
            /** Set status to accepted */
            $offer->setStatus(Offer::STATUS_ACCEPTED);
            $offer->setAcceptedAt(new \DateTime());
            /** @var Task $task */
            $task->setAcceptedOffer($offer);
            /** Update task price to equal accepted offer price */
            $task->setPrice($offer->getPrice($user->getCountry()));
            $task->setMaatloobTax($this->calculateMaatloobTax($task));
            $task->setCountryTax($this->calculateCountryTax($task));
            $em->persist($offer);
            $em->persist($task);
            $em->flush();

            $notification = new Notification();
            $notification->setTitle('notification.offer_accepted.title');
            $notification->setBody('notification.offer_accepted.body');
            $notification->setToUser($offer->getUser());
            $notification->setFromUser($user);
            $notification->setExtra([
                'type' => Notification::TYPE_TASK,
                'task_id' => $task->getId()
            ]);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setParams([
                '#user#'=>$user->getName(),
                '#task#'=>$task->getTitle(),
            ]);
            $em->persist($notification);

            /** Create discussion between the offer owner and task owner */
            $taskDiscussion = new TaskDiscussion();
            $taskDiscussion->setFrom($task->getUser());
            $taskDiscussion->setTo($offer->getUser());
            $taskDiscussion->setTask($task);
            $em->persist($taskDiscussion);
            $em->flush();
            $msg = $translator->transDb('offer.accept.success');


        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }
        return [
            'code' => $code,
            'msg' => $msg,
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"offer_reply_details","user_info","file_info"}
     * )
     * @Rest\Post("/task/offer/reply" )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Offer reply successfully created",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=201),
     *         @SWG\Property(property="msg", type="string", example="Reply was added"),
     *         @SWG\Property(property="data",type="object",ref=@Model(type=OfferReply::class,groups={"offer_reply_details","user_info","file_info"}))
     *      )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Offer not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=404),
     *         @SWG\Property(property="msg", type="string", example="Offer not found"),
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Invalid request",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=400),
     *         @SWG\Property(property="msg", type="string", example="Invalid request"),
     *      )
     * )
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Request Body Params",
     *     @Model(type=ApiBundle\Form\OfferReplyForm::class)
     * )
     * @Security(name="Bearer")
     * @param Request $request
     * @return array
     */
    public function addOfferReplyAction(Request $request): array
    {
        $translator = $this->get('app.database_translator');
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        try {
            /** @var User $user */
            $user = $this->getUser();
            $offerId = $request->get('offer_id');
            /** @var Offer $offer */
            $offer = $em->getRepository(Offer::class)
                ->findOneBy(['id'=>$offerId,'isDeleted'=>false]);

            if (!$offer){
                throw new \Exception($translator->transDb('offer_not_found'), Response::HTTP_NOT_FOUND);
            }
            $task = $offer->getTask();
            if (!$task->is(Task::STATUS_PUBLISHED)) {
                throw new \Exception($translator->transDb('offer.add.task_not_published'), Response::HTTP_UNAUTHORIZED);
            }
            if ($task->isTaskExpired()) {
                throw new \Exception($translator->transDb('offer.add.error_task_expired'), Response::HTTP_UNAUTHORIZED);
            }

            $text = trim($request->get('text'));
            if (empty($text)) {
                throw new \Exception($translator->transDb('offer.add.empty_text'), Response::HTTP_BAD_REQUEST);
            }
            $offerReply = new OfferReply();
            $offerReply->setUser($user);
            $offerReply->setOffer($offer);
            $offerReply->setText($text);
            $file = $request->files->get('file');
            if ($file) {
                $fileObject = $this->get('file_uploader')
                    ->uploadFile($this->getParameter('picture_directory'),$file, $user);
                $em->persist($fileObject);
                $offerReply->addFile($fileObject);
            }
            $em->persist($offerReply);
            $toUser = $offer->getUser()->getId() === $user->getId()?$task->getUser():$offer->getUser();
            $notification = new Notification();
            $notification->setTitle('notification.offer_reply.title');
            $notification->setBody('notification.offer_reply.body');
            $notification->setToUser($toUser);
            $notification->setFromUser($user);
            $notification->setExtra([
                'type' => Notification::TYPE_TASK,
                'task_id' => $offer->getTask()->getId(),
                'offer_id'=>$offerId
            ]);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setParams([
                '#user#'=>$user->getName(),
                '#msg#'=>$offerReply->getText(),
            ]);
            $em->persist($notification);
            $em->flush();
            $data = $offerReply;
            $msg = $translator->transDb('offer.reply.created');
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }

        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }


    /** Calculate tax for a tasker offer
     * @param Task $task
     * @return float|int
     * @throws \Exception
     */
    private function calculateMaatloobTax(Task $task){
        $tax = 0;
        $config = $task->getTasker()->getCountry()->getConfig();
        $freeTaxDayParam = $config->getEnableMaatloobProfitsAfterDays();
        $userRegisterDate = $task->getTasker()->getCreatedAt();
        $userRegisterDate = $userRegisterDate->modify('+' . $freeTaxDayParam . ' day');
        $price = $task->getPrice();
        if (new \DateTime() > $userRegisterDate) {
            $maatloobProfits = $config->getTaskProfits();
            foreach ($maatloobProfits as $maatloobProfit) {
                if ($price >= $maatloobProfit['min'] && $price<= $maatloobProfit['max']) {
                    $tax = $maatloobProfit['percent'];break;
                }
                if (0 === (int)$maatloobProfit['max'] && $price >= $maatloobProfit['min']) {
                    $tax = $maatloobProfit['percent'];break;
                }
            }
            $maxAllowedTax = $config->getMaxTaskProfits();
            $tax = $task->getPrice() * ($tax / 100);
            if ($maxAllowedTax < $tax) {
                $tax = $maxAllowedTax;
            }
        }
        return $tax;
    }

    /**
     * @param Task $task
     * @return float|int
     */
    public function calculateCountryTax(Task $task){
        return $task->getMaatloobTax() * ($task->getUser()->getCountry()->getConfig()->getCountryTax() / 100);
    }

    /**
     * @Rest\View(
     *     serializerGroups={"invoice_details","file_info"}
     * )
     * @Rest\Post("/task/offer/send-invoice")
     * @SWG\Response(
     *     response=200,
     *     description="Task invoice sent",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=201),
     *         @SWG\Property(property="msg", type="string", example="Invoice has been sent"),
     *         @SWG\Property(property="data",type="object",ref=@Model(type=TaskInvoice::class,groups={"invoice_details","file_info"}))
     *      )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Task not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=404),
     *         @SWG\Property(property="msg", type="string", example="Task not found")
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Can't do this operation",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=400),
     *         @SWG\Property(property="msg", type="string", example="Can't do this operation")
     *      )
     * )
     * @Security(name="Bearer")
     * @param Request $request
     * @return array
     */
    public function sendTaskInvoiceAction(Request $request): array
    {
        $em =$this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $dbTranslator = $this->get('app.database_translator');
        /** @var Task $task */
        $task = $em->getRepository(Task::class)
                    ->findOneBy([
                        'id'=>$request->get('task'),
                        'status'=>Task::STATUS_ASSIGNED,
                    ]);
        try{
            if (!$task) {
                throw new \Exception($dbTranslator->transDb('task_not_found'), Response::HTTP_NOT_FOUND);
            }
            if($task->getTasker()->getId() !== $user->getId()) {
                throw new \Exception($dbTranslator->transDb('invalid_request'), Response::HTTP_UNAUTHORIZED);
            }
            $files = $request->files->get('files',[]);
            if(count($files)===0) {
                throw new \Exception($dbTranslator->transDb('send_task_invoice.error.no_file'));
            }
            $price = $request->get('price',0);
            /** @var TaskInvoice $invoice */
            foreach ($task->getInvoices() as $invoice){
                if($invoice->getStatus()===TaskInvoice::STATUS_PENDING){
                    throw new \Exception($dbTranslator->transDb('send_task_invoice.error.already_uploaded'));
                }
            }
            $taskInvoice = new TaskInvoice();
            $taskInvoice->setPrice($price);
            foreach ($files as $file){
                $fileObject = $this->get('file_uploader')
                    ->uploadFile($this->getParameter('picture_directory'),$file, $user);
                $em->persist($fileObject);
                $taskInvoice->addFile($fileObject);
            }
            $taskInvoice->setUser($user);
            $task->addInvoice($taskInvoice);
            $em->persist($taskInvoice);
            $em->persist($task);
            $em->flush();
            $data = $taskInvoice;
            $msg = $dbTranslator->transDb('task.invoice.sent');
            $code = Response::HTTP_OK;
            $notification = new Notification();
            $notification->setTitle('notification.task_invoice_received.title');
            $notification->setBody('notification.task_invoice_received.body');
            $notification->setToUser($task->getUser());
            $notification->setFromUser($user);
            $notification->setExtra([
                'type' => Notification::TYPE_TASK,
                'task_id' => $task->getId()
            ]);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setParams([
                '#user#' => $user->getName(),
                '#task#' => $task->getTitle(),
            ]);
            $em->persist($notification);
            $em->flush();
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/invoice/cancel")
     * @param Request $request
     * @return array
     */
    public function cancelTaskInvoiceAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $taskInvoice = $request->get('invoice');
        /** @var TaskInvoice $taskInvoice */
        $taskInvoice = $em->getRepository(TaskInvoice::class)->find($taskInvoice);
        try {
            if(!$taskInvoice || $taskInvoice->getTask()->getTasker()->getId() !== $user->getId()){
                throw new Exception('task_invoice.cancel.error_not_found',Response::HTTP_NOT_FOUND);
            }
            if($taskInvoice->getStatus() === TaskInvoice::STATUS_PAYED){
                throw new Exception('task_invoice.cancel.error_already_paid',Response::HTTP_CONFLICT);
            }
            foreach ($taskInvoice->getFiles() as $file){
                $em->remove($file);
            }
            $em->flush();
            $em->remove($taskInvoice);
            $em->flush();
            $code = Response::HTTP_OK;
            $msg = 'task_invoice.cancel.success';
        }catch (Exception $e){
            $code = $e->getCode();
            $msg = $e->getMessage();
        }
        return [
            'code' => $code,
            'msg' => $this->get('app.database_translator')->transDb($msg)
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/offer/request-update-price")
     * @param Request $request
     * @return array
     */
    public function requestOfferUpdatePriceAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $dbTranslator = $this->get('app.database_translator');
        /** @var Task $task */
        $offer = $em->getRepository(Offer::class)
            ->findOneBy([
                'id' => $request->get('offer_id'),
                'user' => $user,
                'status' => Offer::STATUS_ACCEPTED
            ]);
        try {
            $newPrice = (float)$request->get('new_price');
            if(empty($newPrice)){
                throw new \Exception($dbTranslator->transDb('offer.add.empty_price'), Response::HTTP_BAD_REQUEST);
            }
            if (!$offer) {
                throw new \Exception($dbTranslator->transDb('offer_not_found'), Response::HTTP_NOT_FOUND);
            }
            if ($offer->getTask()->getStatus() !== Task::STATUS_ASSIGNED) {
                throw new \Exception($dbTranslator->transDb('invalid_request'), Response::HTTP_CONFLICT);
            }

            $OfferUpdateRequest = $em->getRepository(OfferUpdateRequest::class)
                ->findOneBy([
                    'status' => OfferUpdateRequest::STATUS_WAITING,
                    'offer' => $offer
                ]);

            if($OfferUpdateRequest) {
                throw new \Exception($dbTranslator->transDb('offer.update_price.already_requested'), Response::HTTP_CONFLICT);
            }

            $OfferUpdateRequest = new OfferUpdateRequest();
            $OfferUpdateRequest->setOffer($offer);
            $OfferUpdateRequest->setNewPrice($newPrice);
            $em->persist($OfferUpdateRequest);
            $notification = new Notification();
            $notification->setTitle('notification.offer_update_price.title');
            $notification->setBody('notification.offer_update_price.body');
            $notification->setToUser($offer->getTask()->getUser());
            $notification->setFromUser($user);
            $notification->setExtra([
                'type' => Notification::TYPE_TASK,
                'task_id' => $offer->getTask()->getId()
            ]);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setParams([
                '#user#' => $user->getName(),
                '#task#' => $offer->getTask()->getTitle(),
            ]);
            $em->persist($notification);
            $em->flush();
            $msg = $dbTranslator->transDb('offer.update_price.submitted');
            $code = Response::HTTP_OK;
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }
        return [
            'code' => $code,
            'msg' => $msg,
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/offer/treat-update-price")
     * @param Request $request
     * @return array
     */
    public function TreatOfferUpdatePriceRequestAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $dbTranslator = $this->get('app.database_translator');
        /** @var OfferUpdateRequest $OfferUpdateRequest */
        $OfferUpdateRequest = $em->getRepository(OfferUpdateRequest::class)
            ->findOneBy([
                'id' => $request->get('id'),
                'status' => OfferUpdateRequest::STATUS_WAITING
            ]);
        try {
            $action = $request->get('action');
            if (!$OfferUpdateRequest) {
                throw new \Exception($dbTranslator->transDb('offer_edit_request_not_found'), Response::HTTP_NOT_FOUND);
            }
            $offer = $OfferUpdateRequest->getOffer();
            $task = $offer->getTask();
            if ($task->getUser()->getId() !== $user->getId()) {
                throw new \Exception($dbTranslator->transDb('invalid_request'), Response::HTTP_UNAUTHORIZED);
            }
            if((int)$action===OfferUpdateRequest::STATUS_ACCEPTED){
                $userWallet = $user->getWallet();
                /** update task locked price */
                $userWallet->unlockBalance($task->getPrice()-$task->getUsedCredit());
                $userWallet->unlockCredit($task->getUsedCredit());
                $offer->setPrice($OfferUpdateRequest->getNewPrice());
                if($offer->getPrice($user->getCountry())>$userWallet->getAvailableBalance()){
                    throw new \Exception($dbTranslator->transDb('ask.add.no_enough_balance'), Response::HTTP_CONFLICT);
                }
                $task->setPrice($offer->getPrice($user->getCountry()));
                $usedCredit = $userWallet->lockTaskPrice($task->getPrice());
                $task->setUsedCredit($usedCredit);
                $em->persist($userWallet);
                $OfferUpdateRequest->setStatus(OfferUpdateRequest::STATUS_ACCEPTED);
                $em->persist($offer);
                $task->setMaatloobTax($this->calculateMaatloobTax($task));
                $task->setCountryTax($this->calculateCountryTax($task));
                $em->persist($task);
                $msg = $dbTranslator->transDb('offer.update_price.accepted');
                $notifyBody =  'notification.offer_update_price_accepted.body';
            }else{
                $msg = $dbTranslator->transDb('offer.update_price.refuse_success');
                $notifyBody =  'notification.offer_update_price_refused.body';
                $OfferUpdateRequest->setStatus(OfferUpdateRequest::STATUS_REFUSED);
            }
            $em->persist($OfferUpdateRequest);
            $notification = new Notification();
            $notification->setTitle('notification.offer_update_price.title');
            $notification->setBody($notifyBody);
            $notification->setToUser($offer->getUser());
            $notification->setFromUser($user);
            $notification->setExtra([
                'type' => Notification::TYPE_TASK,
                'task_id' => $offer->getTask()->getId()
            ]);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setParams([
                '#task#' => $offer->getTask()->getTitle(),
            ]);
            $em->persist($notification);
            $em->flush();
            $code = Response::HTTP_OK;
        } catch (\Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }
        return [
            'code' => $code,
            'msg' => $msg,
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Get("/task/offer/calculate-tax" )
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function calculateTaxAction(Request $request): array
    {

        $user = $this->getUser();
        $amount = $request->query->get('amount',0);
        $fakeTask = new Task();
        $fakeTask->setUser($user);
        $fakeTask->setPrice($amount);
        $fakeOffer = new Offer();
        $fakeOffer->setUser($user);
        $fakeTask->setAcceptedOffer($fakeOffer);
        $fakeTask->setMaatloobTax($this->calculateMaatloobTax($fakeTask));
        $fakeTask->setCountryTax($this->calculateCountryTax($fakeTask));
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'calculated offer tax',
            'data' => [
                'maatloobTax' => $fakeTask->getMaatloobTax(),
                'countryTax' => $fakeTask->getCountryTax(),
            ]
        ];
    }

}
