<?php

namespace ApiBundle\Controller;

use ApiBundle\Events\TaskEvent;
use ApiBundle\Form\AddTaskForm;
use ApiBundle\Form\UpdateTaskForm;
use ApiBundle\Services\DBTranslator;
use AppBundle\Entity\CancelTaskRequest;
use AppBundle\Entity\Country;
use AppBundle\Entity\File;
use AppBundle\Entity\Notification;
use AppBundle\Entity\Offer;
use AppBundle\Entity\OfferUpdateRequest;
use AppBundle\Entity\Referral;
use AppBundle\Entity\SystemConfig;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskCategory;
use AppBundle\Entity\TaskDiscussion;
use AppBundle\Entity\TaskInvoice;
use AppBundle\Entity\TaskMessage;
use AppBundle\Entity\TaskQuestion;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBan;
use AppBundle\Entity\UserReview;
use AppBundle\Services\AppService;
use Components\Helper;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Psr\Log\LogLevel;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Xendit\Cards;
use Xendit\Disbursements;
use Xendit\EWallets;
use Xendit\Xendit;

/**
 * Class TaskController
 * @package ApiBundle\Controller
 * @SWG\Tag(name="Task")
 */
class TaskController extends Controller
{

    /**
     * Task list API
     * @Rest\Get("/task/list", name="task_list", methods={"GET","POST"})
     * @Rest\View(
     *     serializerGroups={"task_list","user_info","file_info","task_address"}
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Return list of tasks",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Task list"),
     *         @SWG\Property(property="data",type="object",ref=@Model(type=Task::class, groups={"task_list","user_info","file_info","task_address"}))
     *      )
     * )
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Request Body Params",
     *      @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="filter", type="array",description="Filter task list",@SWG\Items(type="string")),
     *         @SWG\Property(property="distance", type="object",
     *                 @SWG\Property(property="max", type="integer",description="max distance with km"),
     *                 @SWG\Property(property="longitude", type="string"),
     *                 @SWG\Property(property="latitude", type="string"))
     *      )
     * )
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function indexAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        $filters = $request->get('filter') ?? [];
        $distance = $request->get('distance') ?? [];
        $taskListQuery = $em->getRepository(Task::class)->findTaskList($distance);
        $isOnlineWork = isset($filters['eg']['isOnlineWork']) && $filters['eg']['isOnlineWork'];
        if (!$isOnlineWork && !empty($distance) && count($distance) === 3) {
            $taskListQuery->join('t.address', 'a');
            $taskListQuery->addDistanceCheck($taskListQuery, $distance);
        }
        $user = $this->getUser();
        if ($user) {
            $country = $user->getCountry();
            $taskListQuery
                ->join('t.user', 'user')
                ->join('user.country', 'country')
                ->andWhere('user != :user')
                ->andWhere('country.identifier = :userCountry or (t.isOnlineWork = 1 and country.defaultLanguage in (:languages))')
                ->setParameter('user', $user)
                ->setParameter('languages', ['en', $country->getDefaultLanguage()->getIdentifier()])
                ->setParameter('userCountry', $country->getIdentifier());
        } else {
            $country = $request->headers->get('country', 'us');
            $country = $em->getRepository(Country::class)->find($country);
            $taskListQuery
                ->join('t.user', 'user')
                ->join('user.country', 'country')
                ->andWhere('country.identifier = :userCountry or (t.isOnlineWork = 1 and country.defaultLanguage in (:languages))')
                ->setParameter('languages', ['en', $country->getDefaultLanguage()->getIdentifier()])
                ->setParameter('userCountry', $country->getIdentifier());
        }

        /** Add filter to the task list query  */
        if (!empty($filters)) {
            $objectFilter = $this->get('api.object_filter');
            $taskListQuery = $objectFilter->getRequestFromFilter($taskListQuery, $filters, $country);
        }

        $taskListQuery
            ->andWhere('t.status = :published')
            ->setParameter('published', Task::STATUS_PUBLISHED)
            ->orderBy('t.publishedAt', 'desc');
        $paginator = $this->get('knp_paginator');
        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($taskListQuery->getQuery(), $request->get('page', 1), 15);
        $data['tasks'] = [];
        /** @var Task $item */
        foreach ($pagination->getItems() as $item) {
            $data['tasks'][] = $item->setPrice($item->getPrice($country));
        }
        $data['totalResults'] = $pagination->getTotalItemCount();
        $data['totalPages'] = $pagination->getPageCount();
        $data['resultsPerPage'] = $pagination->getItemNumberPerPage();
        $data['currentPage'] = $pagination->getCurrentPageNumber();
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'Task list',
            'data' => $data,
        ];
    }

    /**
     * User task list API
     * @Rest\Post("/user/tasks", name="user_tasks", methods={"POST"})
     * @Rest\View(
     *     serializerGroups={"task_list","user_info","file_info","task_address"}
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Return user list of tasks",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="User task list"),
     *         @SWG\Property(property="data",type="object",ref=@Model(type=Task::class, groups={"task_list","user_info","file_info","task_address"}))
     *      )
     * )
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Request Body Params",
     *      @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="filter", type="array",description="Filter task list with status and search title",
     *          @SWG\Items(type="array",@SWG\Items(type="string"))),
     * ))
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function userTasksAction(Request $request): array
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $userTasksQuery = $em->getRepository(Task::class)
            ->createQueryBuilder('t')
            ->join('t.user', 'user')
            ->join('user.country', 'country')
            ->leftJoin('t.offers', 'o')
            ->where('t.status != :cancelled')
            ->andWhere('t.isDeleted = false')
            ->orderBy('t.createdAt', 'DESC')
            ->setParameters(['user' => $user, 'cancelled' => Task::STATUS_CANCELLED]);

        $filters = $request->get('filter') ?? [];

        /** Check for role key */
        if (array_key_exists('role', $filters)) {
            if ($filters['role'] === 'poster') {
                $userTasksQuery->andWhere('user = :user');
            } else {
                $userTasksQuery->andWhere('o.user = :user and o.status = 2');
            }
            /** remove filter */
            unset($filters['role']);
        } else {
            $userTasksQuery->andWhere('user = :user or ( o.user = :user and o.status = 2)');
        }

        /** Add filter to the task list query  */
        if (!empty($filters)) {
            $objectFilter = $this->get('api.object_filter');
            $userTasksQuery = $objectFilter->getRequestFromFilter($userTasksQuery, $filters, $user->getCountry());
        }

        $userTasksQuery->orderBy('t.status')->addOrderBy('t.date', 'desc');

        $paginator = $this->get('knp_paginator');
        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($userTasksQuery->getQuery()
            ->getResult(), $request->get('page', 1), 15);

        /** Get all my task status */
        $status = $em->getRepository(Task::class)->createQueryBuilder('t')
            ->select('t.status', 'count(t) as total')
            ->leftJoin('t.acceptedOffer', 'o')
            ->where('t.user = :user or o.user = :user')
            ->setParameter('user', $user)
            ->groupBy('t.status')
            ->indexBy('t', 't.status')
            ->getQuery()->getResult();
        $data['status'] = [];
        foreach (range(Task::STATUS_DRAFT, Task::STATUS_COMPLETED) as $state) {
            $data['status'][$state] = isset($status[$state]) ? (int)$status[$state]['total'] : 0;
        }


        $data['tasks'] = [];
        /** @var Task $item */
        foreach ($pagination->getItems() as $item) {
            $taskPrice = $item->getPrice($user->getCountry());
            if ($item->getAcceptedOffer() && ($item->getTasker()->getId() === $user->getId())) {
                $taskPrice = $item->getAcceptedOffer()->getPrice();
            }
            $data['tasks'][] = $item->setPrice($taskPrice);
        }
        $data['totalResults'] = $pagination->getTotalItemCount();
        $data['totalPages'] = $pagination->getPageCount();
        $data['resultsPerPage'] = $pagination->getItemNumberPerPage();
        $data['currentPage'] = $pagination->getCurrentPageNumber();
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'Task list',
            'data' => $data,
        ];
    }


    /**
     * User delivery task list API
     * @Rest\Get("/user/delivery-tasks", name="user_delivery_tasks", methods={"GET"})
     * @Rest\View()
     * @param Request $request
     * @return array
     */
    public function userDeliveryTasksAction(Request $request): array
    {
        $user = $this->getUser();
        $tasks = $this->getDoctrine()->getRepository(Task::class)->createQueryBuilder('t')
            ->select('t.id')
            ->join('t.acceptedOffer', 'o')
            ->join('t.category', 'c')
            ->where('o.user = :user')
            ->andWhere('t.status = :assigned')
            ->andWhere('c.type = :delivery')
            ->andWhere('t.isDeleted = 0')
            ->setParameters([
                'user' => $user,
                'assigned' => Task::STATUS_ASSIGNED,
                'delivery' => TaskCategory::DELIVERY_TYPE
            ])->getQuery()->getResult();
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'delivery task list',
            'data' => array_map(static function ($item) {
                return $item['id'];
            }, $tasks),
        ];
    }

    /**
     * Task Details API
     * @Rest\Get("/task/details", name="task_details", methods={"GET"})
     * @Rest\View(
     *     serializerGroups={"task_details","offer_details","offer_reply_details",
     *     "user_info","file_info","task_address","invoice_details"}
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Return task details",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Task details"),
     *         @SWG\Property(property="data", type="object",ref=@Model(type=Task::class,
     *          groups={"task_details","user_info","file_info","task_address","offer_details","offer_reply_details"})),
     *      )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Task not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=404),
     *         @SWG\Property(property="msg", type="string", example="Task not found"),
     *      )
     * )
     * @SWG\Parameter(
     *     name="task_id",
     *     in="query",
     *     type="string",
     *     description="The task id",
     *     required=true
     * )
     * @SWG\Parameter(
     *     name="show_all",
     *     in="query",
     *     type="boolean",
     *     description="Show all offers",
     * )
     * @param Request $request
     * @return array
     */
    public function detailsAction(Request $request): array
    {
        $msg = 'Task details';
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $translator = $this->get('app.database_translator');
        if ($user) {
            $country = $user->getCountry();
        } else {
            $country = $request->headers->get('country', 'sa');
            $country = $em->getRepository(Country::class)->find($country);
        }
        try {
            $taskId = $request->get('task_id');
            /** @var Task $task */
            $task = $em->getRepository(Task::class)->find($taskId);
            if (!$task) {
                throw new Exception($translator->transDb('task_not_found'), 404);
            }
            $maxResult = false;
            if ($request->query->has('show_all')) {
                $maxResult = $request->get('show_all') === 'true' ? false : 3;
            }

            if ($user && $task->getStatus() === Task::STATUS_PAID) {
                $userReviewed = $em->getRepository(UserReview::class)
                    ->findOneBy(['createdBy' => $this->getUser(), 'task' => $task]);
                if ($userReviewed) {
                    $task->setStatus(Task::STATUS_COMPLETED);
                }
            }
            $questions = $this->getDoctrine()->getRepository(TaskQuestion::class)->getByTask($task, $maxResult);
            $task->setQuestions(new ArrayCollection($questions));
            $offers = $this->getDoctrine()->getRepository(Offer::class)->getByTask($task, $maxResult);
            $task->setOffers(new ArrayCollection($offers));
            /** @var Offer $offer */
            foreach ($offers as $offer) {
                /** Hide offer price from user other than tasker or poster */
                if (!$user || (!in_array($user->getId(), [$task->getUser()->getId(), $offer->getUser()->getId()], true))) {
                    $offer->setPrice(0);
                    /** Check if task is assigned to a tasker from another country to convert offer price to locale country price */
                } elseif ($user->getId() === $task->getUser()->getId() &&
                    $offer->getUser()->getCountry()->getIdentifier() !== $user->getCountry()->getIdentifier()) {
                    if ($task->getAcceptedOffer() && $offer->getId() === $task->getAcceptedOffer()->getId()) {
                        $offer->setPrice($task->getPrice());
                    } else {
                        $offer->setPrice($offer->getPrice($user->getCountry()));
                    }
                }
            }
            /** Hide invoice details and accepted offer for other users than poster & tasker */
            if ($task->getAcceptedOffer() && (!$user ||
                    ($user && !in_array($user->getId(), [$task->getUser()->getId(), $task->getAcceptedOffer()->getUser()->getId()], true)))) {
                $task->setInvoices(null);
            }


            if ($user && $task->getAcceptedOffer() && $user->getId() === $task->getTasker()->getId()) {
                $task->setPrice($task->getAcceptedOffer()->getPrice());
            } else {
                $task->setPrice($task->getPrice($country));
            }


            /** Check if there is a request offer update */
            if ($user && $task->is(Task::STATUS_ASSIGNED) && ($task->getUser()->getId() === $user->getId() ||
                    $task->getTasker()->getId() === $user->getId())) {
                $acceptedOffer = $task->getAcceptedOffer();
                /** @var OfferUpdateRequest $offerRequestUpdate */
                $offerRequestUpdate = $em->getRepository(OfferUpdateRequest::class)->findOneBy([
                    'status' => OfferUpdateRequest::STATUS_WAITING,
                    'offer' => $acceptedOffer
                ]);
                if ($offerRequestUpdate) {
                    $convertedNewPrice = (float)$this->get('app.currency.converter')->convertMoney($offerRequestUpdate->getNewPrice(),
                        $offerRequestUpdate->getFromUser()->getCountry(), $user->getCountry());
                    $offerRequestUpdate->setNewPrice($convertedNewPrice);
                    $task->setOfferRequest($offerRequestUpdate);
                }
            }
            $data = $task;
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }
        return [
            'code' => $code,
            'msg' => $this->get('translator')->trans($msg),
            'data' => $data
        ];
    }

    /**
     * Add Task API
     * @Rest\Post("/task/add", name="task_add", methods={"POST"})
     * @Rest\View(
     *     serializerGroups={"task_list","user_info","file_info","task_address"}
     * )
     * @SWG\Response(
     *     response=201,
     *     description="Add task",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Task Created"),
     *         @SWG\Property(property="data", type="object", ref=@Model(type=Task::class, groups={"task_list","user_info","file_info","task_address"}))
     *      )
     * )
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Request Body Params",
     *     @Model(type=ApiBundle\Form\AddTaskForm::class)
     * )
     * @Security(name="Bearer")
     * @return array
     */
    public function addAction(): array
    {
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('app.database_translator');
        /** @var User $user */
        $user = $this->getUser();
        if (!$user->isVerified()) {
            $userTasks = $em->getRepository(Task::class)->findBy(['user' => $user]);
            $configMaxAllowedTask = $user->getCountry()->getConfig()->getAllowedTaskOfferBeforeVerification();
            if (count($userTasks) >= $configMaxAllowedTask) {
                return [
                    'code' => Response::HTTP_UNAUTHORIZED,
                    'msg' => $translator->transDb('task.add.exceeded_allowed_number')
                ];
            }
        }
        $categories = $user->getCountry()->getCategories(true);
        $task = new Task();
        $form = $this->createForm(AddTaskForm::class, $task, [
            'categories' => $categories,
            'entity_manager' => $em,
            'user' => $user,
            'translator' => $translator
        ]);
        $data = $this->get('json.request')->getParameters();
        $form->submit($data);
        if ($form->isValid()) {
            $task->setUser($user);
            $em->persist($task);
            $em->flush();
            return [
                'code' => Response::HTTP_OK,
                'msg' => 'Task has been created',
                'data' => $task
            ];
        }

        return [
            'code' => Response::HTTP_BAD_REQUEST,
            'msg' => Helper::getErrorsFromForm($form, false)[0]
        ];
    }

    /**
     * Update Task API
     * @Rest\Post("/task/update", name="task_update", methods={"POST"})
     * @Rest\View(
     *     serializerGroups={"task_list","user_info","file_info","task_address"}
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Update task",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Task updated"),
     *         @SWG\Property(property="data",type="object",
     *            @SWG\Property(property="token", type="string", example=""),
     *            @SWG\Property(property="user", type="object", ref=@Model(type=Task::class, groups={"task_list","user_info","file_info","task_address"})))
     *      )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Invalid request input",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=400),
     *         @SWG\Property(property="msg", type="string", example="Title is required"),
     *      )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Task not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=404),
     *         @SWG\Property(property="msg", type="string", example="Task not found"),
     *      )
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Request Body Params",
     *     @Model(type=ApiBundle\Form\UpdateTaskForm::class)
     * )
     * @SWG\Parameter(
     *     name="files[0]",
     *     in="formData",
     *     description="Image or video or text linked with the task",
     *     type="file",
     * )
     * @SWG\Parameter(
     *     name="files[1]",
     *     in="formData",
     *     description="Image or video or text linked with the task",
     *     type="file",
     *     required=false
     * )
     * @Security(name="Bearer")
     * @param Request $request
     * @return array
     */
    public function updateAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('app.database_translator');
        /** @var User $user */
        $user = $this->getUser();
        /** @var Task $task */
        $task = $em->getRepository(Task::class)
            ->findOneBy(['id' => $request->get('task_id'), 'user' => $user]);
        if (!$task) {
            return [
                'code' => Response::HTTP_NOT_FOUND,
                'msg' => $translator->transDb('invalid_request')
            ];
        }
        if ($task->getStatus() > Task::STATUS_ASSIGNED) {
            return [
                'code' => Response::HTTP_NOT_ACCEPTABLE,
                'msg' => $translator->transDb('invalid_request')
            ];
        }
        $categories = $user->getCountry()->getCategories(true);
        $form = $this->createForm(UpdateTaskForm::class, $task, [
            'categories' => $categories,
            'entity_manager' => $em,
            'user' => $user,
            'translator' => $translator
        ]);
        $data = $this->get('json.request')->getParameters();
        $oldTaskPrice = $task->getPrice();
        $form->submit($data, false);
        if ($form->isValid()) {
            $code = Response::HTTP_OK;
            $msg = $translator->transDb('task.add.updated');
            /** Validate task due date */
            if (isset($data['date']) && $task->getDate() <= new DateTime()) {
                return [
                    'code' => Response::HTTP_BAD_REQUEST,
                    'msg' => $translator->transDb('task.add.wrong_date'),
                    'data' => null
                ];
            }
            /** Validate task payment method */
           if(isset($data['payment_type']) && $data['payment_type'] === Task::PAYMENT_TYPE_CASH && $task->getIsOnlineWork()){
               return [
                   'code' => Response::HTTP_BAD_REQUEST,
                   'msg' => $translator->transDb('task.add.invalid_payment_method'),
                   'data' => null
               ];
           }

            $minTaskBudget = $user->getCountry()->getConfig()->getMinAcceptedTaskBudget();
            /** Validate task minimum allowed price */
            if (isset($data['price']) && !empty($data['price'])) {
                if ($task->getPrice() < $user->getCountry()->getConfig()->getMinAcceptedTaskBudget()) {
                    return [
                        'code' => Response::HTTP_FORBIDDEN,
                        'msg' => $translator->transDb('task.add.min_task_budget', ['#price#' => $minTaskBudget . $user->getCountry()->getCurrencySymbol()]),
                        'data' => null
                    ];
                }
                if ($task->isInvoicePayed()) {
                    return [
                        'code' => Response::HTTP_FORBIDDEN,
                        'msg' => $translator->transDb('task.add.task_invoice_payed'),
                        'data' => null
                    ];
                }
                if(!$task->getDate()){
                    return [
                        'code' => Response::HTTP_FORBIDDEN,
                        'msg' => $translator->transDb('task.add.due_date_required'),
                        'data' => null
                    ];
                }
                /** Calculate total task price for multiple taskers */
                $task->setPrice($task->getPrice() * $task->getTasksCount());
                if ($task->is(Task::STATUS_DRAFT) && $task->getPrice() > 0 && $task->getPaymentType() !== Task::PAYMENT_TYPE_WALLET) {
                    $task->setStatus(Task::STATUS_PUBLISHED);
                    $dispatcher = $this->get('event_dispatcher');
                    $event = new TaskEvent($task);
                    $dispatcher->dispatch(TaskEvent::TASK_PUBLISHED, $event);
                    $msg = $translator->transDb('task.add.published');
                }
                $userWallet = $user->getWallet();
                /** Check if the user set or updated the task price for payment type wallet*/
                if ($task->getPaymentType() === Task::PAYMENT_TYPE_WALLET && $task->getPrice() > 0 &&
                    ($oldTaskPrice !== $task->getPrice() || $task->is(Task::STATUS_DRAFT))) {
                    /** Check if task not published yet mean the user set task price for the first time */
                    if ($task->is(Task::STATUS_DRAFT)) {
                        if ($user->getWallet()->getAvailableBalance() > $task->getPrice()) {
                            $usedCredit = $userWallet->lockTaskPrice($task->getPrice());
                            $task->setUsedCredit($usedCredit);
                            $em->persist($userWallet);
                            $em->flush();
                            $task->setStatus(Task::STATUS_PUBLISHED);
                            $dispatcher = $this->get('event_dispatcher');
                            $event = new TaskEvent($task);
                            $dispatcher->dispatch(TaskEvent::TASK_PUBLISHED, $event);
                            $msg = $translator->transDb('task.add.published');
                        } else {
                            $task->setPrice($oldTaskPrice);
                            $task->setNotPublishedReason(Task::NOT_PUBLISHED_NO_BALANCE);
                            $msg = $translator->transDb('task.add.no_enough_balance');
                            $code = Response::HTTP_BAD_REQUEST;
                        }
                    } elseif ($task->is(Task::STATUS_PUBLISHED)) {
                        if ($task->getPrice() > $userWallet->getAvailableBalance() + $oldTaskPrice) {
                            $task->setPrice($oldTaskPrice);
                            $msg = $translator->transDb('task.add.no_enough_balance');
                            $code = Response::HTTP_BAD_REQUEST;
                        } else {
                            /** update task locked price */
                            $userWallet->unlockBalance($oldTaskPrice - $task->getUsedCredit());
                            $userWallet->unlockCredit($task->getUsedCredit());
                            $usedCredit = $userWallet->lockTaskPrice($task->getPrice());
                            $task->setUsedCredit($usedCredit);
                            $em->persist($userWallet);
                            $em->flush();
                        }
                    }
                }
            }
            /** Add task files*/
            $files = $request->files->get('files');
            if (is_array($files)) {
                foreach ($files as $file) {
                    $fileObject = $this->get('file_uploader')->uploadFile(
                        $this->getParameter('tasks_directory'), $file, $user
                    );
                    $fileObject->setUser($user);
                    $em->persist($fileObject);
                    $task->addFile($fileObject);
                }
            }
            /** If task is already assigned then notify to tasker */
            if ($task->is(Task::STATUS_ASSIGNED)) {
                $this->notifyTaskerForTaskUpdate($task);
            }

            $em->persist($task);
            $em->flush();

            return [
                'code' => $code,
                'msg' => $msg,
                'data' => $task
            ];
        }
        return [
            'code' => Response::HTTP_BAD_REQUEST,
            'msg' => Helper::getErrorsFromForm($form, false)[0],
            'data' => null
        ];
    }

    /**
     * @param Task $task
     */
    public function notifyTaskerForTaskUpdate(Task $task): void
    {
        $em = $this->getDoctrine()->getManager();
        $tasker = $task->getAcceptedOffer()->getUser();
        $notification = new Notification();
        $notification->setTitle('notification.task_update.title');
        $notification->setBody('notification.task_update.body');
        $notification->setToUser($tasker);
        $notification->setFromUser($task->getUser());
        $notification->setExtra([
            'type' => Notification::TYPE_TASK,
            'task_id' => $task->getId()
        ]);
        $notification->setType(Notification::ACTION_TASK_UPDATE);
        $em->persist($notification);
        $em->flush();
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/delete")
     * @param Request $request
     * @return array
     */
    public function deleteAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('app.database_translator');
        $user = $this->getUser();
        $taskId = $request->get('task_id');
        /** @var Task $task */
        $task = $em->getRepository(Task::class)
            ->findOneBy(['id' => $taskId, 'user' => $user, 'status' => Task::STATUS_DRAFT]);
        if (!$task) {
            return [
                'code' => Response::HTTP_NOT_FOUND,
                'msg' => $translator->transDb('task_not_found'),
            ];
        }
        $task->setIsDeleted(true);
        $em->persist($task);
        $em->flush();
        return [
            'code' => Response::HTTP_OK,
            'msg' => $translator->transDb('task.delete.success'),
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/file/delete")
     * @param Request $request
     * @return array
     */
    public function deleteFileAction(Request $request): array
    {
        $translator = $this->get('app.database_translator');
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $fileId = $request->get('file_id');
        /** @var File $file */
        $file = $em->getRepository(File::class)
            ->find($fileId);

        if (!$file) {
            return [
                'code' => Response::HTTP_NOT_FOUND,
                'msg' => $translator->transDb('file.delete.not_found')
            ];
        }
        if ($file->getUser()->getId() !== $user->getId()) {
            return [
                'code' => Response::HTTP_UNAUTHORIZED,
                'msg' => $translator->transDb('invalid_request')
            ];
        }
        $em->remove($file);
        $em->flush();
        return [
            'code' => Response::HTTP_OK,
            'msg' => $translator->transDb('file.delete.success'),
        ];
    }


    /**
     * @Rest\View(
     *     serializerGroups={"task_message","task_message_discussion","user_info","file_info"}
     * )
     * @Rest\Post("/task/message")
     * @param Request $request
     * @return array
     */
    public function getMessageAction(Request $request): array
    {
        $msg = 'Task messages';
        $code = Response::HTTP_OK;
        $data = null;
        $translator = $this->get('app.database_translator');
        try {
            $user = $this->getUser();
            $taskId = (int)$request->get('task_id');
            $task = $this->getDoctrine()
                ->getRepository(Task::class)
                ->find($taskId);
            if (!$task) {
                throw new Exception($translator->transDb('invalid_request'), Response::HTTP_BAD_REQUEST);
            }

            $taskDiscussion = $this->getDoctrine()
                ->getRepository(TaskDiscussion::class)
                ->findOneByTaskAndUser($task, $user);

            if ($taskDiscussion) {
                $taskMessages = $this->getDoctrine()
                    ->getRepository(TaskMessage::class)
                    ->findBy(array('discussion' => $taskDiscussion));
                $data = $taskMessages;
            }

        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }

        return [
            'code' => $code,
            'msg' => $this->get('translator')->trans($msg),
            'data' => $data
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/request_payment")
     * @param Request $request
     * @return array
     */
    public function requestPaymentAction(Request $request): array
    {
        $code = Response::HTTP_OK;
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('app.database_translator');
        try {
            $user = $this->getUser();
            $taskId = (int)$request->get('task');
            /** @var Offer $offer */
            $offer = $em->getRepository(Offer::class)->findOneBy(['user' => $user, 'task' => $taskId, 'status' => Offer::STATUS_ACCEPTED]);
            if (!$offer || $offer->getTask()->getStatus() !== Task::STATUS_ASSIGNED) {
                throw new Exception($translator->transDb('invalid_request'), Response::HTTP_BAD_REQUEST);
            }
            /** @var Task $task */
            $task = $offer->getTask();
            if ($task->getPaymentType() === Task::PAYMENT_TYPE_WALLET) {
                $notifBody = 'notification.payment_request.body';
            } elseif ($task->getPaymentType() === Task::PAYMENT_TYPE_CARD) {
                /** @var TaskInvoice $invoice */
                foreach ($task->getInvoices() as $invoice) {
                    if ($invoice->getPrice() > 0 && $invoice->getStatus() !== TaskInvoice::STATUS_PAYED) {
                        throw new Exception($translator->transDb('unpaid_invoice'), Response::HTTP_BAD_REQUEST);
                    }
                }
                $notifBody = 'notification.payment_request.body_card';
            }
            $task->setStatus(Task::STATUS_WAITING_PAYMENT);
            $em->persist($task);
            $notification = new Notification();
            $notification->setTitle('notification.payment_request.title');
            $notification->setBody($notifBody);
            $notification->setToUser($task->getUser());
            $notification->setFromUser($user);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setExtra([
                'type' => Notification::TYPE_TASK,
                'task_id' => $task->getId()
            ]);
            $notification->setParams([
                '#user#' => $user->getName(),
                '#task#' => $task->getTitle(),
                '#price#' => $task->getPrice() . $task->getUser()->getCountry()->getCurrencySymbol()
            ]);
            $em->persist($notification);
            $em->flush();
            $data = [
                'id' => $task->getId(),
                'status' => $task->getStatus()
            ];
            $msg = $translator->transDb('task.request_payment.success');

        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }

        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/confirm-cash-payment")
     * @param Request $request
     * @return array
     */
    public function confirmCashPaymentAction(Request $request): array
    {
        $code = Response::HTTP_OK;
        $translator = $this->get('app.database_translator');
        $em = $this->getDoctrine()->getManager();
        try {
            /** @var User $user */
            $user = $this->getUser();
            $taskId = (int)$request->get('task');
            /** @var Task $task */
            $task = $this->getDoctrine()
                ->getRepository(Task::class)
                ->findOneBy([
                    'id' => $taskId,
                    'paymentType' => Task::PAYMENT_TYPE_CASH,
                    'status' => Task::STATUS_ASSIGNED
                ]);
            if (!$task) {
                throw new \Exception($translator->transDb('invalid_request'), Response::HTTP_BAD_REQUEST);
            }
            if ($task->getTasker()->getId() !== $user->getId()) {
                throw new Exception($translator->transDb('invalid_request'), Response::HTTP_BAD_REQUEST);
            }
            $task->setStatus(Task::STATUS_PAID);
            $this->completePayment($task);
            $em->persist($task);
            /** Send notification message to tasker */
            $this->sendNotificationPaymentCompleted($task);
            $em->flush();
            $this->checkReferralBonus($task);
            $data = ['id' => $taskId, 'status' => Task::STATUS_PAID];
            $msg = $translator->transDb('task.release_payment_cash.success');
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/release_payment")
     * @param Request $request
     * @return array
     */
    public function releasePaymentAction(Request $request): array
    {
        $code = Response::HTTP_OK;
        $translator = $this->get('app.database_translator');
        $em = $this->getDoctrine()->getManager();
        try {
            /** @var User $user */
            $user = $this->getUser();
            $taskId = (int)$request->get('task');
            /** @var Task $task */
            $task = $this->getDoctrine()
                ->getRepository(Task::class)
                ->findOneBy([
                    'id' => $taskId,
                    'user' => $user,
                    'status' => Task::STATUS_WAITING_PAYMENT
                ]);
            if (!$task || $task->getPaymentType() === Task::PAYMENT_TYPE_CASH) {
                throw new Exception($translator->transDb('invalid_request'), Response::HTTP_BAD_REQUEST);
            }

            $this->releasePayment($task);
            $this->completePayment($task);

            /** Check if poster rewarded tasker with extras tip */
            if (($tip = (float)$request->get('tip', 0)) && $tip > 0 && $user->getWallet()->getBalance() > $tip) {
                if ($tip > $task->getPrice() / 2) {
                    $tip = $task->getPrice() / 2;
                }
                $this->createTipTransaction($task, $tip);
            }
            /** Send notification message to tasker */
            $this->sendNotificationPaymentCompleted($task);

            $task->setStatus(Task::STATUS_PAID);
            $em->persist($task);
            $em->flush();
            $this->checkReferralBonus($task);
            $data = ['id' => $taskId, 'status' => Task::STATUS_PAID];
            $msg = $translator->transDb('task.release_payment.success');
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }

    /**
     * @param Task $task
     */
    private function completePayment(Task $task): void
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Transaction $transaction */
        $transaction = $em->getRepository(Transaction::class)->findOneBy(['task' => $task, 'type' => Transaction::TYPE_TASK_COMPLETE]);
        if(!$transaction){
            /** Create Transaction between poster and tasker */
            $transaction = new Transaction();
            $transaction->setPaymentType($task->getPaymentType());
            $transaction->setFromUser($task->getUser());
            $transaction->setToUser($task->getAcceptedOffer()->getUser());
            $transaction->setTask($task);
            $transaction->setFromPrice($task->getPrice());
            $transaction->setPrice($task->getAcceptedOffer()->getPrice());
        }
        $transaction->setType(Transaction::TYPE_TASK_COMPLETE);
        $em->persist($transaction);
        $em->flush();
        /** Subtract maatloob profits from the price */
        $this->maatloobTaxTransaction($task);
        /** Subtract country tax from */
        $this->countryTaxTransaction($task);
    }

    /**
     * @param Task $task
     */
    private function checkReferralBonus(Task $task): void
    {
        $config = $task->getUser()->getCountry()->getConfig();
        $em = $this->getDoctrine()->getManager();
        /** @var Referral $referral */
        $referral = $em->getRepository(Referral::class)->findOneBy(['forUser' => $task->getUser(), 'completed' => false]);
        if ($referral) {
            $this->createReferralTransaction($referral->getForUser(), $referral->getFromUser(), $config->getReferralCredit(), 'referral');
            $this->createReferralTransaction($referral->getFromUser(), $referral->getForUser(), $config->getReferringCredit(), 'referring');
            $referral->setCompleted(true);
            $em->persist($referral);
            $em->flush();
        }
    }

    /**
     * @param User $forUser
     * @param User $fromUser
     * @param $credit
     * @param $type
     */
    private function createReferralTransaction(User $forUser, User $fromUser, $credit, $type): void
    {
        $em = $this->getDoctrine()->getManager();
        $forUser->getWallet()->addCredit($credit);
        $em->persist($forUser->getWallet());
        $transaction = new Transaction();
        $transaction->setPaymentType(Task::PAYMENT_TYPE_WALLET);
        $transaction->setPrice($credit);
        $transaction->setToUser($forUser);
        $transaction->setType(Transaction::TYPE_REFERRAL);
        $em->persist($transaction);
        /** Send notification  */
        $notification = new Notification();
        $notification->setType(Notification::ACTION_TRANSACTION);
        $notification->setTitle('notification.referral.title');
        $notification->setBody("notification.${$type}.body");
        $notification->setToUser($forUser);
        $notification->setParams([
            '#price#' => $credit . $forUser->getCountry()->getCurrencySymbol(),
            '#user#' => $fromUser->getName()
        ]);
        $notification->setExtra([
            'type' => Notification::TYPE_TRANS,
            'direction' => 'INCOMING'
        ]);
        $em->persist($notification);
        $em->flush();
    }

    /**
     * @param Task $task
     * @param int $tip
     */
    private function createTipTransaction(Task $task, int $tip): void
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $poster */
        $poster = $task->getUser();
        $tasker = $task->getAcceptedOffer()->getUser();

        $posterWallet = $poster->getWallet()->withdraw($tip);
        $convertedCurrencyTip = (float)$this->get('app.currency.converter')->convertMoney($tip, $poster->getCountry(), $tasker->getCountry());
        $taskerWallet = $tasker->getWallet()->deposit($convertedCurrencyTip);

        $em->persist($posterWallet);
        $em->persist($taskerWallet);

        /** Create Transaction between poster and tasker */
        $transaction = new Transaction();
        $transaction->setType(Transaction::TYPE_TASK_TIP);
        $transaction->setPaymentType(Task::PAYMENT_TYPE_WALLET);
        $transaction->setFromUser($poster);
        $transaction->setToUser($tasker);
        $transaction->setTask($task);
        $transaction->setFromPrice($tip);
        $transaction->setPrice($convertedCurrencyTip);
        $em->persist($transaction);
        $em->flush();
    }

    /**
     * @param Task $task
     */
    private function releasePayment(Task $task): void
    {
        $em = $this->getDoctrine()->getManager();
        $offer = $task->getAcceptedOffer();
        $poster = $task->getUser();
        $tasker = $task->getTasker();
        $posterWallet = $poster->getWallet();
        $posterWallet->setLockedBalance($posterWallet->getLockedBalance() - ($task->getPrice() - $task->getUsedCredit()));
        $posterWallet->setLockedCredit($posterWallet->getLockedCredit() - $task->getUsedCredit());
        $taskerWallet = $tasker->getWallet();
        /** release invoice price to tasker */
        $transactions = $em->getRepository(Transaction::class)->findBy([
            'type' => Transaction::TYPE_TASK_INVOICE,
            'status'=> Transaction::STATUS_CAPTURED,
            'task'=> $task
        ]);
        /** @var Transaction $transaction */
        foreach ($transactions as $transaction){
            $transaction->setStatus(Transaction::STATUS_COMPLETED);
            $posterWallet->setLockedBalance($posterWallet->getLockedBalance() - $transaction->getPrice());
            $taskerWallet->deposit($transaction->getPrice());
        }
        $offer->setPrice($task->getPrice($tasker->getCountry()));
        $taskerWallet->deposit($offer->getPrice());
        $em->persist($offer);
        $em->persist($taskerWallet);
        $em->persist($posterWallet);
    }

    /**
     * Extract maatloob profits from completed task price
     * @param Task $task
     */
    private function maatloobTaxTransaction(Task $task): void
    {
        $em = $this->getDoctrine()->getManager();
        if ($task->getMaatloobTax() > 0) {
            /** Create transaction between tasker and maatloob money system */
            $transaction = new Transaction();
            $transaction->setType(Transaction::TYPE_FEES);
            $transaction->setPaymentType(Task::PAYMENT_TYPE_WALLET);
            $transaction->setPrice($task->getMaatloobTax($task->getTasker()->getCountry()));
            $transaction->setTask($task);
            $transaction->setFromUser($task->getTasker());
            $em->persist($transaction);
            $userWallet = $task->getTasker()->getWallet();
            $userWallet->withdraw($transaction->getPrice());
            $em->persist($userWallet);
            $em->flush();
        }
    }

    /**
     * Extract country tax from completed task price
     * @param Task $task
     */
    public function countryTaxTransaction(Task $task): void
    {
        if ($task->getCountryTax() > 0) {
            $em = $this->getDoctrine()->getManager();
            /** Create transaction between tasker and maatloob money system */
            $transaction = new Transaction();
            $transaction->setPaymentType(Task::PAYMENT_TYPE_WALLET);
            $transaction->setType(Transaction::TYPE_COUNTRY_TAX);
            $transaction->setPrice($task->getCountryTax($task->getTasker()->getCountry()));
            $transaction->setTask($task);
            $transaction->setFromUser($task->getTasker());
            $em->persist($transaction);
            $userWallet = $task->getTasker()->getWallet();
            $userWallet->withdraw($transaction->getPrice());
            $em->persist($userWallet);
            $em->flush();
        }
    }

    /**
     * Send notification for both tasker and poster after payment release
     * @param Task $task
     */
    public function sendNotificationPaymentCompleted(Task $task): void
    {
        $country = $task->getTasker()->getCountry();
        $isCash = $task->getPaymentType() === Task::PAYMENT_TYPE_CASH ? '_cash' : '';

        $em = $this->getDoctrine()->getManager();

        /** Create notification for poster */
        $notificationForPoster = new Notification();
        $notificationForPoster->setTitle("notification.payment_released_poster{$isCash}.title");
        $notificationForPoster->setBody("notification.payment_released_poster{$isCash}.body");
        $notificationForPoster->setToUser($task->getUser());
        $notificationForPoster->setType(Notification::ACTION_TRANSACTION);
        $notificationForPoster->setExtra([
            'type' => Notification::TYPE_TRANS,
            'direction' => 'OUTGOING'
        ]);
        $notificationForPoster->setParams([
            '#task#' => $task->getTitle(),
            '#user#' => $task->getTasker()->getName(),
        ]);
        $em->persist($notificationForPoster);


        /** Create notification for tasker */
        $withTax = $task->getMaatloobTax() > 0 ? '_with_tax' : '';
        $notificationForTasker = new Notification();
        $notificationForTasker->setTitle('notification.payment_released_tasker.title');
        $notificationForTasker->setBody("notification.payment_released_tasker{$isCash}$withTax.body");
        $notificationForTasker->setToUser($task->getTasker());
        $notificationForTasker->setExtra([
            'type' => Notification::TYPE_TRANS,
            'direction' => 'INCOMING'
        ]);
        $notificationForTasker->setType(Notification::ACTION_TRANSACTION);
        $notificationForTasker->setParams([
            '#task#' => $task->getTitle(),
            '#price#' => $task->getAcceptedOffer()->getPrice() . ' ' . $country->getCurrencySymbol(),
            '#user#' => $task->getUser()->getName(),
            '#tax#' => $task->getCountryTax($country) + $task->getMaatloobTax($country) . ' ' . $country->getCurrencySymbol()
        ]);
        $em->persist($notificationForTasker);
        $em->flush();
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/update-payment-method" )
     * @SWG\Response(
     *     response=200,
     *     description="Payment method updated",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Payment method has been updated"),
     *      )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Task not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=404),
     *         @SWG\Property(property="msg", type="string", example="Task not found")
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Can't do this operation",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=400),
     *         @SWG\Property(property="msg", type="string", example="Can't do this operation")
     *      )
     * )
     *
     * @Security(name="Bearer")
     * @param Request $request
     * @return array
     */
    public function changePaymentTypeAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $dbTranslator = $this->get('app.database_translator');
        /** @var Task $task */
        $task = $em->getRepository(Task::class)
            ->findOneBy(['id' => $request->get('task_id'), 'user' => $user]);
        $changeTo = (int) $request->get('Type');
        try {
            if (!$task) {
                throw new Exception($dbTranslator->transDb('task_not_found'), Response::HTTP_NOT_FOUND);
            }
            if (!$changeTo ||  !in_array($changeTo, [Task::PAYMENT_TYPE_WALLET, Task::PAYMENT_TYPE_CASH, Task::PAYMENT_TYPE_CARD], true) ||
                ($task->getPaymentType() === $changeTo) || $task->getIsOnlineWork() || !in_array($task->getStatus(), [Task::STATUS_ASSIGNED, Task::STATUS_PUBLISHED], true)||
                ($changeTo === Task::PAYMENT_TYPE_CARD && !$task->getCategory()->isDelivery())) {
                throw new Exception($dbTranslator->transDb('invalid_request'), Response::HTTP_CONFLICT);
            }

            if ($task->isPaymentTypeUpdated()) {
                throw new Exception($dbTranslator->transDb('task.update_payment_wallet.already_changed'), Response::HTTP_CONFLICT);
            }

            $taskPrice = $task->getPrice();
            $userWallet = $user->getWallet();

            if ($changeTo === Task::PAYMENT_TYPE_WALLET && $taskPrice > $userWallet->getAvailableBalance()) {
                throw new Exception($dbTranslator->transDb('task.update_payment_wallet.no_balance'), Response::HTTP_BAD_REQUEST);
            }
            if ($changeTo === Task::PAYMENT_TYPE_WALLET) {
                $usedCredit = $userWallet->lockTaskPrice($task->getPrice());
                $task->setUsedCredit($usedCredit);
            } else if($task->getPaymentType() === Task::PAYMENT_TYPE_WALLET){
                $userWallet->unlockCredit($task->getUsedCredit());
                $userWallet->unlockBalance($task->getPrice() - $task->getUsedCredit());
                $task->setUsedCredit(0);
            }
            $task->setPaymentType($changeTo);
            $task->setPaymentTypeUpdated(true);
            $em->persist($task);
            $em->persist($userWallet);

            $code = Response::HTTP_OK;
            $msg = $dbTranslator->transDb('task.update_payment_wallet.confirmed');
            if ($task->is(Task::STATUS_ASSIGNED)) {
                $notification = new Notification();
                $notification->setTitle('notification.task_update_payment_wallet.title');
                $notification->setBody('notification.task_update_payment_wallet.body');
                $notification->setToUser($task->getTasker());
                $notification->setFromUser($user);
                $notification->setExtra([
                    'type' => Notification::TYPE_TASK,
                    'task_id' => $task->getId()
                ]);
                $notification->setType(Notification::ACTION_TASK_UPDATE);
                $notification->setParams([
                    '#task#' => $task->getTitle(),
                ]);
                $em->persist($notification);
            }

            $em->flush();
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }
        return [
            'code' => $code,
            'msg' => $msg,
        ];
    }


    /**
     * @Rest\View()
     * @Rest\Post("/task/cofnirm-invoice" )
     * @SWG\Response(
     *     response=200,
     *     description="Confirm task invoice",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Invoice has been confirmed"),
     *      )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Task not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=404),
     *         @SWG\Property(property="msg", type="string", example="Task not found")
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Can't do this operation",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=400),
     *         @SWG\Property(property="msg", type="string", example="Can't do this operation")
     *      )
     * )
     *
     * @Security(name="Bearer")
     * @param Request $request
     * @return array
     */
    public function confirmTaskInvoiceAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $dbTranslator = $this->get('app.database_translator');
        /** @var Task $task */
        $task = $em->getRepository(Task::class)
            ->findOneBy([
                'id' => $request->get('task'),
                'status' => Task::STATUS_ASSIGNED,
                'user' => $user,
            ]);
        try {
            if (!$task) {
                throw new Exception($dbTranslator->transDb('task_not_found'), Response::HTTP_NOT_FOUND);
            }
            if (!$task->getInvoices()->count()) {
                throw new Exception($dbTranslator->transDb('confirm_task_invoice.error.no_invoice'), Response::HTTP_CONFLICT);
            }
            /** First delete old unpaid invoice transaction */
            $oldInvoiceTransaction = $em->getRepository(Transaction::class)->createQueryBuilder('t')
            ->where('t.type = :type')
            ->andWhere('t.task = :task')
            ->andWhere('t.status in (:status)')
            ->setParameters([
                'type' => Transaction::TYPE_TASK_INVOICE,
                'status' => [Transaction::STATUS_PENDING,Transaction::STATUS_PROCESSING],
                'task' => $task
            ])->getQuery()->getResult();
            if($oldInvoiceTransaction && $oldInvoiceTransaction = $oldInvoiceTransaction[0]){
                /** @var Transaction $oldInvoiceTransaction */
                if($user->getCountry()->getIdentifier() !== 'id'){
                    $info = $this->get('hyperpay_api')->getPaymentStatus($oldInvoiceTransaction->getCheckoutId());
                    if(isset($info['result']['code']) && strpos($info['result']['code'], '000.') === 0){
                        throw new Exception($dbTranslator->transDb('confirm_task_invoice.error.pending_payment'), Response::HTTP_CONFLICT);
                    }
                    /** Check if payment with e-wallet still processing */
                }elseif($oldInvoiceTransaction->getStatus() === Transaction::STATUS_PROCESSING){
                    $info = EWallets::getPaymentStatus($oldInvoiceTransaction->getCheckoutId(),$oldInvoiceTransaction->getExtras()['method']);
                    if($info['status'] === 'PENDING'){
                        throw new Exception($dbTranslator->transDb('confirm_task_invoice.error.pending_payment'), Response::HTTP_CONFLICT);
                    }
                }
                $em->remove($oldInvoiceTransaction);
            }
            $priceToPay = 0;
            $totalInvoicesPrice = 0;
            $invoicesToPay = [];
            /** @var TaskInvoice $invoice */
            foreach ($task->getInvoices() as $invoice) {
                if ($invoice->getStatus() !== TaskInvoice::STATUS_PAYED) {
                    $totalInvoicesPrice += $invoice->getPrice();
                    $priceToPay += $invoice->getPrice();
                    $invoice->setStatus(TaskInvoice::STATUS_PROCESSED);
                    $em->persist($invoice);
                    $invoicesToPay[] = $invoice->getId();
                }
            }
            $data = [];
            if ($totalInvoicesPrice) {
                $invoiceTransaction = new Transaction();
                $invoiceTransaction->setFromUser($user);
                $invoiceTransaction->setToUser($task->getTasker());
                $invoiceTransaction->setType(Transaction::TYPE_TASK_INVOICE);
                $invoiceTransaction->setPrice($totalInvoicesPrice);
                $invoiceTransaction->setStatus(Transaction::STATUS_PENDING);
                $invoiceTransaction->setTask($task);
                $invoiceTransaction->setExtras(['invoices' => $invoicesToPay]);
                if ($task->getPaymentType() === Task::PAYMENT_TYPE_CARD) {
                    $invoiceTransaction->setPaymentType(Task::PAYMENT_TYPE_CARD);
                    $transaction = $em->getRepository(Transaction::class)->findOneBy([
                        'task' => $task,
                        'type' => Transaction::TYPE_TASK_COMPLETE
                    ]);
                    if (!$transaction) {
                        $priceToPay += $task->getPrice();
                        $transaction = new Transaction();
                        $transaction->setStatus(Transaction::STATUS_PENDING);
                        $transaction->setPrice($task->getPrice());
                        $transaction->setType(Transaction::TYPE_TASK_COMPLETE);
                        $transaction->setFromUser($user);
                        $transaction->setToUser($task->getTasker());
                        $transaction->setTask($task);
                        $em->persist($transaction);
                        $transaction->setPaymentType(Task::PAYMENT_TYPE_CARD);
                        $invoiceTransaction->addExtras('includeTaskPrice',true);
                    }elseif($transaction->getStatus() !== Transaction::STATUS_CAPTURED){
                        $transaction->setPrice($task->getPrice());
                        $em->persist($transaction);
                        $priceToPay += $task->getPrice();
                        $invoiceTransaction->addExtras('includeTaskPrice',true);
                    }
                    if ($user->getCountry()->getIdentifier() !== 'id') {
                        /** Create checkout id for the total price to go throw payment process */
                        $hyper = $this->get('hyperpay_api');
                        $em->flush();
                        $data = $hyper->createCheckout($invoiceTransaction->getId(),$priceToPay, $user,
                            $this->generateUrl('hyperpay_payment_callback', [], UrlGeneratorInterface::ABSOLUTE_URL),
                            $user->getWallet()->getRegistrationCardId()
                        );
                        if(!isset($data['id'])){
                            throw new \Exception($dbTranslator->transDb('prepare_checkout_failed'));
                        }
                        $invoiceTransaction->setCheckoutId($data['id']);
                        $em->persist($invoiceTransaction);
                        $em->flush();
                        $data = [
                            'checkoutId' => $data['id'],
                            'shopperUrl' => $this->generateUrl('hyperpay_payment_callback', [], UrlGeneratorInterface::ABSOLUTE_URL),
                            'html' => $this->generateInvoiceDetails($task,$totalInvoicesPrice,$priceToPay)
                        ];
                    } else {
                        $invoiceTransaction->setCheckoutId(Helper::generateToken());
                        $em->persist($invoiceTransaction);
                        $em->flush();
                        $data = [
                            'checkoutId' => $invoiceTransaction->getCheckoutId(),
                            'amount' => $priceToPay,
                            'html' => $this->generateInvoiceDetails($task,$totalInvoicesPrice,$priceToPay)
                        ];
                    }
                } else {
                    $posterWallet = $user->getWallet();
                    $invoiceTransaction->setPaymentType(Task::PAYMENT_TYPE_WALLET);
                    if ($posterWallet->getBalance()>=$totalInvoicesPrice) {
                        $posterWallet->lockBalance($totalInvoicesPrice);
                        $invoiceTransaction->setStatus(Transaction::STATUS_CAPTURED);
                        $em->persist($posterWallet);
                        $em->persist($invoiceTransaction);
                        foreach ($task->getInvoices() as $invoice) {
                            $invoice->setStatus(TaskInvoice::STATUS_PAYED);
                            $em->persist($invoice);
                        }
                        $data = [
                            'amount' => $priceToPay,
                            'html' => $this->generateInvoiceDetails($task,$totalInvoicesPrice,$priceToPay)
                        ];
                    } else {
                        throw new \Exception($dbTranslator->transDb('task_invoice.pay.no_enough_balance'));
                    }
                }
            }
            $em->flush();
            $code = Response::HTTP_OK;
            $msg = 'processing payment';
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/task/cancel")
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function cancelAction(Request $request): array
    {

        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $taskId = $request->get('task');
        $translator = $this->get('app.database_translator');
        /** @var Task $task */
        $task = $em->getRepository(Task::class)
            ->findOneBy(['id' => $taskId, 'user' => $user]);
        if (!$task) {
            return [
                'code' => Response::HTTP_NOT_FOUND,
                'msg' => $translator->transDb('task_not_found')
            ];
        }
        /** Check if task is assigned & category delivery & and assigned time didnt pass certain time */
        $validDelivery = false;
        if (!$task->isInvoicePayed() && $task->is(Task::STATUS_ASSIGNED) && $task->getCategory()->isDelivery()) {
            $validDelivery = $task->getAcceptedOffer()->getAcceptedAt()->modify('+10 minute') > new DateTime();
        }
        if (!$validDelivery && !$task->is(Task::STATUS_PUBLISHED)) {
            return [
                'code' => Response::HTTP_BAD_REQUEST,
                'msg' => $translator->transDb('task.cancel.wrong_status')
            ];
        }
        if ($task->getPaymentType() === Task::PAYMENT_TYPE_WALLET || $task->isInvoicePayed()) {
            $this->cancelTask($task);
            /** Send notification do tasker with an apologies for the cancel */
            if ($validDelivery) {
                $notification = new Notification();
                $notification->setTitle('notification.task_cancelled.title');
                $notification->setBody('notification.cancel_task_apology.body');
                $notification->setType(Notification::ACTION_TASK_UPDATE);
                $notification->setFromUser($user);
                $notification->setToUser($task->getTasker());
                $notification->setParams([
                    '#task#' => $task->getTitle(),
                    '#user#' => $user->getName()
                ]);
                $notification->setExtra([
                    'type' => Notification::TYPE_TASK,
                    'task_id' => $task->getId()
                ]);
            }
        }
        $task->setStatus(Task::STATUS_CANCELLED);
        $em->persist($task);
        $em->flush();

        return [
            'code' => Response::HTTP_OK,
            'msg' => $translator->transDb('task.cancel.success')
        ];

    }

    /**
     * Cancel task request ( when task is assigned )
     * @Rest\View()
     * @Rest\Post("/task/cancel-request" )
     * @param Request $request
     * @return array
     */
    public function createCancelRequestAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $dbTranslator = $this->get('app.database_translator');
        /** @var Task $task */
        $task = $em->getRepository(Task::class)->findOneBy(['id' => $request->get('task'), 'status' => Task::STATUS_ASSIGNED]);
        try {
            if (!$task || ($task->getUser()->getId() !== $user->getId() && $task->getAcceptedOffer()->getUser()->getId() !== $user->getId())) {
                throw new Exception($dbTranslator->transDb('task_not_found'), Response::HTTP_NOT_FOUND);
            }

            $cancelRequest = $em->getRepository(CancelTaskRequest::class)->findOneBy(['task' => $task]);
            if ($cancelRequest) {
                throw new Exception($dbTranslator->transDb('task.cancel_request.exist'), Response::HTTP_CONFLICT);
            }
            $cancelRequest = new CancelTaskRequest();
            $cancelRequest->setFromUser($user);
            $cancelRequest->setForUser($task->getUser()->getId() === $user->getId() ? $task->getAcceptedOffer()->getUser() : $task->getUser());
            $cancelRequest->setTask($task);
            $cancelRequest->setReason($request->get('cancel_reason'));
            $em->persist($cancelRequest);
            $task->setCancelRequest($cancelRequest);
            $em->persist($task);

            /** Notify other user of this request */
            $notification = new Notification();
            $notification->setToUser($cancelRequest->getForUser());
            $notification->setTitle('notification.task_cancel_request.title');
            $notification->setBody('notification.task_cancel_request.body');
            $notification->setFromUser($user);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setExtra([
                'task_id' => $task->getId(),
                'type' => Notification::TYPE_TASK
            ]);
            $em->persist($notification);
            $em->flush();

            $msg = $dbTranslator->transDb('task.cancel_request.sent');
            $code = Response::HTTP_OK;
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }

        return [
            'code' => $code,
            'msg' => $msg,
        ];
    }

    /**
     * Cancel request refuse or accept
     * @Rest\View()
     * @Rest\Post("/task/process-cancel-request" )
     * @param Request $request
     * @return array
     */
    public function processCancelRequestAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $dbTranslator = $this->get('app.database_translator');
        /** @var CancelTaskRequest $cancelRequest */
        $cancelRequest = $em->getRepository(CancelTaskRequest::class)->findOneBy([
            'task' => $request->get('task'),
            'forUser' => $user,
            'status' => CancelTaskRequest::STATUS_WAITING
        ]);
        try {
            $action = $request->get('action');
            if (!$cancelRequest || !$cancelRequest->getTask()->is(Task::STATUS_ASSIGNED)) {
                throw new Exception($dbTranslator->transDb('invalid_request'), Response::HTTP_BAD_REQUEST);
            }
            $task = $cancelRequest->getTask();
            if ($action === CancelTaskRequest::STATUS_ACCEPTED) {
                $cancelRequest->setStatus(CancelTaskRequest::STATUS_ACCEPTED);
                $task->setStatus(Task::STATUS_CANCELLED);
                if ($task->getPaymentType() === Task::PAYMENT_TYPE_WALLET || $task->isInvoicePayed()) {
                    $this->cancelTask($task);
                }
                $em->persist($task);
                /** Send notification to other user to inform him that task has been cancelled */
                $notificationBody = 'notification.task_cancelled_accepted.body';
                self::cancelTaskPenalty($cancelRequest->getFromUser(), $task, $em, $dbTranslator, $this->get('app_service'));
            } else {
                $cancelRequest->setStatus(CancelTaskRequest::STATUS_REFUSED);
                $notificationBody = 'notification.task_cancel_refused.body';
            }
            $notification = new Notification();
            $notification->setToUser($cancelRequest->getFromUser());
            $notification->setTitle('notification.task_cancelled.title');
            $notification->setBody($notificationBody);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setParams([
                '#task#' => $task->getTitle(),
                '#email#' => $this->getParameter('mailer_support')
            ]);
            $notification->setExtra([
                'task_id' => $task->getId(),
                'type' => Notification::TYPE_TASK
            ]);
            $em->persist($notification);
            $em->persist($cancelRequest);
            $em->flush();
            $msg = $dbTranslator->transDb('task.cancel_request.done');
            $code = Response::HTTP_OK;
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }
        return [
            'code' => $code,
            'msg' => $msg,
        ];

    }

    /**
     * @param User $user
     * @param Task $task
     * @param ObjectManager $em
     * @param DBTranslator $DBTranslator
     * @param AppService $appService
     * @throws Exception
     */
    public static function cancelTaskPenalty(User $user, Task $task, ObjectManager $em, DBTranslator $DBTranslator, AppService $appService): void
    {
        /** @var SystemConfig $countryConfig */
        $countryConfig = $user->getCountry()->getConfig();

        /** Retrieve maatloob percentage from user  */
        if ($task->getMaatloobTax()) {
            /** Create transaction */
            $transaction = new Transaction();
            $transaction->setPaymentType(Task::PAYMENT_TYPE_WALLET);
            $transaction->setType(Transaction::TYPE_DEAL_CANCELLED);
            $transaction->setPrice($task->getMaatloobTax($user->getCountry()));
            $transaction->setFromUser($user);
            $transaction->setTask($task);
            $em->persist($transaction);
            $user->getWallet()->withdraw($transaction->getPrice());
            $em->persist($user->getWallet());
            $notification = new Notification();
            $notification->setTitle('notification.cancel_task_penalty.title');
            $notification->setBody('notification.cancel_task_penalty.body');
            $notification->setToUser($user);
            $notification->setExtra([
                'type' => Notification::TYPE_TRANS,
                'direction' => 'OUTGOING'
            ]);
            $notification->setType(Notification::ACTION_TRANSACTION);
            $notification->setParams([
                '#task#' => $task->getTitle(),
                '#price#' => $transaction->getPrice() . ' ' . $user->getCountry()->getCurrencySymbol()
            ]);
            $em->persist($notification);
        }

        /** Check cancel */
        $totalCancelledTasks = $em->getRepository(CancelTaskRequest::class)->createQueryBuilder('ctr')
            ->where('ctr.closed != :closed')
            ->andWhere('ctr.fromUser = :guilty')
            ->setParameters([
                'guilty' => $user,
                'closed' => true
            ])->getQuery()->getResult();

        $maxAllowedCancelTask = $countryConfig->getMaxAllowedTaskCancel();
        $banDuration = $countryConfig->getTaskCancelBanDuration();
        if (count($totalCancelledTasks) === $maxAllowedCancelTask) {
            $notification = new Notification();
            $notification->setTitle('task.cancel.alert.title');
            $notification->setBody('task.cancel.alert.body');
            $notification->setToUser($user);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setParams([
                '#days#' => $banDuration
            ]);
            $notification->setExtra([
                'type' => Notification::TYPE_TASK,
                'task_id' => $task->getId(),
            ]);
            $em->persist($notification);
            $em->persist($user);
        } elseif (count($totalCancelledTasks) > $maxAllowedCancelTask) {
            $user->blockStars();
            /** Ban the user account */
            $userBan = new UserBan();
            $userBan->setUser($user);
            $userBan->setBanReason($DBTranslator->transDb('ban_reason_cancelling_tasks', ['nbr' => $maxAllowedCancelTask], $user->getLanguage()));
            $userBan->setUnbanDate((new DateTime())->modify("+${$banDuration} day"));
            $userBan->setUnbanPenalty($user->getWallet()->getTotalBalance() > 0 ? 0 : $user->getWallet()->getTotalBalance() * -1);
            $em->persist($userBan);
            $appService->sendBanNotification($userBan);
        }
        $em->flush();
    }


    /**
     * @Rest\View(
     *     serializerGroups={"user_data"}
     * )
     * @Rest\Post("/invoice" )
     * @param Request $request
     * @return array
     */
    public function getInvoiceAction(Request $request): array
    {
        $em = $this->getDoctrine();
        /** @var User $user */
        $user = $this->getUser();
        /** @var Task $task */
        $task = $em->getRepository(Task::class)->find($request->get('task'));

        if (!$task || !in_array($task->getStatus(), [Task::STATUS_PAID, Task::STATUS_COMPLETED], true) ||
            (!in_array($user->getId(), [$task->getTasker()->getId(), $task->getUser()->getId()], true))) {
            return [
                'code' => Response::HTTP_BAD_REQUEST,
                'msg' => 'Wrong inputs',
            ];
        }
        $isTasker = $user->getId() === $task->getTasker()->getId();
        $maatloobTax = 0;
        $countryTax = 0;
        $taskPrice = $task->getPrice();
        if ($isTasker) {
            $taskPrice = $task->getAcceptedOffer()->getPrice();
            /** @var Transaction $maatloobTax */
            $maatloobTax = $em->getRepository(Transaction::class)->findOneBy([
                'fromUser' => $user, 'type' => Transaction::TYPE_FEES, 'task' => $task]);
            if ($maatloobTax) {
                $maatloobTax = $maatloobTax->getFromPrice();
            }
            /** @var Transaction $countryTax */
            $countryTax = $em->getRepository(Transaction::class)->findOneBy([
                'fromUser' => $user, 'type' => Transaction::TYPE_COUNTRY_TAX, 'task' => $task]);
            if ($countryTax) {
                $countryTax = $countryTax->getFromPrice();
            }
        }
        $transaction = $em->getRepository(Transaction::class)->findOneBy(['task'=>$task,'type'=> Transaction::TYPE_TASK_COMPLETE]);
        $html = $this->renderView('@App/invoice_template.html.twig', [
            'transactionId' => $transaction?$transaction->getId():'',
            'maatloobTax' => $maatloobTax,
            'countryTax' => $countryTax,
            'totalTax' => $maatloobTax + $countryTax,
            'netProfit' => $taskPrice - ($maatloobTax + $countryTax),
            'totalPrice' => $taskPrice,
            'taxNumber' => $user->getCountry()->getConfig()->getTaxIdentifier(),
            'task' => $task
        ]);
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'invoice',
            'data' => $html,
            'user' => $user
        ];
    }

    /**
     * @param Task $task
     * @param int $totalInvoicesPrice
     * @param int $priceToPay
     * @return string
     */
    private function generateInvoiceDetails(Task $task, int $totalInvoicesPrice, int $priceToPay): string
    {
        return str_replace("\n", '',$this->renderView('@Api/invoice.payment.details.html.twig',['task'=>$task,'invoicePrice'=>$totalInvoicesPrice,'totalPrice'=>$priceToPay]));
    }

    /**
     * @param Task $task
     */
    private function cancelTask(Task $task): void
    {
        $em = $this->getDoctrine()->getManager();
        $user = $task->getUser();
        $wallet = $user->getWallet();
        $wallet->unlockCredit($task->getUsedCredit());
        $wallet->unlockBalance($task->getPrice() - $task->getUsedCredit());
        $em->persist($wallet);
        if($task->isInvoicePayed()){
            /** @var Transaction $taskTransaction */
            $taskTransaction = $em->getRepository(Transaction::class)->findOneBy([
                'task' => $task,
                'status' => Transaction::STATUS_CAPTURED,
                'type' => Transaction::TYPE_TASK_COMPLETE
            ]);
            $taskTransaction->setStatus(Transaction::STATUS_CANCELLED);
            $em->persist($taskTransaction);

            $invoicesTransactions = $em->getRepository(Transaction::class)->findBy([
                'type' => Transaction::TYPE_TASK_INVOICE,
                'task' => $task,
                'status' => Transaction::STATUS_CAPTURED
            ]);
            /** @var Transaction $transaction */
            foreach ($invoicesTransactions as $transaction) {
                $wallet = $user->getWallet();
                $wallet->unlockCredit($transaction->getPrice());
                $em->persist($wallet);
                $transaction->setStatus(Transaction::STATUS_CANCELLED);
                $em->persist($transaction);
                $paymentInformation = $transaction->getPaymentInformation();
                $refundSuccess = false;
                /** If payment done with card then try to refund user money */
                if($transaction->getCheckoutId() && $transaction->getPaymentMethod()){
                    if($user->getCountry() === 'id'){
                        Xendit::setApiKey($this->getParameter('xendit_payment_secret'));
                        if(array_key_exists('ewallet_type',$paymentInformation)){
                            $params = [
                                'external_id' => $transaction->getCheckoutId(),
                                'amount' => $paymentInformation['amount'],
                                'email_to' => [$user->getEmail()],
                                'bank_code' => $paymentInformation['ewallet_type'],
                                'account_holder_name' => $user->getFullName(),
                                'account_number' => $paymentInformation['phone'],
                                'description' => 'refund user money after task cancelled'
                            ];
                            try {
                                Disbursements::create($params);
                                $refundSuccess = true;
                            }catch (\Exception $e){}
                        }else{
                            try{
                                Cards::createRefund($paymentInformation['id'],['amount'=>$paymentInformation['amount'],'external_id'=>Helper::generateToken()]);
                                $refundSuccess = true;
                            }catch (\Exception $e){}
                        }
                    }else{
                        $hyperPay = $this->get('hyperpay_api');
                        $data = $hyperPay->refundPayment($paymentInformation['id'],$paymentInformation['amount']);
                        $refundSuccess = preg_match('/^(000\.000\.|000\.100\.1|000\.[36])/', $data['result']['code']);
                    }
                }
                if($refundSuccess){
                    $wallet->withdraw($paymentInformation['amount']);
                    $em->persist($wallet);
                }
            }
        }
    }
}
