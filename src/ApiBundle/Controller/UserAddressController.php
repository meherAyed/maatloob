<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\AddressForm;
use AppBundle\Entity\Address;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserAddressController
 * @package ApiBundle\Controller
 */
class UserAddressController extends Controller
{

    /**
     * @Rest\View(
     *     serializerGroups={"address_details"}
     * )
     * @Rest\Get("/user/address")
     */
    public function listAction(): array
    {
        /** @var User $user */
        $user = $this->getUser();
        $userAddress = $this->getDoctrine()->getRepository(Address::class)
            ->findBy(['user' => $user, 'isDeleted' => false]);
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'Addresses list',
            'data' => $userAddress
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"address_details"}
     * )
     * @Rest\Post("/user/address/add")
     * @return array
     */
    public function addAction(): array
    {
        $translator = $this->get('app.database_translator');
        $userAddress = new Address();
        $addressForm = $this->createForm(AddressForm::class, $userAddress);
        $data = $this->get('json.request')->getParameters();
        $addressForm->submit($data);
        if ($addressForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $userAddress->setUser($this->getUser());
            $em->persist($userAddress);
            $em->flush();
            return [
                'code' => Response::HTTP_OK,
                'msg' => $translator->transDb('address.add.success'),
                'data' => $userAddress
            ];
        }
        return [
            'code' => Response::HTTP_BAD_REQUEST,
            'msg' => $translator->transDb('invalid_request'),
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("user/address/remove")
     * @param Request $request
     * @return array
     */
    public function deleteAction(Request $request): array
    {
        $translator = $this->get('app.database_translator');
        $em = $this->getDoctrine()->getManager();
        if ($request->request->has('address_id')) {
            $user = $this->getUser();
            $address = $em->getRepository(Address::class)
                ->findOneBy(['user' => $user, 'id' => $request->get('address_id')]);
            if ($address) {
                $address->setIsDeleted(true);
                $em->persist($address);
                $em->flush();
                return [
                    'code' => Response::HTTP_OK,
                    'msg' =>  $translator->transDb('address.delete.success')
                ];
            }
        }
        return [
            'code' => Response::HTTP_NOT_FOUND,
            'msg' =>  $translator->transDb('address_not_found')
        ];
    }
}
