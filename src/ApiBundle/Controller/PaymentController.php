<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 30/11/2019
 * Time: 23:46
 */

namespace ApiBundle\Controller;


use AppBundle\Entity\Notification;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskInvoice;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use Components\Helper;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Xendit\Cards;
use Xendit\EWallets;
use Xendit\Xendit;


class PaymentController extends Controller
{
    public CONST E_WALLET_OVO = 'OVO';
    public CONST E_WALLET_DANA = 'DANA';
    public CONST E_WALLET_LINKAJA = 'LINKAJA';

    /**
     * @Rest\View()
     * @SWG\Response(
     *     response=200,
     *     description="Payment method updated",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="Checkout has been created"),
     *      )
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Request Body Params",
     *      @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="amount", type="string",description="rechage amount")
     * )
     * )
     * @Rest\Post("/user/payment/checkout")
     * @param Request $request
     * @return array
     */
    public function prepareCheckoutAction(Request $request): ?array
    {
        /** @var User $user */
        $user = $this->getUser();
        $dbTranslator = $this->get('app.database_translator');
        $amount = (float)$request->get('amount', 0);
        $em = $this->getDoctrine()->getManager();
        try {
            $currency = strtoupper($user->getCountry()->getCurrencyCode());
            $minDepositBalance = $user->getCountry()->getConfig()->getMinAllowedBalanceRecharge() ?? 0.0;
            if ($amount < $minDepositBalance) {
                throw new Exception($dbTranslator->transDb('recharge_wallet_insufficient',
                    [
                        '#amount#' => $minDepositBalance,
                        '#currency#' => $currency
                    ]), Response::HTTP_BAD_REQUEST);
            }
            /** Create transaction */
            $transaction = new Transaction();
            $transaction->setToUser($user);
            $transaction->setPaymentType(Task::PAYMENT_TYPE_CARD);
            $transaction->setType(Transaction::TYPE_DEPOSIT);
            $transaction->setPrice($amount);
            $transaction->setStatus(Transaction::STATUS_PENDING);
            $em->persist($transaction);
            $em->flush();
            if($user->getCountry()->getIdentifier()!=='id'){
                $hyperPayApi = $this->get('hyperpay_api');
                $data = $hyperPayApi->createCheckout($transaction->getId(),$transaction->getPrice(),$user,
                    $this->generateUrl('hyperpay_payment_callback',[],UrlGeneratorInterface::ABSOLUTE_URL),$user->getWallet()->getRegistrationCardId());
                if(!isset($data['id'])){
                    $this->get('logger')->addError('Error create checkout : '.json_encode($data));
                    $em->remove($transaction);
                    $em->flush();
                    throw new Exception($dbTranslator->transDb('prepare_checkout_failed'));
                }
                $transaction->setCheckoutId($data['id']);
                $data['shopperUrl'] = $this->generateUrl('hyperpay_payment_callback', [], UrlGeneratorInterface::ABSOLUTE_URL);
            }else{
                $transaction->setCheckoutId(Helper::generateToken());
                $data = [
                    'amount' => $amount,
                    'paymentUrl' => $this->generateUrl('xendit_payment_interface',['checkoutId'=>$transaction->getCheckoutId()])
                ];
            }
            $em->persist($transaction);
            $em->flush();
            return
                [
                    'code' => Response::HTTP_OK,
                    'msg' => 'transaction has been created',
                    'data' => $data
                ];
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'msg' => $e->getMessage(),
                'data' => null
            ];
        }

    }

    /**
     * @param $checkoutId
     * @return Response
     */
    public function hyperPayPaymentInterfaceAction($checkoutId): Response
    {
        /** @var Transaction $transaction */
        $transaction = $this->getDoctrine()->getRepository(Transaction::class)->findOneBy([
            'checkoutId'=>$checkoutId,
            'status' => Transaction::STATUS_PENDING
        ]);
        return $this->render('@App/hyperpay.payment.interface.html.twig',['transaction' => $transaction]);
    }

    /**
     * @Rest\View(
     *     serializerGroups={"user_wallet"}
     * )
     * @param Request $request
     * @return Response
     */
    public function hyperPayPaymentCallbackAction(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $dbTranslator = $this->get('app.database_translator');
        $checkoutId = $request->get('id', null);
        if(!empty($checkoutId)){
            /** @var Transaction $transaction */
            $transaction = $em->getRepository(Transaction::class)->findOneBy([
                'checkoutId' => $checkoutId,
            ]);
            if ($transaction && in_array($transaction->getStatus(), [Transaction::STATUS_PENDING, Transaction::STATUS_PROCESSING], true)) {
                $hyperPayApi = $this->get('hyperpay_api');
                $data = $hyperPayApi->getPaymentStatus($transaction->getCheckoutId());
                if (preg_match('/^(000\.000\.|000\.100\.1|000\.[36])/', $data['result']['code'])) {
                    $transaction->setPaymentInformation(json_encode($data));
                    $transaction->setExtras(['method' => $data['paymentBrand']]);
                    $em->persist($transaction);
                    $msg = 'Transaction success';
                    if($transaction->getType() === Transaction::TYPE_DEPOSIT){
                        $transaction->setStatus(Transaction::STATUS_COMPLETED);
                        $this->walletChargeSuccess($transaction);
                        $param['price'] = $transaction->getPrice();
                        $user = $transaction->getToUser();
                        $param['currency'] = $user->getCountry()->getCurrencySymbol();
                        $msg = $dbTranslator->transDb('notification.transaction_deposit.body',$param,$user->getLanguage());
                    }elseif ($transaction->getType() === Transaction::TYPE_TASK_INVOICE){
                        $transaction->setStatus(Transaction::STATUS_CAPTURED);
                        $this->taskInvoicePaymentSuccess($transaction);
                        $msg = $dbTranslator->transDb('invoice_payment.success');
                    }
                    /** Register user credit card if present */
                    if(array_key_exists('registrationId', $data) && !in_array($data['registrationId'], $user->getWallet()->getRegistrationCardId(), true)) {
                        $user->getWallet()->addRegistrationCardId($data['registrationId']);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($user->getWallet());
                        $em->flush();
                    }
                    $success = true;
                }else{
                    $success = false;
                    $msg = 'Payment has failed . please try again';
                }
            }else{
                $success = false;
                $msg = 'Invalid payment transaction';
            }
        }else{
            $success = false;
            $msg = 'Invalid payment transaction';
        }
        return $this->paymentResultAction($success,$msg);
    }
    /**
     * @param $checkoutId
     * @param Request $request
     * @return Response
     */
    public function xenditPaymentInterfaceAction($checkoutId,Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $error = null;
        $dbTranslator = $this->get('app.database_translator');
        /** @var Transaction $transaction */
        $transaction = $em->getRepository(Transaction::class)->findOneBy([
            'status' => Transaction::STATUS_PENDING,
            'checkoutId' => $checkoutId
        ]);
        if($transaction && $request->isMethod(Request::METHOD_POST)) {
            $paymentType = $request->get('paymentType');
            $token = $request->get('token');
            $phone = $request->get('phone');
            if ($paymentType ==='card' && !empty($token)) {
                Xendit::setApiKey($this->getParameter('xendit_payment_secret'));
                $params = [
                    'token_id' => $token,
                    'external_id' => $transaction->getCheckoutId(),
                    'amount' => $transaction->getPrice(),
                ];
                try {
                    $charge = Cards::create($params);
                    if(isset($charge) && $charge['status'] === 'CAPTURED' ){
                        $transaction->setPaymentInformation(json_encode($charge));
                        $transaction->setExtras(['method' => $charge['card_brand']]);
                        $em->persist($transaction);
                        $success = true;
                        $msg = $dbTranslator->transDb('payment.success');
                        /** Check if transaction is wallet charge */
                        if($transaction->getType() === Transaction::TYPE_DEPOSIT){
                            $transaction->setStatus(Transaction::STATUS_COMPLETED);
                            $this->walletChargeSuccess($transaction);
                            $msg = $dbTranslator->transDb('deposit.success');
                        }elseif($transaction->getType() === Transaction::TYPE_TASK_INVOICE){
                            $transaction->setStatus(Transaction::STATUS_CAPTURED);
                            $em->persist($transaction);
                            $this->taskInvoicePaymentSuccess($transaction);
                            $msg = $dbTranslator->transDb('invoice_payment.success');
                        }
                    }else{
                        $success = false;
                        $msg = $dbTranslator->transDb('deposit.failed');
                    }
                    return $this->paymentResultAction($success,$msg);
                }catch (Exception $exception){
                    $error = $exception->getMessage();
                }
            }
            if(!empty($phone) && in_array($paymentType, $transaction->getToUser()->getCountry()->getConfig()->getSupportedPaymentBrand(), true)) {
                Xendit::setApiKey($this->getParameter('xendit_payment_secret'));
                $params = [
                    'external_id' => $transaction->getCheckoutId(),
                    'amount' => $transaction->getPrice(),
                    'phone' => $phone,
                    'ewallet_type' => $paymentType
                ];
                if($paymentType !== self::E_WALLET_OVO){
                    $params['callback_url'] = $this->generateUrl('xendit_payment_callback',[],UrlGeneratorInterface::ABSOLUTE_URL
                    );
                    $params['redirect_url'] = $this->generateUrl('xendit_payment_redirect',[],UrlGeneratorInterface::ABSOLUTE_URL);
                    if ($paymentType===self::E_WALLET_LINKAJA){
                        $params['items'] = [[
                            'id'=> $transaction->getCheckoutId(),
                            'name'=> 'Charging maatloob wallet with '.$transaction->getPrice().' IDR',
                            'price' => $transaction->getPrice(),
                            'quantity' => 1
                        ]];
                    }
                }
                try {
                    $res = EWallets::create($params);
                    $transaction->setStatus(Transaction::STATUS_PROCESSING);
                    $transaction->setPaymentInformation(json_encode($res));
                    $transaction->setExtras(['method' => $paymentType]);
                    $em->persist($transaction);
                    $em->flush();
                    if($paymentType === self::E_WALLET_OVO){
                        $msg = $dbTranslator->transDb('ovo.complete_payment');
                        return $this->paymentResultAction(true,$msg);
                    }
                    return $this->redirect($res['checkout_url']);
                }catch (Exception $e){
                    $error = $e->getMessage();
                }
            }
        }

        return $this->render('@App/xendit.payment.interface.html.twig',[
            'transaction'=>$transaction,
            'xenditPublicKey' => $this->getParameter('xendit_payment_public'),
            'error' => $error
        ]);
    }


    /**
     * Callback from xendit after payment operation done for e-wallet
     * @Rest\View(
     *     serializerGroups={"user_wallet"}
     * )
     * @param Request $request
     * @return array
     */
    public function xenditPaymentCallbackAction(Request $request): array
    {
        $params = $request->request->all();
        $this->get('logger')->addError('response : '.json_encode($params));
        if(isset($params['ewallet_type'],$params['external_id'])){
            try {
                Xendit::setApiKey($this->getParameter('xendit_payment_secret'));
                $paymentStatus = EWallets::getPaymentStatus($params['external_id'],$params['ewallet_type']);
                $this->get('logger')->addError('$paymentStatus : '.json_encode($paymentStatus));
                if($paymentStatus && in_array($paymentStatus['status'],['COMPLETED','PAID'])){
                    $em = $this->getDoctrine()->getManager();
                    /** Search for the transaction */
                    /** @var Transaction $transaction */
                    $transaction = $em->getRepository(Transaction::class)->findOneBy([
                        'checkoutId' => $params['external_id'],
                        'status' => Transaction::STATUS_PROCESSING
                    ]);
                    if($transaction){
                        $transaction->setStatus(Transaction::STATUS_COMPLETED);
                        $transaction->setPaymentInformation(json_encode($paymentStatus));
                        $transaction->setPrice((float)$paymentStatus['amount']);
                        $em->persist($transaction);
                        /** Check if transaction is wallet charge */
                        if($transaction->getType() === Transaction::TYPE_DEPOSIT){
                            $this->walletChargeSuccess($transaction);
                        }elseif($transaction->getType() === Transaction::TYPE_TASK_INVOICE){
                            $transaction->setStatus(Transaction::STATUS_CAPTURED);
                            $em->persist($transaction);
                            $this->taskInvoicePaymentSuccess($transaction);
                        }
                        $em->flush();
                    }
                }
            }catch (Exception $e){
                $this->get('logger')->addError('error payment : '.$e->getMessage());
            }
        }
        return [
            'code' => Response::HTTP_OK,
            'message' => 'verified'
        ];
    }

    private function taskInvoicePaymentSuccess(Transaction $transaction): void
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($transaction->getExtras()['invoices'] as $invoiceId) {
            /** @var TaskInvoice $invoice */
            $invoice = $em->getRepository(TaskInvoice::class)->find($invoiceId);
            $invoice->setStatus(TaskInvoice::STATUS_PAYED);
            $em->persist($invoice);
        }
        $posterWallet = $transaction->getTask()->getUser()->getWallet();
        $posterWallet->deposit($transaction->getPrice());
        $posterWallet->lockBalance($transaction->getPrice());
        $em->persist($posterWallet);
        $notification = new Notification();
        $notification->setTitle('notification.task_invoice_confirmed.title');
        $notification->setBody('notification.task_invoice_confirmed.body');
        $notification->setToUser($transaction->getToUser());
        $notification->setFromUser($transaction->getFromUser());
        $notification->setExtra([
            'type' => Notification::TYPE_TASK,
            'task_id' => $transaction->getTask()->getId()
        ]);
        $notification->setType(Notification::ACTION_TASK_UPDATE);
        $notification->setParams([
            '#user#' => $transaction->getFromUser()->getName(),
            '#task#' => $transaction->getTask()->getTitle(),
        ]);
        $em->persist($notification);
        /** Check if task price is included in this invoice transaction  */
        if(array_key_exists('includeTaskPrice',$transaction->getExtras())){
            /** @var Transaction $taskTransaction */
            $taskTransaction = $em->getRepository(Transaction::class)->findOneBy([
                'task' => $transaction->getTask(),
                'type' => Transaction::TYPE_TASK_COMPLETE
            ]);
            if($taskTransaction && in_array($taskTransaction->getStatus(), [Transaction::STATUS_PROCESSING, Transaction::STATUS_PENDING], true)){
                /** Put task complete transaction to captured  and lock the task price */
                $posterWallet = $taskTransaction->getTask()->getUser()->getWallet();
                $posterWallet->deposit($taskTransaction->getPrice());
                $posterWallet->lockBalance($taskTransaction->getPrice());
                $em->persist($posterWallet);
                $taskTransaction->setStatus(Transaction::STATUS_CAPTURED);
                $em->persist($taskTransaction);
            }
        }
        $em->flush();
    }

    /**
     * Callback from xendit after payment operation done (DANA or LINKAJA)
     * @param Request $request
     * @return Response
     */
    public function xenditPaymentRedirectAction(Request $request): Response
    {
        $success = true;
        $msg = 'Payment completed';
        return $this->paymentResultAction($success,$msg);
    }


    /**
     * @param Transaction $transaction
     */
    private function walletChargeSuccess(Transaction $transaction): void
    {
        $em = $this->getDoctrine()->getManager();
        $user = $transaction->getToUser();
        $userWaller = $user->getWallet();
        $userWaller->deposit($transaction->getPrice());
        $user->setWarnedForBan(false);
        $em->persist($user);
        $em->persist($userWaller);
        $em->flush();
        /** Notify user transaction completed */
        $em = $this->getDoctrine()->getManager();
        $notification = new Notification();
        $notification->setToUser($transaction->getToUser());
        $notification->setTitle('notification.transaction_deposit.title');
        $notification->setBody('notification.transaction_deposit.body');
        $notification->setExtra(['type' => Notification::TYPE_TRANS]);
        $notification->setType(Notification::ACTION_TRANSACTION);
        $notification->setParams([
            '#price#' => $transaction->getPrice(),
            '#currency#' => $transaction->getToUser()->getCountry()->getCurrencySymbol()
        ]);
        $em->persist($notification);
        $em->flush();
        $this->verifyUserDebts($user);
    }

    /**
     * @param $success
     * @param $msg
     * @return Response
     */
    public function paymentResultAction($success,$msg): Response
    {
        return $this->render('@App/payment.result.html.twig',[
            'success' => $success,
            'message' => $msg
        ]);
    }

    /**
     * @Rest\View()
     * @return array
     */
    public function pendingWithdrawRequestAction(): array
    {
        $user = $this->getUser();
        $data = $this->getDoctrine()->getRepository(Transaction::class)
            ->createQueryBuilder('t')
            ->select('t.createdAt as created_at', 't.status', 't.id', 't.price')
            ->where('t.status in (:status)')
            ->andWhere('t.fromUser = :user')
            ->setParameters([
                'status' => [Transaction::STATUS_PENDING, Transaction::STATUS_PROCESSING],
                'user' => $user
            ])->getQuery()->getResult();
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'withdrawal requests',
            'data' => $data
        ];
    }


    /**
     * /**
     * @Rest\View(
     *     serializerGroups={"user_wallet"}
     * )
     * @param Request $request
     * @return array
     */
    public function withdrawBalanceRequestAction(Request $request): array
    {
        /** @var User $user */
        $user = $this->getUser();
        $dbTranslator = $this->get('app.database_translator');
        $amount = $request->get('amount', 0);
        $method = $request->get('method',Transaction::WITHDRAW_TYPE_MANUAL_BANK_TRANSFER);
        $em = $this->getDoctrine()->getManager();
        $minBalanceWithdraw = $user->getCountry()->getConfig()->getAllowWithdrawBalanceFrom();
        try {
            if ($amount > $user->getWallet()->getBalance()) {
                throw new Exception($dbTranslator->transDb('payment.withdraw.no_enough_balance'));
            }
            if ($amount < $minBalanceWithdraw) {
                throw new Exception($dbTranslator->transDb('payment.withdraw.under_allowed_balance',
                    ['#balance#' => $minBalanceWithdraw, '#currency#' => $user->getCountry()->getCurrencyCode()]));
            }
            if(!in_array($method, [Transaction::WITHDRAW_TYPE_DISBURSEMENT, Transaction::WITHDRAW_TYPE_MANUAL_BANK_TRANSFER,
                Transaction::WITHDRAW_TYPE_STC_PAY], true)){
                throw new Exception($dbTranslator->transDb('invalid_request'));
            }
            /** Check if there is already a withdraw request */
            $withdrawRequest = $em->getRepository(Transaction::class)->createQueryBuilder('t')
                ->where('t.type = :type')
                ->andWhere('t.fromUser = :user')
                ->andWhere('t.status in (:status)')
                ->setParameters([
                    'type' => Transaction::TYPE_WITHDRAW,
                    'user' => $user,
                    'status' => [Transaction::STATUS_PENDING,Transaction::STATUS_PROCESSING],
                ])->getQuery()->getResult();

            if (count($withdrawRequest)) {
                throw new Exception($dbTranslator->transDb('payment.withdraw.already_pending'));
            }
            $transaction = new Transaction();
            $transaction->setPaymentType(Task::PAYMENT_TYPE_CARD);
            $transaction->setCheckoutId(Helper::generateToken());
            $transaction->setPrice($amount);
            $transaction->setStatus(Transaction::STATUS_PENDING);
            $transaction->setFromUser($user);
            $transaction->setType(Transaction::TYPE_WITHDRAW);
            $transaction->setExtras([
                'method' => $method
            ]);
            $em->persist($transaction);
            /** Lock withdrawal balance from wallet */
            $user->getWallet()->lockBalance($amount);
            $em->persist($user->getWallet());
            /** Notify admin for the request */
            if($method === Transaction::WITHDRAW_TYPE_MANUAL_BANK_TRANSFER){
                $em->flush();
                $this->notifyAdminWithdrawRequest($transaction);
                $msg = $dbTranslator->transDb('payment.withdraw.created');
            }else{
                $payoutService = $this->get('payout');
                [$success,$msg] = $payoutService->createPayout($user,$transaction);
                if(!$success) {
                    throw new \Exception($msg);
                }
                $transaction->setStatus(Transaction::STATUS_PROCESSING);
                $em->flush();
            }
            $code = Response::HTTP_OK;
            $data['wallet'] = $user->getWallet();
            $data['transaction'] = [
                'id' => $transaction->getId(),
                'price' => $transaction->getPrice(),
                'created_at' => $transaction->getCreatedAt()
            ];
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }


    /**
     * /**
     * @Rest\View()
     * @param Request $request
     * @return array
     */
    public function xenditDisbursementCallbackAction(Request $request): array
    {
        $params = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $this->get('logger')->addError('disbursement : '.json_encode($request->request->all()));
        /** @var Transaction $transaction */
        $transaction = $em->getRepository(Transaction::class)->findOneBy([
            'checkoutId' => $params['external_id']
        ]);
        if($transaction && $transaction->getStatus() !== Transaction::STATUS_COMPLETED){
            if($params['status']==='COMPLETED'){
                $transaction->setStatus(Transaction::STATUS_COMPLETED);
                $transaction->setPaymentInformation(json_encode($params));
                $user = $transaction->getFromUser();
                $user->getWallet()->unlockBalance($transaction->getPrice());
                $user->getWallet()->withdraw($transaction->getPrice());
                $em->persist($user->getWallet());
                $em->flush();
                $msg = $this->get('app.database_translator')->transDb('xendit.disbursement.success',['price' => $transaction->getPrice().' RP'],$user->getLanguage());
                $this->notifyUserWithdrawCompleted($transaction,'withdraw.completed',$msg);
            }else{
                $transaction->setStatus(Transaction::STATUS_FAILED);
                $transaction->setPaymentInformation(json_encode($params));
                $user = $transaction->getFromUser();
                $user->getWallet()->unlockBalance($transaction->getPrice());
                $em->persist($user->getWallet());
                $em->flush();
                $msg = $this->get('app.database_translator')->transDb('xendit.disbursement.failed',[],$user->getLanguage());
                $this->notifyUserWithdrawCompleted($transaction,'withdraw.failed',$msg);
            }
        }

        return [
            'code' => Response::HTTP_OK,
            'message' => 'verified'
        ];
    }

    /**
     * /**
     * @Rest\View(
     *     serializerGroups={"user_wallet"}
     * )
     * @param Request $request
     * @return array
     */
    public function cancelWithdrawRequestAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $dbTranslator = $this->get('app.database_translator');
        $transactionId = $request->get('transaction_id');
        /** @var Transaction $transaction */
        $transaction = $em->getRepository(Transaction::class)
            ->findOneBy([
                'type' => Transaction::TYPE_WITHDRAW,
                'fromUser' => $user,
                'id' => $transactionId
            ]);
        try {
            if (!$transaction) {
                throw new Exception($dbTranslator->transDb('payment.withdraw.not_found'));
            }
            if($transaction->getStatus() === Transaction::STATUS_CANCELLED){
                throw new Exception($dbTranslator->transDb('payment.withdraw.already_cancelled'));
            }
            $withdrawMethod = $transaction->getExtras()['method'];
            if ($transaction->getStatus() === Transaction::STATUS_COMPLETED || $withdrawMethod === Transaction::WITHDRAW_TYPE_DISBURSEMENT ||
                ($withdrawMethod === Transaction::WITHDRAW_TYPE_MANUAL_BANK_TRANSFER && $transaction->getStatus() !== Transaction::STATUS_PENDING)) {
                throw new Exception($dbTranslator->transDb('payment.withdraw.cant_cancel'));
            }
            $payoutService = $this->get('payout');
            if($withdrawMethod === Transaction::WITHDRAW_TYPE_STC_PAY && $transaction->getStatus() === Transaction::STATUS_PROCESSING){
                $info = $payoutService->getStcPayStatus($transaction->getExtras()['paymentReference'],$transaction->getCheckoutId());
                if((int)$info['Payments'][0]['Status'] !== 2 && !$payoutService->cancelStcTransaction($transaction->getExtras()['paymentReference'],$transaction->getCheckoutId())){
                    throw new Exception($dbTranslator->transDb('payment.withdraw.cant_cancel'));
                }
            }
            $user->getWallet()->unlockBalance($transaction->getPrice());
            $em->remove($transaction);
            $em->persist($user->getWallet());
            $em->flush();
            $code = Response::HTTP_OK;
            $msg = $dbTranslator->transDb('payment.withdraw.cancelled');
            $data = $user->getWallet();
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }

    /**
     * @param Transaction $transaction
     */
    private function notifyAdminWithdrawRequest(Transaction $transaction): void
    {
        $translator = $this->get('translator');
        $notification = new Notification();
        $notification->setFromUser($transaction->getFromUser());
        $notification->setType(Notification::ADMIN_NEW_WITHDRAW_REQUEST);
        $notification->setTitle($translator->trans('notification.request_withdraw.title'));
        $notification->setBody($translator->trans('notification.request_withdraw.body',
            ['_name_' => $notification->getFromUser()->getFullName()]));
        $notification->setLink($this->generateUrl('admin_withdraw_request_details', ['id' => $transaction->getId()]));
        $this->get('notify_admin')->notifyAdmin($notification);

    }

    /**
     * /**
     * @Rest\View(
     *     serializerGroups={"user_withdrawal"}
     * )
     * @return array
     */
    public function getWithdrawalRequestAction(): array
    {
        $withdrawalRequests = $this->getDoctrine()->getRepository(Transaction::class)
            ->createQueryBuilder('t')
            ->where('t.fromUser = :user')
            ->andWhere('t.status != :status')
            ->andWhere('t.type = :type')
            ->setParameters([
                'user' => $this->getUser(),
                'status' => Transaction::STATUS_COMPLETED,
                'type' => Transaction::TYPE_WITHDRAW
            ])->getQuery()->getResult();

        return [
            'code' => Response::HTTP_OK,
            'msg' => 'withdrawal requests',
            'data' => $withdrawalRequests
        ];
    }

    /**
     * @param User $user
     */
    private function verifyUserDebts(User $user): void
    {
        $em = $this->getDoctrine()->getManager();
        $compensationTransaction = $em->getRepository(Transaction::class)->findBy(
            ['fromUser' => $user, 'status' => Transaction::STATUS_PENDING, 'type' => Transaction::TYPE_COMPENSATION]);
        $userWallet = $user->getWallet();
        /** @var Transaction $transaction */
        foreach ($compensationTransaction as $transaction) {
            if ($userWallet->getBalance() >= $transaction->getPrice()) {
                $wallet = $transaction->getToUser()->getWallet();
                $transaction->setStatus(Transaction::STATUS_COMPLETED);
                $userWallet->withdraw($transaction->getFromPrice());
                $wallet->deposit($transaction->getPrice());
                $em->persist($userWallet);
                $em->persist($wallet);
                /** Send notification for both users */
                $em->persist(\AppBundle\Controller\Administration\TaskController::sendCompensationNotification($transaction, $transaction->getToUser(), $transaction->getPrice()));
                $em->persist(\AppBundle\Controller\Administration\TaskController::sendCompensationNotification($transaction, $transaction->getFromUser(), $transaction->getFromPrice()));
            }
            $em->flush();
        }
    }

    /**
     * @param Transaction $transaction
     * @param string $title
     * @param string $msg
     */
    private function notifyUserWithdrawCompleted(Transaction $transaction,string $title ,string $msg): void
    {
        $em = $this->getDoctrine()->getManager();
        $notification = new Notification();
        $notification->setToUser($transaction->getFromUser());
        $notification->setTitle($title);
        $notification->setBody($msg);
        $notification->setExtra(['type' => Notification::TYPE_TRANS]);
        $notification->setType(Notification::ACTION_TRANSACTION);
        $em->persist($transaction);
        $em->flush();
    }
}
