<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\bankAccountForm;
use ApiBundle\Form\UserReviewForm;
use ApiBundle\Form\UserUpdateForm;
use AppBundle\Entity\Badge;
use AppBundle\Entity\File;
use AppBundle\Entity\Notification;
use AppBundle\Entity\NotificationConfig;
use AppBundle\Entity\Offer;
use AppBundle\Entity\PhoneVerificationRequest;
use AppBundle\Entity\Skills;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskDiscussion;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBadge;
use AppBundle\Entity\UserBan;
use AppBundle\Entity\UserBankAccount;
use AppBundle\Entity\UserDocument;
use AppBundle\Entity\UserOldPhones;
use AppBundle\Entity\UserReview;
use AppBundle\Entity\UserWallet;
use Components\Helper;
use DateTime;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Exception\InvalidParameterException;
use FOS\UserBundle\Model\UserManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 * @package ApiBundle\Controller
 * @SWG\Tag(name="User")
 */
class UserController extends Controller
{
    /**
     * @Rest\View()
     * @Rest\Get("/user/dashboard")
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function dashboardAction(Request $request): array
    {
        $msg = 'User dashboard';
        $code = Response::HTTP_OK;
        $doc = $this->getDoctrine();
        /** @var User $user */
        $user = $this->getUser();
        $trans = $this->get('app.database_translator');
        $startDate = $request->query->get('startDate', null);
        $endDate = $request->query->get('endDate', null);

        $taskerStat = [
            'bidOn' => count($doc->getRepository(Offer::class)->findBidOn($user, $startDate, $endDate)),
            'assigned' => count($doc->getRepository(Offer::class)->findAssigned($user, $startDate, $endDate)),
            'overdue' => count($doc->getRepository(Offer::class)->findOverdue($user, $startDate, $endDate)),
            'awaitingPayment' => count($doc->getRepository(Offer::class)->findAwaitingPayment($user, null, $startDate, $endDate)),
            'awaitingPaymentCash' => count($doc->getRepository(Offer::class)->findAwaitingPayment($user, Task::PAYMENT_TYPE_CASH, $startDate, $endDate)),
            'awaitingPaymentCard' => count($doc->getRepository(Offer::class)->findAwaitingPayment($user, Task::PAYMENT_TYPE_WALLET, $startDate, $endDate)),
            'completed' => count($doc->getRepository(Offer::class)->findCompleted($user, null, $startDate, $endDate)),
            'completedCash' => count($doc->getRepository(Offer::class)->findCompleted($user, Task::PAYMENT_TYPE_CASH, $startDate, $endDate)),
            'completedCard' => count($doc->getRepository(Offer::class)->findCompleted($user, Task::PAYMENT_TYPE_WALLET, $startDate, $endDate)),
            'total' => count($doc->getRepository(Offer::class)->findTotal($user, $startDate, $endDate)),
            'awaitingForReview' => count($doc->getRepository(Offer::class)->findAwaitingReview($user, $startDate, $endDate))
        ];

        $totalReviews = $doc->getRepository(UserReview::class)
            ->findBy(['asA' => User::TYPE_TASKER, 'user' => $user]);
        $totalReviewNote = 0;
        $taskerStat['reviewsNotes'] = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0];
        /** @var UserReview $review */
        foreach ($totalReviews as $review) {
            $totalReviewNote += $review->getNote();
            $taskerStat['reviewsNotes'][$review->getNote()]++;
        }
        $taskerStat['totalReviews'] = count($totalReviews);
        if (count($totalReviews)) {
            $totalReviewNote = $totalReviewNote / count($totalReviews);
        }
        $taskerStat['totalReviewsNote'] = number_format($totalReviewNote, 2);
        $posterStat = [
            'openForOffer' => count($doc->getRepository(Task::class)->findOpenForOffer($user, $startDate, $endDate)),
            'assigned' => count($doc->getRepository(Task::class)->findAssigned($user, $startDate, $endDate)),
            'overdue' => count($doc->getRepository(Task::class)->findOverdue($user, $startDate, $endDate)),
            'awaitingPayment' => count($doc->getRepository(Task::class)->findAwaitingPayment($user, null, $startDate, $endDate)),
            'awaitingPaymentCash' => count($doc->getRepository(Task::class)->findAwaitingPayment($user, Task::PAYMENT_TYPE_CASH, $startDate, $endDate)),
            'awaitingPaymentCard' => count($doc->getRepository(Task::class)->findAwaitingPayment($user, Task::PAYMENT_TYPE_WALLET, $startDate, $endDate)),
            'completed' => count($doc->getRepository(Task::class)->findCompleted($user, null, $startDate, $endDate)),
            'completedCash' => count($doc->getRepository(Task::class)->findCompleted($user, Task::PAYMENT_TYPE_CASH, $startDate, $endDate)),
            'completedCard' => count($doc->getRepository(Task::class)->findCompleted($user, Task::PAYMENT_TYPE_WALLET, $startDate, $endDate)),
            'awaitingForReview' => count($doc->getRepository(Task::class)->findAwaitingReview($user, $startDate, $endDate)),
            'total' => count($doc->getRepository(Task::class)->findTotal($user, $startDate, $endDate))
        ];

        $totalReviews = $doc->getRepository(UserReview::class)
            ->findBy(['asA' => User::TYPE_POSTER, 'user' => $user]);
        $totalReviewNote = 0;
        $posterStat['reviewsNotes'] = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0];
        /** @var UserReview $review */
        foreach ($totalReviews as $review) {
            $totalReviewNote += $review->getNote();
            $posterStat['reviewsNotes'][$review->getNote()]++;
        }
        $posterStat['totalReviews'] = count($totalReviews);
        if (count($totalReviews)) {
            $totalReviewNote = $totalReviewNote / count($totalReviews);
        }
        $posterStat['totalReviewsNote'] = number_format($totalReviewNote, 2);
        /** Add last 10 notifications */
        $notifications = $this->getDoctrine()->getRepository(Notification::class)
            ->findBy(['toUser' => $user], ['createdAt' => 'DESC'], 10);
        $notificationsData = [];
        $oh = $this->get('api.object_filter');
        /** @var Notification $notification */
        foreach ($notifications as $notification) {
            if ($notification->getType() !== Notification::TYPE_MSG) {
                $notificationsData[] = [
                    'id' => $notification->getId(),
                    'title' => $trans->transDb($notification->getTitle(), $notification->getParams()),
                    'body' => $trans->transDb($notification->getBody(), $notification->getParams()),
                    'shownOn' => $notification->isShownOn(),
                    'readOn' => $notification->isReadOn(),
                    'date' => $notification->getCreatedAt(),
                    'user' => $oh->getDataFromObject($notification->getFromUser(), [
                        'id',
                        'firstName',
                        'lastName',
                        'picture' => [
                            'id',
                            'path',
                            'size',
                            'extension'
                        ]
                    ])
                ];
            }
        }
        $haveToPay = $doc->getRepository(Task::class)
            ->getTotalUnpaidTask($user);
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => [
                'balance' => $user->getWallet()->getTotalBalance(),
                'lockedBalance' => $user->getWallet()->getLockedBalance(),
                'haveToPay' => $haveToPay,
                'tasker' => $taskerStat,
                'poster' => $posterStat,
                'notifications' => $notificationsData
            ]
        ];
    }

    /**
     * Get user profile API
     *
     * @Rest\Get("/user/get/profile", name="user_profile", methods={"Get"})
     * @Rest\View(
     *     serializerGroups={"user_data","file_info","user_reviews"}
     * )
     *
     * @param Request $request
     * @return array
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return user profile data",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=200),
     *         @SWG\Property(property="msg", type="string", example="User profile data"),
     *         @SWG\Property(property="data",type="object",ref=@Model(type=User::class,groups={"user_data","file_info"}))
     *     )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="User not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer", example=404),
     *         @SWG\Property(property="msg", type="string", example="User not found"),
     *      )
     * )
     * @SWG\Parameter(
     *     name="user_id",
     *     in="query",
     *     type="string",
     *     description="The user id",
     *     required=true
     * )
     */
    public function profileAction(Request $request): array
    {
        $msg = 'User profile';
        $code = Response::HTTP_OK;
        $userId = $request->get('user_id');

        $translator = $this->get('app.database_translator');
        /** @var User $userProfile */
        $userProfile = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['id' => $userId,'enabled' => true]);

        if (!$userProfile) {
            $msg = $translator->transDb('user_not_found');
            $code = Response::HTTP_NOT_FOUND;
            $data = null;
        } else {
            $reviewSetting = $userProfile->getCountry()->getConfig()->getShowReviewsAfter();
            /** @var User $user */
            $user = $this->getUser();
            /** If the profile of logged in user then set reviews setting to 0  */
            if ($user && $userProfile->getId() === $user->getId()) {
                $reviewSetting = 0;
            }

            $reviewsPoster = $this->getDoctrine()
                ->getRepository(UserReview::class)
                ->findBy(['user' => $userProfile, 'asA' => User::TYPE_POSTER]);
            $reviewsTasker = $this->getDoctrine()
                ->getRepository(UserReview::class)
                ->findBy(['user' => $userProfile, 'asA' => User::TYPE_TASKER]);
            $reviewsPoster = count($reviewsPoster) >= $reviewSetting ? $reviewsPoster : [];
            $reviewsTasker = count($reviewsTasker) >= $reviewSetting ? $reviewsTasker : [];
            $data['user'] = $userProfile;
            $data['completion_rater_tasker'] = $userProfile->getCompletionRateTasker();
            $data['completion_rate_poster'] = $userProfile->getCompletionRatePoster();
            $data['review_note_asa_poster'] = $userProfile->getReviewNoteAsaPoster();
            $data['review_note_asa_tasker'] = $userProfile->getReviewNoteAsaTasker();

            $data['completed_task_tasker'] = count($this->getDoctrine()->getRepository(Offer::class)
                ->findCompleted($userProfile));
            $data['completed_task_poster'] = count($this->getDoctrine()->getRepository(Task::class)
                ->findCompleted($userProfile));


            $data['poster_stars'] = ['1stars' => 0, '2stars' => 0, '3stars' => 0, '4stars' => 0, '5stars' => 0];
            /** @var UserReview $reviewPoster */
            foreach ($reviewsPoster as $reviewPoster) {
                ++$data['poster_stars'][$reviewPoster->getNote() . 'stars'];
            }

            $data['tasker_stars'] = ['1stars' => 0, '2stars' => 0, '3stars' => 0, '4stars' => 0, '5stars' => 0];
            /** @var UserReview $reviewTasker */
            foreach ($reviewsTasker as $reviewTasker) {
                ++$data['tasker_stars'][$reviewTasker->getNote() . 'stars'];
            }

            $data['reviews_tasker'] = $reviewsTasker;
            $data['reviews_poster'] = $reviewsPoster;
        }
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data,
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"user_data","file_info","country_data"}
     * )
     * @Rest\Post("/update_profile")
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function updateAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(UserUpdateForm::class, $user);
        $form->submit($this->get('json.request')->getParameters(), false);
        if ($form->isSubmitted() && $form->isValid()) {
            /** Check if referral code isn't generated yet */
            if ($user->getReferralCode() === $user->getEmail()) {
                $user->setReferralCode(Helper::slugify($user->getFirstName() . '-' . mb_substr($user->getLastName(), 0, 1, 'utf8')) . '-' . $user->getId());
            }
            $em->persist($user);
            if (isset($request->files)) {
                $file = $request->files->get('picture');
                if (is_object($file)) {
                    $fileObject = $this->get('file_uploader')->uploadFile($this->getParameter('picture_directory'), $file, $user);
                    $em->persist($fileObject);
                    $user->setPicture($fileObject);
                }
                $file = $request->files->get('coverPicture');
                if (is_object($file)) {
                    $fileObject = $this->get('file_uploader')->uploadFile($this->getParameter('picture_directory'), $file, $user);
                    $em->persist($fileObject);
                    $user->setCoverPicture($fileObject);
                }
            }
            $em->flush();
            $code = Response::HTTP_OK;
            $msg = 'profile successfully updated';
            $data = $user;
        } else {
            $code = Response::HTTP_BAD_REQUEST;
            $msg = Helper::getErrorsFromForm($form);
            $data = [];
        }
        return [
            'code' => $code,
            'msg' => $this->get('translator')->trans($msg),
            'data' => $data,
        ];
    }

    /**
     * Get user profile API
     *
     * @Rest\Get("/user/balance", name="user_balance", methods={"Get"})
     * @Rest\View()
     */
    public function getUserBalanceAction(): array
    {

        /** @var UserWallet $wallet */
        $wallet = $this->getUser()->getWallet();
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'user balance',
            'data' => [
                'balance' => $wallet->getTotalBalance(),
                'locked' => $wallet->getLockedBalance()
            ]
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"user_data"}
     * )
     * @Rest\Get("/user/skills")
     * @return array
     */
    public function userSkillsAction(): array
    {
        /** @var User $user */
        $user = $this->getUser();
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'User skills',
            'data' => $user->getSkills(),
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"user_data"}
     * )
     * @Rest\Post("/update_skills")
     * @param Request $request
     * @return array
     */
    public function updateSkillsAction(Request $request): array
    {
        $data = $request->request->all();
        /** @var User $user */
        $user = $this->getUser();
        $skills = $user->getSkills();
        if (!$skills) {
            $skills = new Skills();
            $user->setSkills($skills);
        }
        if (isset($data['good_at'])) {
            $skills->setGoodAt(array_filter($data['good_at'],static function($elem){ return !empty($elem);}));
        }
        if (isset($data['get_around'])) {
            $skills->setGetAround(array_filter($data['get_around'],static function($elem){ return !empty($elem);}));
        }
        if (isset($data['languages'])) {
            $skills->setLanguages(array_filter($data['languages'],static function($elem){ return !empty($elem);}));
        }
        if (isset($data['qualifications'])) {
            $skills->setQualifications(array_filter($data['qualifications'],static function($elem){ return !empty($elem);}));
        }
        if (isset($data['experience'])) {
            $skills->setExperience(array_filter($data['experience'],static function($elem){ return !empty($elem);}));
        }
        if (isset($data['education'])) {
            $skills->setEducation(array_filter($data['education'],static function($elem){ return !empty($elem);}));
        }
        if (isset($data['work'])) {
            $skills->setWork(array_filter($data['work'],static function($elem){ return !empty($elem);}));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return [
            'code' => Response::HTTP_OK,
            'msg' => $this->get('app.database_translator')->transDb('skill.update.success'),
            'data' => $user->getSkills(),
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"notif_config"}
     * )
     * @Rest\Post("/user/notification_config")
     * @param Request $request
     * @return array
     */
    public function notificationsConfigAction(Request $request): array
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        try {
            if (isset($data['action']) && in_array(strtoupper($data['action']), NotificationConfig::$actions, true)) {
                $config = $em->getRepository(NotificationConfig::class)
                    ->findOneBy(['action' => strtoupper($data['action']), 'user' => $user]);

                if (!is_object($config)) {
                    $config = new NotificationConfig();
                    $config->setUser($user);
                    $config->setAction(strtoupper($data['action']));
                }
                $config->setSms((int)$data['sms'] > 0);
                $config->setPush((int)$data['push'] > 0);
                $config->setEmail((int)$data['email'] > 0);
                $em->persist($config);
                $em->flush();

                $data = $em->getRepository(NotificationConfig::class)
                    ->findBy(['user' => $user]);
                $msg = 'update_success';
                $code = Response::HTTP_OK;
            } else {
                throw new InvalidParameterException('invalid_request');
            }
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
            $data = null;
        }
        return [
            'code' => $code,
            'msg' => $this->get('app.database_translator')->transDb($msg),
            'data' => $data,
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Get("/user/notification_config")
     * @return array
     */
    public function getNotificationsConfigAction(): array
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if (!is_object($user)) {
            throw new InvalidParameterException('invalid User');
        }
        $userNotificationConfig = $em->getRepository(NotificationConfig::class)
            ->findBy(['user' => $user]);

        $oh = $this->get('api.object_filter');
        $data = $oh->getDataFromArray($userNotificationConfig, [
            'id',
            'sms',
            'push',
            'email',
            'action'
        ]);
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'Notification config list',
            'data' => $data,
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/profile_completed")
     * @return array
     */
    public function profileCompletedAction(): array
    {
        $user = $this->getUser();
        $translator = $this->get('app.database_translator');
        $errors = [];
        if (!is_object($user->getPicture())) {
            $errors['profilePicture'] = $translator->transDb('complete_profile.error.add_picture');
        }

        if (!is_object($user->getBirthDate())) {
            $errors['birthDate'] = $translator->transDb('complete_profile.error.add_birthdate');
        }

        if (!is_object($user->getPhone())) {
            $errors['phone'] = $translator->transDb('complete_profile.error.add_phone');
        }

        return [
            'code' => Response::HTTP_OK,
            'msg' => 'Profile completion',
            'data' => [
                'verified' => count($errors) === 0,
                'errors' => $errors ?? []
            ],
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"user_reviews","user_info","file_info"}
     * )
     * @Rest\Get("/user/reviews")
     * @return array
     */
    public function getReviewsAction(): array
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $reviews = $em->getRepository(UserReview::class)
            ->findBy(['user' => $user]);
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'reviews',
            'data' => $reviews
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/user/review/add")
     * @return array
     */
    public function addReviewAction(): array
    {
        $em = $this->getDoctrine()->getManager();
        $dbTranslator = $this->get('app.database_translator');
        /** @var User $user */
        $user = $this->getUser();
        $userReview = new UserReview();
        $userReview->setCreatedBy($user);
        $form = $this->createForm(UserReviewForm::class, $userReview);
        $form->submit($this->get('json.request')->getParameters());
        if ($form->isValid() && $userReview->getUser() && $userReview->getTask() && $userReview->getTask()->is(Task::STATUS_PAID)
            && $user->getId() !== $userReview->getUser()->getId()) {
            $poster = $userReview->getTask()->getUser();
            $tasker = $userReview->getTask()->getAcceptedOffer()->getUser();
            if (!in_array($user->getId(), [$poster->getId(), $tasker->getId()], true)) {
                return [
                    'code' => Response::HTTP_UNAUTHORIZED,
                    'msg' => $dbTranslator->transDb('invalid_request')
                ];
            }
            /** Check if already reviewed */
            $userReviewed = $em->getRepository(UserReview::class)
                ->findOneBy(['user' => $userReview->getUser(), 'task' => $userReview->getTask()]);
            if ($userReviewed) {
                return [
                    'code' => Response::HTTP_CONFLICT,
                    'msg' => $dbTranslator->transDb('review.add.already_reviewed')
                ];
            }
            $userToReviewType = $user->getId() === $tasker->getId() ? User::TYPE_POSTER : User::TYPE_TASKER;
            $userReview->setAsA($userToReviewType);
            $em->persist($userReview);

            $notification = new Notification();
            $notification->setTitle('notification.reviewed.title');
            $notification->setBody('notification.reviewed.body');
            $notification->setToUser($userReview->getUser());
            $notification->setFromUser($user);
            $notification->setExtra([
                'type' => Notification::TYPE_REVIEW,
                'task_id' => $userReview->getTask()->getId()
            ]);
            $notification->setType(Notification::ACTION_TASK_UPDATE);
            $notification->setParams([
                '#user#' => $user->getName(),
                '#task#' => $userReview->getTask()->getTitle(),
            ]);
            $em->persist($notification);
            $em->flush();

            /** If both tasker and poster reviewed close discussion & change task status to completed */
            if (count($em->getRepository(UserReview::class)->findBy(['task' => $userReview->getTask()])) === 2) {
                $userReview->getTask()->setStatus(Task::STATUS_COMPLETED);
                $em->persist($userReview->getTask());
                /** Close discussion between tasker and poster */
                $discussion = $em->getRepository(TaskDiscussion::class)->findOneBy(['task' => $userReview->getTask()]);
                if ($discussion) {
                    $discussion->setClosed(true);
                    $em->persist($discussion);
                }
                $em->flush();
            }
            return [
                'code' => Response::HTTP_OK,
                'msg' => $dbTranslator->transDb('review.add.success'),
            ];
        }
        return [
            'code' => Response::HTTP_BAD_REQUEST,
            'msg' => $dbTranslator->transDb('invalid_request')
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"user_documents","file_info"}
     * )
     * @Rest\Get("/user/documents")
     * @param Request $request
     * @return array
     */
    public function getDocumentsAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $documents = $em->getRepository(UserDocument::class)->findBy(['user' => $user]);
        /** @var UserDocument $document */
        foreach ($documents as $document) {
            $document->getType()->setDefaultLocale($request->getLocale());
        }
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'User documents',
            'data' => $documents
        ];
    }

    /**
     * @Rest\View(
     *     serializerGroups={"user_documents","file_info"}
     * )
     * @Rest\Post("/user/document/update")
     * @param Request $request
     * @return array
     */
    public function updateDocumentAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $translator = $this->get('app.database_translator');
        /** @var UserDocument $document */
        $document = $em->getRepository(UserDocument::class)
            ->findOneBy(['id' => $request->get('id'), 'user' => $user]);
        if (!$document) {
            return [
                'code' => Response::HTTP_BAD_REQUEST,
                'msg' => $translator->transDb('document_not_found')
            ];
        }
        if($document->isVerified()){
            return [
                'code' => Response::HTTP_FORBIDDEN,
                'msg' => $translator->transDb('invalid_request')
            ];
        }
        if ($request->request->has('description')) {
            $document->setDescription($request->get('description'));
        }

        $deletedFiles = $request->get('deleted_files', []);
        foreach ($deletedFiles as $deletedFile) {
            $file = $em->getRepository(File::class)
                ->findOneBy(['id' => $deletedFile, 'userDocument' => $document]);
            if ($file) {
                $em->remove($file);
            }
        }
        $files = $request->files->get('files') ?? [];
        foreach ($files as $file) {
            $fileObject = $this->get('file_uploader')->uploadFile($this->getParameter('document_directory'), $file, $user);
            $em->persist($fileObject);
            $document->addFile($fileObject);
        }
        $document->setVerificationDate(null);
        $document->setRequestVerificationDate(new \DateTime());
        $document->setStatus(UserDocument::STATUS_WAITING_FOR_VERIFICATION);
        $em->persist($document);
        if(count($files)){
            /** Create notification for admin */
            $notification = new Notification();
            $notification->setFromUser($user);
            $notification->setType(Notification::ADMIN_NEW_DOCUMENT);
            $notification->setTitle('notification.admin.document_title');
            $notification->setBody('notification.admin.document_body');
            $notification->setParams([
                '_user_' => $user->getName()
            ]);
            $notification->setLink($this->generateUrl('user_documents'));
            $this->get('notify_admin')->notifyAdmin($notification);
        }
        $em->flush();
        return [
            'code' => Response::HTTP_OK,
            'msg' => $translator->transDb('user_document.update.success'),
            'data' => $document
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/user/document/delete")
     * @param Request $request
     * @return array
     */
    public function deleteDocumentAction(Request $request): array
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $translator = $this->get('app.database_translator');
        /** @var UserDocument $document */
        $document = $em->getRepository(UserDocument::class)
            ->findOneBy(['id' => $request->get('id'), 'user' => $user]);
        if (!$document) {
            return [
                'code' => Response::HTTP_NOT_FOUND,
                'msg' => $translator->transDb('document_not_found')
            ];
        }
        if ($document->isVerified()) {
            return [
                'code' => Response::HTTP_FORBIDDEN,
                'msg' => $translator->transDb('user_document.delete.verified')
            ];
        }
        $document->setStatus(0);
        $document->setDescription('');
        foreach ($document->getFiles() as $file) {
            $em->remove($file);
        }
        $em->flush();
        return [
            'code' => Response::HTTP_OK,
            'msg' => $translator->transDb('user_document.delete.success'),
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/user/portfolio/delete")
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function deletePortfolioAction(Request $request): array
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $fileId = $request->get('file_id');
        $translator = $this->get('app.database_translator');
        $portfolio = $em->getRepository(File::class)
            ->findOneBy(['userPortfolio' => $user, 'id' => $fileId]);
        if (!$portfolio) {
            return [
                'code' => Response::HTTP_BAD_REQUEST,
                'msg' => $translator->transDb('file_not_found'),
            ];
        }
        $em->remove($portfolio);
        $em->flush();
        return [
            'code' => Response::HTTP_OK,
            'msg' => $translator->transDb('file.delete.success'),
        ];
    }

    /**
     * @Rest\View()
     * @Rest\Post("/user/portfolio/add")
     * @param Request $request
     * @return array
     */
    public function addPortfolioAction(Request $request): array
    {
        $msg = 'Add portfolio success';
        $code = Response::HTTP_OK;
        $data = null;
        /** @var User $user */
        $user = $this->getUser();
        if (isset($request->files)) {
            $em = $this->getDoctrine()->getManager();
            $file = $request->files->get('file');
            if (is_object($file)) {
                $portfolioFile = $this->get('file_uploader')->uploadFile(
                    $this->getParameter('portfolios_directory'), $file, $user
                );
                $user->addPortfolio($portfolioFile);
                $em->persist($portfolioFile);
                $em->flush();
                $oh = $this->get('api.object_filter');
                $data = $oh->getData($portfolioFile, [
                    'id',
                    'path',
                    'extension'
                ]);
            }
        }
        return [
            'code' => $code,
            'msg' => $this->get('translator')->trans($msg),
            'data' => $data
        ];
    }


    /**
     * @Rest\View()
     * @Rest\Get("/user/balance")
     * @return array
     */
    public function getBalanceAction(): array
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        /** @var UserWallet $userBalance */
        $userBalance = $em->getRepository(UserWallet::class)
            ->findOneBy(['user' => $user, 'active' => true]);
        if (!$userBalance) {
            $data = array('balance' => 0);
        } else {
            $data = array('balance' => $userBalance->getBalance());
        }

        return [
            'code' => Response::HTTP_OK,
            'msg' => 'user balance',
            'data' => $data
        ];
    }


    /**
     * @Rest\View()
     * @Rest\Post("/user/set_fcm_token")
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function setFcmTokenAction(Request $request): array
    {
        $msg = 'Fcm token stored';
        $em = $this->getDoctrine()->getManager();
        $code = Response::HTTP_OK;
        try {
            /** @var User $user */
            $user = $this->getUser();
            if (empty($request->get('fcm_token_ios')) && empty($request->get('fcm_token_android'))) {
                throw new InvalidParameterException('No fcm token provided');
            }
            if ($request->request->has('fcm_token_ios')) {
                $user->setIosFcmToken($request->get('fcm_token_ios'));
                $user->setSource(User::SOURCE_IPHONE);
                $user->setAndroidFcmToken(null);
            }
            if ($request->request->has('fcm_token_android')) {
                $user->setAndroidFcmToken($request->get('fcm_token_android'));
                $user->setSource(User::SOURCE_ANDROID);
                $user->setIosFcmToken(null);
            }
            $em->persist($user);
            $em->flush();
            $this->sendWelcomeBackNotification($user);
        } catch (Exception $e) {
            $code = $e->getCode();
            $msg = $e->getMessage();
        }
        return [
            'code' => $code,
            'msg' => $this->get('translator')->trans($msg),
        ];
    }

    /**
     * @param User $user
     */
    private function sendWelcomeBackNotification(User $user): void
    {
        $trans = $this->get('app.database_translator');
        $message['title'] = $trans->transDb('notification.welcome_back',[],$user->getLanguage());
        $message['body'] = '';
        $message['to'] = $user->getAndroidFcmToken()??$user->getAndroidFcmToken();
        $message['data'] = ['type'=>''];
        $message['icon']  = $this->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost().'/assets/img/logo/logo_v3.png';
        $fcm = $this->get('fcm_client');
        $fcm->createMessage($message)->sendMessage();
    }

    /**
     * @Rest\View()
     * @Rest\Get("/user/transactions")
     * @param Request $request
     * @return array
     */
    public function getTransactionAction(Request $request): array
    {
        $dbTranslator = $this->get('app.database_translator');
        $user = $this->getUser();
        $em = $this->getDoctrine();
        if ($request->get('export')) {
            $allTransaction = $em->getRepository(Transaction::class)
                ->createQueryBuilder('t')
                ->where('t.fromUser = :user or t.toUser = :user')
                ->andWhere('t.status = 2')
                ->setParameter('user', $user)->getQuery()->getResult();

            $lockedBalance = $em->getRepository(Task::class)->findUserLockedBalanceTasks($user);
            $withdrawRequest = $em->getRepository(Transaction::class)->findLockedBalance($user);
            $taskInvoices = $em->getRepository(Transaction::class)->findLockedInvoiceBalance($user);

            /** Sort */
            $lockedBalance = array_merge($lockedBalance, $withdrawRequest,$taskInvoices);
            usort($lockedBalance, static function ($a, $b) {
                return $a->getCreatedAt() > $b->getCreatedAt() ? 1 : -1;
            });
            $view = $this->renderView('@Api/payment-history.html.twig', [
                'transactions' => $allTransaction,
                'lockedBalance' => $lockedBalance,
                'user' => $user
            ]);
            return [
                'code' => Response::HTTP_OK,
                'msg' => 'transactions',
                'data' => $view
            ];
        }

        $transactionOut = $em->getRepository(Transaction::class)
            ->findBy(['fromUser' => $user, 'status' => Transaction::STATUS_COMPLETED]);
        $transactionIn = $em->getRepository(Transaction::class)
            ->findBy(['toUser' => $user, 'status' => Transaction::STATUS_COMPLETED]);

        $data = $em->getRepository(Transaction::class)
            ->getNetTransaction($user);


        $data['transactionIn'] = [];
        $data['transactionOut'] = [];
        /** @var Transaction $transaction */
        foreach ($transactionIn as $transaction) {
            $data['transactionIn'][] = [
                'id' => $transaction->getId(),
                'createdAt' => $transaction->getCreatedAt(),
                'user' => $transaction->getFromUser() ? [
                    'id' => $transaction->getFromUser()->getId(),
                    'first_name' => $transaction->getFromUser()->getFirstName(),
                    'last_name' => $transaction->getFromUser()->getLastName(),
                    'picture' => [
                        'path' => $transaction->getFromUser()->getPicture() ? $transaction->getFromUser()->getPicture()->getPath() : ''
                    ]
                ] : null,
                'price' => $transaction->getPrice(),
                'type' => $dbTranslator->transDb('transaction.type.' . $transaction->getType()),
                'payment_method' => $transaction->getPaymentMethod(),
                'task' => $transaction->getTask() ? [
                    'id' => $transaction->getTask()->getId(),
                    'title' => $transaction->getTask()->getTitle()
                ] : null,
                'isCash' => $transaction->isCash(),
            ];
        }
        /** @var Transaction $transaction */
        foreach ($transactionOut as $transaction) {
            $data['transactionOut'][] = [
                'id' => $transaction->getId(),
                'createdAt' => $transaction->getCreatedAt(),
                'user' => $transaction->getToUser() ? [
                    'id' => $transaction->getToUser()->getId(),
                    'first_name' => $transaction->getToUser()->getFirstName(),
                    'last_name' => $transaction->getToUser()->getLastName(),
                    'picture' => [
                        'path' => $transaction->getToUser()->getPicture() ? $transaction->getToUser()->getPicture()->getPath() : ''
                    ]
                ] : null,
                'price' => $transaction->getPrice($user),
                'type' => $dbTranslator->transDb('transaction.type.' . $transaction->getType()),
                'payment_method' => $transaction->getPaymentMethod(),
                'task' => $transaction->getTask() ? [
                    'id' => $transaction->getTask()->getId(),
                    'title' => $transaction->getTask()->getTitle()
                ] : null,
                'isCash' => $transaction->isCash(),
                'invoice' => $transaction->getWithdrawInvoice() ? ['path' => $transaction->getWithdrawInvoice()->getPath()] : null
            ];
        }
        return [
            'code' => Response::HTTP_OK,
            'msg' => 'Transaction historic',
            'data' => $data
        ];
    }


    /**
     * @Rest\View(
     *     serializerGroups={"user_data"}
     * )
     * @Rest\Post("/user/bank-account/update")
     * @return array
     */
    public function editBankAccountAction(): array
    {
        $dbTranslator = $this->get('app.database_translator');
        /** @var User $user */
        $user = $this->getUser();
        $bankAccount = $user->getBankAccount() ?? new UserBankAccount();
        $form = $this->createForm(bankAccountForm::class, $bankAccount);
        $form->submit($this->get('json.request')->getParameters(),false);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setBankAccount($bankAccount);
            $em->persist($user);
            $em->flush();
            return [
                'code' => Response::HTTP_OK,
                'msg' => $dbTranslator->transDb('bank_account.edit.success'),
                'data' => $bankAccount
            ];
        }
        return [
            'code' => Response::HTTP_BAD_REQUEST,
            'msg' => $dbTranslator->transDb('invalid_request'),
            'data' => null
        ];
    }


    /**
     * @Rest\View()
     * @Rest\Post("/user/update-password")
     * @param Request $request
     * @return array
     */
    public function updatePasswordAction(Request $request): array
    {

        $user = $this->getUser();
        $trans = $this->get('app.database_translator');
        $oldPassword = $request->get('old_password', null);
        $newPassword = $request->get('new_password', null);

        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $oldPassword);

        if (empty($oldPassword) || empty($newPassword)) {
            $code = Response::HTTP_BAD_REQUEST;
            $msg = $trans->transDb('invalid_request');
        } elseif (!$isValid) {
            $code = Response::HTTP_BAD_REQUEST;
            $msg = $trans->transDb('reset_password.invalid_old_password');
        } else {
            /** @var $userManager UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $user->setPlainPassword($newPassword);
            $userManager->updateUser($user);
            $code = Response::HTTP_OK;
            $msg = $trans->transDb('reset_password.success');
        }

        return [
            'code' => $code,
            'msg' => $msg
        ];
    }


    /**
     * @Rest\View()
     * @Rest\Post("/user/phone-verification")
     * @param Request $request
     * @return array
     */
    public function phoneNumberVerificationAction(Request $request): array
    {
        $trans = $this->get('app.database_translator');
        $phone = $request->get('phone');
        /** @var User $user */
        $user = $this->getUser();
        if (!empty($phone) && $user->getPhone() !== $phone) {
            $em = $this->getDoctrine()->getManager();
            /** @var PhoneVerificationRequest $verificationRequest */
            $verificationRequest = $em->getRepository(PhoneVerificationRequest::class)->findOneBy(['userId'=>$user->getId()]);
            if (($code = $request->get('code')) && $verificationRequest->getPhoneNumber() === $phone) {
                if ($verificationRequest->getVerificationCode() !== $code) {
                    return [
                        'code' => Response::HTTP_BAD_REQUEST,
                        'msg' => $trans->transDb('phone_verification_failed')
                    ];
                }
                /** If user has an old phone number than save it */
                if(!empty($user->getPhone())){
                    $oldPhone = new UserOldPhones();
                    $oldPhone->setUser($user);
                    $oldPhone->setPhoneNumber($user->getPhone());
                    $em->persist($oldPhone);
                }
                /** Replace the new phone number */
                $user->setPhone($phone);
                /** Validate phone badge */
                $phoneBadge = $em->getRepository(Badge::class)->findOneBy(['name' => Badge::Mobile]);
                /** @var UserBadge $userPhoneBadge */
                $userPhoneBadge = $em->getRepository(UserBadge::class)->findOneBy(['user' => $user, 'badge' => $phoneBadge]);
                $userPhoneBadge->setActive(true);
                $userPhoneBadge->setActivationDate(new \DateTime());
                $userPhoneBadge->setData($phone);
                $em->persist($userPhoneBadge);
                $em->persist($user);
                Helper::setCountryTimeZone($user->getCountry()->getIdentifier());
                $userBan = $this->checkUserPhoneBanned($user,$phone);
                $em->remove($verificationRequest);
                $em->flush();
                $response = [
                    'code' => Response::HTTP_OK,
                    'msg' => $trans->transDb('phone_verification_succeeded')
                ];
                if($userBan){
                    $dbTranslator = $this->get('app.database_translator');
                    $banMsg = $dbTranslator->transDb(
                        $userBan->isPermanent() ?
                            'account_permanently_banned' :
                            'account_suspended_to_date'
                        , ['#date#' => $userBan->getUnbanDate() ? $userBan->getUnbanDate()->format('d-m-Y H:i') : '-']);
                    $response['data'] = ['banned' => true, 'msg' => $banMsg];
                }
                return $response;
            }

            if($verificationRequest && $verificationRequest->getRequestedAt()->modify('+1 hour')>new \DateTime()){
                $msg = $trans->transDb('phone_verification_code_already_sent',
                    ['#time#'=>$verificationRequest->getRequestedAt()->modify('+1 hour')->diff(new \DateTime())->i]);
                $code = Response::HTTP_TOO_EARLY;
            }else{
                if (!$verificationRequest) {
                    $verificationRequest = new PhoneVerificationRequest();
                    $verificationRequest->setUserId($user->getId());
                }
                $verificationRequest->setPhoneNumber($phone);
                $verificationRequest->setVerificationCode();
                $verificationRequest->setRequestedAt();
                /** Call whatsapp api to send the code */
                try{
                    $msg = $trans->transDb('phone_verification_message',['#code#'=> $verificationRequest->getVerificationCode()]);
                    $this->get('app.whatsapp')->sendMessage($msg, $verificationRequest->getPhoneNumber());
                    $em->persist($verificationRequest);
                    $em->flush();
                    return [
                        'code' => Response::HTTP_OK,
                        'msg' => $trans->transDb('phone_verification_code_sent')
                    ];
                }catch (Exception $e){
                    $msg = $e->getMessage();
                    $code = Response::HTTP_BAD_REQUEST;
                }
            }
        }else{
            $msg = $trans->transDb('phone_already_verified');
            $code = Response::HTTP_ACCEPTED;
        }
        return [
            'code' => $code,
            'msg' => $msg
        ];
    }

    /**
     * @param User $user
     * @param $phone
     * @return UserBan
     */
    private function checkUserPhoneBanned(User $user,$phone): ?UserBan
    {
        $em = $this->getDoctrine()->getManager();
        $bannedUser = $em->getRepository(User::class)
            ->createQueryBuilder('u')
            ->leftJoin('u.ban', 'ban')
            ->where('u.phone = :phone')
            ->andWhere('ban.permanent = 1 or ban.unbanDate >= :currentDate')
            ->andWhere('ban.banLifted = 0')
            ->setParameters(['phone' =>$phone, 'currentDate' => new DateTime()])
            ->getQuery()->getResult();
        if (count($bannedUser) > 0) {
            /** @var UserBan $newBan */
            $newBan = clone $bannedUser[0]->getActiveBan();
            $newBan->setId('');
            $newBan->setUser($user);
            $em->persist($newBan);
            $em->flush();
            return $newBan;
        }
        return null;
    }
}
