<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 01/09/2019
 * Time: 23:20
 */

namespace ApiBundle\Listener;


use ApiBundle\Services\DBTranslator;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Translation\Translator;


class LocaleListener implements EventSubscriberInterface
{

    private $defaultLocale;
    private $acceptedLocales;
    private $dbTranslator;
    private $translator;
    private $tokenStorage;
    private $em;

    public function __construct(
        $defaultLocale,
        $acceptedLocales,
        Translator $translator,
        DBTranslator $dbTranslator,
        TokenStorage $token,
        EntityManagerInterface $entityManager)
    {
        $this->defaultLocale = $defaultLocale;
        $this->acceptedLocales = $acceptedLocales;
        $this->translator = $translator;
        $this->dbTranslator = $dbTranslator;
        $this->tokenStorage = $token;
        $this->em = $entityManager;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!empty($newLocale = $request->headers->get('lang'))) {
            if (in_array($newLocale, $this->acceptedLocales)) {
                $this->translator->setLocale($newLocale);
                $request->setLocale($newLocale);
                $this->dbTranslator->setLocale($newLocale);
            }else{
                $this->translator->setLocale($this->defaultLocale);
            }
            if($token = $this->tokenStorage->getToken() ){
                if($token->getUser() instanceof UserInterface){
                    $token->getUser()->setLanguage($this->translator->getLocale());
                    $this->em->persist($token->getUser());
                    $this->em->flush();
                }
            }
        }else{
            $this->dbTranslator->setLocale($request->getLocale());
        }
    }

    public static function getSubscribedEvents()
    {
        return [KernelEvents::REQUEST => [['onKernelRequest', 200]]];
    }
}