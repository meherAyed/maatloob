<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 22/09/2019
 * Time: 22:15
 */

namespace ApiBundle\Services;


use AppBundle\Entity\LanguageMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;

class DBTranslator
{
    private $em;
    private $translator;
    private $locale;

    public function __construct(EntityManagerInterface $em,TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
    }

    /**
     * Translate message from database
     * @param $identifier
     * @param array $params
     * @param string $language
     * @return string
     */

    public function transDb($identifier, $params = [], $language = null)
    {
        if(!$language){
            $language = $this->locale;
        }
        $translation = $this->em->getRepository(LanguageMessage::class)
            ->findOneBy(['language' => $language, 'identifier' => $identifier]);
        if ($translation) {
            $translation = $translation->getMessage();
            foreach ($params as $key => $param) {
                $translation = str_replace($key, $param, $translation);
            }
        } else {
            $translation = $this->translator->trans($identifier,$params);
        }
        return $translation;
    }

    public function setLocale(string $locale){
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }
}