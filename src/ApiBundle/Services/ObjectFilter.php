<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ApiBundle\Services;

use AppBundle\Entity\Country;
use AppBundle\Entity\Task;
use AppBundle\Repository\CustomQueryBuilder;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class ObjectFilter
{

    private $em;
    private $container;

    public function __construct(EntityManager $entityManager, Container $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    function objectToArray($adminBar)
    {
        $reflector = new \ReflectionObject($adminBar);
        $nodes = $reflector->getProperties();
        $out = [];
        foreach ($nodes as $node) {
            $nod = $reflector->getProperty($node->getName());
            $nod->setAccessible(true);
            $out[$node->getName()] = $nod->getValue($adminBar);
        }
        return $out;
    }

    function getObjectMethods($object)
    {
        $className = get_class($object);
        $methods = get_class_methods($className);

        $methodLists = [];
        foreach ($methods as $method) {
            $methodLists[strtolower($method)] = $method;
        }
        return $methodLists;
    }

    function getDataFromObject($object, $data)
    {
        if ($object) {
            $result = [];
            $methodLists = $this->getObjectMethods($object);
            foreach ($data as $index => $value) {
                if (is_array($value)) {
                    $value_data = $object->{'get' . ucfirst($index)}();
                    if ($value_data instanceOf \Doctrine\Common\Collections\Collection)
                        $value_data = $value_data->getValues();
                    $result[$index] = $this->getData($value_data, $value);
                } else
                    if (array_key_exists('get' . strtolower($value), $methodLists)) {
                        $list = $object->{$methodLists['get' . strtolower($value)]}();
                        if ($list instanceOf \Doctrine\Common\Collections\Collection)
                            $result[$value] = $list->getValues();
                        else
                            $result[$value] = $list;
                    } elseif (array_key_exists('is' . strtolower($value), $methodLists))
                        $result[$value] = $object->{$methodLists['is' . strtolower($value)]}();
            }
            if (count($result) == 0)
                return null;
            else
                return $result;
        }
        return null;
    }

    function getData($object, $data)
    {
        if (is_array($object))
            return $this->getDataFromArray($object, $data);
        if (is_object($object))
            if (!$object instanceof \DateTime)
                return $this->getDataFromObject($object, $data);

        return $object;
    }

    public function getDataFromArray($array, $data): array
    {
        $result = [];
        if (is_array($array)) {
            foreach ($array as $object) {
                if (is_array($object)) {
                    $result[] = $this->getDataFromArray($object, $data);
                } else {
                    $result[] = $this->getDataFromObject($object, $data);
                }
            }
        }
        return $result;
    }

    /**
     * @param CustomQueryBuilder $repository
     * @param $filters
     * @param Country $country
     * @return CustomQueryBuilder
     */
    public function getRequestFromFilter(CustomQueryBuilder $repository, $filters, Country $country): CustomQueryBuilder
    {
        if (array_key_exists('keyword', $filters)) {
            $repository->andWhere('t.title like :keyword or t.description like :keyword')
                ->setParameter('keyword', '%' . $filters['keyword'] . '%');
            unset($filters['keyword']);
        }
        $repository->join('country.currencyExchange','exchange','with','exchange.toCurrency = :currentCountryCurrency')
            ->setParameter('currentCountryCurrency', $country->getCurrencyCode());

        $counter = 0;
        foreach ($filters as $index => $value) {
            $compare = '=';
            if ($index === 'eg') {
                $compare = '=';
            }
            if ($index === 'lte') {
                $compare = '<=';
            }
            if ($index === 'gte') {
                $compare = '>=';
            }
            if ($index === 'lt') {
                $compare = '<';
            }
            if ($index === 'gt') {
                $compare = '>';
            }
            if ($index === 'ne') {
                $compare = '<>';
            }
            if ($index === 'cnt') {
                $compare = 'like';
            }
            foreach ($value as $column => $columnValue) {
                $entity = $repository->getRootEntities()[0];
                if ($column === 'place') {
                    $repository->join('t.address', 'a', 'with', 't.address = a.id')
                        ->andWhere('a.place like :place')
                        ->setParameter('place', '%' . $columnValue . '%');
                }
                if (property_exists($entity, $column)) {
                    if ($columnValue === 'true') {
                        $columnValue = 1;
                    } elseif ($columnValue === 'false') {
                        $columnValue = 0;
                    }

                    if ($column === 'price') {
                        $repository
                            ->andWhere('( (t.price * exchange.rate ) ' . $compare . ' :' . ($column . $counter) . ')');
                        $repository->setParameter(($column . $counter), ($columnValue));

                    } else if ($compare === 'like') {
                        $repository->andWhere('(LOWER( t.' . $column . ') ' . $compare . " '%" . strtolower($columnValue) . "%'  )");
                    } else {
                        $repository->andWhere('( t.' . $column . ' ' . $compare . ' :' . ($column . $counter) . ')');
                        $repository->setParameter(($column . $counter), ($columnValue));
                    }
                    $counter++;
                }
            }
            $counter++;
        }

        return $repository;
    }
}
