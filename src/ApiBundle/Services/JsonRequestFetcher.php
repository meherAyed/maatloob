<?php
/**
 * Created by PhpStorm.
 * User: Mobelite
 * Date: 22/11/2018
 * Time: 11:10
 */

namespace ApiBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class JsonRequestFetcher
 * @package API\CoreBundle\Services
 */
class JsonRequestFetcher
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * JsonRequestFetcher constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param string $from
     * @return mixed|null
     */
    public function getParameters(string $from = 'request')
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($request === null) {
            return null;
        }

        $params = $request->$from->all();
        if (count($params) === 0) {
            $params = json_decode($request->getContent(), true);
        }

        return $params;
    }

    /**
     * @param string $from
     * @param string $parameter
     * @return null
     */
    public function getParameter(string $parameter, string $from = 'request')
    {
        return $this->getParameters($from)[$parameter] ?? null;
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getFile(string $name)
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($request === null) {
            return null;
        }

        return $request->files->has($name) ? $request->files->get($name) : null;
    }
}
