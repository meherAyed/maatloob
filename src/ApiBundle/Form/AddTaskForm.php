<?php
/**
 * Created by PhpStorm.
 * Date: 09/11/2018
 * Time: 11:31
 */

namespace ApiBundle\Form;

use AppBundle\Entity\Task;
use AppBundle\Entity\TaskCategory;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class AddTaskForm
 * @package ApiBundle\Form
 */
class AddTaskForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'documentation' => [
                    'required' => true,
                    'description' => 'task title',
                    'example' => 'Create a website'
                ],
                'constraints' => [new NotBlank(['message'=>'task.add.empty_title'])]
            ])
            ->add('description', TextType::class, [
                'documentation' => [
                    'required' => true,
                    'description' => 'a short description for this task',
                    'example' => 'i need a web developer to create for me an amazing website'
                ],
                'required' => true,
                'constraints' => [new NotBlank(['message'=>'task.add.empty_description'])]
            ])
            ->add('category', EntityType::class, [
                'choices' => $options['categories'],
                'choice_label' => static function (TaskCategory $entity) {
                    return $entity->translate()->getName()??'';
                },
                'choice_value' => static function (?TaskCategory $entity) {
                    return $entity ? (int)$entity->getId() : 0;
                },
                'class' => TaskCategory::class,
                'documentation' => [
                    'required' => true,
                    'description' => 'Category id',
                    'example' => 1
                ],
                'invalid_message' => 'task.add.invalid_category',
                'required' => true,
                'constraints' => [new NotBlank(['message'=>'task.add.invalid_category'])]
            ])
            ->add('address',AddressForm::class,[
                'entity_manager'=>$options['entity_manager'],
                'user'=>$options['user'],
                'translator'=>$options['translator']
            ])
            ->add('delivery_address',AddressForm::class,[
                'property_path'=>'deliveryAddress',
                'entity_manager'=>$options['entity_manager'],
                'user'=>$options['user'],
                'translator'=>$options['translator']
            ])
            ->add('is_online_work',CheckboxType::class, [
                'property_path' => 'isOnlineWork',
                'documentation' => [
                    'name' => 'is_online_work',
                    'required' => true,
                    'description' => 'Can the job be done remotely 1 remotely 0 in place',
                    'example' => 0
                ],
                'required' => true,
            ]);
            $builder->addEventListener(
                FormEvents::PRE_SUBMIT ,
                static function (FormEvent $event) use ($options) {
                    /** @var EntityManager $em */
                    $em = $options['entity_manager'];
                    $data = $event->getData();
                    $form = $event->getForm();
                    $deliveryCategory =$em->getRepository(TaskCategory::class)
                        ->findOneBy(['type'=>TaskCategory::DELIVERY_TYPE]);
                    if (isset($data['category']) && (int)$data['category'] !== $deliveryCategory->getId()) {
                        $form->remove('delivery_address')->remove('duration');
                    }
                    if(isset($data['is_online_work'])){
                        $data['is_online_work'] = (bool)$data['is_online_work'];
                        if ($data['is_online_work']) {
                            $form->remove('address')->remove('delivery_address');
                        }
                    }
                    $event->setData($data);
                }
            );
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
            'categories'=>[],
            'entity_manager' => null,
            'user' => null,
            'translator' => null,
            'csrf_protection'=>false,
            'allow_extra_fields' => true])
            ->setRequired('categories')
            ->setRequired('entity_manager')
            ->setRequired('user')
            ->setRequired('translator');
        parent::configureOptions($resolver);
    }
}
