<?php

namespace ApiBundle\Form;

use AppBundle\Entity\Skills;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SkillsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('languages', TextType::class, [
                'label' => 'users.details.skills.languages',
                'attr' => [
                    'data-role' => 'tagsinput',
                ],
                'label_attr' => ['class' => 'mt-15'],
            ])
            ->add('experience', TextType::class, [
                'label' => 'users.details.skills.experience',
                'attr' => [
                    'data-role' => 'tagsinput',
                ],
                'label_attr' => ['class' => 'mt-15'],
            ])
            ->add('education', TextType::class, [
                'label' => 'users.details.skills.educations',
                'attr' => [
                    'data-role' => 'tagsinput',
                ],
                'label_attr' => ['class' => 'mt-15'],
            ])
            ->add('qualifications', TextType::class, [
                'label' => 'users.details.skills.qualifications',
                'attr' => [
                    'data-role' => 'tagsinput',
                ],
                'label_attr' => ['class' => 'mt-15'],
            ])
            ->add('goodAt', TextType::class, [
                'label' => 'users.details.skills.good_at',
                'attr' => [
                    'data-role' => 'tagsinput',
                ],
                'label_attr' => ['class' => 'mt-15'],
            ])
            ->add('getAround', TextType::class, [
                'label' => 'users.details.skills.get_around',
                'attr' => [
                    'data-role' => 'tagsinput',
                ],
                'label_attr' => ['class' => 'mt-15'],

            ])
            ->add('work', TextType::class, [
                'label' => 'users.details.skills.work',
                'attr' => [
                    'data-role' => 'tagsinput',
                ],
                'label_attr' => ['class' => 'mt-15'],
            ]);
        /** @var FormBuilder $view */
        foreach ($builder->all() as $view) {
            $view->addModelTransformer(new CallbackTransformer(
                static function ($tagsAsArray) {
                    if (is_array($tagsAsArray)) {
                        return implode(',', $tagsAsArray);
                    }
                    return $tagsAsArray;
                },
                static function ($tagsAsString) {
                    if(!empty($tagsAsString)){
                        return explode(',', $tagsAsString);
                    }
                    return [];
                }
            ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(['data_class' => Skills::class]);
    }

}
