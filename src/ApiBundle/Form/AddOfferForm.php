<?php
/**
 * Created by PhpStorm.
 * Date: 09/11/2018
 * Time: 11:31
 */

namespace ApiBundle\Form;

use AppBundle\Entity\Offer;
use AppBundle\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class AddOfferForm
 * @package ApiBundle\Form
 */
class AddOfferForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('task', null, [
                'documentation' => [
                    'required' => true,
                    'description' => 'The task id',
                    'example' => '47F85ZAX10'
                ],
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('text', TextType::class, [
                'documentation' => [
                    'required' => true,
                    'description' => 'Description for you offer',
                    'example' => 'I can do it very quickly and i guarantee that'
                ],
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('price', NumberType::class, [
                'documentation' => [
                    'required' => true,
                    'description' => 'The price you gonna do the task for',
                    'example' => 250
                ],
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('files', null, [
                'documentation' => [
                    'description' => 'file can be image video or text file',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Offer::class]);
        parent::configureOptions($resolver);
    }
}
