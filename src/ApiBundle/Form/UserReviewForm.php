<?php
/**
 * Created by PhpStorm.
 * Date: 09/11/2018
 * Time: 11:31
 */

namespace ApiBundle\Form;

use AppBundle\Entity\Task;
use AppBundle\Entity\User;
use AppBundle\Entity\UserReview;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

/**
 * Class UserReviewForm
 * @package ApiBundle\Form
 */
class UserReviewForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, [
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('note', NumberType::class, [
                'required' => true,
                'constraints' => [new Range(['min' => 1, 'max' => 5])]
            ])
            ->add('user',null,['required'=>true,'data_class'=>User::class])
            ->add('task',null,['required'=>true,'data_class'=>Task::class]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => UserReview::class,
                'csrf_protection' => false,
                'allow_extra_fields' => true,
            ]);
    }
}
