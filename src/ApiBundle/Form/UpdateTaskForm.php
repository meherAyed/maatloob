<?php
/**
 * Created by PhpStorm.
 * Date: 09/11/2018
 * Time: 11:31
 */

namespace ApiBundle\Form;

use AppBundle\Entity\Task;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UpdateTaskForm
 * @package ApiBundle\Form
 */
class UpdateTaskForm extends AddTaskForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->getData();
        parent::buildForm($builder,$options);
        $builder
            ->remove('category')
            ->add('task_id',null,['required'=>true,'property_path'=>'id'])
            ->add('date',TextType::class)
            ->add('times', ChoiceType::class,
                [
                    'choices' => array(
                        Task::TIME_BEFORE_MORNING => Task::TIME_BEFORE_MORNING,
                        Task::TIME_MIDDAY => Task::TIME_MIDDAY,
                        Task::TIME_EVENING => Task::TIME_EVENING,
                        Task::TIME_AFTER_NIGHT => Task::TIME_AFTER_NIGHT
                    ),
                    'multiple' => true
                ])
            ->add('price', MoneyType::class, [
                'documentation' => [
                    'type' => 'float',
                    'required' => true,
                    'description' => 'What is your budget',
                    'example' => 250
                ],
                'required' => true,
                'constraints' => [new GreaterThan(['value'=>0])]
            ])
            ->add('payment_type', ChoiceType::class, [
                'property_path'=> 'paymentType',
                'choices' => array(
                    Task::PAYMENT_TYPE_CASH => Task::PAYMENT_TYPE_CASH,
                    Task::PAYMENT_TYPE_WALLET => Task::PAYMENT_TYPE_WALLET,
                    Task::PAYMENT_TYPE_CARD => Task::PAYMENT_TYPE_CARD
                ),
                'documentation' => [
                    'type' => 'int',
                    'required' => true,
                    'description' => 'Payment type can be cash 1 or from wallet 2 or 3 for card',
                    'example' => Task::PAYMENT_TYPE_CASH
                ],
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('pricingType', ChoiceType::class, [
                'choices' => array(
                    Task::PRICE_ALL_AMOUNT => Task::PRICE_ALL_AMOUNT,
                    Task::PRICE_PER_HOUR => Task::PRICE_PER_HOUR
                ),
                'documentation' => [
                    'type' => 'int',
                    'required' => true,
                    'description' => 'Payment hourly or for all the job',
                    'example' => Task::PRICE_ALL_AMOUNT
                ],
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('must_have', CollectionType::class, [
                'property_path' => 'mustHave',
                'documentation' => [
                    'type' => 'array',
                    'required' => false,
                    'description' => 'Criteria that should the tasker have',
                    'example' => [
                        'a driving licence',
                        'a car with air conditioner',
                        'a suit'
                    ]
                ],
                'required' => false,
            ])
            ->add('tasksCount', NumberType::class, [
                'documentation' => [
                    'required' => false,
                    'description' => 'Tasker needed to do the job',
                    'example' => 1
                ],
                'required' => false,
            ])
            ->add('duration');



        $builder->get('date')->addModelTransformer(new CallbackTransformer(
            function ($date) {
                return $date;
            },
            function ($date) {
                return new \DateTime($date);
            }
        ));

        $builder->addEventListener(
            FormEvents::PRE_SUBMIT ,
            static function (FormEvent $event) use ($builder){
                /** @var Task $task */
                $task = $event->getForm()->getData();
                if($task->is(Task::STATUS_ASSIGNED)){
                    $event->getForm()->remove('payment_type')->remove('pricingType')->remove('must_have')
                        ->remove('tasksCount')->remove('title')->remove('description')
                        ->remove('address')->remove('delivery_address')->remove('is_online_work');
                }
                if($task->is(Task::STATUS_ASSIGNED)){
                    $event->getForm()->remove('price');
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
    }
}
