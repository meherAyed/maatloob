<?php
/**
 * Created by PhpStorm.
 * Date: 09/11/2018
 * Time: 11:31
 */

namespace ApiBundle\Form;

use AppBundle\Entity\TaskAlert;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddOfferForm
 * @package ApiBundle\Form
 */
class AddTaskAlertForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $builder
            ->add('keyword', TextType::class)
            ->add('remote', CheckboxType::class)
            ->add('radius', NumberType::class)
            ->add('address', AddressForm::class);

        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            array($this, 'isForRemoteTask')
        );

    }

    /**
     *
     * @param FormEvent $event
     */
    public function isForRemoteTask(FormEvent $event): void
    {
        $data = $event->getData();
        $data['remote'] = (bool)$data['remote'];
        if (isset($data['remote']) && $data['remote']) {
            $form = $event->getForm();
            $form->remove('address')->remove('radius');
        }
        $event->setData($data);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) : void
    {
        $resolver->setDefaults(['data_class' => TaskAlert::class,'csrf_protection' => false,'allow_extra_fields'=>true]);
        parent::configureOptions($resolver);
    }
}
