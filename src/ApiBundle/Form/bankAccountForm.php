<?php
/**
 * Created by PhpStorm.
 * Date: 09/11/2018
 * Time: 11:31
 */

namespace ApiBundle\Form;

use AppBundle\Entity\UserBankAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class bankAccountForm
 * @package ApiBundle\Form
 */
class bankAccountForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('holderName', TextType::class, [
                'documentation' => [
                    'required' => true,
                    'description' => 'User bank account name',
                    'example' => 'My name'
                ],
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('accountNumber', NumberType::class, [
                'documentation' => [
                    'required' => true,
                    'description' => 'Account number',
                    'example' => '1548844965577455'
                ],
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('routingNumber', NumberType::class, [
                'documentation' => [
                    'required' => true,
                    'description' => 'Sort Code',
                    'example' => '00-00-00'
                ],
                'required' => true,
                'constraints' => [$options['country'] !== 'id'?new NotBlank():null]
            ])
            ->add('bankCode', TextType::class, [
                'documentation' => [
                    'required' => true,
                    'description' => 'Bank name',
                    'example' => 'Bank al business'
                ],
                'required' => true,
                'constraints' => [$options['country'] === 'id'?new NotBlank():null]
            ])
            ->add('bankName', TextType::class, [
                'documentation' => [
                    'required' => true,
                    'description' => 'Bank name',
                    'example' => 'Bank al business'
                ],
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('phone', TextType::class, [
                'documentation' => [
                    'required' => true,
                    'description' => 'Phone number',
                    'example' => '778 5884 222'
                ],
                'required' => true,
                'constraints' => [new NotBlank()]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => UserBankAccount::class,
            'country' => 'sa'
        ]);
    }
}
