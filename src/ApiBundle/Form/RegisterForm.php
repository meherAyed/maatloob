<?php
/**
 * Created by PhpStorm.
 * Date: 09/11/2018
 * Time: 11:31
 */

namespace ApiBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class RegisterForm
 * @package ApiBundle\Form
 */
class RegisterForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('country',EntityType::class,[
                'class' => Country::class,
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('email', EmailType::class, [
                'documentation' => [
                    'required' => true,
                    'description' => 'User email',
                    'example' => 'register@email.com'
                ],
                'required' => true,
                'constraints' => [new Email()]
            ])
            ->add('password', PasswordType::class, [
                'property_path' => 'plainPassword',
                'documentation' => [
                    'required' => true,
                    'description' => 'User password',
                    'example' => 'thisisapassword'
                ],
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('referral_code');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'csrf_protection'=>false
        ]);
        parent::configureOptions($resolver);
    }
}
