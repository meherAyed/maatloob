<?php
/**
 * Created by PhpStorm.
 * Date: 09/11/2018
 * Time: 11:31
 */

namespace ApiBundle\Form;

use AppBundle\Entity\Address;
use AppBundle\Entity\UserReview;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

/**
 * Class AddressForm
 * @package ApiBundle\Form
 */
class AddressForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => false,
            ])
            ->add('place', TextType::class, [
                'required' => false,
                'documentation' => [
                    'required' => true,
                    'description' => 'Address where job should be done',
                    'example' => "California united state of america"
                ],
                'constraints' => [new NotBlank(['message'=>'address.empty_place'])]
            ])
            ->add('longitude', TextType::class, [
                'required' => false,
                'documentation' => [
                    'required' => true,
                    'description' => 'Longitude of the place',
                    'example' => '10.1458445'
                ],
                'constraints' => [new NotBlank(['message'=>'address.empty_longitude'])]
            ])
            ->add('latitude', TextType::class, [
                'required' => false,
                'documentation' => [
                    'required' => true,
                    'description' => 'Latitude of the place',
                    'example' => '36.8650939'
                ],
                'constraints' => [new NotBlank(['message'=>'address.empty_latitude'])]
            ])
            ->add('postalCode', TextType::class, [
                'documentation' => [
                    'example' => 5035
                ],
            ])
            ->add('area',TextType::class, [
                'required' => false,
                'documentation' => [
                    'name' => 'area',
                    'required' => false,
                    'description' => 'Task area',
                ],
            ])
            ->add('street',TextType::class, [
                'required' => false,
                'documentation' => [
                    'name' => 'street',
                    'required' => false,
                    'description' => 'Task street',
                ],
            ])
            ->add('addressInfo',TextType::class, [
                'required' => false,
                'documentation' => [
                    'name' => 'addressInfo',
                    'required' => false,
                    'description' => 'task address info',
                ],
            ])
            ->add('hideAddress',CheckboxType::class, [
                'documentation' => [
                    'name' => 'hideAddress',
                    'required' => true,
                    'description' => 'Hide or show address info',
                    'example' => 0
                ],
            ]);
        if($options['user']){
            $builder->add('id',TextType::class,['error_bubbling'=>true]);
            $builder->addEventListener(
                FormEvents::PRE_SUBMIT, static function (FormEvent $event) use ($options){
                $data = $event->getData();
                $em  = $options['entity_manager'];
                $user = $options['user'];
                $form = $event->getForm();
                if (isset($data['id'])) {
                    $form->remove('place')
                        ->remove('longitude')
                        ->remove('latitude')
                        ->remove('area')
                        ->remove('street')
                        ->remove('addressInfo')
                        ->remove('hideAddress');
                    /** @var Address $address */
                    $address = $em->getRepository(Address::class)->findOneBy(['user'=>$user,'id'=>$data['id']]);
                    if($address){
                        $event->getForm()->setData($address);
                    }else{
                        $event->getForm()->get('id')->addError(
                            new FormError($options['translator']->transDb('address.invalid',[],'validators'))
                        );
                    }
                }
                if(isset($data['hideAddress'])){
                    $data['hideAddress'] = (bool) $data['hideAddress'];
                    $event->setData($data);
                }
            }
            );
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Address::class,
                'csrf_protection' => false,
                'allow_extra_fields' => true,
                'entity_manager' => null,
                'user' => null,
                'translator' => null
            ])
            ->setRequired('entity_manager')
            ->setRequired('user')
            ->setRequired('translator');
    }
}
