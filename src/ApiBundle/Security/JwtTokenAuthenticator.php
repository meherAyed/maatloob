<?php

namespace ApiBundle\Security;

use ApiBundle\Services\DBTranslator;
use AppBundle\Entity\User;
use Components\Helper;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\MissingTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class JwtTokenAuthenticator extends AbstractGuardAuthenticator
{
    private $jwtEncoder;
    private $em;
    private $dbTranslator;

    public function __construct(JWTEncoderInterface $jwtEncoder, EntityManagerInterface $em,DBTranslator $translator)
    {
        $this->jwtEncoder = $jwtEncoder;
        $this->em = $em;
        $this->dbTranslator = $translator;
    }

    public function getCredentials(Request $request)
    {
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );
        $token = $extractor->extract($request);

        if (!$token) {
            return;
        }

        return $token;
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return User|object|UserInterface|null
     * @throws CustomUserMessageAuthenticationException
     * @throws \Exception
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try{
            $data = $this->jwtEncoder->decode($credentials);
        } catch (\Exception $e) {
            throw new \Symfony\Component\Security\Core\Exception\BadCredentialsException($e->getMessage(), 0, $e);
        }

        if ($data === false) {
            throw new CustomUserMessageAuthenticationException('Invalid Token');
        }

        $username = $data['username'];
        /** @var User $user */
        $user = $this->em
            ->getRepository(User::class)
            ->findOneBy(['username' => $username]);
        if(!$user || !$user->isEnabled() || $user->getisDeleted()){
            throw new CustomUserMessageAuthenticationException('Invalid user',[],Response::HTTP_FORBIDDEN);
        }
        /** Check if user is banned  */
        if($activeBan = $user->getActiveBan()){
            throw new CustomUserMessageAuthenticationException(
                $this->dbTranslator->transDb(
                    ($activeBan->isPermanent()?
                    'account_permanently_banned':
                    'account_suspended_to_date')
                ,['#date#'=>$activeBan->isPermanent()?'-':$activeBan->getUnbanDate()->format('d-m-Y H:i')],$user->getLanguage())
                ,[],Response::HTTP_LOCKED);
        }
        return $user;
    }


    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }


    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'code' => $exception->getCode(),
            'msg' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // do nothing - let the controller be called
        if ($request->isMethod(Request::METHOD_GET))
            $data = $request->query->all();
        else
            $data = $request->request->all();
        if (array_key_exists('key', $data)){
            $key = $data['key'];
            unset($data['key']);
            $str = '';
            foreach ($data as $index => $value) {
                if (is_array($value)) {
                    $str .= serialize($value);
                }
                else {
                    $str .= $value;
                }
            }
            $str = md5(crc32($str));
            if ($str !== $key && $key !== 'dev') {
                throw new CustomUserMessageAuthenticationException('Invalid Data');
            }
        }
    }


    public function supportsRememberMe()
    {
        return false;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $exception = new MissingTokenException($authException->getMessage(), 0, $authException);
        $event = new JWTNotFoundEvent($exception, new JWTAuthenticationFailureResponse($authException->getMessageKey()));

        return $event->getResponse();
    }

    public function onJWTExpired(JWTExpiredEvent $event)
    {
        /** @var JWTAuthenticationFailureResponse */
        $response = $event->getResponse();

        $response->setMessage('Your token is expired, please renew it.');
    }
}
