<?php


namespace ApiBundle\EventSubscriber;


use ApiBundle\Events\TaskEvent;
use AppBundle\Entity\Country;
use AppBundle\Entity\Language;
use AppBundle\Entity\Notification;
use AppBundle\Entity\TaskAlert;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TaskSubscriber implements EventSubscriberInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents() :array
    {
        return [
            TaskEvent::TASK_PUBLISHED => 'taskAlert'
        ];
    }

    public function taskAlert(TaskEvent $taskEvent): void
    {
        $task = $taskEvent->getTask();
        $em = $this->container->get('doctrine.orm.entity_manager');
        $this->container->get('event_dispatcher')->addListener('kernel.terminate', static function() use ($em,$task){
            $taskAlerts = $em->getRepository(TaskAlert::class)->createQueryBuilder('ta')
                                    ->join('ta.user','u')
                                    ->leftJoin('ta.address','address')
                                    ->where('u != :user')
                                    ->andWhere('ta.remote = :taskRemote')
                                    ->andWhere(":title like concat('%',ta.keyword,'%') or :description like concat('%',ta.keyword,'%') or :category like concat('%',ta.keyword,'%')")
                                    ->andWhere('u.country = :country or ta.remote = 1')
                                    ->setParameters([
                                        'taskRemote' => $task->getIsOnlineWork(),
                                        'title' => $task->getTitle(),
                                        'description' => $task->getDescription(),
                                        'user' => $task->getUser(),
                                        'category' => $task->getCategory()->translate()->getName(),
                                        'country' => $task->getUser()->getCountry()->getIdentifier()
                                    ])->getQuery()->getResult();
            /** @var Language $taskLanguage */
            $taskLanguage = $task->getUser()->getCountry()->getDefaultLanguage();
            $notifiedUsers = [];
            /** @var TaskAlert $taskAlert */
            foreach ($taskAlerts as $taskAlert){
                $user = $taskAlert->getUser();
                if(!$task->getIsOnlineWork() || ($taskLanguage->getIdentifier() === $user->getCountry()->getDefaultLanguage()->getIdentifier() || $taskLanguage->getIdentifier() === 'en')){
                    /** Check if user already notified */
                    if(!isset($notifiedUsers[$user->getId()]) && !$user->isBanned()){
                        if($taskAlert->isRemote() || ($taskAlert->getAddress()->distance($task->getAddress())<$taskAlert->getRadius())){
                            $notification = new Notification();
                            $notification->setTitle('notification.task_alert.title');
                            $notification->setBody('notification.task_alert.body');
                            $notification->setToUser($user);
                            $notification->setType(Notification::ACTION_MAATLOOB_ALERT);
                            $notification->setParams(['title' => $task->getTitle()]);
                            $notification->setExtra([
                                'type' => Notification::TYPE_TASK,
                                'task_id' => $task->getId()
                            ]);
                            $em->persist($notification);
                        }
                    }
                    $notifiedUsers[$user->getId()]=true;
                }
            }
            $em->flush();
        });
    }
}