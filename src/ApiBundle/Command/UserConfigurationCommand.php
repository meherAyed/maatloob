<?php

namespace ApiBundle\Command;

use AppBundle\Entity\Country;
use AppBundle\Entity\DocumentType;
use AppBundle\Entity\Skills;
use AppBundle\Entity\User;
use AppBundle\Entity\UserDocument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UserConfigurationCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('api:fix_document')
            ->setDescription('Config user : init document and skill');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $users = $em->getRepository(User::class)->findAll();

        /** Set Default user document */
        $documents = $em->getRepository(DocumentType::class)->findAll();
        $countries[] = $em->getRepository(Country::class)->find('us');
        $countries[] = $em->getRepository(Country::class)->find('ae');
        $countries[] = $em->getRepository(Country::class)->find('id');

        /** @var Country $country */
        foreach ($countries as $country) {
            foreach ($documents as $document){
                $doc = new DocumentType();
                $doc->setCountry($country);
                if($country->getIdentifier()!=='en'){
                    $doc->translate('en')->setName($document->translate('en')->getName());
                    $doc->translate('en')->setDescription($document->translate('en')->getDescription());
                }
                $doc->translate($country->getDefaultLanguage()->getIdentifier())->setName($document->translate($country->getDefaultLanguage()->getIdentifier())->getName());
                $doc->translate($country->getDefaultLanguage()->getIdentifier())->setDescription($document->translate($country->getDefaultLanguage()->getIdentifier())->getDescription());
                $doc->mergeNewTranslations();
                $em->persist($doc);
            }
        }
        $em->flush();
        $output->writeln('Users were configured !');
    }
}
