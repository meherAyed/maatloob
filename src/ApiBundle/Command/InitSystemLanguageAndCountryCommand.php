<?php

namespace ApiBundle\Command;

use AppBundle\Entity\Country;
use AppBundle\Entity\Language;
use AppBundle\Entity\LanguageMessage;
use AppBundle\Entity\SystemConfig;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitSystemLanguageAndCountryCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('api:init_system_language_command')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $assetHelper = $this->getContainer()->get('assets.packages');
        $path = $this->getContainer()->getParameter('web_dir').'/languages-messages.json';
        $content = file_get_contents($path);
        $messages = json_decode($content, true);

        $usa = $em->getRepository(Country::class)->find('us');
        $saudi = $em->getRepository(Country::class)->find('sa');
        if(!$usa){
            $usa = new Country();
            $usa->setName('United state');
            $usa->setIdentifier('us');
            $usa->setCurrencyName('Dollar');
            $usa->setCurrencySymbol('$');
            $usa->setCurrencyCode('USD');
            $usa->setFlag('/assets/img/countries/us.png');
            $usa->setConfig(new SystemConfig());
            $em->persist($usa);
        }
        if(!$saudi){
            $saudi = new Country();
            $saudi->setName('Saudi Arabia ( العربية السعودية )');
            $saudi->setIdentifier('sa');
            $saudi->setCurrencyCode('SAR');
            $saudi->setCurrencyName('﷼');
            $saudi->setCurrencySymbol('ر.س');
            $saudi->setFlag('/assets/img/countries/sa.png');
            $saudi->setConfig(new SystemConfig());
            $em->persist($saudi);
        }
        $em->flush();
        $englishLanguage = $em->getRepository(Language::class)->findOneBy(['identifier'=>'en']);
        $arabicLanguage = $em->getRepository(Language::class)->findOneBy(['identifier'=>'ar']);
        if (!$englishLanguage) {
            $englishLanguage = new Language('en');
            $englishLanguage->setName('English');
            $englishLanguage->setIcon($assetHelper->getUrl('/assets/img/flags/en.png'));
            $englishLanguage->setActive(true);
            $em->persist($englishLanguage);
        }

        if(!$arabicLanguage){
            $arabicLanguage = new Language('ar');
            $arabicLanguage->setName('Arabic');
            $arabicLanguage->setIcon($assetHelper->getUrl('/assets/img/flags/ar.png'));
            $em->persist($arabicLanguage);
            $englishLanguage->setActive(true);
        }


        $em->flush();
        $systemLanguages = $em->getRepository(Language::class)->findAll();
        /** @var language $language */
        foreach ($systemLanguages as $language){
            foreach ($messages as $message) {
                /** Check message type */
                $languageMessage = $em->getRepository(LanguageMessage::class)
                    ->findOneBy(['language'=>$language->getIdentifier(),'identifier'=>$message['key']]);
                if(!$languageMessage){
                    $languageMessage = new LanguageMessage();
                    $languageMessage->setIdentifier($message['key']);
                    $language->addMessage($languageMessage);
                    $messageText = $message[strtolower($language->getName())]??$message['key'];
                    $languageMessage->setMessage($messageText);
                }
            }
            $em->persist($language);
            $em->flush();
        }
        $output->writeln('System language has been initialized');
    }
}
