<?php

namespace ApiBundle\Command;

use AppBundle\Entity\ReportType;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReportCommand extends ContainerAwareCommand
{
    const REPORTS = [
        [
            'id' => 1,
            'name' => 'Spam',
            'nameAr' => 'بريد مؤذي',
            'sections' => [
                ReportType::SECTION_OFFER,
                ReportType::SECTION_QUESTION,
                ReportType::SECTION_TASK,
                ReportType::SECTION_USER,
            ]
        ],
        [
            'id' => 2,
            'name' => 'Rude or offensive',
            'nameAr' => 'غير مهذب أو مسيئ',
            'sections' => [
                ReportType::SECTION_OFFER,
                ReportType::SECTION_QUESTION,
                ReportType::SECTION_TASK,
                ReportType::SECTION_USER,
            ]
        ],
        [
            'id' => 3,
            'name' => 'Breach of marketplace rule',
            'nameAr' => 'خرق حكم السوق',
            'sections' => [
                ReportType::SECTION_OFFER,
                ReportType::SECTION_QUESTION,
                ReportType::SECTION_TASK,
                ReportType::SECTION_USER,
            ]
        ],
        [
            'id' => 4,
            'name' => 'Other',
            'nameAr' => 'آخر',
            'sections' => [
                ReportType::SECTION_OFFER,
                ReportType::SECTION_QUESTION,
                ReportType::SECTION_TASK,
                ReportType::SECTION_USER,
            ]
        ]
    ];


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('api:report_type_create')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        foreach (ReportCommand::REPORTS as $report){
            $reportType = $em->getRepository(ReportType::class)
                        ->find($report['id']);
            if(!$reportType)
                $reportType = new ReportType();
            $reportType->translate('en')->setName($report['name']);
            $reportType->translate('ar')->setName($report['nameAr']);
            $reportType->setSections($report['sections']);
            $em->persist($reportType);
        }
        $em->flush();
        $output->writeln('report types created !');
    }
}
