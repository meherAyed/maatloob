<?php


namespace Components\Validator\Constraints;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Twilio\Exceptions\RestException;
use Twilio\Rest\Client;

class E164NumberValidator extends ConstraintValidator
{
    private $twilio;

    public function __construct(Client $twilio)
    {
        $this->twilio = $twilio;
    }

    public function validate($phoneVerificationRequest, Constraint $constraint)
    {
        try {
            $number = $this->twilio->lookups
                ->phoneNumbers($phoneVerificationRequest->getPhoneNumber())
                ->fetch(['countryCode' => $phoneVerificationRequest->getCountryCode()]);

            $phoneVerificationRequest->setPhoneNumber($number->phoneNumber);
        } catch (RestException $e) {
            if ($e->getStatusCode() === Response::HTTP_NOT_FOUND) {
                $this->context
                    ->buildViolation($constraint->invalidNumberMessage)
                    ->atPath('phoneNumber')
                    ->addViolation();
            }
        }
    }
}