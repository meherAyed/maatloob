<?php
/**
 * Created by PhpStorm.
 * User: medna
 * Date: 09/08/2019
 * Time: 01:12
 */

namespace Components;


use Symfony\Component\Form\FormInterface;

class Helper
{

    static function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if (!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }


    static function getErrorsFromForm(FormInterface $form, $indexBy = 'id')
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = Helper::getErrorsFromForm($childForm)) {
                    if ($indexBy) {
                        $errors[$childForm->createView()->vars[$indexBy]] = array_pop($childErrors);
                    } else {
                        array_push($errors, array_pop($childErrors) . ' , field:' . $childForm->createView()->vars['name']);
                    }
                }
            }
        }
        return $errors;
    }


    /**
     * Return textLimit with three dot if exceeded
     * @param $text
     * @param int $limit
     * @return string
     */
    static function textLimit($text, $limit = 30)
    {
        if (strlen($text) > $limit) {
            return substr($text, 0, $limit) . '...';
        } else
            return $text;
    }

    /**
     * generate a random token
     * @return string
     */
    public static function generateToken(): string
    {
        return sha1(random_int(11111, 99999) . date('d-m-Y') . time());
    }

    /**
     * @param $string
     * @param string $with
     * @return string
     */
    public static function slugify($string, $with = '-'): string
    {
        return strtolower(trim(str_replace(' ', $with, $string), $with));
    }

    /**
     * @param $latitudeFrom
     * @param $longitudeFrom
     * @param $latitudeTo
     * @param $longitudeTo
     * @param $unit
     * @return float|int
     */
    public static function distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $unit = 'KM')
    {
        if (($latitudeFrom === $latitudeTo) && ($longitudeFrom === $longitudeTo)) {
            return 0;
        }
        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) + cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        if ($unit === "KM") {
            return ($miles * 1.609344);
        }
        if ($unit === "N") {
            return ($miles * 0.8684);
        }

        return $miles;
    }

    static function csvToArray($filename = '', $delimiter = ',', $indexBy = null)
    {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                if (!$header)
                    $header = $row;
                elseif (count($header) === count($row)) {
                    if (($position = array_search($indexBy, $header, true)) !== false) {
                        $data[$row[$position]] = array_combine($header, $row);
                    } else {
                        $data[] = array_combine($header, $row);
                    }
                }
            }
            fclose($handle);
        }
        return $data;
    }

    /**
     * @param $string
     * @param $tagName
     * @return mixed
     */
    public static function getTextBetweenTags($string, $tagName)
    {
        $pattern = "/<$tagName>(.*?)<\/$tagName>/";
        preg_match($pattern, $string, $matches);
        return $matches[1];
    }


    /**
     * @return array
     */
    public static function getBrowserSupport(): array
    {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
        $bname = 'Internet Explorer';
        $ub = "MSIE";
        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                $version = $matches['version'][0];
            } elseif (isset($matches['version'][1])) {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }

        // check if we have a number
        if ($version == null || $version == "") {
            $version = "?";
        }

        return array(
            'userAgent' => $u_agent,
            'name' => $bname,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern
        );
    }

    /**
     * @param $countryCode
     */
    public static function setCountryTimeZone($countryCode): void
    {
        $timeZoneName = timezone_identifiers_list(4096,strtoupper($countryCode));
        if(count($timeZoneName)>0){
            date_default_timezone_set($timeZoneName[0]);
        }
    }
}